/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#include "efct_driver.h"
#include "efct_unsol.h"

/* Post node event callback argument. */
struct efct_xport_post_node_event_s {
	struct semaphore sem;
	atomic_t refcnt;
	struct efc_node_s *node;
	u32	evt;
	void *context;
};

static struct dentry *efct_debugfs_root;
static atomic_t efct_debugfs_count;

static struct scsi_host_template efct_template = {
	.module			= THIS_MODULE,
	.name			= EFCT_DRIVER_NAME,
};

/* globals */
static struct fc_function_template efct_xport_functions;
static struct fc_function_template efct_vport_functions;

static struct scsi_transport_template *efct_xport_fc_tt;
static struct scsi_transport_template *efct_vport_fc_tt;

static void efct_xport_link_stats_cb(int status, u32 num_counters,
				     struct efct_hw_link_stat_counts_s *c,
				     void *arg);
static void efct_xport_host_stats_cb(int status, u32 num_counters,
				     struct efct_hw_host_stat_counts_s *c,
				     void *arg);
static void efct_xport_async_link_stats_cb(int status,
					   u32 num_counters,
				struct efct_hw_link_stat_counts_s *counters,
				void *arg);
static void efct_xport_async_host_stats_cb(int status,
					   u32 num_counters,
				struct efct_hw_host_stat_counts_s *counters,
				void *arg);
static void efct_xport_stats_timer_cb(struct timer_list *t);
static void efct_xport_config_stats_timer(struct efct_s *efct);

/**
 * @brief Allocate a transport object.
 *
 * @par Description
 * A transport object is allocated, and associated with a device instance.
 *
 * @param efct Pointer to device instance.
 *
 * @return Returns the pointer to the allocated transport object,
 * or NULL if failed.
 */
struct efct_xport_s *
efct_xport_alloc(struct efct_s *efct)
{
	struct efct_xport_s *xport;

	efct_assert(efct, NULL);
	xport = kmalloc(sizeof(*xport), GFP_KERNEL);
	if (!xport)
		return xport;

	memset(xport, 0, sizeof(*xport));
	xport->efct = efct;
	return xport;
}

static int
efct_xport_init_debugfs(struct efct_s *efct)
{
	/* Setup efct debugfs root directory */
	if (!efct_debugfs_root) {
		efct_debugfs_root = debugfs_create_dir("efct", NULL);
		atomic_set(&efct_debugfs_count, 0);
		if (!efct_debugfs_root) {
			efct_log_err(efct, "failed to create debugfs entry\n");
			goto debugfs_fail;
		}
	}

	/* Create a directory for sessions in root */
	if (!efct->sess_debugfs_dir) {
		efct->sess_debugfs_dir = debugfs_create_dir("sessions", NULL);
		if (!efct->sess_debugfs_dir) {
			efct_log_err(efct,
				     "failed to create debugfs entry for sessions\n");
			goto debugfs_fail;
		}
		atomic_inc(&efct_debugfs_count);
	}

	return 0;

debugfs_fail:
	return -1;
}

static void efct_xport_delete_debugfs(struct efct_s *efct)
{
	/* Remove session debugfs directory */
	debugfs_remove(efct->sess_debugfs_dir);
	efct->sess_debugfs_dir = NULL;
	atomic_dec(&efct_debugfs_count);

	if (atomic_read(&efct_debugfs_count) == 0) {
		/* remove root debugfs directory */
		debugfs_remove(efct_debugfs_root);
		efct_debugfs_root = NULL;
	}
}

/**
 * @brief Do as much allocation as possible, but do not initialization
 * the device.
 *
 * @par Description
 * Performs the functions required to get a device ready to run.
 *
 * @param xport Pointer to transport object.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
efct_xport_attach(struct efct_xport_s *xport)
{
	struct efct_s *efct = xport->efct;
	int rc;
	u32 max_sgl;
	u32 n_sgl;
	u32 i;
	u32 value;

	/* booleans used for cleanup if initialization fails */
	bool io_pool_created = false;

	for (i = 0; i < SLI4_MAX_FCFI; i++) {
		xport->fcfi[i].hold_frames = true;
		spin_lock_init(&xport->fcfi[i].pend_frames_lock);
		INIT_LIST_HEAD(&xport->fcfi[i].pend_frames);
	}

	rc = efct_hw_setup(&efct->hw, efct, efct->efct_os.pdev);
	if (rc) {
		efct_log_err(efct, "%s: Can't setup hardware\n", efct->desc);
		return -1;
	} else if (efct->ctrlmask & EFCT_CTRLMASK_CRASH_RESET) {
		efct_log_debug(efct, "stopping after efct_hw_setup\n");
		return -1;
	}

	efct_hw_set(&efct->hw, EFCT_HW_RQ_SELECTION_POLICY,
		    efct->rq_selection_policy);
	efct_hw_get(&efct->hw, EFCT_HW_RQ_SELECTION_POLICY, &value);
	efct_log_debug(efct, "RQ Selection Policy: %d\n", value);

	efct_hw_set_ptr(&efct->hw, EFCT_HW_FILTER_DEF,
			(void *)efct->filter_def);

	efct_hw_get(&efct->hw, EFCT_HW_MAX_SGL, &max_sgl);
	max_sgl -= SLI4_SGE_MAX_RESERVED;
	n_sgl = (max_sgl > EFCT_FC_MAX_SGL) ? EFCT_FC_MAX_SGL : max_sgl;

	/* Note: number of SGLs must be set for efc_node_create_pool */
	if (efct_hw_set(&efct->hw, EFCT_HW_N_SGL, n_sgl) !=
			EFCT_HW_RTN_SUCCESS) {
		efct_log_err(efct,
			     "%s: Can't set number of SGLs\n", efct->desc);
		return -1;
	}

	efct_log_debug(efct, "%s: Configured for %d SGLs\n", efct->desc,
		       n_sgl);

	/*
	 * EVT: if testing chained SGLs allocate EFCT_FC_MAX_SGL SGE's
	 * in the IO
	 */
	xport->io_pool = efct_io_pool_create(efct, EFCT_NUM_SCSI_IOS, n_sgl);
	if (!xport->io_pool) {
		efct_log_err(efct, "Can't allocate IO pool\n");
		goto efct_xport_attach_cleanup;
	} else {
		io_pool_created = true;
	}

	return 0;

efct_xport_attach_cleanup:
	if (io_pool_created)
		efct_io_pool_free(xport->io_pool);

	return -1;
}

/**
 * @brief Initializes the device.
 *
 * @par Description
 * Performs the functions required to make a device functional.
 *
 * @param xport Pointer to transport object.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
efct_xport_initialize(struct efct_xport_s *xport)
{
	struct efct_s *efct = xport->efct;
	int rc;
	u32 max_hw_io;
	u32 max_sgl;
	u32 rq_limit;

	/* booleans used for cleanup if initialization fails */
	bool ini_device_set = false;
	bool tgt_device_set = false;
	bool hw_initialized = false;

	efct_hw_get(&efct->hw, EFCT_HW_MAX_IO, &max_hw_io);
	if (efct_hw_set(&efct->hw, EFCT_HW_N_IO, max_hw_io) !=
			EFCT_HW_RTN_SUCCESS) {
		efct_log_err(efct, "%s: Can't set number of IOs\n",
			     efct->desc);
		return -1;
	}

	efct_hw_get(&efct->hw, EFCT_HW_MAX_SGL, &max_sgl);
	max_sgl -= SLI4_SGE_MAX_RESERVED;

	efct_hw_get(&efct->hw, EFCT_HW_MAX_IO, &max_hw_io);

	if (efct_hw_set(&efct->hw, EFCT_HW_TOPOLOGY, efct->topology) !=
			EFCT_HW_RTN_SUCCESS) {
		efct_log_err(efct, "%s: Can't set the toplogy\n", efct->desc);
		return -1;
	}
	efct_hw_set(&efct->hw, EFCT_HW_RQ_DEFAULT_BUFFER_SIZE,
		    EFCT_FC_RQ_SIZE_DEFAULT);

	if (efct_hw_set(&efct->hw, EFCT_HW_LINK_SPEED, efct->speed) !=
			EFCT_HW_RTN_SUCCESS) {
		efct_log_err(efct, "%s: Can't set the link speed\n",
			     efct->desc);
		return -1;
	}

	/* currently only lancer support setting the CRC seed value */
	if (efct->hw.sli.asic_type == SLI4_ASIC_INTF_2) {
		if (efct_hw_set(&efct->hw, EFCT_HW_DIF_SEED,
				EFCT_FC_DIF_SEED) != EFCT_HW_RTN_SUCCESS) {
			efct_log_err(efct, "%s: Can't set the DIF seed\n",
				     efct->desc);
			return -1;
		}
	}

	if (efct->target_io_timer_sec) {
		efct_log_debug(efct, "setting target io timer=%d\n",
			       efct->target_io_timer_sec);
		efct_hw_set(&efct->hw, EFCT_HW_EMULATE_TARGET_WQE_TIMEOUT,
			    true);
	}

	/* Initialize vport list */
	INIT_LIST_HEAD(&xport->vport_list);
	spin_lock_init(&xport->io_pending_lock);
	INIT_LIST_HEAD(&xport->io_pending_list);
	atomic_set(&xport->io_active_count, 0);
	atomic_set(&xport->io_pending_count, 0);
	atomic_set(&xport->io_total_free, 0);
	atomic_set(&xport->io_total_pending, 0);
	atomic_set(&xport->io_alloc_failed_count, 0);
	atomic_set(&xport->io_pending_recursing, 0);
	rc = efct_hw_init(&efct->hw);
	if (rc) {
		efct_log_err(efct, "efct_hw_init failure\n");
		goto efct_xport_init_cleanup;
	} else {
		hw_initialized = true;
	}

	rq_limit = max_hw_io / 2;
	if (efct_hw_set(&efct->hw, EFCT_HW_RQ_PROCESS_LIMIT, rq_limit) !=
			EFCT_HW_RTN_SUCCESS)
		efct_log_err(efct, "%s: Can't set the RQ process limit\n",
			     efct->desc);

	rc = efct_scsi_tgt_new_device(efct);
	if (rc) {
		efct_log_err(efct, "failed to initialize target\n");
		goto efct_xport_init_cleanup;
	} else {
		tgt_device_set = true;
	}

	rc = efct_scsi_new_device(efct);
	if (rc) {
		efct_log_err(efct, "failed to initialize initiator\n");
		goto efct_xport_init_cleanup;
	} else {
		ini_device_set = true;
	}

	/* Get FC link and host statistics perodically*/
	efct_xport_config_stats_timer(efct);

	efct_xport_init_debugfs(efct);

	return 0;

efct_xport_init_cleanup:
	if (ini_device_set)
		efct_scsi_del_device(efct);

	if (tgt_device_set)
		efct_scsi_tgt_del_device(efct);

	if (hw_initialized) {
		/* efct_hw_teardown can only execute after efct_hw_init */
		efct_hw_teardown(&efct->hw);
	}

	return -1;
}

/**
 * @brief Detaches the transport from the device.
 *
 * @par Description
 * Performs the functions required to shut down a device.
 *
 * @param xport Pointer to transport object.
 *
 * @return Returns 0 on success or a non-zero value on failure.
 */
int
efct_xport_detach(struct efct_xport_s *xport)
{
	struct efct_s *efct = xport->efct;

	/* free resources associated with target-server and initiator-client */
	efct_scsi_tgt_del_device(efct);

	efct_scsi_del_device(efct);

	/*Shutdown FC Statistics timer*/
	del_timer(&efct->xport->stats_timer);

	efct_hw_teardown(&efct->hw);

	efct_xport_delete_debugfs(efct);

	return 0;
}

/**
 * @brief domain list empty callback
 *
 * @par Description
 * Function is invoked when the device domain list goes empty. By convention
 * @c arg points to an struct semaphore instance, that is incremented.
 *
 * @param efct Pointer to device object.
 * @param arg Pointer to semaphore instance.
 *
 * @return None.
 */
static void
efct_xport_domain_list_empty_cb(struct efc_lport *efc, void *arg)
{
	struct efct_s *efct = efc->base;
	struct semaphore *sem = arg;

	efct_assert(efct);
	efct_assert(sem);

	up(sem);
}

/**
 * @brief post node event callback
 *
 * @par Description
 * This function is called from the mailbox completion interrupt context to
 * post an event to a node object. By doing this in the interrupt context,
 * it has the benefit of only posting events in the interrupt context,
 * deferring the need to create a per event node lock.
 *
 * @param hw Pointer to HW structure.
 * @param status Completion status for mailbox command.
 * @param mqe Mailbox queue completion entry.
 * @param arg Callback argument.
 *
 * @return Returns 0 on success, a negative error code value on failure.
 */

static int
efct_xport_post_node_event_cb(struct efct_hw_s *hw, int status,
			      u8 *mqe, void *arg)
{
	struct efct_xport_post_node_event_s *payload = arg;

	if (payload) {
		efc_node_post_shutdown(payload->node, payload->evt,
				       payload->context);
		up(&payload->sem);
		if (atomic_sub_and_test(1, &payload->refcnt))
			kfree(payload);
	}
	return 0;
}

/**
 * @brief Initiate force free.
 *
 * @par Description
 * Perform force free of EFCT.
 *
 * @param xport Pointer to transport object.
 *
 * @return None.
 */

static void
efct_xport_force_free(struct efct_xport_s *xport)
{
	struct efct_s *efct = xport->efct;
	struct efc_lport *efc = efct->efcport;
	struct efc_domain_s *domain;
	struct efc_domain_s *next;

	efct_log_debug(efct, "reset required, do force shutdown\n");

	list_for_each_entry_safe(domain, next, &efc->domain_list,
				 list_entry) {
		efc_domain_force_free(domain);
	}
}

/**
 * @brief Perform transport attach function.
 *
 * @par Description
 * Perform the attach function, which for the FC transport makes a HW call
 * to bring up the link.
 *
 * @param xport pointer to transport object.
 * @param cmd command to execute.
 *
 * efct_xport_control(struct efct_xport_s *xport, EFCT_XPORT_PORT_ONLINE)
 * efct_xport_control(struct efct_xport_s *xport, EFCT_XPORT_PORT_OFFLINE)
 * efct_xport_control(struct efct_xport_s *xport, EFCT_XPORT_PORT_SHUTDOWN)
 * efct_xport_control(struct efct_xport_s *xport, EFCT_XPORT_POST_NODE_EVENT,
 *		     struct efct_node_s *node, efc_sm_event_e, void *context)
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

int
efct_xport_control(struct efct_xport_s *xport, enum efct_xport_ctrl_e cmd, ...)
{
	u32 rc = 0;
	struct efct_s *efct = NULL;
	va_list argp;

	efct_assert(xport, -1);
	efct_assert(xport->efct, -1);
	efct = xport->efct;

	switch (cmd) {
	case EFCT_XPORT_PORT_ONLINE: {
		/* Bring the port on-line */
		rc = efct_hw_port_control(&efct->hw, EFCT_HW_PORT_INIT, 0,
					  NULL, NULL);
		if (rc)
			efct_log_err(efct,
				     "%s: Can't init port\n", efct->desc);
		else
			xport->configured_link_state = cmd;
		break;
	}
	case EFCT_XPORT_PORT_OFFLINE: {
		if (efct_hw_port_control(&efct->hw, EFCT_HW_PORT_SHUTDOWN, 0,
					 NULL, NULL))
			efct_log_err(efct, "port shutdown failed\n");
		else
			xport->configured_link_state = cmd;
		break;
	}

	case EFCT_XPORT_SHUTDOWN: {
		struct semaphore sem;
		u32 reset_required;

		/* if a PHYSDEV reset was performed (e.g. hw dump), will affect
		 * all PCI functions; orderly shutdown won't work,
		 * just force free
		 */
		if (efct_hw_get(&efct->hw, EFCT_HW_RESET_REQUIRED,
				&reset_required) != EFCT_HW_RTN_SUCCESS)
			reset_required = 0;

		if (reset_required) {
			efct_log_debug(efct,
				       "reset required, do force shutdown\n");
			efct_xport_force_free(xport);
			break;
		}
		sema_init(&sem, 0);

		efc_register_domain_list_empty_cb(efct->efcport,
					efct_xport_domain_list_empty_cb, &sem);

		if (efct_hw_port_control(&efct->hw, EFCT_HW_PORT_SHUTDOWN, 0,
					 NULL, NULL)) {
			efct_log_debug(efct,
				       "port shutdown failed, do force shutdown\n");
			efct_xport_force_free(xport);
		} else {
			efct_log_debug(efct,
				       "Waiting %d seconds for domain shutdown.\n",
			(EFCT_FC_DOMAIN_SHUTDOWN_TIMEOUT_USEC / 1000000));

			rc = down_timeout(&sem,
			usecs_to_jiffies(EFCT_FC_DOMAIN_SHUTDOWN_TIMEOUT_USEC));
			if (rc) {
				efct_log_debug(efct,
					       "Domain shutdown timed out!!\n");
				efct_xport_force_free(xport);
			}
		}

		efc_register_domain_list_empty_cb(efct->efcport, NULL, NULL);

		/* Free up any saved virtual ports */
		efc_vport_del_all(efct->efcport);
		break;
	}

	/*
	 * POST_NODE_EVENT:  post an event to a node object
	 *
	 * This transport function is used to post an event to a node object.
	 * It does this by submitting a NOP mailbox command to defer execution
	 * to the interrupt context (thereby enforcing the serialized execution
	 * of event posting to the node state machine instances)
	 *
	 * A counting semaphore is used to make the call synchronous (we wait
	 * until the callback increments the semaphore before returning
	 * (or times out)
	 */
	case EFCT_XPORT_POST_NODE_EVENT: {
		struct efc_node_s *node;
		//enum efc_sm_event_e evt;
		u32	evt;
		void *context;
		struct efct_xport_post_node_event_s *payload = NULL;
		struct efct_s *efct;
		struct efct_hw_s *hw;

		/* Retrieve arguments */
		va_start(argp, cmd);
		node = va_arg(argp, struct efc_node_s *);
		//evt = va_arg(argp, enum efc_sm_event_e);
		evt = va_arg(argp, u32);
		context = va_arg(argp, void *);
		va_end(argp);

		efct_assert(node, -1);
		efct_assert(node->efc, -1);

		payload = kmalloc(sizeof(*payload), GFP_KERNEL);
		if (!payload)
			return -1;

		memset(payload, 0, sizeof(*payload));

		efct = node->efc->base;
		hw = &efct->hw;

		/* if node's state machine is disabled,
		 * don't bother continuing
		 */
		if (!node->sm.current_state) {
			efct_log_test(efct, "node %p state machine disabled\n",
				      node);
			return -1;
		}

		/* Setup payload */
		sema_init(&payload->sem, 0);

		/* one for self and one for callback */
		atomic_set(&payload->refcnt, 2);
		payload->node = node;
		payload->evt = evt;
		payload->context = context;

		if (efct_hw_async_call(hw, efct_xport_post_node_event_cb,
				       payload)) {
			efct_log_test(efct, "efct_hw_async_call failed\n");
			kfree(payload);
			rc = -1;
			break;
		}

		/* Wait for completion */
		if (down_interruptible(&payload->sem)) {
			efct_log_test(efct,
				      "POST_NODE_EVENT: sem wait failed\n");
			rc = -1;
		}
		if (atomic_sub_and_test(1, &payload->refcnt))
			kfree(payload);

		break;
	}
	/*
	 * Set wwnn for the port. This will be used instead of the default
	 * provided by FW.
	 */
	case EFCT_XPORT_WWNN_SET: {
		u64 wwnn;

		/* Retrieve arguments */
		va_start(argp, cmd);
		wwnn = va_arg(argp, uint64_t);
		va_end(argp);

		efct_log_debug(efct, " WWNN %016llx\n", wwnn);
		xport->req_wwnn = wwnn;

		break;
	}
	/*
	 * Set wwpn for the port. This will be used instead of the default
	 * provided by FW.
	 */
	case EFCT_XPORT_WWPN_SET: {
		u64 wwpn;

		/* Retrieve arguments */
		va_start(argp, cmd);
		wwpn = va_arg(argp, uint64_t);
		va_end(argp);

		efct_log_debug(efct, " WWPN %016llx\n", wwpn);
		xport->req_wwpn = wwpn;

		break;
	}

	default:
		break;
	}
	return rc;
}

/**
 * @brief Return status on a link.
 *
 * @par Description
 * Returns status information about a link.
 *
 * @param xport Pointer to transport object.
 * @param cmd Command to execute.
 * @param result Pointer to result value.
 *
 * efct_xport_status(*xport, EFCT_XPORT_PORT_STATUS)
 * efct_xport_status(*xport, EFCT_XPORT_LINK_SPEED, *result)
 *	return link speed in MB/sec
 * efct_xport_status(*xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED, *result)
 *	[in] *result is speed to check in MB/s
 *	returns 1 if supported, 0 if not
 * efct_xport_status(*xport, EFCT_XPORT_LINK_STATISTICS, *result)
 *	return link/host port stats
 * efct_xport_status(*xport, EFCT_XPORT_LINK_STAT_RESET, *result)
 *	resets link/host stats
 *
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

int
efct_xport_status(struct efct_xport_s *xport, enum efct_xport_status_e cmd,
		  union efct_xport_stats_u *result)
{
	u32 rc = 0;
	struct efct_s *efct = NULL;
	struct efc_lport *efc = NULL;
	union efct_xport_stats_u value;
	enum efct_hw_rtn_e hw_rc;

	efct_assert(xport, -1);
	efct_assert(xport->efct, -1);

	efct = xport->efct;

	switch (cmd) {
	case EFCT_XPORT_CONFIG_PORT_STATUS:
		efct_assert(result, -1);
		if (xport->configured_link_state == 0) {
			/*
			 * Initial state is offline. configured_link_state is
			 * set to online explicitly when port is brought online
			 */
			xport->configured_link_state = EFCT_XPORT_PORT_OFFLINE;
		}
		result->value = xport->configured_link_state;
		break;

	case EFCT_XPORT_PORT_STATUS:
		efct_assert(result, -1);
		/* Determine port status based on link speed. */
		hw_rc = efct_hw_get(&efct->hw, EFCT_HW_LINK_SPEED,
				    &value.value);
		if (hw_rc == EFCT_HW_RTN_SUCCESS) {
			if (value.value == 0)
				result->value = 0;
			else
				result->value = 1;
			rc = 0;
		} else {
			rc = -1;
		}
		break;

	case EFCT_XPORT_LINK_SPEED: {
		u32 speed;

		efct_assert(result, -1);
		result->value = 0;

		rc = efct_hw_get(&efct->hw, EFCT_HW_LINK_SPEED, &speed);
		if (rc == 0)
			result->value = speed;
		break;
	}

	case EFCT_XPORT_IS_SUPPORTED_LINK_SPEED: {
		u32 speed;
		u32 link_module_type;

		efct_assert(result, -1);
		speed = result->value;

		rc = efct_hw_get(&efct->hw, EFCT_HW_LINK_MODULE_TYPE,
				 &link_module_type);
		if (rc == 0) {
			switch (speed) {
			case 1000:
				rc = (link_module_type &
					EFCT_HW_LINK_MODULE_TYPE_1GB) != 0;
				break;
			case 2000:
				rc = (link_module_type &
					EFCT_HW_LINK_MODULE_TYPE_2GB) != 0;
				break;
			case 4000:
				rc = (link_module_type &
					EFCT_HW_LINK_MODULE_TYPE_4GB) != 0;
				break;
			case 8000:
				rc = (link_module_type &
					EFCT_HW_LINK_MODULE_TYPE_8GB) != 0;
				break;
			case 10000:
				rc = (link_module_type &
					EFCT_HW_LINK_MODULE_TYPE_10GB) != 0;
				break;
			case 16000:
				rc = (link_module_type &
					EFCT_HW_LINK_MODULE_TYPE_16GB) != 0;
				break;
			case 32000:
				rc = (link_module_type &
					EFCT_HW_LINK_MODULE_TYPE_32GB) != 0;
				break;
			default:
				rc = 0;
				break;
			}
		} else {
			rc = 0;
		}
		break;
	}
	case EFCT_XPORT_LINK_STATISTICS:
		memcpy((void *)result, &efct->xport->fc_xport_stats,
		       sizeof(union efct_xport_stats_u));
		break;
	case EFCT_XPORT_LINK_STAT_RESET: {
		/* Create a semaphore to synchronize the stat reset process. */
		sema_init(&result->stats.semaphore, 0);

		/* First reset the link stats */
		rc = efct_hw_get_link_stats(&efct->hw, 0, 1, 1,
					    efct_xport_link_stats_cb, result);

		/* Wait for semaphore to be signaled when the cmd completes */
		if (down_interruptible(&result->stats.semaphore)) {
			/* Undefined failure */
			efct_log_test(efct, "sem wait failed\n");
			rc = -ENXIO;
			break;
		}

		/* Next reset the host stats */
		rc = efct_hw_get_host_stats(&efct->hw, 1,
					    efct_xport_host_stats_cb, result);

		/* Wait for semaphore to be signaled when the cmd completes */
		if (down_interruptible(&result->stats.semaphore)) {
			/* Undefined failure */
			efct_log_test(efct, "sem wait failed\n");
			rc = -ENXIO;
			break;
		}
		break;
	}
	case EFCT_XPORT_IS_QUIESCED:
		efc = efct->efcport;
		result->value = list_empty(&efc->domain_list);
		break;
	default:
		rc = -1;
		break;
	}

	return rc;
}

static void
efct_xport_link_stats_cb(int status, u32 num_counters,
			 struct efct_hw_link_stat_counts_s *counters, void *arg)
{
	union efct_xport_stats_u *result = arg;

	result->stats.link_stats.link_failure_error_count =
		counters[EFCT_HW_LINK_STAT_LINK_FAILURE_COUNT].counter;
	result->stats.link_stats.loss_of_sync_error_count =
		counters[EFCT_HW_LINK_STAT_LOSS_OF_SYNC_COUNT].counter;
	result->stats.link_stats.primitive_sequence_error_count =
		counters[EFCT_HW_LINK_STAT_PRIMITIVE_SEQ_COUNT].counter;
	result->stats.link_stats.invalid_transmission_word_error_count =
		counters[EFCT_HW_LINK_STAT_INVALID_XMIT_WORD_COUNT].counter;
	result->stats.link_stats.crc_error_count =
		counters[EFCT_HW_LINK_STAT_CRC_COUNT].counter;

	up(&result->stats.semaphore);
}

static void
efct_xport_host_stats_cb(int status, u32 num_counters,
			 struct efct_hw_host_stat_counts_s *counters, void *arg)
{
	union efct_xport_stats_u *result = arg;

	result->stats.host_stats.transmit_kbyte_count =
		counters[EFCT_HW_HOST_STAT_TX_KBYTE_COUNT].counter;
	result->stats.host_stats.receive_kbyte_count =
		counters[EFCT_HW_HOST_STAT_RX_KBYTE_COUNT].counter;
	result->stats.host_stats.transmit_frame_count =
		counters[EFCT_HW_HOST_STAT_TX_FRAME_COUNT].counter;
	result->stats.host_stats.receive_frame_count =
		counters[EFCT_HW_HOST_STAT_RX_FRAME_COUNT].counter;

	up(&result->stats.semaphore);
}

static void
efct_xport_async_link_stats_cb(int status, u32 num_counters,
			       struct efct_hw_link_stat_counts_s *counters,
			       void *arg)
{
	union efct_xport_stats_u *result = arg;

	result->stats.link_stats.link_failure_error_count =
		counters[EFCT_HW_LINK_STAT_LINK_FAILURE_COUNT].counter;
	result->stats.link_stats.loss_of_sync_error_count =
		counters[EFCT_HW_LINK_STAT_LOSS_OF_SYNC_COUNT].counter;
	result->stats.link_stats.primitive_sequence_error_count =
		counters[EFCT_HW_LINK_STAT_PRIMITIVE_SEQ_COUNT].counter;
	result->stats.link_stats.invalid_transmission_word_error_count =
		counters[EFCT_HW_LINK_STAT_INVALID_XMIT_WORD_COUNT].counter;
	result->stats.link_stats.crc_error_count =
		counters[EFCT_HW_LINK_STAT_CRC_COUNT].counter;
}

static void
efct_xport_async_host_stats_cb(int status, u32 num_counters,
			       struct efct_hw_host_stat_counts_s *counters,
			       void *arg)
{
	union efct_xport_stats_u *result = arg;

	result->stats.host_stats.transmit_kbyte_count =
		counters[EFCT_HW_HOST_STAT_TX_KBYTE_COUNT].counter;
	result->stats.host_stats.receive_kbyte_count =
		counters[EFCT_HW_HOST_STAT_RX_KBYTE_COUNT].counter;
	result->stats.host_stats.transmit_frame_count =
		counters[EFCT_HW_HOST_STAT_TX_FRAME_COUNT].counter;
	result->stats.host_stats.receive_frame_count =
		counters[EFCT_HW_HOST_STAT_RX_FRAME_COUNT].counter;
}

static void
efct_xport_stats_timer_cb(struct timer_list *t)
{
	struct efct_xport_s *xport = from_timer(xport, t, stats_timer);
	struct efct_s *efct = xport->efct;

	efct_xport_config_stats_timer(efct);
}

/**
 * @brief Get FC link and host Statistics periodically
 *
 * @param hw Hardware context.
 *
 * @return NONE.
 */
static void
efct_xport_config_stats_timer(struct efct_s *efct)
{
	u32 timeout = 3 * 1000;
	struct efct_xport_s *xport = NULL;

	if (!efct) {
		efct_log_err(efct, "failed to locate EFCT device\n");
		return;
	}

	xport = efct->xport;
	efct_hw_get_link_stats(&efct->hw, 0, 0, 0,
			       efct_xport_async_link_stats_cb,
			       &xport->fc_xport_stats);
	efct_hw_get_host_stats(&efct->hw, 0, efct_xport_async_host_stats_cb,
			       &xport->fc_xport_stats);

	timer_setup(&xport->stats_timer,
		    &efct_xport_stats_timer_cb, 0);
	mod_timer(&xport->stats_timer,
		  jiffies + msecs_to_jiffies(timeout));
}

/**
 * @brief Free a transport object.
 *
 * @par Description
 * The transport object is freed.
 *
 * @param xport Pointer to transport object.
 *
 * @return None.
 */

void
efct_xport_free(struct efct_xport_s *xport)
{
	struct efct_s *efct;

	if (xport) {
		efct = xport->efct;
		efct_io_pool_free(xport->io_pool);

		kfree(xport);
	}
}

struct scsi_transport_template *
efct_attach_fc_transport(void)
{
	struct scsi_transport_template *efct_fc_template = NULL;

	efct_fc_template = fc_attach_transport(&efct_xport_functions);

	if (!efct_fc_template)
		efct_log_err(NULL, "failed to attach EFCT with fc transport\n");

	return efct_fc_template;
}

struct scsi_transport_template *
efct_attach_vport_fc_transport(void)
{
	struct scsi_transport_template *efct_fc_template = NULL;

	efct_fc_template = fc_attach_transport(&efct_vport_functions);

	if (!efct_fc_template)
		efct_log_err(NULL, "failed to attach EFCT with fc transport\n");

	return efct_fc_template;
}

void
efct_release_fc_transport(struct scsi_transport_template *transport_template)
{
	if (transport_template)
		efct_log_err(NULL, "releasing transport layer\n");

	/* Releasing FC transport */
	fc_release_transport(transport_template);
}

/**
 * @ingroup scsi_api_initiator
 * @brief Remove host from transport.
 *
 * @par Description
 * Function called by the SCSI mid-layer module to terminate any
 * transport-related elements for a SCSI host.
 *
 * @return None.
 */
void
efct_xport_remove_host(struct Scsi_Host *shost)
{
	/*
	 * Remove host from FC Transport layer
	 *
	 * 1. fc_remove_host()
	 * a. for each vport: queue vport_delete_work (fc_vport_sched_delete())
	 *	b. for each rport: queue rport_delete_work
	 *		(fc_rport_final_delete())
	 *	c. scsi_flush_work()
	 * 2. fc_rport_final_delete()
	 * a. fc_terminate_rport_io
	 *		i. call LLDD's terminate_rport_io()
	 *		ii. scsi_target_unblock()
	 *	b. fc_starget_delete()
	 *		i. fc_terminate_rport_io()
	 *			1. call LLDD's terminate_rport_io()
	 *			2. scsi_target_unblock()
	 *		ii. scsi_remove_target()
	 *      c. invoke LLDD devloss callback
	 *      d. transport_remove_device(&rport->dev)
	 *      e. device_del(&rport->dev)
	 *      f. transport_destroy_device(&rport->dev)
	 *      g. put_device(&shost->shost_gendev) (for fc_host->rport list)
	 *      h. put_device(&rport->dev)
	 */
	fc_remove_host(shost);
}

/**
 * @brief Copy the vport DID into the SCSI host port id.
 *
 * @param shost Kernel SCSI host pointer.
 */
static void
efct_get_host_port_id(struct Scsi_Host *shost)
{
	struct efct_vport_s *vport = (struct efct_vport_s *)shost->hostdata;
	struct efct_s *efct = vport->efct;
	struct efc_lport *efc = efct->efcport;
	struct efc_sli_port_s *sport;

	if (efc->domain && efc->domain->sport) {
		sport = efc->domain->sport;
		fc_host_port_id(shost) = sport->fc_id;
	}
}

/**
 * @brief Set the value of the SCSI host port type.
 *
 * @param shost Kernel SCSI host pointer.
 */
static void
efct_get_host_port_type(struct Scsi_Host *shost)
{
	struct efct_vport_s *vport = (struct efct_vport_s *)shost->hostdata;
	struct efct_s *efct = vport->efct;
	struct efc_lport *efc = efct->efcport;
	struct efc_sli_port_s *sport;
	int type = FC_PORTTYPE_UNKNOWN;

	if (efc->domain && efc->domain->sport) {
		if (efc->domain->is_loop) {
			type = FC_PORTTYPE_LPORT;
		} else {
			sport = efc->domain->sport;
			if (sport->is_vport)
				type = FC_PORTTYPE_NPIV;
			else if (sport->topology == EFC_SPORT_TOPOLOGY_P2P)
				type = FC_PORTTYPE_PTP;
			else if (sport->topology == EFC_SPORT_TOPOLOGY_UNKNOWN)
				type = FC_PORTTYPE_UNKNOWN;
			else
				type = FC_PORTTYPE_NPORT;
		}
	}
	fc_host_port_type(shost) = type;
}

static void
efct_get_host_vport_type(struct Scsi_Host *shost)
{
	fc_host_port_type(shost) = FC_PORTTYPE_NPIV;
}

/**
 * @brief Set the value of the SCSI host port state
 *
 * @param shost Kernel SCSI host pointer.
 */
static void
efct_get_host_port_state(struct Scsi_Host *shost)
{
	struct efct_vport_s *vport = (struct efct_vport_s *)shost->hostdata;
	struct efct_s *efct = vport->efct;
	struct efc_lport *efc = efct->efcport;

	if (efc->domain)
		fc_host_port_state(shost) = FC_PORTSTATE_ONLINE;
	else
		fc_host_port_state(shost) = FC_PORTSTATE_OFFLINE;
}

/**
 * @brief Set the value of the SCSI host speed.
 *
 * @param shost Kernel SCSI host pointer.
 */
static void
efct_get_host_speed(struct Scsi_Host *shost)
{
	struct efct_vport_s *vport = (struct efct_vport_s *)shost->hostdata;
	struct efct_s *efct = vport->efct;
	struct efc_lport *efc = efct->efcport;
	union efct_xport_stats_u speed;
	u32 fc_speed = FC_PORTSPEED_UNKNOWN;
	int rc;

	if (efc->domain && efc->domain->sport) {
		rc = efct_xport_status(efct->xport,
				       EFCT_XPORT_LINK_SPEED, &speed);
		if (rc == 0) {
			switch (speed.value) {
			case 1000:
				fc_speed = FC_PORTSPEED_1GBIT;
				break;
			case 2000:
				fc_speed = FC_PORTSPEED_2GBIT;
				break;
			case 4000:
				fc_speed = FC_PORTSPEED_4GBIT;
				break;
			case 8000:
				fc_speed = FC_PORTSPEED_8GBIT;
				break;
			case 10000:
				fc_speed = FC_PORTSPEED_10GBIT;
				break;
			case 16000:
				fc_speed = FC_PORTSPEED_16GBIT;
				break;
			case 32000:
				fc_speed = FC_PORTSPEED_32GBIT;
				break;
			}
		}
	}
	fc_host_speed(shost) = fc_speed;
}

/**
 * @brief Set the value of the SCSI host fabric name.
 *
 * @param shost Kernel SCSI host pointer.
 */
static void
efct_get_host_fabric_name(struct Scsi_Host *shost)
{
	struct efct_vport_s *vport = (struct efct_vport_s *)shost->hostdata;
	struct efct_s *efct = vport->efct;
	struct efc_lport *efc = efct->efcport;

	if (efc->domain) {
		struct fc_plogi_payload_s  *sp =
			(struct fc_plogi_payload_s  *)
				efc->domain->flogi_service_params;

		fc_host_fabric_name(shost) =
			((uint64_t)be32_to_cpu(sp->node_name_hi) << 32ll) |
			(be32_to_cpu(sp->node_name_lo));
	}
}

/**
 * @brief Return statistical information about the adapter.
 *
 * @par Description
 * Returns NULL on error for link down, no mbox pool, sli2 active,
 * management not allowed, memory allocation error, or mbox error.
 *
 * @param shost Kernel SCSI host pointer.
 *
 * @return NULL for error address of the adapter host statistics.
 */
static struct fc_host_statistics *
efct_get_stats(struct Scsi_Host *shost)
{
	struct efct_vport_s *vport = (struct efct_vport_s *)shost->hostdata;
	struct efct_s *efct = vport->efct;
	union efct_xport_stats_u stats;
	u32 rc = 1;

	rc = efct_xport_status(efct->xport, EFCT_XPORT_LINK_STATISTICS, &stats);
	if (rc != 0) {
		pr_err("efct_xport_status returned non 0 - %d\n", rc);
		return NULL;
	}

	vport->fc_host_stats.loss_of_sync_count =
		stats.stats.link_stats.loss_of_sync_error_count;
	vport->fc_host_stats.link_failure_count =
		stats.stats.link_stats.link_failure_error_count;
	vport->fc_host_stats.prim_seq_protocol_err_count =
		stats.stats.link_stats.primitive_sequence_error_count;
	vport->fc_host_stats.invalid_tx_word_count =
		stats.stats.link_stats.invalid_transmission_word_error_count;
	vport->fc_host_stats.invalid_crc_count =
		stats.stats.link_stats.crc_error_count;
	/* mbox returns kbyte count so we need to convert to words */
	vport->fc_host_stats.tx_words =
		stats.stats.host_stats.transmit_kbyte_count * 256;
	/* mbox returns kbyte count so we need to convert to words */
	vport->fc_host_stats.rx_words =
		stats.stats.host_stats.receive_kbyte_count * 256;
	vport->fc_host_stats.tx_frames =
		stats.stats.host_stats.transmit_frame_count;
	vport->fc_host_stats.rx_frames =
		stats.stats.host_stats.receive_frame_count;
	if (efct->xport) {
		struct efct_xport_s *xport = efct->xport;

		vport->fc_host_stats.fcp_input_requests =
				xport->fcp_stats.input_requests;
		vport->fc_host_stats.fcp_output_requests =
				xport->fcp_stats.output_requests;
		vport->fc_host_stats.fcp_output_megabytes =
				xport->fcp_stats.output_bytes >> 20;
		vport->fc_host_stats.fcp_input_megabytes =
				xport->fcp_stats.input_bytes >> 20;
		vport->fc_host_stats.fcp_control_requests =
				xport->fcp_stats.control_requests;
	}

	return &vport->fc_host_stats;
}

/**
 * @brief Copy the adapter link stats information.
 *
 * @param shost Kernel SCSI host pointer.
 */
static void
efct_reset_stats(struct Scsi_Host *shost)
{
	struct efct_vport_s *vport = (struct efct_vport_s *)shost->hostdata;
	struct efct_s *efct = vport->efct;
	/* argument has no purpose for this action */
	union efct_xport_stats_u dummy;
	u32 rc = 0;

	rc = efct_xport_status(efct->xport, EFCT_XPORT_LINK_STAT_RESET, &dummy);
	if (rc != 0)
		pr_err("efct_xport_status returned non 0 - %d\n", rc);
}

/**
 * @brief Set the target port id to the ndlp DID or -1.
 *
 * @param starget Kernel SCSI target pointer.
 */
static void
efct_get_starget_port_id(struct scsi_target *starget)
{
	pr_err("%s\n", __func__);
}

/**
 * @brief Set the target node name.
 *
 * @par Description
 * Set the target node name to the ndlp node name wwn or zero.
 *
 * @param starget Kernel SCSI target pointer.
 */
static void
efct_get_starget_node_name(struct scsi_target *starget)
{
	pr_err("%s\n", __func__);
}

/**
 * @brief Set the target port name.
 *
 * @par Description
 * Set the target port name to the ndlp port name wwn or zero.
 *
 * @param starget Kernel SCSI target pointer.
 */
static void
efct_get_starget_port_name(struct scsi_target *starget)
{
	pr_err("%s\n", __func__);
}

/**
 * @brief Set the rport dev loss tmo.
 *
 * @par Description
 * If timeout is non zero, set the dev_loss_tmo to timeout; else set
 * dev_loss_tmo to one.
 *
 * @param rport FC rport address.
 * @param timeout New value for dev loss tmo.
 */
static void
efct_set_rport_loss_tmo(struct fc_rport *rport, u32 timeout)
{
	pr_err("%s: rport=%p timeout=%d\n", __func__, rport, timeout);

	if (timeout == 0)
		timeout = 1;

	rport->dev_loss_tmo = timeout;
}

/**
 * @brief Set the vport's symbolic name.
 *
 * @par Description
 * This function is called by the transport after the fc_vport's symbolic name
 * has been changed. This function re-registers the symbolic name with the
 * switch to propagate the change into the fabric, if the vport is active.
 *
 * @param fc_vport The fc_vport who's symbolic name has been changed.
 */
static void
efct_set_vport_symbolic_name(struct fc_vport *fc_vport)
{
	pr_err("%s\n", __func__);
}

/**
 * @brief Gracefully take the link down and reinitialize it
 * (does not issue LIP).
 *
 * @par Description
 * Bring the link down gracefully then re-init the link. The firmware will
 * re-initialize the Fibre Channel interface as required.
 * It does not issue a LIP.
 *
 * @param shost Scsi_Host pointer.
 *
 * @return
 * - 0 - Success.
 * - EPERM - Port is offline or management commands are being blocked.
 * - ENOMEM - Unable to allocate memory for the mailbox command.
 * - EIO - Error in sending the mailbox command.
 */
static int
efct_issue_lip(struct Scsi_Host *shost)
{
	struct efct_vport_s *vport =
			shost ? (struct efct_vport_s *)shost->hostdata : NULL;
	struct efct_s *efct = vport ? vport->efct : NULL;

	if (!shost || !vport || !efct) {
		pr_err("%s: shost=%p vport=%p efct=%p\n", __func__,
		       shost, vport, efct);
		return -EPERM;
	}

	if (efct_xport_control(efct->xport, EFCT_XPORT_PORT_OFFLINE))
		efct_log_test(efct, "EFCT_XPORT_PORT_OFFLINE failed\n");

	if (efct_xport_control(efct->xport, EFCT_XPORT_PORT_ONLINE))
		efct_log_test(efct, "EFCT_XPORT_PORT_ONLINE failed\n");

	return 0;
}

struct efct_vport_s *
efct_scsi_new_vport(struct efct_s *efct, struct device *dev)
{
	struct Scsi_Host *shost = NULL;
	int error = 0;
	struct efct_vport_s *vport = NULL;
	union efct_xport_stats_u speed;
	u32 supported_speeds = 0;

	shost = scsi_host_alloc(&efct_template, sizeof(*vport));
	if (!shost) {
		efct_log_err(efct, "failed to allocate Scsi_Host struct\n");
		return NULL;
	}

	/* save efct information to shost LLD-specific space */
	vport = (struct efct_vport_s *)shost->hostdata;
	vport->efct = efct;
	vport->is_vport = true;

	shost->can_queue = efct_scsi_get_property(efct, EFCT_SCSI_MAX_IOS);
	shost->max_cmd_len = 16; /* 16-byte CDBs */
	shost->max_id = 0xffff;
	shost->max_lun = 0xffffffff;

	/* can only accept (from mid-layer) as many SGEs as we've pre-regited*/
	shost->sg_tablesize = efct_scsi_get_property(efct, EFCT_SCSI_MAX_SGL);

	/* attach FC Transport template to shost */
	shost->transportt = efct_vport_fc_tt;
	efct_log_debug(efct, "vport transport template=%p\n",
		       efct_vport_fc_tt);

	/* get pci_dev structure and add host to SCSI ML */
	error = scsi_add_host_with_dma(shost, dev, &efct->efct_os.pdev->dev);
	if (error) {
		efct_log_test(efct, "failed scsi_add_host_with_dma\n");
		return NULL;
	}

	/* Set symbolic name for host port */
	snprintf(fc_host_symbolic_name(shost),
		 sizeof(fc_host_symbolic_name(shost)),
		     "Emulex %s FV%s DV%s", efct->model,
		     efct->fw_version, efct->driver_version);

	/* Set host port supported classes */
	fc_host_supported_classes(shost) = FC_COS_CLASS3;

	speed.value = 1000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_1GBIT;
	}
	speed.value = 2000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_2GBIT;
	}
	speed.value = 4000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_4GBIT;
	}
	speed.value = 8000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_8GBIT;
	}
	speed.value = 10000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_10GBIT;
	}
	speed.value = 16000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_16GBIT;
	}
	speed.value = 32000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_32GBIT;
	}

	fc_host_supported_speeds(shost) = supported_speeds;
	vport->shost = shost;

	return vport;
}

int efct_scsi_del_vport(struct efct_s *efct, struct Scsi_Host *shost)
{
	if (shost) {
		efct_log_debug(efct,
			       "Unregistering vport with Transport Layer\n");
		efct_xport_remove_host(shost);
		efct_log_debug(efct, "Unregistering vport with SCSI Midlayer\n");
		scsi_remove_host(shost);
		scsi_host_put(shost);
		return 0;
	}
	return -1;
}

static int
efct_vport_create(struct fc_vport *fc_vport, bool disable)
{
	struct Scsi_Host *shost = fc_vport ? fc_vport->shost : NULL;
	struct efct_vport_s *pport = shost ?
					(struct efct_vport_s *)shost->hostdata :
					NULL;
	struct efct_s *efct = pport ? pport->efct : NULL;
	struct efct_vport_s *vport = NULL;

	if (!fc_vport || !shost)
		goto fail;

	vport = efct_scsi_new_vport(efct, &fc_vport->dev);
	if (!vport) {
		efct_log_err(NULL, "failed to create vport\n");
		goto fail;
	}

	vport->fc_vport = fc_vport;
	vport->npiv_wwpn = fc_vport->port_name;
	vport->npiv_wwnn = fc_vport->node_name;
	fc_host_node_name(vport->shost) = vport->npiv_wwnn;
	fc_host_port_name(vport->shost) = vport->npiv_wwpn;
	*(struct efct_vport_s **)fc_vport->dd_data = vport;

	return 0;

fail:
	efct_log_err(NULL, "failed\n");
	return -1;
}

static int
efct_vport_delete(struct fc_vport *fc_vport)
{
	struct efct_vport_s *vport = *(struct efct_vport_s **)fc_vport->dd_data;
	struct Scsi_Host *shost = vport ? vport->shost : NULL;
	struct efct_s *efct = vport ? vport->efct : NULL;
	int rc = -1;

	rc = efct_scsi_del_vport(efct, shost);

	if (rc)
		efct_log_err(NULL, "failed\n");

	return rc;
}

/*
 * This function will be called when dev_loss_tmo fires
 */
static void
efct_dev_loss_tmo_callbk(struct fc_rport *rport)
{
	struct efct_rport_data_s *rport_data = rport->dd_data;
	struct efc_node_s *node = rport_data->node;

	struct efct_s *efct = node->efc->base;

	if (node) {
		efct_log_debug(efct, "devloss for node %s\n",
			       node->display_name);
		/*
		 * If the link is disrupted, we may get a call where the xport
		 * is gone. check for NULL before calling to prevent an assert.
		 */
		if (efct && efct->xport) {
			efct_xport_control(efct->xport,
					   EFCT_XPORT_POST_NODE_EVENT, node,
				EFCT_XPORT_SHUTDOWN, NULL);
		}
	} else {
		/*
		 * Although we do not have a link to the node, we can still
		 * report back to the FC transport that the node is removed.
		 */
		efct_log_debug(NULL, "devloss for node already deleted, fc_id 0x%x\n",
			       rport->port_id);
		fc_remote_port_delete(rport);
	}
}

static void
efct_terminate_rport_io(struct fc_rport *rport)
{
}

static int
efct_vport_disable(struct fc_vport *fc_vport, bool disable)
{
	return 0;
}

static struct fc_function_template efct_xport_functions = {
	.set_rport_dev_loss_tmo = efct_set_rport_loss_tmo,

	.get_starget_node_name = efct_get_starget_node_name,
	.get_starget_port_name = efct_get_starget_port_name,
	.get_starget_port_id  = efct_get_starget_port_id,

	.get_host_port_id = efct_get_host_port_id,
	.get_host_port_type = efct_get_host_port_type,
	.get_host_port_state = efct_get_host_port_state,
	.get_host_speed = efct_get_host_speed,
	.get_host_fabric_name = efct_get_host_fabric_name,

	.get_fc_host_stats = efct_get_stats,
	.reset_fc_host_stats = efct_reset_stats,

	.issue_fc_host_lip = efct_issue_lip,

	.dev_loss_tmo_callbk = efct_dev_loss_tmo_callbk,
	.terminate_rport_io = efct_terminate_rport_io,

	.set_vport_symbolic_name = efct_set_vport_symbolic_name,
	.vport_disable = efct_vport_disable,

	/* allocation lengths for host-specific data */
	.dd_fcrport_size = sizeof(struct efct_rport_data_s),
	.dd_fcvport_size = 128, /* should be sizeof(...) */

	/* remote port fixed attributes */
	.show_rport_maxframe_size = 1,
	.show_rport_supported_classes = 1,
	.show_rport_dev_loss_tmo = 1,

	/* target dynamic attributes */
	.show_starget_node_name = 1,
	.show_starget_port_name = 1,
	.show_starget_port_id = 1,

	/* host fixed attributes */
	.show_host_node_name = 1,
	.show_host_port_name = 1,
	.show_host_supported_classes = 1,
	.show_host_supported_fc4s = 1,
	.show_host_supported_speeds = 1,
	.show_host_maxframe_size = 1,

	/* host dynamic attributes */
	.show_host_port_id = 1,
	.show_host_port_type = 1,
	.show_host_port_state = 1,
	/* active_fc4s is shown but doesn't change (thus no get function) */
	.show_host_active_fc4s = 1,
	.show_host_speed = 1,
	.show_host_fabric_name = 1,
	.show_host_symbolic_name = 1,
	.vport_create = efct_vport_create,
	.vport_delete = efct_vport_delete,
};

static struct fc_function_template efct_vport_functions = {
	.set_rport_dev_loss_tmo = efct_set_rport_loss_tmo,

	.get_starget_node_name = efct_get_starget_node_name,
	.get_starget_port_name = efct_get_starget_port_name,
	.get_starget_port_id  = efct_get_starget_port_id,

	.get_host_port_id = efct_get_host_port_id,
	.get_host_port_type = efct_get_host_vport_type,
	.get_host_port_state = efct_get_host_port_state,
	.get_host_speed = efct_get_host_speed,
	.get_host_fabric_name = efct_get_host_fabric_name,

	.get_fc_host_stats = efct_get_stats,
	.reset_fc_host_stats = efct_reset_stats,

	.issue_fc_host_lip = efct_issue_lip,

	.dev_loss_tmo_callbk = efct_dev_loss_tmo_callbk,
	.terminate_rport_io = efct_terminate_rport_io,

	.set_vport_symbolic_name = efct_set_vport_symbolic_name,

	/* allocation lengths for host-specific data */
	.dd_fcrport_size = sizeof(struct efct_rport_data_s),
	.dd_fcvport_size = 128, /* should be sizeof(...) */

	/* remote port fixed attributes */
	.show_rport_maxframe_size = 1,
	.show_rport_supported_classes = 1,
	.show_rport_dev_loss_tmo = 1,

	/* target dynamic attributes */
	.show_starget_node_name = 1,
	.show_starget_port_name = 1,
	.show_starget_port_id = 1,

	/* host fixed attributes */
	.show_host_node_name = 1,
	.show_host_port_name = 1,
	.show_host_supported_classes = 1,
	.show_host_supported_fc4s = 1,
	.show_host_supported_speeds = 1,
	.show_host_maxframe_size = 1,

	/* host dynamic attributes */
	.show_host_port_id = 1,
	.show_host_port_type = 1,
	.show_host_port_state = 1,
	/* active_fc4s is shown but doesn't change (thus no get function) */
	.show_host_active_fc4s = 1,
	.show_host_speed = 1,
	.show_host_fabric_name = 1,
	.show_host_symbolic_name = 1,
};

int
efct_scsi_new_device(struct efct_s *efct)
{
	struct Scsi_Host *shost = NULL;
	int error = 0;
	struct efct_vport_s *vport = NULL;
	union efct_xport_stats_u speed;
	u32 supported_speeds = 0;

	shost = scsi_host_alloc(&efct_template, sizeof(*vport));
	if (!shost) {
		efct_log_err(efct, "failed to allocate Scsi_Host struct\n");
		return -1;
	}

	/* save shost to initiator-client context */
	efct->shost = shost;

	/* save efct information to shost LLD-specific space */
	vport = (struct efct_vport_s *)shost->hostdata;
	vport->efct = efct;

	/*
	 * Set initial can_queue value to the max SCSI IOs. This is the maximum
	 * global queue depth (as opposed to the per-LUN queue depth --
	 * .cmd_per_lun This may need to be adjusted for I+T mode.
	 */
	shost->can_queue = efct_scsi_get_property(efct, EFCT_SCSI_MAX_IOS);
	shost->max_cmd_len = 16; /* 16-byte CDBs */
	shost->max_id = 0xffff;
	shost->max_lun = 0xffffffff;

	/*
	 * can only accept (from mid-layer) as many SGEs as we've
	 * pre-registered
	 */
	shost->sg_tablesize = efct_scsi_get_property(efct, EFCT_SCSI_MAX_SGL);

	/* attach FC Transport template to shost */
	shost->transportt = efct_xport_fc_tt;
	efct_log_debug(efct, "transport template=%p\n", efct_xport_fc_tt);

	/* get pci_dev structure and add host to SCSI ML */
	error = scsi_add_host_with_dma(shost, &efct->efct_os.pdev->dev,
				       &efct->efct_os.pdev->dev);
	if (error) {
		efct_log_test(efct, "failed scsi_add_host_with_dma\n");
		return -1;
	}

	/* Set symbolic name for host port */
	snprintf(fc_host_symbolic_name(shost),
		 sizeof(fc_host_symbolic_name(shost)),
		     "Emulex %s FV%s DV%s", efct->model,
		     efct->fw_version, efct->driver_version);

	/* Set host port supported classes */
	fc_host_supported_classes(shost) = FC_COS_CLASS3;

	speed.value = 1000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_1GBIT;
	}
	speed.value = 2000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_2GBIT;
	}
	speed.value = 4000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_4GBIT;
	}
	speed.value = 8000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_8GBIT;
	}
	speed.value = 10000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_10GBIT;
	}
	speed.value = 16000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_16GBIT;
	}
	speed.value = 32000;
	if (efct_xport_status(efct->xport, EFCT_XPORT_IS_SUPPORTED_LINK_SPEED,
			      &speed)) {
		supported_speeds |= FC_PORTSPEED_32GBIT;
	}

	fc_host_supported_speeds(shost) = supported_speeds;

	fc_host_node_name(shost) = efct_get_wwn(&efct->hw, EFCT_HW_WWN_NODE);
	fc_host_port_name(shost) = efct_get_wwn(&efct->hw, EFCT_HW_WWN_PORT);
	fc_host_max_npiv_vports(shost) = 128;

	return 0;
}

int efct_scsi_del_device(struct efct_s *efct)
{
	if (efct->shost) {
		efct_log_debug(efct, "Unregistering with Transport Layer\n");
		efct_xport_remove_host(efct->shost);
		efct_log_debug(efct, "Unregistering with SCSI Midlayer\n");
		scsi_remove_host(efct->shost);
		scsi_host_put(efct->shost);
		efct->shost = NULL;
	}
	return 0;
}

int
efct_scsi_reg_fc_transport(void)
{
	/* attach to appropriate scsi_tranport_* module */
	efct_xport_fc_tt = efct_attach_fc_transport();
	if (!efct_xport_fc_tt) {
		pr_err("%s: failed to attach to scsi_transport_*", __func__);
		return -1;
	}

	efct_vport_fc_tt = efct_attach_vport_fc_transport();
	if (!efct_vport_fc_tt) {
		pr_err("%s: failed to attach to scsi_transport_*", __func__);
		efct_release_fc_transport(efct_xport_fc_tt);
		efct_xport_fc_tt = NULL;
		return -1;
	}

	return 0;
}

int
efct_scsi_release_fc_transport(void)
{
	/* detach from scsi_transport_* */
	efct_release_fc_transport(efct_xport_fc_tt);
	efct_xport_fc_tt = NULL;
	if (efct_vport_fc_tt)
		efct_release_fc_transport(efct_vport_fc_tt);
	efct_vport_fc_tt = NULL;

	return 0;
}

