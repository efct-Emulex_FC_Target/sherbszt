/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#if !defined(__EFCT_SCSI_H__)
#define __EFCT_SCSI_H__

/* efct_scsi_rcv_cmd() efct_scsi_rcv_tmf() flags */
#define EFCT_SCSI_CMD_DIR_IN		BIT(0)
#define EFCT_SCSI_CMD_DIR_OUT		BIT(1)
#define EFCT_SCSI_CMD_SIMPLE		BIT(2)
#define EFCT_SCSI_CMD_HEAD_OF_QUEUE	BIT(3)
#define EFCT_SCSI_CMD_ORDERED		BIT(4)
#define EFCT_SCSI_CMD_UNTAGGED		BIT(5)
#define EFCT_SCSI_CMD_ACA		BIT(6)
#define EFCT_SCSI_FIRST_BURST_ERR	BIT(7)
#define EFCT_SCSI_FIRST_BURST_ABORTED	BIT(8)

/* efct_scsi_send_rd_data/recv_wr_data/send_resp flags */
#define EFCT_SCSI_LAST_DATAPHASE		BIT(0)
#define EFCT_SCSI_NO_AUTO_RESPONSE	BIT(1)
#define EFCT_SCSI_LOW_LATENCY		BIT(2)

#define EFCT_SCSI_SNS_BUF_VALID(sense)	((sense) && \
			(0x70 == (((const u8 *)(sense))[0] & 0x70)))

#define EFCT_SCSI_WQ_STEERING_SHIFT	(16)
#define EFCT_SCSI_WQ_STEERING_MASK	(0xf << EFCT_SCSI_WQ_STEERING_SHIFT)
#define EFCT_SCSI_WQ_STEERING_CLASS	(0 << EFCT_SCSI_WQ_STEERING_SHIFT)
#define EFCT_SCSI_WQ_STEERING_REQUEST	BIT(EFCT_SCSI_WQ_STEERING_SHIFT)
#define EFCT_SCSI_WQ_STEERING_CPU	(2 << EFCT_SCSI_WQ_STEERING_SHIFT)

#define EFCT_SCSI_WQ_CLASS_SHIFT		(20)
#define EFCT_SCSI_WQ_CLASS_MASK		(0xf << EFCT_SCSI_WQ_CLASS_SHIFT)
#define EFCT_SCSI_WQ_CLASS(x)		((x & EFCT_SCSI_WQ_CLASS_MASK) << \
						EFCT_SCSI_WQ_CLASS_SHIFT)

#define EFCT_SCSI_WQ_CLASS_LOW_LATENCY	(1)

/*!
 * @defgroup scsi_api_base SCSI Base Target/Initiator
 * @defgroup scsi_api_target SCSI Target
 * @defgroup scsi_api_initiator SCSI Initiator
 */

/**
 * @brief SCSI command response.
 *
 * This structure is used by target-servers to specify SCSI status and
 * sense data.  In this case all but the @b residual element are used. For
 * initiator-clients, this structure is used by the SCSI API to convey the
 * response data for issued commands, including the residual element.
 */
struct efct_scsi_cmd_resp_s {
	u8 scsi_status;			/**< SCSI status */
	u16 scsi_status_qualifier;		/**< SCSI status qualifier */
	/**< pointer to response data buffer */
	u8 *response_data;
	/**< length of response data buffer (bytes) */
	u32 response_data_length;
	u8 *sense_data;		/**< pointer to sense data buffer */
	/**< length of sense data buffer (bytes) */
	u32 sense_data_length;
	/* command residual (not used for target), positive value
	 * indicates an underflow, negative value indicates overflow
	 */
	int residual;
	/**< Command response length received in wcqe */
	u32 response_wire_length;
};

struct efct_vport_s {
	struct efct_s *efct;
	bool is_vport;
	struct fc_host_statistics fc_host_stats;
	struct Scsi_Host *shost;
	struct fc_vport *fc_vport;
	u64 npiv_wwpn;
	u64 npiv_wwnn;

};

/* Status values returned by IO callbacks */
enum efct_scsi_io_status_e {
	EFCT_SCSI_STATUS_GOOD = 0,
	EFCT_SCSI_STATUS_ABORTED,
	EFCT_SCSI_STATUS_ERROR,
	EFCT_SCSI_STATUS_DIF_GUARD_ERR,
	EFCT_SCSI_STATUS_DIF_REF_TAG_ERROR,
	EFCT_SCSI_STATUS_DIF_APP_TAG_ERROR,
	EFCT_SCSI_STATUS_DIF_UNKNOWN_ERROR,
	EFCT_SCSI_STATUS_PROTOCOL_CRC_ERROR,
	EFCT_SCSI_STATUS_NO_IO,
	EFCT_SCSI_STATUS_ABORT_IN_PROGRESS,
	EFCT_SCSI_STATUS_CHECK_RESPONSE,
	EFCT_SCSI_STATUS_COMMAND_TIMEOUT,
	EFCT_SCSI_STATUS_TIMEDOUT_AND_ABORTED,
	EFCT_SCSI_STATUS_SHUTDOWN,
	EFCT_SCSI_STATUS_NEXUS_LOST,

};

struct efct_io_s;
struct efc_node_s;
struct efc_domain_s;
struct efc_sli_port_s;

/* Callback used by send_rd_data(), recv_wr_data(), send_resp() */
typedef int (*efct_scsi_io_cb_t)(struct efct_io_s *io,
				    enum efct_scsi_io_status_e status,
				    u32 flags, void *arg);

/* Callback used by send_rd_io(), send_wr_io() */
typedef int (*efct_scsi_rsp_io_cb_t)(struct efct_io_s *io,
			enum efct_scsi_io_status_e status,
			struct efct_scsi_cmd_resp_s *rsp,
			u32 flags, void *arg);

/* efct_scsi_cb_t flags */
#define EFCT_SCSI_IO_CMPL		BIT(0)	/* IO completed */
/* IO completed, response sent */
#define EFCT_SCSI_IO_CMPL_RSP_SENT	BIT(1)
#define EFCT_SCSI_IO_ABORTED		BIT(2)	/* IO was aborted */

/* efct_scsi_recv_tmf() request values */
enum efct_scsi_tmf_cmd_e {
	EFCT_SCSI_TMF_ABORT_TASK = 1,
	EFCT_SCSI_TMF_QUERY_TASK_SET,
	EFCT_SCSI_TMF_ABORT_TASK_SET,
	EFCT_SCSI_TMF_CLEAR_TASK_SET,
	EFCT_SCSI_TMF_QUERY_ASYNCHRONOUS_EVENT,
	EFCT_SCSI_TMF_LOGICAL_UNIT_RESET,
	EFCT_SCSI_TMF_CLEAR_ACA,
	EFCT_SCSI_TMF_TARGET_RESET,
};

/* efct_scsi_send_tmf_resp() response values */
enum efct_scsi_tmf_resp_e {
	EFCT_SCSI_TMF_FUNCTION_COMPLETE = 1,
	EFCT_SCSI_TMF_FUNCTION_SUCCEEDED,
	EFCT_SCSI_TMF_FUNCTION_IO_NOT_FOUND,
	EFCT_SCSI_TMF_FUNCTION_REJECTED,
	EFCT_SCSI_TMF_INCORRECT_LOGICAL_UNIT_NUMBER,
	EFCT_SCSI_TMF_SERVICE_DELIVERY,
};

/**
 * @brief property names for efct_scsi_get_property() functions
 */

enum efct_scsi_property_e {
	EFCT_SCSI_MAX_SGE,
	EFCT_SCSI_MAX_SGL,
	EFCT_SCSI_WWNN,
	EFCT_SCSI_WWPN,
	EFCT_SCSI_SERIALNUMBER,
	EFCT_SCSI_PARTNUMBER,
	EFCT_SCSI_PORTNUM,
	EFCT_SCSI_BIOS_VERSION_STRING,
	EFCT_SCSI_MAX_IOS,
	EFCT_SCSI_DIF_CAPABLE,
	EFCT_SCSI_DIF_MULTI_SEPARATE,
	EFCT_SCSI_MAX_FIRST_BURST,
	EFCT_SCSI_ENABLE_TASK_SET_FULL,
};

#define DIF_SIZE		8

/**
 * @brief T10 DIF operations
 *
 *	WARNING: do not reorder or insert to this list without making
 *	appropriate changes in efct_dif.c
 */
enum efct_scsi_dif_oper_e {
	EFCT_SCSI_DIF_OPER_DISABLED,
	EFCT_SCSI_DIF_OPER_IN_NODIF_OUT_CRC,
	EFCT_SCSI_DIF_OPER_IN_CRC_OUT_NODIF,
	EFCT_SCSI_DIF_OPER_IN_NODIF_OUT_CHKSUM,
	EFCT_SCSI_DIF_OPER_IN_CHKSUM_OUT_NODIF,
	EFCT_SCSI_DIF_OPER_IN_CRC_OUT_CRC,
	EFCT_SCSI_DIF_OPER_IN_CHKSUM_OUT_CHKSUM,
	EFCT_SCSI_DIF_OPER_IN_CRC_OUT_CHKSUM,
	EFCT_SCSI_DIF_OPER_IN_CHKSUM_OUT_CRC,
	EFCT_SCSI_DIF_OPER_IN_RAW_OUT_RAW,
};

#define EFCT_SCSI_DIF_OPER_PASS_THRU	EFCT_SCSI_DIF_OPER_IN_CRC_OUT_CRC
#define EFCT_SCSI_DIF_OPER_STRIP	EFCT_SCSI_DIF_OPER_IN_CRC_OUT_NODIF
#define EFCT_SCSI_DIF_OPER_INSERT	EFCT_SCSI_DIF_OPER_IN_NODIF_OUT_CRC

/**
 * @brief T10 DIF block sizes
 */
enum efct_scsi_dif_blk_size_e {
	EFCT_SCSI_DIF_BK_SIZE_512,
	EFCT_SCSI_DIF_BK_SIZE_1024,
	EFCT_SCSI_DIF_BK_SIZE_2048,
	EFCT_SCSI_DIF_BK_SIZE_4096,
	EFCT_SCSI_DIF_BK_SIZE_520,
	EFCT_SCSI_DIF_BK_SIZE_4104
};

/**
 * @brief generic scatter-gather list structure
 */

struct efct_scsi_sgl_s {
	uintptr_t	addr;		/**< physical address */
	/**< address of DIF segment, zero if DIF is interleaved */
	uintptr_t	dif_addr;
	size_t		len;		/**< length */
};

/**
 * @brief T10 DIF information passed to the transport
 */

struct efct_scsi_dif_info_s {
	enum efct_scsi_dif_oper_e dif_oper;
	enum efct_scsi_dif_blk_size_e blk_size;
	u32 ref_tag;
	bool check_ref_tag;
	bool check_app_tag;
	bool check_guard;
	bool dif_separate;

	/* If the APP TAG is 0xFFFF, disable checking
	 * the REF TAG and CRC fields
	 */
	bool disable_app_ffff;

	/* if the APP TAG is 0xFFFF and REF TAG is 0xFFFF_FFFF,
	 * disable checking the received CRC field.
	 */
	bool disable_app_ref_ffff;
	u64 lba;
	u16 app_tag;
};

/* Return values for calls from base driver to
 * target-server/initiator-client
 */
#define EFCT_SCSI_CALL_COMPLETE	0 /* All work is done */
#define EFCT_SCSI_CALL_ASYNC	1 /* Work will be completed asynchronously */

/* Calls from target/initiator to base driver */

enum efct_scsi_io_role_e {
	EFCT_SCSI_IO_ROLE_ORIGINATOR,
	EFCT_SCSI_IO_ROLE_RESPONDER,
};

void efct_scsi_io_alloc_enable(struct efc_lport *efc, struct efc_node_s *node);
void efct_scsi_io_alloc_disable(struct efc_lport *efc, struct efc_node_s *node);
extern struct efct_io_s *
efct_scsi_io_alloc(struct efc_node_s *node, enum efct_scsi_io_role_e);
void efct_scsi_io_free(struct efct_io_s *io);
struct efct_io_s *efct_io_get_instance(struct efct_s *efct, u32 index);

/* Calls from base driver to target-server */

int efct_scsi_tgt_driver_init(void);
int efct_scsi_tgt_driver_exit(void);
int efct_scsi_tgt_io_init(struct efct_io_s *io);
int efct_scsi_tgt_io_exit(struct efct_io_s *io);
int efct_scsi_tgt_new_device(struct efct_s *efct);
int efct_scsi_tgt_del_device(struct efct_s *efct);
int
efct_scsi_tgt_new_domain(struct efc_lport *efc, struct efc_domain_s *domain);
void
efct_scsi_tgt_del_domain(struct efc_lport *efc, struct efc_domain_s *domain);
int
efct_scsi_tgt_new_sport(struct efc_lport *efc, struct efc_sli_port_s *sport);
void
efct_scsi_tgt_del_sport(struct efc_lport *efc, struct efc_sli_port_s *sport);
int
efct_scsi_validate_initiator(struct efc_lport *efc, struct efc_node_s *node);
int
efct_scsi_new_initiator(struct efc_lport *efc, struct efc_node_s *node);

enum efct_scsi_del_initiator_reason_e {
	EFCT_SCSI_INITIATOR_DELETED,
	EFCT_SCSI_INITIATOR_MISSING,
};

extern int
efct_scsi_del_initiator(struct efc_lport *efc, struct efc_node_s *node,
			int reason);
extern int
efct_scsi_recv_cmd(struct efct_io_s *io, uint64_t lun, u8 *cdb,
		   u32 cdb_len, u32 flags);
extern int
efct_scsi_recv_cmd_first_burst(struct efct_io_s *io, uint64_t lun,
			       u8 *cdb, u32 cdb_len, u32 flags,
	struct efc_dma_s first_burst_buffers[], u32 first_burst_bytes);
extern int
efct_scsi_recv_tmf(struct efct_io_s *tmfio, u32 lun,
		   enum efct_scsi_tmf_cmd_e cmd, struct efct_io_s *abortio,
		  u32 flags);
extern struct efc_sli_port_s *
efct_sport_get_instance(struct efc_domain_s *domain, u32 index);
extern struct efc_domain_s *
efct_domain_get_instance(struct efct_s *efct, u32 index);

/* Calls from target-server to base driver */

extern int
efct_scsi_send_rd_data(struct efct_io_s *io, u32 flags,
		       struct efct_scsi_dif_info_s *dif_info,
		      struct efct_scsi_sgl_s *sgl, u32 sgl_count,
		      u64 wire_len, efct_scsi_io_cb_t cb, void *arg);
extern int
efct_scsi_recv_wr_data(struct efct_io_s *io, u32 flags,
		       struct efct_scsi_dif_info_s *dif_info,
		      struct efct_scsi_sgl_s *sgl, u32 sgl_count,
		      u64 wire_len, efct_scsi_io_cb_t cb, void *arg);
extern int
efct_scsi_send_resp(struct efct_io_s *io, u32 flags,
		    struct efct_scsi_cmd_resp_s *rsp, efct_scsi_io_cb_t cb,
		   void *arg);
extern int
efct_scsi_send_tmf_resp(struct efct_io_s *io,
			enum efct_scsi_tmf_resp_e rspcode,
		       u8 addl_rsp_info[3],
		       efct_scsi_io_cb_t cb, void *arg);
extern int
efct_scsi_tgt_abort_io(struct efct_io_s *io, efct_scsi_io_cb_t cb, void *arg);

void efct_scsi_io_complete(struct efct_io_s *io);

extern u32
efct_scsi_get_property(struct efct_s *efct, enum efct_scsi_property_e prop);

//extern void efct_scsi_del_initiator_complete(struct efc_node_s *node);

extern void
efct_scsi_update_first_burst_transferred(struct efct_io_s *io,
					 u32 transferred);

/* Calls from base driver to initiator-client */

int efct_scsi_ini_driver_init(void);
int efct_scsi_ini_driver_exit(void);
int efct_scsi_reg_fc_transport(void);
int efct_scsi_release_fc_transport(void);
int efct_scsi_ini_io_init(struct efct_io_s *io);
int efct_scsi_ini_io_exit(struct efct_io_s *io);
int efct_scsi_new_device(struct efct_s *efct);
int efct_scsi_del_device(struct efct_s *efct);
int
efct_scsi_ini_new_domain(struct efc_lport *efc, struct efc_domain_s *domain);
void
efct_scsi_ini_del_domain(struct efc_lport *efc, struct efc_domain_s *domain);
int
efct_scsi_ini_new_sport(struct efc_lport *efc, struct efc_sli_port_s *sport);
void
efct_scsi_ini_del_sport(struct efc_lport *efc, struct efc_sli_port_s *sport);
int
efct_scsi_new_target(struct efc_lport *efc, struct efc_node_s *node);
void _efct_scsi_io_free(struct kref *arg);

enum efct_scsi_del_target_reason_e {
	EFCT_SCSI_TARGET_DELETED,
	EFCT_SCSI_TARGET_MISSING,
};

int efct_scsi_del_target(struct efc_lport *efc,
			 struct efc_node_s *node, int reason);

/* Calls from the initiator-client to the base driver */

int efct_scsi_send_rd_io(struct efc_node_s *node,
			 struct efct_io_s *io,
			 u32 lun, void *cdb, u32 cdb_len,
			 struct efct_scsi_dif_info_s *dif_info,
			 struct efct_scsi_sgl_s *sgl,
			 u32 sgl_count, u32 wire_len,
			 efct_scsi_rsp_io_cb_t cb, void *arg);

int efct_scsi_send_wr_io(struct efc_node_s *node,
			 struct efct_io_s *io,
			 u32 lun, void *cdb, u32 cdb_len,
			 struct efct_scsi_dif_info_s *dif_info,
			 struct efct_scsi_sgl_s *sgl,
			 u32 sgl_count, u32 wire_len,
			 efct_scsi_rsp_io_cb_t cb, void *arg);

int
efct_scsi_send_wr_io_first_burst(struct efc_node_s *node,
				 struct efct_io_s *io,
				 u32 lun, void *cdb, u32 cdb_len,
				 struct efct_scsi_dif_info_s *dif_info,
				 struct efct_scsi_sgl_s *sgl, u32 sgl_count,
				 u32 wire_len, u32 first_burst,
				 efct_scsi_rsp_io_cb_t cb, void *arg);

int efct_scsi_send_tmf(struct efc_node_s *node,
		       struct efct_io_s *io,
		       struct efct_io_s *io_to_abort, u32 lun,
		       enum efct_scsi_tmf_cmd_e tmf,
		       struct efct_scsi_sgl_s *sgl,
		       u32 sgl_count, u32 len,
		       efct_scsi_rsp_io_cb_t cb, void *arg);

int efct_scsi_send_nodata_io(struct efc_node_s *node,
			     struct efct_io_s *io, u32 lun, void *cdb,
			     u32 cdb_len,
			     efct_scsi_rsp_io_cb_t cb, void *arg);

struct efct_scsi_vaddr_len_s {
	void *vaddr;
	u32 length;
};

extern int
efct_scsi_get_block_vaddr(struct efct_io_s *io, uint64_t blocknumber,
			  struct efct_scsi_vaddr_len_s addrlen[],
			  u32 max_addrlen, void **dif_vaddr);
extern int
efct_scsi_del_vport(struct efct_s *efct, struct Scsi_Host *shost);

extern struct efct_vport_s *
efct_scsi_new_vport(struct efct_s *efct, struct device *dev);

/* Calls within base driver */
int efct_scsi_io_dispatch(struct efct_io_s *io, void *cb);
int efct_scsi_io_dispatch_abort(struct efct_io_s *io, void *cb);
void efct_scsi_check_pending(struct efct_s *efct);

#endif /* __EFCT_SCSI_H__ */
