/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#include "efct_driver.h"
#include "efct_hw.h"
#include "efct_hw_queues.h"
#include "efct_unsol.h"

#define EFCT_HW_MQ_DEPTH	128
#define EFCT_HW_READ_FCF_SIZE	4096
#define EFCT_HW_DEFAULT_AUTO_XFER_RDY_IOS	256
#define EFCT_HW_WQ_TIMER_PERIOD_MS	500

/* values used for setting the auto xfer rdy parameters */
#define EFCT_HW_AUTO_XFER_RDY_BLK_SIZE_DEFAULT		0 /* 512 bytes */
#define EFCT_HW_AUTO_XFER_RDY_REF_TAG_IS_LBA_DEFAULT	true
#define EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALID_DEFAULT	false
#define EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALUE_DEFAULT	0
#define EFCT_HW_REQUE_XRI_REGTAG			65534
/* max command and response buffer lengths -- arbitrary at the moment */
#define EFCT_HW_DMTF_CLP_CMD_MAX	256
#define EFCT_HW_DMTF_CLP_RSP_MAX	256

/* HW global data */
struct efct_hw_global_s hw_global;

static void
efct_hw_queue_hash_add(struct efct_queue_hash_s *, u16, u16);
static void
efct_hw_adjust_wqs(struct efct_hw_s *hw);
static u32
efct_hw_get_num_chutes(struct efct_hw_s *hw);
static int
efct_hw_cb_link(void *, void *);
static int
efct_hw_command_process(struct efct_hw_s *, int, u8 *, size_t);
static int
efct_hw_mq_process(struct efct_hw_s *, int, struct sli4_queue_s *);
static int
efct_hw_cb_read_fcf(struct efct_hw_s *, int, u8 *, void *);
static int
efct_hw_cb_node_attach(struct efct_hw_s *, int, u8 *, void *);
static int
efct_hw_cb_node_free(struct efct_hw_s *, int, u8 *, void *);
static int
efct_hw_cb_node_free_all(struct efct_hw_s *, int, u8 *, void *);
static enum efct_hw_rtn_e
efct_hw_setup_io(struct efct_hw_s *);
static enum efct_hw_rtn_e
efct_hw_init_io(struct efct_hw_s *);
static int
efct_hw_flush(struct efct_hw_s *);
static int
efct_hw_command_cancel(struct efct_hw_s *);
static int
efct_hw_io_cancel(struct efct_hw_s *);
static void
efct_hw_io_quarantine(struct efct_hw_s *hw, struct hw_wq_s *wq,
		      struct efct_hw_io_s *io);
static void
efct_hw_io_restore_sgl(struct efct_hw_s *, struct efct_hw_io_s *);
static int
efct_hw_io_ini_sge(struct efct_hw_s *, struct efct_hw_io_s *,
		   struct efc_dma_s *, u32,
		   struct efc_dma_s *);
static enum efct_hw_rtn_e
efct_hw_firmware_write_sli4_intf_2(struct efct_hw_s *hw, struct efc_dma_s *dma,
				   u32 size, u32 offset, int last,
			void (*cb)(int status, u32 bytes_written,
				   u32 change_status, void *arg),
			void *arg);
static int
efct_hw_cb_fw_write(struct efct_hw_s *, int, u8 *, void  *);
static int
efct_hw_cb_sfp(struct efct_hw_s *, int, u8 *, void  *);
static int
efct_hw_cb_temp(struct efct_hw_s *, int, u8 *, void  *);
static int
efct_hw_cb_link_stat(struct efct_hw_s *, int, u8 *, void  *);
static int
efct_hw_cb_host_stat(struct efct_hw_s *hw, int status,
		     u8 *mqe, void  *arg);
static void
efct_hw_dmtf_clp_cb(struct efct_hw_s *hw, int status,
		    u8 *mqe, void  *arg);
static int
efct_hw_clp_resp_get_value(struct efct_hw_s *hw, const char *keyword,
			   char *value, u32 value_len, const char *resp,
			   u32 resp_len);
typedef void (*efct_hw_dmtf_clp_cb_t)(struct efct_hw_s *hw, int status,
	      u32 result_len, void *arg);
static enum efct_hw_rtn_e
efct_hw_exec_dmtf_clp_cmd(struct efct_hw_s *hw, struct efc_dma_s *dma_cmd,
			  struct efc_dma_s *dma_resp, u32 opts,
			  efct_hw_dmtf_clp_cb_t cb, void *arg);
static void
efct_hw_linkcfg_dmtf_clp_cb(struct efct_hw_s *hw, int status,
			    u32 result_len, void *arg);

static int
__efct_read_topology_cb(struct efct_hw_s *, int, u8 *, void *);
static enum efct_hw_rtn_e
efct_hw_get_linkcfg(struct efct_hw_s *, u32,
		    void (*efct_hw_port_control_cb_t)
		    (int status, uintptr_t value, void *arg), void *);
static enum efct_hw_rtn_e
efct_hw_get_linkcfg_sli4_intf_2(struct efct_hw_s *, u32,
				void (*efct_hw_port_control_cb_t)
				(int status, uintptr_t value, void *arg),
				void *);
static enum efct_hw_rtn_e
efct_hw_set_linkcfg(struct efct_hw_s *, enum efct_hw_linkcfg_e, u32,
		    void (*efct_hw_port_control_cb_t)
		    (int status, uintptr_t value, void *arg), void *);
static enum efct_hw_rtn_e
efct_hw_set_linkcfg_sli4_intf_2(struct efct_hw_s *, enum efct_hw_linkcfg_e,
				u32,
			void (*efct_hw_port_control_cb_t)(int status,
							  uintptr_t value,
							  void *arg),
			void *);
static void
efct_hw_init_linkcfg_cb(int status, uintptr_t value, void *arg);
static enum efct_hw_rtn_e
efct_hw_set_dif_seed(struct efct_hw_s *hw);
static enum efct_hw_rtn_e
efct_hw_config_set_fdt_xfer_hint(struct efct_hw_s *hw, u32 fdt_xfer_hint);
static void
efct_hw_wq_process_abort(void *arg, u8 *cqe, int status);
static int
efct_hw_config_mrq(struct efct_hw_s *hw, u8, u16);
static enum efct_hw_rtn_e
efct_hw_config_watchdog_timer(struct efct_hw_s *hw);
static enum efct_hw_rtn_e
efct_hw_config_sli_port_health_check(struct efct_hw_s *hw, u8 query,
				     u8 enable);

/* HW domain database operations */
static int
efct_hw_domain_add(struct efct_hw_s *, struct efc_domain_s *);
static int
efct_hw_domain_del(struct efct_hw_s *, struct efc_domain_s *);

static void
efct_hw_port_alloc_read_sparm64(struct efc_sli_port_s *sport, void *data);
static void
efct_hw_port_alloc_init_vpi(struct efc_sli_port_s *sport, void *data);
static void
efct_hw_port_attach_reg_vpi(struct efc_sli_port_s *sport, void *data);
static void
efct_hw_port_free_unreg_vpi(struct efc_sli_port_s *sport, void *data);

static void
efct_hw_domain_alloc_init_vfi(struct efc_domain_s *domain, void *data);
static void
efct_hw_domain_attach_reg_vfi(struct efc_domain_s *domain, void *data);
static void
efct_hw_domain_free_unreg_vfi(struct efc_domain_s *domain, void *data);

static void
efct_hw_check_sec_hio_list(struct efct_hw_s *hw);

/* WQE timeouts */
static void
target_wqe_timer_cb(struct timer_list *);
static void
shutdown_target_wqe_timer(struct efct_hw_s *hw);

static inline void
efct_hw_add_io_timed_wqe(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	unsigned long flags = 0;

	if (hw->config.emulate_tgt_wqe_timeout && io->tgt_wqe_timeout) {
		/*
		 * Active WQE list currently only used for
		 * target WQE timeouts.
		 */
		spin_lock_irqsave(&hw->io_lock, flags);
		INIT_LIST_HEAD(&io->wqe_link);
		list_add_tail(&io->wqe_link, &hw->io_timed_wqe);
		io->submit_ticks = jiffies_64;
		spin_unlock_irqrestore(&hw->io_lock, flags);
	}
}

static inline void
efct_hw_remove_io_timed_wqe(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	unsigned long flags = 0;

	if (hw->config.emulate_tgt_wqe_timeout) {
		/*
		 * If target wqe timeouts are enabled,
		 * remove from active wqe list.
		 */
		spin_lock_irqsave(&hw->io_lock, flags);
		if (io->wqe_link.next)
			list_del(&io->wqe_link);
		spin_unlock_irqrestore(&hw->io_lock, flags);
	}
}

static u8 efct_hw_iotype_is_originator(u16 io_type)
{
	switch (io_type) {
	case EFCT_HW_IO_INITIATOR_READ:
	case EFCT_HW_IO_INITIATOR_WRITE:
	case EFCT_HW_IO_INITIATOR_NODATA:
	case EFCT_HW_FC_CT:
	case EFCT_HW_ELS_REQ:
		return 1;
	default:
		return 0;
	}
}

static bool efct_hw_wcqe_abort_needed(u16 status, u8 ext,
				      u8 xb)
{
	/* if exchange not active, nothing to abort */
	if (!xb)
		return false;
	if (status == SLI4_FC_WCQE_STATUS_LOCAL_REJECT) {
		switch (ext) {
		/* exceptions where abort is not needed */
		/*  FW returns this after unreg_rpi */
		case SLI4_FC_LOCAL_REJECT_INVALID_RPI:
		/* abort already in progress */
		case SLI4_FC_LOCAL_REJECT_ABORT_REQUESTED:
			return false;
		default:
			break;
		}
	}
	return true;
}

/**
 * @brief Determine the number of chutes on the device.
 *
 * @par Description
 * Some devices require queue resources allocated per protocol processor
 * (chute). This function returns the number of chutes on this device.
 *
 * @param hw Hardware context allocated by the caller.
 *
 * @return Returns the number of chutes on the device for protocol.
 */
static u32
efct_hw_get_num_chutes(struct efct_hw_s *hw)
{
	u32 num_chutes = 1;
	struct sli4_s *sli = &hw->sli;

	if (sli->dual_ulp_capable &&
	    sli->is_ulp_fc[0] &&
	    sli->is_ulp_fc[1])
		num_chutes = 2;
	return num_chutes;
}

static enum efct_hw_rtn_e
efct_hw_link_event_init(struct efct_hw_s *hw)
{
	if (!hw) {
		efct_log_err(hw->os, "bad parameter hw=%p\n", hw);
		return EFCT_HW_RTN_ERROR;
	}

	hw->link.status = SLI_LINK_STATUS_MAX;
	hw->link.topology = SLI_LINK_TOPO_NONE;
	hw->link.medium = SLI_LINK_MEDIUM_MAX;
	hw->link.speed = 0;
	hw->link.loop_map = NULL;
	hw->link.fc_id = U32_MAX;

	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup devInitShutdown
 * @brief If this is physical port 0, then read the max dump size.
 *
 * @par Description
 * Queries the FW for the maximum dump size
 *
 * @param hw Hardware context allocated by the caller.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static enum efct_hw_rtn_e
efct_hw_read_max_dump_size(struct efct_hw_s *hw)
{
	u8	buf[SLI4_BMBX_SIZE];
	u8 func;
	struct efct_s *efct = hw->os;
	int	rc = 0;

	/*
	 * Make sure the FW is new enough to support this command. If the FW
	 * is too old, the FW will UE.
	 */
	if (hw->workaround.disable_dump_loc) {
		efct_log_test(hw->os,
			      "FW version is too old for this feature\n");
		return EFCT_HW_RTN_ERROR;
	}

	/* attempt to detemine the dump size for function 0 only. */
	func = PCI_FUNC(efct->pdev->devfn);
	if (func == 0) {
		if (sli_cmd_common_set_dump_location(&hw->sli, buf,
						     SLI4_BMBX_SIZE, 1, 0,
						     NULL, 0)) {
			struct sli4_res_common_set_dump_location_s *rsp =
				(struct sli4_res_common_set_dump_location_s *)
				(buf + offsetof(struct sli4_cmd_sli_config_s,
						payload.embed));

			rc = efct_hw_command(hw, buf, EFCT_CMD_POLL, NULL,
					     NULL);
			if (rc != EFCT_HW_RTN_SUCCESS) {
				efct_log_test(hw->os,
					      "set dump location cmd failed\n");
				return rc;
			}
			hw->dump_size =
				(le32_to_cpu(rsp->buffer_length_dword) &
				 SLI4_RES_SET_DUMP_BUFFER_LEN);
			efct_log_debug(hw->os, "Dump size %x\n",
				       hw->dump_size);
		}
	}
	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup devInitShutdown
 * @brief Set up the Hardware Abstraction Layer module.
 *
 * @par Description
 * Calls set up to configure the hardware.
 *
 * @param hw Hardware context allocated by the caller.
 * @param os Device abstraction.
 * @param port_type Protocol type of port, such as FC and NIC.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_setup(struct efct_hw_s *hw, void *os, struct pci_dev *pdev)
{
	u32 i;
	struct sli4_s *sli = &hw->sli;

	if (!hw) {
		efct_log_err(os, "bad parameter(s) hw=%p\n", hw);
		return EFCT_HW_RTN_ERROR;
	}

	if (hw->hw_setup_called) {
		/* Setup run-time workarounds.
		 * Call for each setup, to allow for hw_war_version
		 */
		efct_hw_workaround_setup(hw);
		return EFCT_HW_RTN_SUCCESS;
	}

	/*
	 * efct_hw_init() relies on NULL pointers indicating that a structure
	 * needs allocation. If a structure is non-NULL, efct_hw_init() won't
	 * free/realloc that memory
	 */
	memset(hw, 0, sizeof(struct efct_hw_s));

	hw->hw_setup_called = true;

	hw->os = os;

	spin_lock_init(&hw->cmd_lock);
	INIT_LIST_HEAD(&hw->cmd_head);
	INIT_LIST_HEAD(&hw->cmd_pending);
	hw->cmd_head_count = 0;

	spin_lock_init(&hw->io_lock);
	spin_lock_init(&hw->io_abort_lock);

	atomic_set(&hw->io_alloc_failed_count, 0);

	hw->config.speed = FC_LINK_SPEED_AUTO_16_8_4;
	hw->config.dif_seed = 0;
	hw->config.auto_xfer_rdy_blk_size_chip =
			EFCT_HW_AUTO_XFER_RDY_BLK_SIZE_DEFAULT;
	hw->config.auto_xfer_rdy_ref_tag_is_lba =
			EFCT_HW_AUTO_XFER_RDY_REF_TAG_IS_LBA_DEFAULT;
	hw->config.auto_xfer_rdy_app_tag_valid =
			EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALID_DEFAULT;
	hw->config.auto_xfer_rdy_app_tag_value =
			EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALUE_DEFAULT;

	if (sli_setup(&hw->sli, hw->os, pdev, ((struct efct_os_s *)os)->reg)) {
		efct_log_err(hw->os, "SLI setup failed\n");
		return EFCT_HW_RTN_ERROR;
	}

	memset(hw->domains, 0, sizeof(hw->domains));

	memset(hw->fcf_index_fcfi, 0, sizeof(hw->fcf_index_fcfi));

	efct_hw_link_event_init(hw);

	sli_callback(&hw->sli, SLI4_CB_LINK, efct_hw_cb_link, hw);

	/*
	 * Set all the queue sizes to the maximum allowed. These values may
	 * be changes later by the adjust and workaround functions.
	 */
	for (i = 0; i < ARRAY_SIZE(hw->num_qentries); i++)
		hw->num_qentries[i] = hw->sli.max_qentries[i];

	/*
	 * The RQ assignment for RQ pair mode.
	 */
	hw->config.rq_default_buffer_size = EFCT_HW_RQ_SIZE_PAYLOAD;
	hw->config.n_io = hw->sli.extent[SLI_RSRC_XRI].size;

	/* by default, enable initiator-only auto-ABTS emulation */
	hw->config.i_only_aab = true;

	/* Setup run-time workarounds */
	efct_hw_workaround_setup(hw);

	/* HW_WORKAROUND_OVERRIDE_FCFI_IN_SRB */
	if (hw->workaround.override_fcfi)
		hw->first_domain_idx = -1;

	/* Must be done after the workaround setup */
	(void)efct_hw_read_max_dump_size(hw);

	/* calculate the number of WQs required. */
	efct_hw_adjust_wqs(hw);

	/* Set the default dif mode */
	if (!(sli->features & SLI4_REQFEAT_DIF &&
	      sli->t10_dif_inline_capable)) {
		efct_log_test(hw->os,
			      "not inline capable, setting mode to separate\n");
		hw->config.dif_mode = EFCT_HW_DIF_MODE_SEPARATE;
	}
	if (hw->workaround.use_dif_sec_xri)
		INIT_LIST_HEAD(&hw->sec_hio_wait_list);

	/*
	 * Figure out the starting and max ULP to spread the WQs across the
	 * ULPs.
	 */
	if (sli->dual_ulp_capable) {
		if (sli->is_ulp_fc[0] &&
		    sli->is_ulp_fc[1]) {
			hw->ulp_start = 0;
			hw->ulp_max   = 1;
		} else if (sli->is_ulp_fc[0]) {
			hw->ulp_start = 0;
			hw->ulp_max   = 0;
		} else {
			hw->ulp_start = 1;
			hw->ulp_max   = 1;
		}
	} else {
		if (sli->is_ulp_fc[0]) {
			hw->ulp_start = 0;
			hw->ulp_max   = 0;
		} else {
			hw->ulp_start = 1;
			hw->ulp_max   = 1;
		}
	}
	efct_log_debug(hw->os, "ulp_start %d, ulp_max %d\n",
		       hw->ulp_start, hw->ulp_max);
	hw->config.queue_topology = hw_global.queue_topology_string;

	hw->qtop = efct_hw_qtop_parse(hw, hw->config.queue_topology);
	if (!hw->qtop) {
		efct_log_crit(hw->os, "Queue topology string is invalid\n");
		return EFCT_HW_RTN_ERROR;
	}

	hw->config.n_eq = hw->qtop->entry_counts[QTOP_EQ];
	hw->config.n_cq = hw->qtop->entry_counts[QTOP_CQ];
	hw->config.n_rq = hw->qtop->entry_counts[QTOP_RQ];
	hw->config.n_wq = hw->qtop->entry_counts[QTOP_WQ];
	hw->config.n_mq = hw->qtop->entry_counts[QTOP_MQ];

	return EFCT_HW_RTN_SUCCESS;
}

static void
efct_logfcfi(struct efct_hw_s *hw, u32 j, u32 i, u32 id)
{
	efct_log_info(hw->os,
		      "REG_FCFI: filter[%d] %08X -> RQ[%d] id=%d\n",
		     j, hw->config.filter_def[j], i, id);
}

/**
 * @ingroup devInitShutdown
 * @brief Allocate memory structures to prepare for the device operation.
 *
 * @par Description
 * Allocates memory structures needed by the device and prepares the device
 * for operation.
 * @n @n @b Note: This function may be called more than once (for example, at
 * initialization and then after a reset), but the size of the internal
 * resources may not be changed without tearing down the HW
 * (efct_hw_teardown()).
 *
 * @param hw Hardware context allocated by the caller.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_init(struct efct_hw_s *hw)
{
	enum efct_hw_rtn_e	rc;
	u32	i = 0;
	u8		buf[SLI4_BMBX_SIZE];
	u32	max_rpi;
	int		rem_count;
	u32	count;
	unsigned long flags = 0;
	struct efct_hw_io_s *temp;
	struct sli4_cmd_rq_cfg_s rq_cfg[SLI4_CMD_REG_FCFI_NUM_RQ_CFG];
	struct sli4_s *sli = &hw->sli;
	struct efct_s *efct = hw->os;

	/*
	 * Make sure the command lists are empty. If this is start-of-day,
	 * they'll be empty since they were just initialized in efct_hw_setup.
	 * If we've just gone through a reset, the command and command pending
	 * lists should have been cleaned up as part of the reset
	 * (efct_hw_reset()).
	 */
	spin_lock_irqsave(&hw->cmd_lock, flags);
		if (!list_empty(&hw->cmd_head)) {
			efct_log_test(hw->os, "command found on cmd list\n");
			spin_unlock_irqrestore(&hw->cmd_lock, flags);
			return EFCT_HW_RTN_ERROR;
		}
		if (!list_empty(&hw->cmd_pending)) {
			efct_log_test(hw->os,
				      "command found on pending list\n");
			spin_unlock_irqrestore(&hw->cmd_lock, flags);
			return EFCT_HW_RTN_ERROR;
		}
	spin_unlock_irqrestore(&hw->cmd_lock, flags);

	/* Free RQ buffers if prevously allocated */
	efct_hw_rx_free(hw);

	/*
	 * The IO queues must be initialized here for the reset case. The
	 * efct_hw_init_io() function will re-add the IOs to the free list.
	 * The cmd_head list should be OK since we free all entries in
	 * efct_hw_command_cancel() that is called in the efct_hw_reset().
	 */

	/* If we are in this function due to a reset, there may be stale items
	 * on lists that need to be removed.  Clean them up.
	 */
	rem_count = 0;
	if (hw->io_wait_free.next) {
		while ((!list_empty(&hw->io_wait_free))) {
			rem_count++;
			temp = list_first_entry(&hw->io_wait_free,
						struct efct_hw_io_s,
						list_entry);
			list_del(&temp->list_entry);
		}
		if (rem_count > 0) {
			efct_log_debug(hw->os,
				       "rmvd %d items from io_wait_free list\n",
				rem_count);
		}
	}
	rem_count = 0;
	if (hw->io_inuse.next) {
		while ((!list_empty(&hw->io_inuse))) {
			rem_count++;
			temp = list_first_entry(&hw->io_inuse,
						struct efct_hw_io_s,
						list_entry);
			list_del(&temp->list_entry);
		}
		if (rem_count > 0)
			efct_log_debug(hw->os,
				       "rmvd %d items from io_inuse list\n",
				       rem_count);
	}
	rem_count = 0;
	if (hw->io_free.next) {
		while ((!list_empty(&hw->io_free))) {
			rem_count++;
			temp = list_first_entry(&hw->io_free,
						struct efct_hw_io_s,
						list_entry);
			list_del(&temp->list_entry);
		}
		if (rem_count > 0)
			efct_log_debug(hw->os,
				       "rmvd %d items from io_free list\n",
				       rem_count);
	}
	if (hw->io_port_owned.next) {
		while ((!list_empty(&hw->io_port_owned))) {
			temp = list_first_entry(&hw->io_port_owned,
						struct efct_hw_io_s,
						list_entry);
			list_del(&temp->list_entry);
		}
	}

	INIT_LIST_HEAD(&hw->io_inuse);
	INIT_LIST_HEAD(&hw->io_free);
	INIT_LIST_HEAD(&hw->io_port_owned);
	INIT_LIST_HEAD(&hw->io_wait_free);
	INIT_LIST_HEAD(&hw->io_timed_wqe);
	INIT_LIST_HEAD(&hw->io_port_dnrx);

	/* If MRQ not required, Make sure we dont request feature. */
	if (hw->config.n_rq == 1)
		hw->sli.features &= (~SLI4_REQFEAT_MRQP);

	if (sli_init(&hw->sli)) {
		efct_log_err(hw->os, "SLI failed to initialize\n");
		return EFCT_HW_RTN_ERROR;
	}
	/*
	 * Enable the auto xfer rdy feature if requested.
	 */
	hw->auto_xfer_rdy_enabled = false;

	if (hw->sliport_healthcheck) {
		rc = efct_hw_config_sli_port_health_check(hw, 0, 1);
		if (rc != EFCT_HW_RTN_SUCCESS) {
			efct_log_err(hw->os, "Enable port Health check fail\n");
			return rc;
		}
	}

	/*
	 * Set FDT transfer hint, only works on Lancer
	 */
	if (hw->sli.if_type == SLI4_INTF_IF_TYPE_2 &&
	    EFCT_HW_FDT_XFER_HINT != 0) {
		/*
		 * Non-fatal error. In particular, we can disregard failure to
		 * set EFCT_HW_FDT_XFER_HINT on devices with legacy firmware
		 * that do not support EFCT_HW_FDT_XFER_HINT feature.
		 */
		efct_hw_config_set_fdt_xfer_hint(hw, EFCT_HW_FDT_XFER_HINT);
	}

	/*
	 * Verify that we have not exceeded any queue sizes
	 */
	if (hw->config.n_eq > sli->max_qcount[SLI_QTYPE_EQ]) {
		efct_log_err(hw->os, "requested %d EQ but %d allowed\n",
			     hw->config.n_eq,
			sli->max_qcount[SLI_QTYPE_EQ]);
		return EFCT_HW_RTN_ERROR;
	}
	if (hw->config.n_cq > sli->max_qcount[SLI_QTYPE_CQ]) {
		efct_log_err(hw->os, "requested %d CQ but %d allowed\n",
			     hw->config.n_cq,
			sli->max_qcount[SLI_QTYPE_CQ]);
		return EFCT_HW_RTN_ERROR;
	}
	if (hw->config.n_mq > sli->max_qcount[SLI_QTYPE_MQ]) {
		efct_log_err(hw->os, "requested %d MQ but %d allowed\n",
			     hw->config.n_mq,
			sli->max_qcount[SLI_QTYPE_MQ]);
		return EFCT_HW_RTN_ERROR;
	}
	if (hw->config.n_rq > sli->max_qcount[SLI_QTYPE_RQ]) {
		efct_log_err(hw->os, "requested %d RQ but %d allowed\n",
			     hw->config.n_rq,
			sli->max_qcount[SLI_QTYPE_RQ]);
		return EFCT_HW_RTN_ERROR;
	}
	if (hw->config.n_wq > sli->max_qcount[SLI_QTYPE_WQ]) {
		efct_log_err(hw->os, "requested %d WQ but %d allowed\n",
			     hw->config.n_wq,
			sli->max_qcount[SLI_QTYPE_WQ]);
		return EFCT_HW_RTN_ERROR;
	}

	/* zero the hashes */
	memset(hw->cq_hash, 0, sizeof(hw->cq_hash));
	efct_log_debug(hw->os, "Max CQs %d, hash size = %d\n",
		       EFCT_HW_MAX_NUM_CQ, EFCT_HW_Q_HASH_SIZE);

	memset(hw->rq_hash, 0, sizeof(hw->rq_hash));
	efct_log_debug(hw->os, "Max RQs %d, hash size = %d\n",
		       EFCT_HW_MAX_NUM_RQ, EFCT_HW_Q_HASH_SIZE);

	memset(hw->wq_hash, 0, sizeof(hw->wq_hash));
	efct_log_debug(hw->os, "Max WQs %d, hash size = %d\n",
		       EFCT_HW_MAX_NUM_WQ, EFCT_HW_Q_HASH_SIZE);

	rc = efct_hw_init_queues(hw, hw->qtop);
	if (rc != EFCT_HW_RTN_SUCCESS)
		return rc;

	max_rpi = sli->extent[SLI_RSRC_RPI].size;
	i = sli_fc_get_rpi_requirements(&hw->sli, max_rpi);
	if (i) {
		struct efc_dma_s payload_memory;

		rc = EFCT_HW_RTN_ERROR;

		if (hw->rnode_mem.size) {
			dma_free_coherent(&efct->pdev->dev,
					  hw->rnode_mem.size,
					  hw->rnode_mem.virt,
					  hw->rnode_mem.phys);
			memset(&hw->rnode_mem, 0, sizeof(struct efc_dma_s));
		}

		hw->rnode_mem.size = i;
		hw->rnode_mem.virt = dma_alloc_coherent(&efct->pdev->dev,
							hw->rnode_mem.size,
							&hw->rnode_mem.phys,
							GFP_DMA);
		if (!hw->rnode_mem.virt) {
			efct_log_err(hw->os,
				     "remote node memory allocation fail\n");
			return EFCT_HW_RTN_NO_MEMORY;
		}

		payload_memory.size = 0;
		if (sli_cmd_post_hdr_templates(&hw->sli, buf,
					       SLI4_BMBX_SIZE,
						    &hw->rnode_mem,
						    U16_MAX,
						    &payload_memory)) {
			rc = efct_hw_command(hw, buf, EFCT_CMD_POLL, NULL,
					     NULL);

			if (payload_memory.size != 0) {
				/*
				 * The command was non-embedded - need to
				 * free the dma buffer
				 */
				dma_free_coherent(&efct->pdev->dev,
						  payload_memory.size,
						  payload_memory.virt,
						  payload_memory.phys);
				memset(&payload_memory, 0,
				       sizeof(struct efc_dma_s));
			}
		}

		if (rc != EFCT_HW_RTN_SUCCESS) {
			efct_log_err(hw->os,
				     "header template registration failed\n");
			return rc;
		}
	}

	/* Allocate and post RQ buffers */
	rc = efct_hw_rx_allocate(hw);
	if (rc) {
		efct_log_err(hw->os, "rx_allocate failed\n");
		return rc;
	}

	/* Populate hw->seq_free_list */
	if (!hw->seq_pool) {
		u32 count = 0;
		u32 i;

		/*
		 * Sum up the total number of RQ entries, to use to allocate
		 * the sequence object pool
		 */
		for (i = 0; i < hw->hw_rq_count; i++)
			count += hw->hw_rq[i]->entry_count;

		hw->seq_pool = efct_array_alloc(hw->os,
					sizeof(struct efc_hw_sequence_s),
						count);
		if (!hw->seq_pool) {
			efct_log_err(hw->os, "malloc seq_pool failed\n");
			return EFCT_HW_RTN_NO_MEMORY;
		}
	}

	if (efct_hw_rx_post(hw))
		efct_log_err(hw->os, "WARNING - error posting RQ buffers\n");

	/* Allocate rpi_ref if not previously allocated */
	if (!hw->rpi_ref) {
		hw->rpi_ref = kmalloc(max_rpi * sizeof(*hw->rpi_ref),
				      GFP_KERNEL);
		if (!hw->rpi_ref)
			return EFCT_HW_RTN_NO_MEMORY;

		memset(hw->rpi_ref, 0, max_rpi * sizeof(*hw->rpi_ref));
	}

	for (i = 0; i < max_rpi; i++) {
		atomic_set(&hw->rpi_ref[i].rpi_count, 0);
		atomic_set(&hw->rpi_ref[i].rpi_attached, 0);
	}

	memset(hw->domains, 0, sizeof(hw->domains));

	/* HW_WORKAROUND_OVERRIDE_FCFI_IN_SRB */
	if (hw->workaround.override_fcfi)
		hw->first_domain_idx = -1;

	memset(hw->fcf_index_fcfi, 0, sizeof(hw->fcf_index_fcfi));

	/*
	 * Register a FCFI to allow unsolicited frames to be routed to the
	 * driver
	 */
	if (hw->hw_mrq_count) {
		efct_log_info(hw->os, "using REG_FCFI MRQ\n");

		rc = efct_hw_config_mrq(hw,
					SLI4_CMD_REG_FCFI_SET_FCFI_MODE,
				0);
		if (rc != EFCT_HW_RTN_SUCCESS) {
			efct_log_err(hw->os,
				     "REG_FCFI_MRQ FCFI reg failed\n");
			return rc;
		}

		rc = efct_hw_config_mrq(hw,
					SLI4_CMD_REG_FCFI_SET_MRQ_MODE,
					0);
		if (rc != EFCT_HW_RTN_SUCCESS) {
			efct_log_err(hw->os,
				     "REG_FCFI_MRQ MRQ reg failed\n");
			return rc;
		}
	} else {
		u32 min_rq_count;

		efct_log_info(hw->os, "using REG_FCFI standard\n");

		/*
		 * Set the filter match/mask values from hw's
		 * filter_def values
		 */
		for (i = 0; i < SLI4_CMD_REG_FCFI_NUM_RQ_CFG; i++) {
			rq_cfg[i].rq_id = 0xffff;
			rq_cfg[i].r_ctl_mask = (u8)
					hw->config.filter_def[i];
			rq_cfg[i].r_ctl_match = (u8)
					(hw->config.filter_def[i] >>
					 8);
			rq_cfg[i].type_mask = (u8)
					 (hw->config.filter_def[i] >>
					  16);
			rq_cfg[i].type_match = (u8)
					 (hw->config.filter_def[i] >>
					  24);
		}

		/*
		 * Update the rq_id's of the FCF configuration
		 * (don't update more than the number of rq_cfg
		 * elements)
		 */
		min_rq_count = (hw->hw_rq_count <
				SLI4_CMD_REG_FCFI_NUM_RQ_CFG)
				? hw->hw_rq_count :
				SLI4_CMD_REG_FCFI_NUM_RQ_CFG;
		for (i = 0; i < min_rq_count; i++) {
			struct hw_rq_s *rq = hw->hw_rq[i];
			u32 j;

			for (j = 0; j < SLI4_CMD_REG_FCFI_NUM_RQ_CFG;
			     j++) {
				u32 mask = (rq->filter_mask != 0) ?
						 rq->filter_mask : 1;

				if (mask & (1U << j)) {
					rq_cfg[j].rq_id = rq->hdr->id;
					efct_logfcfi(hw, j, i,
						     rq->hdr->id);
				}
			}
		}

		rc = EFCT_HW_RTN_ERROR;
		if (sli_cmd_reg_fcfi(&hw->sli, buf,
				     SLI4_BMBX_SIZE, 0,
					  rq_cfg)) {
			rc = efct_hw_command(hw, buf, EFCT_CMD_POLL,
					     NULL, NULL);
		}

		if (rc != EFCT_HW_RTN_SUCCESS) {
			efct_log_err(hw->os,
				     "FCFI registration failed\n");
			return rc;
		}
		hw->fcf_indicator =
		le16_to_cpu(((struct sli4_cmd_reg_fcfi_s *)buf)->fcfi);
	}

	/*
	 * Allocate the WQ request tag pool, if not previously allocated
	 * (the request tag value is 16 bits, thus the pool allocation size
	 * of 64k)
	 */
	rc = efct_hw_reqtag_init(hw);
	if (rc) {
		efct_log_err(hw->os,
			     "efct_pool_alloc struct hw_wq_callback_s failed: %d\n",
			rc);
		return rc;
	}

	rc = efct_hw_setup_io(hw);
	if (rc) {
		efct_log_err(hw->os, "IO allocation failure\n");
		return rc;
	}

	rc = efct_hw_init_io(hw);
	if (rc) {
		efct_log_err(hw->os, "IO initialization failure\n");
		return rc;
	}

	/*
	 * get hw link config; polling, so callback will be called
	 * immediately
	 */
	hw->linkcfg = EFCT_HW_LINKCFG_NA;
	efct_hw_get_linkcfg(hw, EFCT_CMD_POLL, efct_hw_init_linkcfg_cb, hw);

	/* Set the DIF seed - only for lancer right now */
	if (sli->if_type == SLI4_INTF_IF_TYPE_2 &&
	    efct_hw_set_dif_seed(hw) != EFCT_HW_RTN_SUCCESS) {
		efct_log_err(hw->os, "Failed to set DIF seed value\n");
		return rc;
	}

	/*
	 * Arming the EQ allows (e.g.) interrupts when CQ completions write EQ
	 * entries
	 */
	for (i = 0; i < hw->eq_count; i++)
		sli_queue_arm(&hw->sli, &hw->eq[i], true);

	/*
	 * Initialize RQ hash
	 */
	for (i = 0; i < hw->rq_count; i++)
		efct_hw_queue_hash_add(hw->rq_hash, hw->rq[i].id, i);

	/*
	 * Initialize WQ hash
	 */
	for (i = 0; i < hw->wq_count; i++)
		efct_hw_queue_hash_add(hw->wq_hash, hw->wq[i].id, i);

	/*
	 * Arming the CQ allows (e.g.) MQ completions to write CQ entries
	 */
	for (i = 0; i < hw->cq_count; i++) {
		efct_hw_queue_hash_add(hw->cq_hash, hw->cq[i].id, i);
		sli_queue_arm(&hw->sli, &hw->cq[i], true);
	}

	/* record the fact that the queues are functional */
	hw->state = EFCT_HW_STATE_ACTIVE;

	/* Note: Must be after the IOs are setup and the state is active*/
	if (efct_hw_rqpair_init(hw))
		efct_log_err(hw->os, "WARNING - error initializing RQ pair\n");

	/* finally kick off periodic timer to check for timed out target WQEs */
	if (hw->config.emulate_tgt_wqe_timeout) {
		timer_setup(&hw->wqe_timer, &target_wqe_timer_cb, 0);

		mod_timer(&hw->wqe_timer, jiffies +
			  msecs_to_jiffies(EFCT_HW_WQ_TIMER_PERIOD_MS));
	}
	/*
	 * Allocate a HW IOs for send frame.  Allocate one for each Class 1
	 * WQ, or if there are none of those, allocate one for WQ[0]
	 */
	count = efct_varray_get_count(hw->wq_class_array[1]);
	if (count > 0) {
		struct hw_wq_s *wq;

		for (i = 0; i < count; i++) {
			wq = efct_varray_iter_next(hw->wq_class_array[1]);
			wq->send_frame_io = efct_hw_io_alloc(hw);
			if (!wq->send_frame_io)
				efct_log_err(hw->os,
					     "alloc for send_frame_io failed\n");
		}
	} else {
		hw->hw_wq[0]->send_frame_io = efct_hw_io_alloc(hw);
		if (!hw->hw_wq[0]->send_frame_io)
			efct_log_err(hw->os,
				     "alloc for send_frame_io failed\n");
	}

	/* Initialize send frame frame sequence id */
	atomic_set(&hw->send_frame_seq_id, 0);

	/* Initialize watchdog timer if enabled by user */
	if (hw->watchdog_timeout) {
		if (hw->watchdog_timeout < 1 ||
		    hw->watchdog_timeout > 65534)
			efct_log_err(hw->os,
				     "WDT out of range: range is 1 - 65534\n");
		else if (!efct_hw_config_watchdog_timer(hw))
			efct_log_info(hw->os,
				      "WDT timer config with tmo = %d secs\n",
				     hw->watchdog_timeout);
	}

	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @brief Configure Multi-RQ
 *
 * @param hw       Hardware context allocated by the caller.
 * @param mode      1 to set MRQ filters and 0 to set FCFI index
 * @param vlanid    valid in mode 0
 * @param fcf_index valid in mode 0
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
efct_hw_config_mrq(struct efct_hw_s *hw, u8 mode, u16 fcf_index)
{
	u8 buf[SLI4_BMBX_SIZE], mrq_bitmask = 0;
	struct hw_rq_s *rq;
	struct sli4_cmd_reg_fcfi_mrq_s *rsp = NULL;
	u32 i, j;
	struct sli4_cmd_rq_cfg_s rq_filter[SLI4_CMD_REG_FCFI_MRQ_NUM_RQ_CFG];
	int rc;

	if (mode == SLI4_CMD_REG_FCFI_SET_FCFI_MODE)
		goto issue_cmd;

	/* Set the filter match/mask values from hw's filter_def values */
	for (i = 0; i < SLI4_CMD_REG_FCFI_NUM_RQ_CFG; i++) {
		rq_filter[i].rq_id = 0xffff;
		rq_filter[i].r_ctl_mask  = (u8)
					    hw->config.filter_def[i];
		rq_filter[i].r_ctl_match = (u8)
					    (hw->config.filter_def[i] >> 8);
		rq_filter[i].type_mask   = (u8)
					    (hw->config.filter_def[i] >> 16);
		rq_filter[i].type_match  = (u8)
					    (hw->config.filter_def[i] >> 24);
	}

	/* Accumulate counts for each filter type used, build rq_ids[] list */
	for (i = 0; i < hw->hw_rq_count; i++) {
		rq = hw->hw_rq[i];
		for (j = 0; j < SLI4_CMD_REG_FCFI_MRQ_NUM_RQ_CFG; j++) {
			if (rq->filter_mask & (1U << j)) {
				if (rq_filter[j].rq_id != 0xffff) {
					/*
					 * Already used. Bailout ifts not RQset
					 * case
					 */
					if (!rq->is_mrq ||
					    rq_filter[j].rq_id !=
					     rq->base_mrq_id) {
						efct_log_err(hw->os,
							     "Wrong q top.\n");
						return EFCT_HW_RTN_ERROR;
					}
					continue;
				}

				if (rq->is_mrq) {
					rq_filter[j].rq_id = rq->base_mrq_id;
					mrq_bitmask |= (1U << j);
				} else {
					rq_filter[j].rq_id = rq->hdr->id;
				}
			}
		}
	}

issue_cmd:
	/* Invoke REG_FCFI_MRQ */
	rc = sli_cmd_reg_fcfi_mrq(&hw->sli,
				  buf,	/* buf */
				 SLI4_BMBX_SIZE, /* size */
				 mode, /* mode 1 */
				 fcf_index, /* fcf_index */
				 /* RQ selection policy*/
				 hw->config.rq_selection_policy,
				 mrq_bitmask, /* MRQ bitmask */
				 hw->hw_mrq_count, /* num_mrqs */
				 rq_filter);/* RQ filter */
	if (rc == 0) {
		efct_log_err(hw->os,
			     "sli_cmd_reg_fcfi_mrq() failed: %d\n", rc);
		return EFCT_HW_RTN_ERROR;
	}

	rc = efct_hw_command(hw, buf, EFCT_CMD_POLL, NULL, NULL);

	rsp = (struct sli4_cmd_reg_fcfi_mrq_s *)buf;

	if (rc != EFCT_HW_RTN_SUCCESS ||
	    le16_to_cpu(rsp->hdr.status)) {
		efct_log_err(hw->os,
			     "FCFI MRQ reg failed. cmd = %x status = %x\n",
			     rsp->hdr.command,
			     le16_to_cpu(rsp->hdr.status));
		return EFCT_HW_RTN_ERROR;
	}

	if (mode == SLI4_CMD_REG_FCFI_SET_FCFI_MODE)
		hw->fcf_indicator = le16_to_cpu(rsp->fcfi);
	return 0;
}

/**
 * @brief Callback function for getting linkcfg during HW initialization.
 *
 * @param status Status of the linkcfg get operation.
 * @param value Link configuration enum to which the link configuration is set.
 * @param arg Callback argument (struct efct_hw_s *).
 *
 * @return None.
 */
static void
efct_hw_init_linkcfg_cb(int status, uintptr_t value, void *arg)
{
	struct efct_hw_s *hw = (struct efct_hw_s *)arg;

	if (status == 0)
		hw->linkcfg = (enum efct_hw_linkcfg_e)value;
	else
		hw->linkcfg = EFCT_HW_LINKCFG_NA;
	efct_log_debug(hw->os, "linkcfg=%d\n", hw->linkcfg);
}

/**
 * @ingroup devInitShutdown
 * @brief Tear down the Hardware Abstraction Layer module.
 *
 * @par Description
 * Frees memory structures needed by the device, and shuts down the device.
 * Does not free the HW context memory (which is done by the caller).
 *
 * @param hw Hardware context allocated by the caller.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_teardown(struct efct_hw_s *hw)
{
	u32	i = 0;
	u32	iters = 10;/*XXX*/
	u32	max_rpi;
	u32 destroy_queues;
	u32 free_memory;
	struct efc_dma_s *dma;
	struct efct_s *efct = hw->os;

	if (!hw) {
		efct_log_err(NULL, "bad parameter(s) hw=%p\n", hw);
		return EFCT_HW_RTN_ERROR;
	}

	destroy_queues = (hw->state == EFCT_HW_STATE_ACTIVE);
	free_memory = (hw->state != EFCT_HW_STATE_UNINITIALIZED);

	/* shutdown target wqe timer */
	shutdown_target_wqe_timer(hw);

	/* Cancel watchdog timer if enabled */
	if (hw->watchdog_timeout) {
		hw->watchdog_timeout = 0;
		efct_hw_config_watchdog_timer(hw);
	}

	/* Cancel Sliport Healthcheck */
	if (hw->sliport_healthcheck) {
		hw->sliport_healthcheck = 0;
		efct_hw_config_sli_port_health_check(hw, 0, 0);
	}

	if (hw->state != EFCT_HW_STATE_QUEUES_ALLOCATED) {
		hw->state = EFCT_HW_STATE_TEARDOWN_IN_PROGRESS;

		efct_hw_flush(hw);

		/*
		 * If there are outstanding commands, wait for them to complete
		 */
		while (!list_empty(&hw->cmd_head) && iters) {
			mdelay(10);
			efct_hw_flush(hw);
			iters--;
		}

		if (list_empty(&hw->cmd_head))
			efct_log_debug(hw->os,
				       "All commands completed on MQ queue\n");
		else
			efct_log_debug(hw->os,
				       "Some cmds still pending on MQ queue\n");

		/* Cancel any remaining commands */
		efct_hw_command_cancel(hw);
	} else {
		hw->state = EFCT_HW_STATE_TEARDOWN_IN_PROGRESS;
	}

	/* Free unregistered RPI if workaround is in force */
	if (hw->workaround.use_unregistered_rpi)
		sli_resource_free(&hw->sli, SLI_RSRC_RPI,
				  hw->workaround.unregistered_rid);

	max_rpi = hw->sli.max_qcount[SLI_RSRC_RPI];
	if (hw->rpi_ref) {
		for (i = 0; i < max_rpi; i++) {
			u32 count;

			count = atomic_read(&hw->rpi_ref[i].rpi_count);
			if (count)
				efct_log_debug(hw->os,
					       "non-zero ref [%d]=%d\n",
					       i, count);
		}
		kfree(hw->rpi_ref);
		hw->rpi_ref = NULL;
	}

	dma_free_coherent(&efct->pdev->dev,
			  hw->rnode_mem.size, hw->rnode_mem.virt,
			  hw->rnode_mem.phys);
	memset(&hw->rnode_mem, 0, sizeof(struct efc_dma_s));

	if (hw->io) {
		for (i = 0; i < hw->config.n_io; i++) {
			if (hw->io[i] && hw->io[i]->sgl &&
			    hw->io[i]->sgl->virt) {
				dma_free_coherent(&efct->pdev->dev,
						  hw->io[i]->sgl->size,
						  hw->io[i]->sgl->virt,
						  hw->io[i]->sgl->phys);
				memset(&hw->io[i]->sgl, 0,
				       sizeof(struct efc_dma_s));
			}
			kfree(hw->io[i]);
			hw->io[i] = NULL;
		}
		kfree(hw->io);
		hw->io = NULL;
		kfree(hw->wqe_buffs);
		hw->wqe_buffs = NULL;
	}

	dma = &hw->xfer_rdy;
	dma_free_coherent(&efct->pdev->dev,
			  dma->size, dma->virt, dma->phys);
	memset(dma, 0, sizeof(struct efc_dma_s));

	dma = &hw->dump_sges;
	dma_free_coherent(&efct->pdev->dev,
			  dma->size, dma->virt, dma->phys);
	memset(dma, 0, sizeof(struct efc_dma_s));

	dma = &hw->loop_map;
	dma_free_coherent(&efct->pdev->dev,
			  dma->size, dma->virt, dma->phys);
	memset(dma, 0, sizeof(struct efc_dma_s));

	for (i = 0; i < hw->wq_count; i++)
		sli_queue_free(&hw->sli, &hw->wq[i], destroy_queues,
			       free_memory);

	for (i = 0; i < hw->rq_count; i++)
		sli_queue_free(&hw->sli, &hw->rq[i], destroy_queues,
			       free_memory);

	for (i = 0; i < hw->mq_count; i++)
		sli_queue_free(&hw->sli, &hw->mq[i], destroy_queues,
			       free_memory);

	for (i = 0; i < hw->cq_count; i++)
		sli_queue_free(&hw->sli, &hw->cq[i], destroy_queues,
			       free_memory);

	for (i = 0; i < hw->eq_count; i++)
		sli_queue_free(&hw->sli, &hw->eq[i], destroy_queues,
			       free_memory);

	efct_hw_qtop_free(hw->qtop);

	/* Free rq buffers */
	efct_hw_rx_free(hw);

	efct_hw_queue_teardown(hw);

	efct_hw_rqpair_teardown(hw);

	if (sli_teardown(&hw->sli))
		efct_log_err(hw->os, "SLI teardown failed\n");

	/* record the fact that the queues are non-functional */
	hw->state = EFCT_HW_STATE_UNINITIALIZED;

	/* free sequence free pool */
	efct_array_free(hw->seq_pool);
	hw->seq_pool = NULL;

	/* free hw_wq_callback pool */
	efct_pool_free(hw->wq_reqtag_pool);

	/* Mark HW setup as not having been called */
	hw->hw_setup_called = false;

	return EFCT_HW_RTN_SUCCESS;
}

static enum efct_hw_rtn_e
efct_hw_sli_reset(struct efct_hw_s *hw, enum efct_hw_reset_e reset,
		  enum efct_hw_state_e prev_state)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;

	switch (reset) {
	case EFCT_HW_RESET_FUNCTION:
		efct_log_debug(hw->os, "issuing function level reset\n");
		if (sli_reset(&hw->sli)) {
			efct_log_err(hw->os, "sli_reset failed\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_RESET_FIRMWARE:
		efct_log_debug(hw->os, "issuing firmware reset\n");
		if (sli_fw_reset(&hw->sli)) {
			efct_log_err(hw->os, "sli_soft_reset failed\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		/*
		 * Because the FW reset leaves the FW in a non-running state,
		 * follow that with a regular reset.
		 */
		efct_log_debug(hw->os, "issuing function level reset\n");
		if (sli_reset(&hw->sli)) {
			efct_log_err(hw->os, "sli_reset failed\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	default:
		efct_log_err(hw->os,
			     "unknown reset type - no reset performed\n");
		hw->state = prev_state;
		rc = EFCT_HW_RTN_INVALID_ARG;
		break;
	}

	return rc;
}

enum efct_hw_rtn_e
efct_hw_reset(struct efct_hw_s *hw, enum efct_hw_reset_e reset)
{
	u32	i;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	u32	iters;
	enum efct_hw_state_e prev_state = hw->state;
	unsigned long flags = 0;
	struct efct_hw_io_s *temp;
	u32 destroy_queues;
	u32 free_memory;

	if (hw->state != EFCT_HW_STATE_ACTIVE)
		efct_log_test(hw->os,
			      "HW state %d is not active\n", hw->state);

	destroy_queues = (hw->state == EFCT_HW_STATE_ACTIVE);
	free_memory = (hw->state != EFCT_HW_STATE_UNINITIALIZED);
	hw->state = EFCT_HW_STATE_RESET_IN_PROGRESS;

	/*
	 * If the prev_state is already reset/teardown in progress,
	 * don't continue further
	 */
	if (prev_state == EFCT_HW_STATE_RESET_IN_PROGRESS ||
	    prev_state == EFCT_HW_STATE_TEARDOWN_IN_PROGRESS)
		return efct_hw_sli_reset(hw, reset, prev_state);

	/* shutdown target wqe timer */
	shutdown_target_wqe_timer(hw);

	if (prev_state != EFCT_HW_STATE_UNINITIALIZED) {
		efct_hw_flush(hw);

		/*
		 * If an mailbox command requiring a DMA is outstanding
		 * (SFP/DDM), then the FW will UE when the reset is issued.
		 * So attempt to complete all mailbox commands.
		 */
		iters = 10;
		while (!list_empty(&hw->cmd_head) && iters) {
			mdelay(10);
			efct_hw_flush(hw);
			iters--;
		}

		if (list_empty(&hw->cmd_head))
			efct_log_debug(hw->os,
				       "All commands completed on MQ queue\n");
		else
			efct_log_debug(hw->os,
				       "Some commands still pending on MQ queue\n");
	}

	/* Reset the chip */
	rc = efct_hw_sli_reset(hw, reset, prev_state);
	if (rc == EFCT_HW_RTN_INVALID_ARG)
		return EFCT_HW_RTN_ERROR;

	/* Not safe to walk command/io lists unless they've been initialized */
	if (prev_state != EFCT_HW_STATE_UNINITIALIZED) {
		efct_hw_command_cancel(hw);

		/* Try to clean up the io_inuse list */
		efct_hw_io_cancel(hw);

		memset(hw->domains, 0, sizeof(hw->domains));
		memset(hw->fcf_index_fcfi, 0, sizeof(hw->fcf_index_fcfi));

		efct_hw_link_event_init(hw);

		spin_lock_irqsave(&hw->io_lock, flags);
			/*
			 * The io lists should be empty, but remove any that
			 * didn't get cleaned up.
			 */
			while (!list_empty(&hw->io_timed_wqe)) {
				temp = list_first_entry(&hw->io_timed_wqe,
							struct efct_hw_io_s,
							list_entry);
				list_del(&temp->wqe_link);
			}

			while (!list_empty(&hw->io_free)) {
				temp = list_first_entry(&hw->io_free,
							struct efct_hw_io_s,
							list_entry);
				list_del(&temp->list_entry);
			}

			while (!list_empty(&hw->io_wait_free)) {
				temp = list_first_entry(&hw->io_wait_free,
							struct efct_hw_io_s,
							list_entry);
				list_del(&temp->list_entry);
			}
		spin_unlock_irqrestore(&hw->io_lock, flags);

		for (i = 0; i < hw->wq_count; i++)
			sli_queue_free(&hw->sli, &hw->wq[i],
				       destroy_queues, free_memory);

		for (i = 0; i < hw->rq_count; i++)
			sli_queue_free(&hw->sli, &hw->rq[i],
				       destroy_queues, free_memory);

		for (i = 0; i < hw->hw_rq_count; i++) {
			struct hw_rq_s *rq = hw->hw_rq[i];

			if (rq->rq_tracker) {
				u32 j;

				for (j = 0; j < rq->entry_count; j++)
					rq->rq_tracker[j] = NULL;
			}
		}

		for (i = 0; i < hw->mq_count; i++)
			sli_queue_free(&hw->sli, &hw->mq[i],
				       destroy_queues, free_memory);

		for (i = 0; i < hw->cq_count; i++)
			sli_queue_free(&hw->sli, &hw->cq[i],
				       destroy_queues, free_memory);

		for (i = 0; i < hw->eq_count; i++)
			sli_queue_free(&hw->sli, &hw->eq[i],
				       destroy_queues, free_memory);

		/* Free rq buffers */
		efct_hw_rx_free(hw);

		/* Teardown the HW queue topology */
		efct_hw_queue_teardown(hw);

		/*
		 * Reset the request tag pool, the HW IO request tags
		 * are reassigned in efct_hw_setup_io()
		 */
		efct_hw_reqtag_reset(hw);
	} else {
		/* Free rq buffers */
		efct_hw_rx_free(hw);
	}

	/*
	 * Re-apply the run-time workarounds after clearing the SLI config
	 * fields in sli_reset.
	 */
	efct_hw_workaround_setup(hw);
	return rc;
}

int
efct_hw_get_num_eq(struct efct_hw_s *hw)
{
	return hw->eq_count;
}

enum efct_hw_rtn_e
efct_hw_get(struct efct_hw_s *hw, enum efct_hw_property_e prop,
	    u32 *value)
{
	enum efct_hw_rtn_e		rc = EFCT_HW_RTN_SUCCESS;
	int			tmp;
	struct sli4_s *sli = &hw->sli;

	if (!value)
		return EFCT_HW_RTN_ERROR;

	*value = 0;

	switch (prop) {
	case EFCT_HW_N_IO:
		*value = hw->config.n_io;
		break;
	case EFCT_HW_N_SGL:
		*value = (hw->config.n_sgl - SLI4_SGE_MAX_RESERVED);
		break;
	case EFCT_HW_MAX_IO:
		*value = sli->extent[SLI_RSRC_XRI].size;
		break;
	case EFCT_HW_MAX_NODES:
		*value = sli->extent[SLI_RSRC_RPI].size;
		break;
	case EFCT_HW_MAX_RQ_ENTRIES:
		*value = hw->num_qentries[SLI_QTYPE_RQ];
		break;
	case EFCT_HW_RQ_DEFAULT_BUFFER_SIZE:
		*value = hw->config.rq_default_buffer_size;
		break;
	case EFCT_HW_AUTO_XFER_RDY_CAPABLE:
		*value = sli->auto_xfer_rdy;
		break;
	case EFCT_HW_AUTO_XFER_RDY_XRI_CNT:
		*value = hw->config.auto_xfer_rdy_xri_cnt;
		break;
	case EFCT_HW_AUTO_XFER_RDY_SIZE:
		*value = hw->config.auto_xfer_rdy_size;
		break;
	case EFCT_HW_AUTO_XFER_RDY_BLK_SIZE:
		switch (hw->config.auto_xfer_rdy_blk_size_chip) {
		case 0:
			*value = 512;
			break;
		case 1:
			*value = 1024;
			break;
		case 2:
			*value = 2048;
			break;
		case 3:
			*value = 4096;
			break;
		case 4:
			*value = 520;
			break;
		default:
			*value = 0;
			rc = EFCT_HW_RTN_ERROR;
			break;
		}
		break;
	case EFCT_HW_AUTO_XFER_RDY_T10_ENABLE:
		*value = hw->config.auto_xfer_rdy_t10_enable;
		break;
	case EFCT_HW_AUTO_XFER_RDY_P_TYPE:
		*value = hw->config.auto_xfer_rdy_p_type;
		break;
	case EFCT_HW_AUTO_XFER_RDY_REF_TAG_IS_LBA:
		*value = hw->config.auto_xfer_rdy_ref_tag_is_lba;
		break;
	case EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALID:
		*value = hw->config.auto_xfer_rdy_app_tag_valid;
		break;
	case EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALUE:
		*value = hw->config.auto_xfer_rdy_app_tag_value;
		break;
	case EFCT_HW_MAX_SGE:
		*value = sli->sge_supported_length;
		break;
	case EFCT_HW_MAX_SGL:
		*value = sli_get_max_sgl(&hw->sli);
		break;
	case EFCT_HW_TOPOLOGY:
		/*
		 * Infer link.status based on link.speed.
		 * Report EFCT_HW_TOPOLOGY_NONE if the link is down.
		 */
		if (hw->link.speed == 0) {
			*value = EFCT_HW_TOPOLOGY_NONE;
			break;
		}
		switch (hw->link.topology) {
		case SLI_LINK_TOPO_NPORT:
			*value = EFCT_HW_TOPOLOGY_NPORT;
			break;
		case SLI_LINK_TOPO_LOOP:
			*value = EFCT_HW_TOPOLOGY_LOOP;
			break;
		case SLI_LINK_TOPO_NONE:
			*value = EFCT_HW_TOPOLOGY_NONE;
			break;
		default:
			efct_log_test(hw->os,
				      "unsupported topology %#x\n",
				     hw->link.topology);
			rc = EFCT_HW_RTN_ERROR;
			break;
		}
		break;
	case EFCT_HW_CONFIG_TOPOLOGY:
		*value = hw->config.topology;
		break;
	case EFCT_HW_LINK_SPEED:
		*value = hw->link.speed;
		break;
	case EFCT_HW_LINK_CONFIG_SPEED:
		switch (hw->config.speed) {
		case FC_LINK_SPEED_10G:
			*value = 10000;
			break;
		case FC_LINK_SPEED_AUTO_16_8_4:
			*value = 0;
			break;
		case FC_LINK_SPEED_2G:
			*value = 2000;
			break;
		case FC_LINK_SPEED_4G:
			*value = 4000;
			break;
		case FC_LINK_SPEED_8G:
			*value = 8000;
			break;
		case FC_LINK_SPEED_16G:
			*value = 16000;
			break;
		case FC_LINK_SPEED_32G:
			*value = 32000;
			break;
		default:
			efct_log_test(hw->os,
				      "unsupported speed %#x\n",
				     hw->config.speed);
			rc = EFCT_HW_RTN_ERROR;
			break;
		}
		break;
	case EFCT_HW_IF_TYPE:
		*value = sli->if_type;
		break;
	case EFCT_HW_SLI_REV:
		*value = sli->sli_rev;
		break;
	case EFCT_HW_SLI_FAMILY:
		*value = sli->sli_family;
		break;
	case EFCT_HW_DIF_CAPABLE:
		*value = sli->features & SLI4_REQFEAT_DIF;
		break;
	case EFCT_HW_DIF_SEED:
		*value = hw->config.dif_seed;
		break;
	case EFCT_HW_DIF_MODE:
		*value = hw->config.dif_mode;
		break;
	case EFCT_HW_DIF_MULTI_SEPARATE:
		/* Lancer supports multiple DIF separates */
		if (hw->sli.if_type == SLI4_INTF_IF_TYPE_2)
			*value = true;
		else
			*value = false;
		break;
	case EFCT_HW_DUMP_MAX_SIZE:
		*value = hw->dump_size;
		break;
	case EFCT_HW_DUMP_READY:
		*value = sli_dump_is_ready(&hw->sli);
		break;
	case EFCT_HW_DUMP_PRESENT:
		*value = sli_dump_is_present(&hw->sli);
		break;
	case EFCT_HW_RESET_REQUIRED:
		tmp = sli_reset_required(&hw->sli);
		if (tmp < 0)
			rc = EFCT_HW_RTN_ERROR;
		else
			*value = tmp;
		break;
	case EFCT_HW_FW_ERROR:
		*value = sli_fw_error_status(&hw->sli);
		break;
	case EFCT_HW_FW_READY:
		*value = sli_fw_ready(&hw->sli);
		break;
	case EFCT_HW_HIGH_LOGIN_MODE:
		*value = sli->features & SLI4_REQFEAT_HLM;
		break;
	case EFCT_HW_PREREGISTER_SGL:
		*value = sli->sgl_pre_registration_required;
		break;
	case EFCT_HW_HW_REV1:
		*value = sli->hw_rev[0];
		break;
	case EFCT_HW_HW_REV2:
		*value = sli->hw_rev[1];
		break;
	case EFCT_HW_HW_REV3:
		*value = sli->hw_rev[2];
		break;
	case EFCT_HW_LINKCFG:
		*value = hw->linkcfg;
		break;
	case EFCT_HW_ETH_LICENSE:
		*value = hw->eth_license;
		break;
	case EFCT_HW_LINK_MODULE_TYPE:
		*value = sli->link_module_type;
		break;
	case EFCT_HW_NUM_CHUTES:
		*value = efct_hw_get_num_chutes(hw);
		break;
	case EFCT_HW_DISABLE_AR_TGT_DIF:
		*value = hw->workaround.disable_ar_tgt_dif;
		break;
	case EFCT_HW_EMULATE_I_ONLY_AAB:
		*value = hw->config.i_only_aab;
		break;
	case EFCT_HW_EMULATE_TARGET_WQE_TIMEOUT:
		*value = hw->config.emulate_tgt_wqe_timeout;
		break;
	case EFCT_HW_VPD_LEN:
		*value = sli->vpd_length;
		break;
	case EFCT_HW_SGL_CHAINING_CAPABLE:
		*value = sli->sgl_chaining_params.chaining_capable ||
					hw->workaround.sglc_misreported;
		break;
	case EFCT_HW_SGL_CHAINING_ALLOWED:
		/*
		 * SGL Chaining is allowed in the following cases:
		 *   1. Lancer with host SGL Lists
		 */
		*value = false;
		if ((sli->sgl_chaining_params.chaining_capable ||
		     hw->workaround.sglc_misreported) &&
		    !sli->sgl_pre_registered &&
		    sli->if_type == SLI4_INTF_IF_TYPE_2)
			*value = true;
		break;
	case EFCT_HW_SGL_CHAINING_HOST_ALLOCATED:
		/* Only lancer supports host allocated SGL Chaining buffers. */
		*value = ((sli->sgl_chaining_params.chaining_capable ||
			  hw->workaround.sglc_misreported) &&
			  (sli->if_type ==
			   SLI4_INTF_IF_TYPE_2));
		break;
	case EFCT_HW_SEND_FRAME_CAPABLE:
		if (hw->workaround.ignore_send_frame)
			*value = 0;
		else
			/* Only type 2 is capable */
			*value = sli->if_type ==
					 SLI4_INTF_IF_TYPE_2;
		break;
	case EFCT_HW_RQ_SELECTION_POLICY:
		*value = hw->config.rq_selection_policy;
		break;
	case EFCT_HW_RR_QUANTA:
		*value = hw->config.rr_quanta;
		break;
	case EFCT_HW_MAX_VPORTS:
		*value = sli->extent[SLI_RSRC_VPI].size;
	default:
		efct_log_test(hw->os, "unsupported property %#x\n", prop);
		rc = EFCT_HW_RTN_ERROR;
	}

	return rc;
}

void *
efct_hw_get_ptr(struct efct_hw_s *hw, enum efct_hw_property_e prop)
{
	void	*rc = NULL;
	struct sli4_s *sli = &hw->sli;

	switch (prop) {
	case EFCT_HW_WWN_NODE:
		rc = sli->wwnn;
		break;
	case EFCT_HW_WWN_PORT:
		rc = sli->wwpn;
		break;
	case EFCT_HW_VPD:
		/* make sure VPD length is non-zero */
		if (sli->vpd_length)
			rc = sli->vpd_data.virt;
		break;
	case EFCT_HW_FW_REV:
		rc = sli->fw_name[0];
		break;
	case EFCT_HW_FW_REV2:
		rc = sli->fw_name[1];
		break;
	case EFCT_HW_IPL:
		rc = sli->ipl_name;
		break;
	case EFCT_HW_PORTNUM:
		rc = sli->port_name;
		break;
	case EFCT_HW_BIOS_VERSION_STRING:
		rc = sli->bios_version_string;
		break;
	default:
		efct_log_test(hw->os, "unsupported property %#x\n", prop);
	}

	return rc;
}

enum efct_hw_rtn_e
efct_hw_set(struct efct_hw_s *hw, enum efct_hw_property_e prop, u32 value)
{
	enum efct_hw_rtn_e		rc = EFCT_HW_RTN_SUCCESS;
	struct sli4_s *sli = &hw->sli;

	switch (prop) {
	case EFCT_HW_N_IO:
		if (value > sli->extent[SLI_RSRC_XRI].size ||
		    value == 0) {
			efct_log_test(hw->os,
				      "IO value out of range %d vs %d\n",
				     value,
				sli->extent[SLI_RSRC_XRI].size);
			rc = EFCT_HW_RTN_ERROR;
		} else {
			hw->config.n_io = value;
		}
		break;
	case EFCT_HW_N_SGL:
		value += SLI4_SGE_MAX_RESERVED;
		if (value > sli_get_max_sgl(&hw->sli)) {
			efct_log_test(hw->os,
				      "SGL value out of range %d vs %d\n",
				     value, sli_get_max_sgl(&hw->sli));
			rc = EFCT_HW_RTN_ERROR;
		} else {
			hw->config.n_sgl = value;
		}
		break;
	case EFCT_HW_TOPOLOGY:
		switch (value) {
		case EFCT_HW_TOPOLOGY_AUTO:
			sli_set_topology(&hw->sli,
					 SLI4_READ_CFG_TOPO_FC);
			break;
		case EFCT_HW_TOPOLOGY_NPORT:
			sli_set_topology(&hw->sli, SLI4_READ_CFG_TOPO_FC_DA);
			break;
		case EFCT_HW_TOPOLOGY_LOOP:
			sli_set_topology(&hw->sli, SLI4_READ_CFG_TOPO_FC_AL);
			break;
		default:
			efct_log_test(hw->os,
				      "unsupported topology %#x\n", value);
			rc = EFCT_HW_RTN_ERROR;
		}
		hw->config.topology = value;
		break;
	case EFCT_HW_LINK_SPEED:

		switch (value) {
		case 0:		/* Auto-speed negotiation */
			hw->config.speed = FC_LINK_SPEED_AUTO_16_8_4;
			break;
		case 2000:	/* FC speeds */
			hw->config.speed = FC_LINK_SPEED_2G;
			break;
		case 4000:
			hw->config.speed = FC_LINK_SPEED_4G;
			break;
		case 8000:
			hw->config.speed = FC_LINK_SPEED_8G;
			break;
		case 16000:
			hw->config.speed = FC_LINK_SPEED_16G;
			break;
		case 32000:
			hw->config.speed = FC_LINK_SPEED_32G;
			break;
		default:
			efct_log_test(hw->os, "unsupported speed %d\n", value);
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_RQ_PROCESS_LIMIT: {
		struct hw_rq_s *rq;
		u32 i;

		/* For each hw_rq object, set its parent CQ limit value */
		for (i = 0; i < hw->hw_rq_count; i++) {
			rq = hw->hw_rq[i];
			hw->cq[rq->cq->instance].proc_limit = value;
		}
		break;
	}
	case EFCT_HW_RQ_DEFAULT_BUFFER_SIZE:
		hw->config.rq_default_buffer_size = value;
		break;
	case EFCT_HW_AUTO_XFER_RDY_XRI_CNT:
		hw->config.auto_xfer_rdy_xri_cnt = value;
		break;
	case EFCT_HW_AUTO_XFER_RDY_SIZE:
		hw->config.auto_xfer_rdy_size = value;
		break;
	case EFCT_HW_AUTO_XFER_RDY_BLK_SIZE:
		switch (value) {
		case 512:
			hw->config.auto_xfer_rdy_blk_size_chip = 0;
			break;
		case 1024:
			hw->config.auto_xfer_rdy_blk_size_chip = 1;
			break;
		case 2048:
			hw->config.auto_xfer_rdy_blk_size_chip = 2;
			break;
		case 4096:
			hw->config.auto_xfer_rdy_blk_size_chip = 3;
			break;
		case 520:
			hw->config.auto_xfer_rdy_blk_size_chip = 4;
			break;
		default:
			efct_log_err(hw->os, "Invalid block size %d\n",
				     value);
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_AUTO_XFER_RDY_T10_ENABLE:
		hw->config.auto_xfer_rdy_t10_enable = value;
		break;
	case EFCT_HW_AUTO_XFER_RDY_P_TYPE:
		hw->config.auto_xfer_rdy_p_type = value;
		break;
	case EFCT_HW_AUTO_XFER_RDY_REF_TAG_IS_LBA:
		hw->config.auto_xfer_rdy_ref_tag_is_lba = value;
		break;
	case EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALID:
		hw->config.auto_xfer_rdy_app_tag_valid = value;
		break;
	case EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALUE:
		hw->config.auto_xfer_rdy_app_tag_value = value;
		break;
	case EFCT_ESOC:
		hw->config.esoc = value;
	case EFCT_HW_HIGH_LOGIN_MODE:
		rc = sli_set_hlm(&hw->sli, value);
		break;
	case EFCT_HW_PREREGISTER_SGL:
		rc = sli_set_sgl_preregister(&hw->sli, value);
		break;
	case EFCT_HW_ETH_LICENSE:
		hw->eth_license = value;
		break;
	case EFCT_HW_EMULATE_I_ONLY_AAB:
		hw->config.i_only_aab = (bool)value;
		break;
	case EFCT_HW_EMULATE_TARGET_WQE_TIMEOUT:
		hw->config.emulate_tgt_wqe_timeout = value;
		break;
	case EFCT_HW_BOUNCE:
		hw->config.bounce = value;
		break;
	case EFCT_HW_RQ_SELECTION_POLICY:
		hw->config.rq_selection_policy = value;
		break;
	case EFCT_HW_RR_QUANTA:
		hw->config.rr_quanta = value;
		break;
	default:
		efct_log_test(hw->os, "unsupported property %#x\n", prop);
		rc = EFCT_HW_RTN_ERROR;
	}

	return rc;
}

enum efct_hw_rtn_e
efct_hw_set_ptr(struct efct_hw_s *hw, enum efct_hw_property_e prop,
		void *value)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;

	switch (prop) {
	case EFCT_HW_WAR_VERSION:
		hw->hw_war_version = value;
		break;
	case EFCT_HW_FILTER_DEF: {
		char *p = NULL;
		char *token;
		u32 idx = 0;

		for (idx = 0; idx < ARRAY_SIZE(hw->config.filter_def); idx++)
			hw->config.filter_def[idx] = 0;

		p = kstrdup(value, GFP_KERNEL);
		if (!p || !*p) {
			efct_log_err(NULL, "p is NULL\n");
			break;
		}

		idx = 0;
		while ((token = strsep(&p, ",")) && *token) {
			if (kstrtou32(token, 0, &hw->config.filter_def[idx++]))
				efct_log_err(NULL, "kstrtoint failed\n");

			if (!p || !*p)
				break;

			if (idx == ARRAY_SIZE(hw->config.filter_def))
				break;
		}
		kfree(p);

		break;
	}
	default:
		efct_log_test(hw->os, "unsupported property %#x\n", prop);
		rc = EFCT_HW_RTN_ERROR;
		break;
	}
	return rc;
}

int
efct_hw_process(struct efct_hw_s *hw, u32 vector,
		u32 max_isr_time_msec)
{
	struct hw_eq_s *eq;
	int rc = 0;

	CPUTRACE(hw->os, "hw process");

	/*
	 * The caller should disable interrupts if they wish to prevent us
	 * from processing during a shutdown. The following states are defined:
	 *   EFCT_HW_STATE_UNINITIALIZED - No queues allocated
	 *   EFCT_HW_STATE_QUEUES_ALLOCATED - The state after a chip reset,
	 *                                    queues are cleared.
	 *   EFCT_HW_STATE_ACTIVE - Chip and queues are operational
	 *   EFCT_HW_STATE_RESET_IN_PROGRESS - reset, we still want completions
	 *   EFCT_HW_STATE_TEARDOWN_IN_PROGRESS - We still want mailbox
	 *                                        completions.
	 */
	if (hw->state == EFCT_HW_STATE_UNINITIALIZED)
		return 0;

	/* Get pointer to struct hw_eq_s */
	eq = hw->hw_eq[vector];
	if (!eq)
		return 0;

	eq->use_count++;

	rc = efct_hw_eq_process(hw, eq, max_isr_time_msec);

	return rc;
}

/**
 * @ingroup interrupt
 * @brief Process events associated with an EQ.
 *
 * @par Description
 * Loop termination:
 * @n @n Without a mechanism to terminate the completion processing loop, it
 * is possible under some workload conditions for the loop to never terminate
 * (or at least take longer than the OS is happy to have an interrupt handler
 * or kernel thread context hold a CPU without yielding).
 * @n @n The approach taken here is to periodically check how much time
 * we have been in this
 * processing loop, and if we exceed a predetermined time (multiple seconds),
 * the loop is terminated, and efct_hw_process() returns.
 *
 * @param hw Hardware context.
 * @param eq Pointer to HW EQ object.
 * @param max_isr_time_msec Maximum time in msec to stay in this function.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
efct_hw_eq_process(struct efct_hw_s *hw, struct hw_eq_s *eq,
		   u32 max_isr_time_msec)
{
	u8		eqe[sizeof(struct sli4_eqe_s)] = { 0 };
	u32	tcheck_count;
	time_t		tstart;
	time_t		telapsed;
	bool		done = false;

	tcheck_count = EFCT_HW_TIMECHECK_ITERATIONS;
	tstart = jiffies_to_msecs(jiffies);

	CPUTRACE(hw->os, "eq process");

	while (!done && !sli_eq_read(&hw->sli, eq->queue, eqe)) {
		u16	cq_id = 0;
		int		rc;

		rc = sli_eq_parse(&hw->sli, eqe, &cq_id);
		if (unlikely(rc)) {
			if (rc > 0) {
				u32 i;

				/*
				 * Received a sentinel EQE indicating the
				 * EQ is full. Process all CQs
				 */
				for (i = 0; i < hw->cq_count; i++)
					efct_hw_cq_process(hw, hw->hw_cq[i]);
				continue;
			} else {
				return rc;
			}
		} else {
			int index;

			index  = efct_hw_queue_hash_find(hw->cq_hash, cq_id);

			if (likely(index >= 0))
				efct_hw_cq_process(hw, hw->hw_cq[index]);
			else
				efct_log_err(hw->os, "bad CQ_ID %#06x\n",
					     cq_id);
		}

		if (eq->queue->n_posted > eq->queue->posted_limit)
			sli_queue_arm(&hw->sli, eq->queue, false);

		if (tcheck_count && (--tcheck_count == 0)) {
			tcheck_count = EFCT_HW_TIMECHECK_ITERATIONS;
			telapsed = jiffies_to_msecs(jiffies) - tstart;
			if (telapsed >= max_isr_time_msec)
				done = true;
		}
	}
	sli_queue_eq_arm(&hw->sli, eq->queue, true);

	return 0;
}

/**
 * @brief Submit queued (pending) mbx commands.
 *
 * @par Description
 * Submit queued mailbox commands.
 * --- Assumes that hw->cmd_lock is held ---
 *
 * @param hw Hardware context.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
static int
efct_hw_cmd_submit_pending(struct efct_hw_s *hw)
{
	struct efct_command_ctx_s *ctx = NULL;
	int rc = 0;

	/* Assumes lock held */

	/* Only submit MQE if there's room */
	while (hw->cmd_head_count < (EFCT_HW_MQ_DEPTH - 1) &&
	       !list_empty(&hw->cmd_pending)) {
		ctx = list_first_entry(&hw->cmd_pending,
				       struct efct_command_ctx_s, list_entry);
		list_del(&ctx->list_entry);

		if (!ctx)
			break;
		INIT_LIST_HEAD(&ctx->list_entry);
		list_add_tail(&ctx->list_entry, &hw->cmd_head);
		hw->cmd_head_count++;
		if (sli_mq_write(&hw->sli, hw->mq, ctx->buf) < 0) {
			efct_log_test(hw->os,
				      "sli_queue_write failed: %d\n", rc);
			rc = -1;
			break;
		}
	}
	return rc;
}

/**
 * @ingroup io
 * @brief Issue a SLI command.
 *
 * @par Description
 * Send a mailbox command to the hardware, and either wait for a completion
 * (EFCT_CMD_POLL) or get an optional asynchronous completion (EFCT_CMD_NOWAIT).
 *
 * @param hw Hardware context.
 * @param cmd Buffer containing a formatted command and results.
 * @param opts Command options:
 *  - EFCT_CMD_POLL - Cmd executes synchronously &
 *		      busy-waits for the completion.
 *  - EFCT_CMD_NOWAIT - Cmd executes asynchronously. Uses callback.
 * @param cb Function callback used for asynchronous mode. May be NULL.
 * @n Prototype is <tt>(*cb)(void *arg, u8 *cmd)</tt>.
 * @n @n @b Note: If the
 * callback function pointer is NULL, the results of the command are silently
 * discarded, allowing this pointer to exist solely on the stack.
 * @param arg Argument passed to an asynchronous callback.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_command(struct efct_hw_s *hw, u8 *cmd, u32 opts, void *cb,
		void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	unsigned long flags = 0;
	void *bmbx = NULL;

	/*
	 * If the chip is in an error state (UE'd) then reject this mailbox
	 *  command.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		efct_log_crit(hw->os,
			      "status=%#x error1=%#x error2=%#x\n",
			sli_reg_read_status(&hw->sli),
			sli_reg_read_err1(&hw->sli),
			sli_reg_read_err2(&hw->sli));

		return EFCT_HW_RTN_ERROR;
	}

	if (opts == EFCT_CMD_POLL) {
		spin_lock_irqsave(&hw->cmd_lock, flags);
		bmbx = hw->sli.bmbx.virt;

		memset(bmbx, 0, SLI4_BMBX_SIZE);
		memcpy(bmbx, cmd, SLI4_BMBX_SIZE);

		if (sli_bmbx_command(&hw->sli) == 0) {
			rc = EFCT_HW_RTN_SUCCESS;
			memcpy(cmd, bmbx, SLI4_BMBX_SIZE);
		}
		spin_unlock_irqrestore(&hw->cmd_lock, flags);
	} else if (opts == EFCT_CMD_NOWAIT) {
		struct efct_command_ctx_s	*ctx = NULL;

		ctx = kmalloc(sizeof(*ctx), GFP_KERNEL);
		if (!ctx)
			return EFCT_HW_RTN_NO_RESOURCES;

		memset(ctx, 0, sizeof(struct efct_command_ctx_s));

		if (hw->state != EFCT_HW_STATE_ACTIVE) {
			efct_log_err(hw->os,
				     "Can't send command, HW state=%d\n",
				    hw->state);
			kfree(ctx);
			return EFCT_HW_RTN_ERROR;
		}

		if (cb) {
			ctx->cb = cb;
			ctx->arg = arg;
		}
		ctx->buf = cmd;
		ctx->ctx = hw;

		spin_lock_irqsave(&hw->cmd_lock, flags);

			/* Add to pending list */
			INIT_LIST_HEAD(&ctx->list_entry);
			list_add_tail(&ctx->list_entry, &hw->cmd_pending);

			/* Submit as much of the pending list as we can */
			if (efct_hw_cmd_submit_pending(hw) == 0)
				rc = EFCT_HW_RTN_SUCCESS;

		spin_unlock_irqrestore(&hw->cmd_lock, flags);
	}

	return rc;
}

/**
 * @ingroup port
 * @brief Allocate a port object.
 *
 * @par Description
 * This function allocates a VPI object for the port and stores it in the
 * indicator field of the port object.
 *
 * @param hw Hardware context.
 * @param sport SLI port object used to connect to the domain.
 * @param domain Domain object associated with this port (may be NULL).
 * @param wwpn Port's WWPN in big-endian order, or NULL to use default.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_port_alloc(struct efc_lport *efc, struct efc_sli_port_s *sport,
		   struct efc_domain_s *domain, u8 *wwpn)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	u8	*cmd = NULL;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	u32 index;

	sport->indicator = U32_MAX;
	sport->hw = hw;
	sport->free_req_pending = false;

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	if (wwpn)
		memcpy(&sport->sli_wwpn, wwpn, sizeof(sport->sli_wwpn));

	if (sli_resource_alloc(&hw->sli, SLI_RSRC_VPI,
			       &sport->indicator, &index)) {
		efct_log_err(hw->os, "VPI allocation failure\n");
		return EFCT_HW_RTN_ERROR;
	}

	if (domain) {
		cmd = kmalloc(SLI4_BMBX_SIZE, GFP_KERNEL);
		if (!cmd) {
			rc = EFCT_HW_RTN_NO_MEMORY;
			goto efct_hw_port_alloc_out;
		}
		memset(cmd, 0, SLI4_BMBX_SIZE);

		/*
		 * If the WWPN is NULL, fetch the default
		 * WWPN and WWNN before initializing the VPI
		 */
		if (!wwpn)
			efct_hw_port_alloc_read_sparm64(sport, cmd);
		else
			efct_hw_port_alloc_init_vpi(sport, cmd);
	} else if (!wwpn) {
		/* This is the convention for the HW, not SLI */
		efct_log_test(hw->os, "need WWN for physical port\n");
		rc = EFCT_HW_RTN_ERROR;
	}
	/* domain NULL and wwpn non-NULL */
	// no-op;

efct_hw_port_alloc_out:
	if (rc != EFCT_HW_RTN_SUCCESS) {
		kfree(cmd);

		sli_resource_free(&hw->sli, SLI_RSRC_VPI,
				  sport->indicator);
	}

	return rc;
}

/**
 * @ingroup port
 * @brief Attach a physical/virtual SLI port to a domain.
 *
 * @par Description
 * This function registers a previously-allocated VPI with the
 * device.
 *
 * @param hw Hardware context.
 * @param sport Pointer to the SLI port object.
 * @param fc_id Fibre Channel ID to associate with this port.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success, or an error code on failure.
 */
enum efct_hw_rtn_e
efct_hw_port_attach(struct efc_lport *efc, struct efc_sli_port_s *sport,
		    u32 fc_id)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	u8	*buf = NULL;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;

	if (!hw || !sport) {
		efct_log_err(hw->os,
			     "bad parameter(s) hw=%p sport=%p\n", hw,
			sport);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	buf = kmalloc(SLI4_BMBX_SIZE, GFP_KERNEL);
	if (!buf)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(buf, 0, SLI4_BMBX_SIZE);
	sport->fc_id = fc_id;
	efct_hw_port_attach_reg_vpi(sport, buf);
	return rc;
}

/**
 * @brief Called when the port control command completes.
 *
 * @par Description
 * We only need to free the mailbox command buffer.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_port_control(struct efct_hw_s *hw, int status, u8 *mqe,
			void  *arg)
{
	kfree(mqe);
	return 0;
}

static void
efct_log_fw(struct efct_hw_s *hw, int type)
{
	efct_log_err(hw->os, "Can't bring up link, fw rev too low.\n");
	if (type)
		efct_log_err(hw->os,
			     "Update fw to %s or later (current version is %s)\n",
		    EFCT_FW_VER_STR(10, 4, 255, 0),
		    (char *)hw->sli.fw_name[0]);
	else
		efct_log_err(hw->os,
			     "Update fw to %s or later (current version is %s)\n",
		    EFCT_FW_VER_STR(10, 4, 255, 0),
		    (char *)hw->sli.fw_name[0]);
}

/**
 * @ingroup port
 * @brief Control a port (initialize, shutdown, or set link configuration).
 *
 * @par Description
 * This function controls a port depending on the @c ctrl parameter:
 * - @b EFCT_HW_PORT_INIT -
 * Issues the CONFIG_LINK and INIT_LINK commands for the specified port.
 * The HW generates an EFC_HW_DOMAIN_FOUND event when the link comes up.
 * .
 * - @b EFCT_HW_PORT_SHUTDOWN -
 * Issues the DOWN_LINK command for the specified port.
 * The HW generates an EFC_HW_DOMAIN_LOST event when the link is down.
 * .
 * - @b EFCT_HW_PORT_SET_LINK_CONFIG -
 * Sets the link configuration.
 *
 * @param hw Hardware context.
 * @param ctrl Specifies the operation:
 * - EFCT_HW_PORT_INIT
 * - EFCT_HW_PORT_SHUTDOWN
 * - EFCT_HW_PORT_SET_LINK_CONFIG
 *
 * @param value Operation-specific value.
 * - EFCT_HW_PORT_INIT - Selective reset AL_PA
 * - EFCT_HW_PORT_SHUTDOWN - N/A
 * - EFCT_HW_PORT_SET_LINK_CONFIG - An enum #efct_hw_linkcfg_e value.
 *
 * @param cb Callback function to invoke the following operation.
 * - EFCT_HW_PORT_INIT/EFCT_HW_PORT_SHUTDOWN - NULL (link events
 * are handled by the EFCT_HW_CB_DOMAIN callbacks).
 * - EFCT_HW_PORT_SET_LINK_CONFIG - Invoked after linkcfg mailbox command
 * completes.
 *
 * @param arg Callback argument invoked after the command completes.
 * - EFCT_HW_PORT_INIT/EFCT_HW_PORT_SHUTDOWN - NULL (link events
 * are handled by the EFCT_HW_CB_DOMAIN callbacks).
 * - EFCT_HW_PORT_SET_LINK_CONFIG - Invoked after linkcfg mailbox command
 * completes.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_port_control(struct efct_hw_s *hw, enum efct_hw_port_e ctrl,
		     uintptr_t value,
		void (*cb)(int status, uintptr_t value, void *arg),
		void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;

	switch (ctrl) {
	case EFCT_HW_PORT_INIT:
	{
		u8	*init_link;
		u32 speed = 0;
		u8 reset_alpa = 0;

		u8	*cfg_link;

		cfg_link = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
		if (!cfg_link)
			return EFCT_HW_RTN_NO_MEMORY;

		if (sli_cmd_config_link(&hw->sli, cfg_link,
					SLI4_BMBX_SIZE))
			rc = efct_hw_command(hw, cfg_link,
					     EFCT_CMD_NOWAIT,
					     efct_hw_cb_port_control,
					     NULL);

		if (rc != EFCT_HW_RTN_SUCCESS) {
			kfree(cfg_link);
			efct_log_err(hw->os, "CONFIG_LINK failed\n");
			break;
		}
		speed = hw->config.speed;
		reset_alpa = (u8)(value & 0xff);

		/* Allocate a new buffer for the init_link command */
		init_link = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
		if (!init_link)
			return EFCT_HW_RTN_NO_MEMORY;

		/*
		 * Bring link up, unless FW version is not supported
		 */
		if (hw->workaround.fw_version_too_low) {
			efct_log_fw(hw, 1);

			return EFCT_HW_RTN_ERROR;
		}

		rc = EFCT_HW_RTN_ERROR;
		if (sli_cmd_init_link(&hw->sli, init_link, SLI4_BMBX_SIZE,
				      speed, reset_alpa))
			rc = efct_hw_command(hw, init_link, EFCT_CMD_NOWAIT,
					     efct_hw_cb_port_control, NULL);
		/* Free buffer on error, since no callback is coming */
		if (rc != EFCT_HW_RTN_SUCCESS) {
			kfree(init_link);
			efct_log_err(hw->os, "INIT_LINK failed\n");
		}
		break;
	}
	case EFCT_HW_PORT_SHUTDOWN:
	{
		u8	*down_link;

		down_link = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
		if (!down_link)
			return EFCT_HW_RTN_NO_MEMORY;

		if (sli_cmd_down_link(&hw->sli, down_link, SLI4_BMBX_SIZE))
			rc = efct_hw_command(hw, down_link, EFCT_CMD_NOWAIT,
					     efct_hw_cb_port_control, NULL);
		/* Free buffer on error, since no callback is coming */
		if (rc != EFCT_HW_RTN_SUCCESS) {
			kfree(down_link);
			efct_log_err(hw->os, "DOWN_LINK failed\n");
		}
		break;
	}
	case EFCT_HW_PORT_SET_LINK_CONFIG:
		rc = efct_hw_set_linkcfg(hw, (enum efct_hw_linkcfg_e)value,
					 EFCT_CMD_NOWAIT, cb, arg);
		break;
	default:
		efct_log_test(hw->os, "unhandled control %#x\n", ctrl);
		break;
	}

	return rc;
}

/**
 * @ingroup port
 * @brief Free port resources.
 *
 * @par Description
 * Issue the UNREG_VPI command to free the assigned VPI context.
 *
 * @param hw Hardware context.
 * @param sport SLI port object used to connect to the domain.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_port_free(struct efc_lport *efc, struct efc_sli_port_s *sport)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_SUCCESS;

	if (!hw || !sport) {
		efct_log_err(hw->os,
			     "bad parameter(s) hw=%p sport=%p\n", hw,
			sport);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	if (sport->attached)
		efct_hw_port_free_unreg_vpi(sport, NULL);
	else
		sport->free_req_pending = true;

	return rc;
}

/**
 * @ingroup domain
 * @brief Allocate a fabric domain object.
 *
 * @par Description
 * This function starts a series of commands needed to connect to the domain,
 * including
 *   - REG_FCFI
 *   - INIT_VFI
 *   - READ_SPARMS
 *   .
 * @b Note: Not all SLI interface types use all of the above commands.
 * @n @n Upon successful allocation, the HW generates a EFC_HW_DOMAIN_ALLOC_OK
 * event. On failure, it generates a EFC_HW_DOMAIN_ALLOC_FAIL event.
 *
 * @param hw Hardware context.
 * @param domain Pointer to the domain object.
 * @param fcf FCF index.
 * @param vlan VLAN ID.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_domain_alloc(struct efc_lport *efc, struct efc_domain_s *domain,
		     u32 fcf, u32 vlan)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;
	u8		*cmd = NULL;
	u32	index;

	if (!hw || !domain || !domain->sport) {
		efct_log_err(NULL,
			     "bad parameter(s) hw=%p domain=%p sport=%p\n",
			    hw, domain, domain ? domain->sport : NULL);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	cmd = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!cmd)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(cmd, 0, SLI4_BMBX_SIZE);

	/* allocate memory for the service parameters */
	domain->dma.size = 112;
	domain->dma.virt = dma_alloc_coherent(&efct->pdev->dev,
					      domain->dma.size,
					      &domain->dma.phys, GFP_DMA);
	if (!domain->dma.virt) {
		efct_log_err(hw->os, "Failed to allocate DMA memory\n");
		kfree(cmd);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	domain->hw = hw;
	domain->sm.app = domain;
	domain->fcf = fcf;
	domain->fcf_indicator = U32_MAX;
	domain->vlan_id = vlan;
	domain->indicator = U32_MAX;

	if (sli_resource_alloc(&hw->sli,
			       SLI_RSRC_VFI, &domain->indicator,
				    &index)) {
		efct_log_err(hw->os, "VFI allocation failure\n");

		kfree(cmd);
		dma_free_coherent(&efct->pdev->dev,
				  domain->dma.size, domain->dma.virt,
				  domain->dma.phys);
		memset(&domain->dma, 0, sizeof(struct efc_dma_s));

		return EFCT_HW_RTN_ERROR;
	}

	efct_hw_domain_alloc_init_vfi(domain, cmd);
	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup domain
 * @brief Attach a SLI port to a domain.
 *
 * @param hw Hardware context.
 * @param domain Pointer to the domain object.
 * @param fc_id Fibre Channel ID to associate with this port.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_domain_attach(struct efc_lport *efc,
		      struct efc_domain_s *domain, u32 fc_id)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	u8	*buf = NULL;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;

	if (!hw || !domain) {
		efct_log_err(hw->os,
			     "bad parameter(s) hw=%p domain=%p\n",
			hw, domain);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	buf = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!buf)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(buf, 0, SLI4_BMBX_SIZE);
	domain->sport->fc_id = fc_id;
	efct_hw_domain_attach_reg_vfi(domain, buf);
	return rc;
}

/**
 * @ingroup domain
 * @brief Free a fabric domain object.
 *
 * @par Description
 * Free both the driver and SLI port resources associated with the domain.
 *
 * @param hw Hardware context.
 * @param domain Pointer to the domain object.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_domain_free(struct efc_lport *efc, struct efc_domain_s *domain)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_SUCCESS;

	if (!hw || !domain) {
		efct_log_err(hw->os,
			     "bad parameter(s) hw=%p domain=%p\n",
			hw, domain);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	efct_hw_domain_free_unreg_vfi(domain, NULL);
	return rc;
}

/**
 * @ingroup domain
 * @brief Free a fabric domain object.
 *
 * @par Description
 * Free the driver resources associated with the domain. The difference between
 * this call and efct_hw_domain_free() is that this call assumes resources no
 * longer exist on the SLI port, due to a reset or after some error conditions.
 *
 * @param hw Hardware context.
 * @param domain Pointer to the domain object.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_domain_force_free(struct efc_lport *efc, struct efc_domain_s *domain)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	if (!hw || !domain) {
		efct_log_err(NULL,
			     "bad parameter(s) hw=%p domain=%p\n", hw, domain);
		return EFCT_HW_RTN_ERROR;
	}

	dma_free_coherent(&efct->pdev->dev,
			  domain->dma.size, domain->dma.virt, domain->dma.phys);
	memset(&domain->dma, 0, sizeof(struct efc_dma_s));
	sli_resource_free(&hw->sli, SLI_RSRC_VFI,
			  domain->indicator);

	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup node
 * @brief Allocate a remote node object.
 *
 * @param hw Hardware context.
 * @param rnode Allocated remote node object to initialize.
 * @param fc_addr FC address of the remote node.
 * @param sport SLI port used to connect to remote node.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_node_alloc(struct efc_lport *efc, struct efc_remote_node_s *rnode,
		   u32 fc_addr, struct efc_sli_port_s *sport)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	/* Check for invalid indicator */
	if (rnode->indicator != U32_MAX) {
		efct_log_err(hw->os,
			     "RPI allocation failure addr=%#x rpi=%#x\n",
			    fc_addr, rnode->indicator);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	/* NULL SLI port indicates an unallocated remote node */
	rnode->sport = NULL;

	if (sli_resource_alloc(&hw->sli, SLI_RSRC_RPI,
			       &rnode->indicator, &rnode->index)) {
		efct_log_err(hw->os, "RPI allocation failure addr=%#x\n",
			     fc_addr);
		return EFCT_HW_RTN_ERROR;
	}

	rnode->fc_id = fc_addr;
	rnode->sport = sport;

	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup node
 * @brief Update a remote node object with the remote port's service parameters.
 *
 * @param hw Hardware context.
 * @param rnode Allocated remote node object to initialize.
 * @param sparms DMA buffer containing the remote port's service parameters.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_node_attach(struct efc_lport *efc, struct efc_remote_node_s *rnode,
		    struct efc_dma_s *sparms)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_ERROR;
	u8		*buf = NULL;
	u32	count = 0;

	if (!hw || !rnode || !sparms) {
		efct_log_err(NULL,
			     "bad parameter(s) hw=%p rnode=%p sparms=%p\n",
			    hw, rnode, sparms);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	buf = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!buf)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(buf, 0, SLI4_BMBX_SIZE);
	/*
	 * If the attach count is non-zero, this RPI has already been reg'd.
	 * Otherwise, register the RPI
	 */
	if (rnode->index == U32_MAX) {
		efct_log_err(NULL, "bad parameter rnode->index invalid\n");
		kfree(buf);
		return EFCT_HW_RTN_ERROR;
	}
	count = atomic_add_return(1, &hw->rpi_ref[rnode->index].rpi_count);
	count--;
	if (count) {
		/*
		 * Can't attach multiple FC_ID's to a node unless High Login
		 * Mode is enabled
		 */
		if (!hw->sli.high_login_mode) {
			efct_log_test(hw->os,
				      "attach to attached node HLM=%d cnt=%d\n",
				     hw->sli.high_login_mode, count);
			rc = EFCT_HW_RTN_SUCCESS;
		} else {
			rnode->node_group = true;
			rnode->attached =
			 atomic_read(&hw->rpi_ref[rnode->index].rpi_attached);
			rc = rnode->attached  ? EFCT_HW_RTN_SUCCESS_SYNC :
							 EFCT_HW_RTN_SUCCESS;
		}
	} else {
		rnode->node_group = false;

		if (sli_cmd_reg_rpi(&hw->sli, buf, SLI4_BMBX_SIZE,
				    rnode->fc_id,
				    rnode->indicator, rnode->sport->indicator,
				    sparms, 0,
				    (hw->auto_xfer_rdy_enabled &&
				    hw->config.auto_xfer_rdy_t10_enable)))
			rc = efct_hw_command(hw, buf, EFCT_CMD_NOWAIT,
					     efct_hw_cb_node_attach, rnode);
	}

	if (count || rc) {
		if (rc < EFCT_HW_RTN_SUCCESS) {
			atomic_sub_return(1,
					  &hw->rpi_ref[rnode->index].rpi_count);
			efct_log_err(hw->os,
				     "%s error\n", count ? "HLM" : "REG_RPI");
		}
		kfree(buf);
	}

	return rc;
}

/**
 * @ingroup node
 * @brief Free a remote node resource.
 *
 * @param hw Hardware context.
 * @param rnode Remote node object to free.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_node_free_resources(struct efc_lport *efc,
			    struct efc_remote_node_s *rnode)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;
	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_SUCCESS;

	if (!hw || !rnode) {
		efct_log_err(NULL, "bad parameter(s) hw=%p rnode=%p\n",
			     hw, rnode);
		return EFCT_HW_RTN_ERROR;
	}

	if (rnode->sport) {
		if (rnode->attached) {
			efct_log_err(hw->os, "Err: rnode is still attached\n");
			return EFCT_HW_RTN_ERROR;
		}
		if (rnode->indicator != U32_MAX) {
			if (sli_resource_free(&hw->sli, SLI_RSRC_RPI,
					      rnode->indicator)) {
				efct_log_err(hw->os,
					     "RPI free fail RPI %d addr=%#x\n",
					    rnode->indicator,
					    rnode->fc_id);
				rc = EFCT_HW_RTN_ERROR;
			} else {
				rnode->node_group = false;
				rnode->indicator = U32_MAX;
				rnode->index = U32_MAX;
				rnode->free_group = false;
			}
		}
	}

	return rc;
}

/**
 * @ingroup node
 * @brief Free a remote node object.
 *
 * @param hw Hardware context.
 * @param rnode Remote node object to free.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_node_detach(struct efc_lport *efc, struct efc_remote_node_s *rnode)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;
	u8	*buf = NULL;
	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_SUCCESS_SYNC;
	u32	index = U32_MAX;

	if (!hw || !rnode) {
		efct_log_err(NULL, "bad parameter(s) hw=%p rnode=%p\n",
			     hw, rnode);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	index = rnode->index;

	if (rnode->sport) {
		u32	count = 0;
		u32	fc_id;

		if (!rnode->attached)
			return EFCT_HW_RTN_SUCCESS_SYNC;

		buf = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
		if (!buf)
			return EFCT_HW_RTN_NO_MEMORY;

		memset(buf, 0, SLI4_BMBX_SIZE);
		count = atomic_sub_return(1, &hw->rpi_ref[index].rpi_count);
		count++;
		if (count <= 1) {
			/*
			 * There are no other references to this RPI so
			 * unregister it
			 */
			fc_id = U32_MAX;
			/* and free the resource */
			rnode->node_group = false;
			rnode->free_group = true;
		} else {
			if (!hw->sli.high_login_mode)
				efct_log_test(hw->os,
					      "Inval cnt with HLM off, cnt=%d\n",
					     count);
			fc_id = rnode->fc_id & 0x00ffffff;
		}

		rc = EFCT_HW_RTN_ERROR;

		if (sli_cmd_unreg_rpi(&hw->sli, buf, SLI4_BMBX_SIZE,
				      rnode->indicator,
				      SLI_RSRC_RPI, fc_id))
			rc = efct_hw_command(hw, buf, EFCT_CMD_NOWAIT,
					     efct_hw_cb_node_free, rnode);

		if (rc != EFCT_HW_RTN_SUCCESS) {
			efct_log_err(hw->os, "UNREG_RPI failed\n");
			kfree(buf);
			rc = EFCT_HW_RTN_ERROR;
		}
	}

	return rc;
}

/**
 * @ingroup node
 * @brief Free all remote node objects.
 *
 * @param hw Hardware context.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_node_free_all(struct efct_hw_s *hw)
{
	u8	*buf = NULL;
	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_ERROR;

	if (!hw) {
		efct_log_err(NULL, "bad parameter hw=%p\n", hw);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Check if the chip is in an error state (UE'd) before proceeding.
	 */
	if (sli_fw_error_status(&hw->sli) > 0) {
		efct_log_crit(hw->os,
			      "Chip is in an error state - reset needed\n");
		return EFCT_HW_RTN_ERROR;
	}

	buf = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!buf)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(buf, 0, SLI4_BMBX_SIZE);

	if (sli_cmd_unreg_rpi(&hw->sli, buf, SLI4_BMBX_SIZE, 0xffff,
			      SLI_RSRC_FCFI, U32_MAX))
		rc = efct_hw_command(hw, buf, EFCT_CMD_NOWAIT,
				     efct_hw_cb_node_free_all,
				     NULL);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_err(hw->os, "UNREG_RPI failed\n");
		kfree(buf);
		rc = EFCT_HW_RTN_ERROR;
	}

	return rc;
}

/**
 * @brief Initialize IO fields on each free call.
 *
 * @n @b Note: This is done on each free call (as opposed to each
 * alloc call) because port-owned XRIs are not
 * allocated with efct_hw_io_alloc() but are freed with this
 * function.
 *
 * @param io Pointer to HW IO.
 */
static inline void
efct_hw_init_free_io(struct efct_hw_io_s *io)
{
	/*
	 * Set io->done to NULL, to avoid any callbacks, should
	 * a completion be received for one of these IOs
	 */
	io->done = NULL;
	io->abort_done = NULL;
	io->status_saved = false;
	io->abort_in_progress = false;
	io->port_owned_abort_count = 0;
	io->rnode = NULL;
	io->type = 0xFFFF;
	io->wq = NULL;
	io->ul_io = NULL;
	io->tgt_wqe_timeout = 0;
}

/**
 * @ingroup io
 * @brief Lockless allocate a HW IO object.
 *
 * @par Description
 * Assume that hw->efct_lock is held. This function is only used if
 * use_dif_sec_xri workaround is being used.
 *
 * @param hw Hardware context.
 *
 * @return Returns a pointer to an object on success, or NULL on failure.
 */
static inline struct efct_hw_io_s *
_efct_hw_io_alloc(struct efct_hw_s *hw)
{
	struct efct_hw_io_s	*io = NULL;

	if (!list_empty(&hw->io_free)) {
		io = list_first_entry(&hw->io_free, struct efct_hw_io_s,
				      list_entry);
		list_del(&io->list_entry);
	}
	if (io) {
		INIT_LIST_HEAD(&io->list_entry);
		INIT_LIST_HEAD(&io->wqe_link);
		INIT_LIST_HEAD(&io->dnrx_link);
		list_add_tail(&io->list_entry, &hw->io_inuse);
		io->state = EFCT_HW_IO_STATE_INUSE;
		io->quarantine = false;
		io->quarantine_first_phase = true;
		io->abort_reqtag = U32_MAX;
		kref_init(&io->ref);
		io->release = efct_hw_io_free_internal;
	} else {
		atomic_add_return(1, &hw->io_alloc_failed_count);
	}

	return io;
}

/**
 * @ingroup io
 * @brief Allocate a HW IO object.
 *
 * @par Description
 * @n @b Note: This function applies to non-port owned XRIs
 * only.
 *
 * @param hw Hardware context.
 *
 * @return Returns a pointer to an object on success, or NULL on failure.
 */
struct efct_hw_io_s *
efct_hw_io_alloc(struct efct_hw_s *hw)
{
	struct efct_hw_io_s	*io = NULL;
	unsigned long flags = 0;

	spin_lock_irqsave(&hw->io_lock, flags);
	io = _efct_hw_io_alloc(hw);
	spin_unlock_irqrestore(&hw->io_lock, flags);

	return io;
}

/**
 * @ingroup io
 * @brief Allocate/Activate a port owned HW IO object.
 *
 * @par Description
 * This function is called by the transport layer when an XRI is
 * allocated by the SLI-Port. This will "activate" the HW IO
 * associated with the XRI received from the SLI-Port to mirror
 * the state of the XRI.
 * @n @n @b Note: This function applies to port owned XRIs only.
 *
 * @param hw Hardware context.
 * @param io Pointer HW IO to activate/allocate.
 *
 * @return Returns a pointer to an object on success, or NULL on failure.
 */
struct efct_hw_io_s *
efct_hw_io_activate_port_owned(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	if (refcount_read(&io->ref.refcount) > 0) {
		efct_log_err(hw->os, "Bad parameter: refcount > 0\n");
		return NULL;
	}

	if (io->wq) {
		efct_log_err(hw->os, "XRI %x already in use\n", io->indicator);
		return NULL;
	}

	kref_init(&io->ref);
	io->release = efct_hw_io_free_port_owned;
	io->xbusy = true;

	return io;
}

/**
 * @ingroup io
 * @brief When an IO is freed, depending on the exchange busy flag, and other
 * workarounds, move it to the correct list.
 *
 * @par Description
 * Note: Assumes that the hw->io_lock is held and the item has been removed
 * from the busy or wait_free list.
 *
 * @param hw Hardware context.
 * @param io Pointer to the IO object to move.
 */
static void
efct_hw_io_free_move_correct_list(struct efct_hw_s *hw,
				  struct efct_hw_io_s *io)
{
	if (io->xbusy) {
		/*
		 * add to wait_free list and wait for XRI_ABORTED CQEs to clean
		 * up
		 */
		INIT_LIST_HEAD(&io->list_entry);
		list_add_tail(&io->list_entry, &hw->io_wait_free);
		io->state = EFCT_HW_IO_STATE_WAIT_FREE;
	} else {
		/* IO not busy, add to free list */
		INIT_LIST_HEAD(&io->list_entry);
		list_add_tail(&io->list_entry, &hw->io_free);
		io->state = EFCT_HW_IO_STATE_FREE;
	}

	if (hw->workaround.use_dif_sec_xri)
		efct_hw_check_sec_hio_list(hw);
}

/**
 * @ingroup io
 * @brief Free a HW IO object. Perform cleanup common to
 * port and host-owned IOs.
 *
 * @param hw Hardware context.
 * @param io Pointer to the HW IO object.
 */
static inline void
efct_hw_io_free_common(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	/* initialize IO fields */
	efct_hw_init_free_io(io);

	/* Restore default SGL */
	efct_hw_io_restore_sgl(hw, io);
}

/**
 * @ingroup io
 * @brief Free a HW IO object associated with a port-owned XRI.
 *
 * @param arg Pointer to the HW IO object.
 */
void
efct_hw_io_free_port_owned(struct kref *arg)
{
	unsigned long flags = 0;
	struct efct_hw_io_s *io =
			container_of(arg, struct efct_hw_io_s, ref);
	struct efct_hw_s *hw = io->hw;

	/*
	 * For auto xfer rdy, if the dnrx bit is set, then add it to the list
	 * of XRIs waiting for buffers.
	 */
	if (io->auto_xfer_rdy_dnrx) {
		spin_lock_irqsave(&hw->io_lock, flags);
		/*
		 * take a reference count because we still own the IO until
		 * the buffer is posted
		 */
		kref_init(&io->ref);
		io->release = efct_hw_io_free_port_owned;
		INIT_LIST_HEAD(&io->dnrx_link);
		list_add_tail(&io->dnrx_link, &hw->io_port_dnrx);
		spin_unlock_irqrestore(&hw->io_lock, flags);
	}

	/* perform common cleanup */
	efct_hw_io_free_common(hw, io);
}

/**
 * @ingroup io
 * @brief Free a previously-allocated HW IO object. Called when
 * IO refcount goes to zero (host-owned IOs only).
 *
 * @param arg Pointer to the HW IO object.
 */
void
efct_hw_io_free_internal(struct kref *arg)
{
	unsigned long flags = 0;
	struct efct_hw_io_s *io =
			container_of(arg, struct efct_hw_io_s, ref);
	struct efct_hw_s *hw = io->hw;

	/* perform common cleanup */
	efct_hw_io_free_common(hw, io);

	spin_lock_irqsave(&hw->io_lock, flags);
		/* remove from in-use list */
		if (io->list_entry.next &&
		    !list_empty(&hw->io_inuse)) {
			list_del(&io->list_entry);
			efct_hw_io_free_move_correct_list(hw, io);
		}
	spin_unlock_irqrestore(&hw->io_lock, flags);
}

/**
 * @ingroup io
 * @brief Free a previously-allocated HW IO object.
 *
 * @par Description
 * @n @b Note: This function applies to port and host owned XRIs.
 *
 * @param hw Hardware context.
 * @param io Pointer to the HW IO object.
 *
 * @return Returns a non-zero value if HW IO was freed, 0 if references
 * on the IO still exist, or a negative value if an error occurred.
 */
int
efct_hw_io_free(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	/* just put refcount */
	if (refcount_read(&io->ref.refcount) <= 0) {
		efct_log_err(hw->os,
			     "Bad parameter: refcount <= 0 xri=%x tag=%x\n",
			    io->indicator, io->reqtag);
		return -1;
	}

	return kref_put(&io->ref, io->release);
}

/**
 * @ingroup io
 * @brief Check if given HW IO is in-use
 *
 * @par Description
 * This function returns TRUE if the given HW IO has been
 * allocated and is in-use, and FALSE otherwise. It applies to
 * port and host owned XRIs.
 *
 * @param hw Hardware context.
 * @param io Pointer to the HW IO object.
 *
 * @return TRUE if an IO is in use, or FALSE otherwise.
 */
u8
efct_hw_io_inuse(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	return (refcount_read(&io->ref.refcount) > 0);
}

/**
 * @brief Write a HW IO to a work queue.
 *
 * @par Description
 * A HW IO is written to a work queue.
 *
 * @param wq Pointer to work queue.
 * @param wqe Pointer to WQ entry.
 *
 * @n @b Note: Assumes the SLI-4 queue lock is held.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
static int
_efct_hw_wq_write(struct hw_wq_s *wq, struct efct_hw_wqe_s *wqe)
{
	int rc;
	int queue_rc;

	/* Every so often, set the wqec bit to generate comsummed completions */
	if (wq->wqec_count)
		wq->wqec_count--;

	if (wq->wqec_count == 0) {
		struct sli4_generic_wqe_s *genwqe = (void *)wqe->wqebuf;

		genwqe->cmdtype_wqec_byte |= SLI4_GEN_WQE_WQEC;
		wq->wqec_count = wq->wqec_set_count;
	}

	/* Decrement WQ free count */
	wq->free_count--;

	queue_rc = sli_wq_write(&wq->hw->sli, wq->queue, wqe->wqebuf);

	if (queue_rc < 0)
		rc = -1;
	else
		rc = 0;

	return rc;
}

/**
 * @brief Write a HW IO to a work queue.
 *
 * @par Description
 * A HW IO is written to a work queue.
 *
 * @param wq Pointer to work queue.
 * @param wqe Pointer to WQE entry.
 *
 * @n @b Note: Takes the SLI-4 queue lock.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int
efct_hw_wq_write(struct hw_wq_s *wq, struct efct_hw_wqe_s *wqe)
{
	int rc = 0;
	unsigned long flags = 0;

	spin_lock_irqsave(&wq->queue->lock, flags);
	if (!list_empty(&wq->pending_list)) {
		INIT_LIST_HEAD(&wqe->list_entry);
		list_add_tail(&wqe->list_entry, &wq->pending_list);
		wq->wq_pending_count++;
		while ((wq->free_count > 0) &&
		       ((wqe = list_first_entry(&wq->pending_list,
					struct efct_hw_wqe_s, list_entry))
			 != NULL)) {
			list_del(&wqe->list_entry);
			rc = _efct_hw_wq_write(wq, wqe);
			if (rc < 0)
				break;
			if (wqe->abort_wqe_submit_needed) {
				wqe->abort_wqe_submit_needed = false;
				sli_abort_wqe(&wq->hw->sli,
					      wqe->wqebuf,
					      wq->hw->sli.wqe_size,
					      SLI_ABORT_XRI,
					      wqe->send_abts, wqe->id,
					      0, wqe->abort_reqtag,
					      SLI4_CQ_DEFAULT);

				INIT_LIST_HEAD(&wqe->list_entry);
				list_add_tail(&wqe->list_entry,
					      &wq->pending_list);
				wq->wq_pending_count++;
			}
		}
	} else {
		if (wq->free_count > 0) {
			rc = _efct_hw_wq_write(wq, wqe);
		} else {
			INIT_LIST_HEAD(&wqe->list_entry);
			list_add_tail(&wqe->list_entry, &wq->pending_list);
			wq->wq_pending_count++;
		}
	}

	spin_unlock_irqrestore(&wq->queue->lock, flags);

	return rc;
}

/**
 * @brief Update free count and submit any pending HW IOs
 *
 * @par Description
 * The WQ free count is updated, and any pending HW IOs are submitted that
 * will fit in the queue.
 *
 * @param wq Pointer to work queue.
 * @param update_free_count Value added to WQs free count.
 *
 * @return None.
 */
static void
hw_wq_submit_pending(struct hw_wq_s *wq, u32 update_free_count)
{
	struct efct_hw_wqe_s *wqe;
	unsigned long flags = 0;

	spin_lock_irqsave(&wq->queue->lock, flags);

	/* Update free count with value passed in */
	wq->free_count += update_free_count;

	while ((wq->free_count > 0) && (!list_empty(&wq->pending_list))) {
		wqe = list_first_entry(&wq->pending_list,
				       struct efct_hw_wqe_s, list_entry);
		list_del(&wqe->list_entry);
		_efct_hw_wq_write(wq, wqe);

		if (wqe->abort_wqe_submit_needed) {
			wqe->abort_wqe_submit_needed = false;
			sli_abort_wqe(&wq->hw->sli, wqe->wqebuf,
				      wq->hw->sli.wqe_size,
				      SLI_ABORT_XRI, wqe->send_abts, wqe->id,
				      0, wqe->abort_reqtag, SLI4_CQ_DEFAULT);
					  INIT_LIST_HEAD(&wqe->list_entry);
			list_add_tail(&wqe->list_entry, &wq->pending_list);
			wq->wq_pending_count++;
		}
	}

	spin_unlock_irqrestore(&wq->queue->lock, flags);
}

/**
 * @brief Check to see if there are any waiting IOs
 *
 * @par Description
 * Checks hw->sec_hio_wait_list, if an IO is waiting for a HW IO, then try
 * to allocate a secondary HW io, and dispatch it.
 *
 * @n @b Note: hw->io_lock MUST be taken when called.
 *
 * @param hw pointer to HW object
 *
 * @return none
 */
static void
efct_hw_check_sec_hio_list(struct efct_hw_s *hw)
{
	struct efct_hw_io_s *io;
	struct efct_hw_io_s *sec_io;
	int rc = 0;

	while (!list_empty(&hw->sec_hio_wait_list)) {
		u16 flags;

		sec_io = _efct_hw_io_alloc(hw);
		if (!sec_io)
			break;

		io = list_first_entry(&hw->sec_hio_wait_list,
				      struct efct_hw_io_s, list_entry);
		list_del(&io->list_entry);
		INIT_LIST_HEAD(&io->list_entry);
		list_add_tail(&io->list_entry, &hw->io_inuse);
		io->state = EFCT_HW_IO_STATE_INUSE;
		io->sec_hio = sec_io;

		/*
		 * mark secondary XRI for second and subsequent data phase as
		 * quarantine
		 */
		if (io->xbusy)
			sec_io->quarantine = true;

		flags = io->sec_iparam.fcp_tgt.flags;
		if (io->xbusy)
			flags |= SLI4_IO_CONTINUATION;
		else
			flags &= ~SLI4_IO_CONTINUATION;

		io->tgt_wqe_timeout = io->sec_iparam.fcp_tgt.timeout;

		/* Complete (continue) TRECV IO */
		if (io->xbusy) {
			if (sli_fcp_cont_treceive64_wqe(&hw->sli,
							io->wqe.wqebuf,
							hw->sli.wqe_size,
				 &io->def_sgl, io->first_data_sge,
				 io->sec_iparam.fcp_tgt.offset, io->sec_len,
				 io->indicator, io->sec_hio->indicator,
				 io->reqtag, SLI4_CQ_DEFAULT,
				 io->sec_iparam.fcp_tgt.ox_id,
				io->rnode->indicator, io->rnode->node_group,
				io->rnode->fc_id, flags,
				io->sec_iparam.fcp_tgt.dif_oper,
				 io->sec_iparam.fcp_tgt.blk_size,
				 io->sec_iparam.fcp_tgt.cs_ctl,
				 io->sec_iparam.fcp_tgt.app_id)) {
				efct_log_test(hw->os, "TRECEIVE WQE error\n");
				break;
			}
		}
		if (sli_fcp_treceive64_wqe(&hw->sli, io->wqe.wqebuf,
					   hw->sli.wqe_size, &io->def_sgl,
			 io->first_data_sge,
			 io->sec_iparam.fcp_tgt.offset, io->sec_len,
			 io->indicator, io->reqtag, SLI4_CQ_DEFAULT,
			 io->sec_iparam.fcp_tgt.ox_id,
			io->rnode->indicator, io->rnode->node_group,
			io->rnode->fc_id, flags,
			 io->sec_iparam.fcp_tgt.dif_oper,
			 io->sec_iparam.fcp_tgt.blk_size,
			 io->sec_iparam.fcp_tgt.cs_ctl,
			 io->sec_iparam.fcp_tgt.app_id)) {
			efct_log_test(hw->os, "TRECEIVE WQE error\n");
			break;
		}

		if (!io->wq) {
			io->wq = efct_hw_queue_next_wq(hw, io);
			efct_hw_assert(io->wq);
		}
		io->xbusy = true;

		/*
		 * Add IO to active io wqe list before submitting, in case the
		 * wcqe processing preempts this thread.
		 */
		efct_hw_add_io_timed_wqe(hw, io);
		rc = efct_hw_wq_write(io->wq, &io->wqe);
		if (rc >= 0) {
			/* non-negative return is success */
			rc = 0;
		} else {
			/* failed to write wqe, remove from active wqe list */
			efct_log_err(hw->os, "sli_queue_write failed: %d\n",
				     rc);
			io->xbusy = false;
			efct_hw_remove_io_timed_wqe(hw, io);
		}
	}
}

/**
 * @ingroup io
 * @brief Send a Single Request/Response Sequence (SRRS).
 *
 * @par Description
 * This routine supports communication sequences consisting of a single
 * request and single response between two endpoints. Examples include:
 *  - Sending an ELS request.
 *  - Sending an ELS response - To send an ELS response, the caller must provide
 * the OX_ID from the received request.
 *  - Sending a FC Common Transport (FC-CT) request - To send a FC-CT request,
 * the caller must provide the R_CTL, TYPE, and DF_CTL
 * values to place in the FC frame header.
 *  .
 * @n @b Note: The caller is expected to provide both send and receive
 * buffers for requests. In the case of sending a response, no receive buffer
 * is necessary and the caller may pass in a NULL pointer.
 *
 * @param hw Hardware context.
 * @param type Type of sequence (ELS request/response, FC-CT).
 * @param io Previously-allocated HW IO object.
 * @param send DMA memory holding data to send (for example, ELS request, BLS
 * response).
 * @param len Length, in bytes, of data to send.
 * @param receive Optional DMA memory to hold a response.
 * @param rnode Destination of data (that is, a remote node).
 * @param iparam IO parameters (ELS response and FC-CT).
 * @param cb Function call upon completion of sending the data (may be NULL).
 * @param arg Argument to pass to IO completion function.
 *
 * @return Returns 0 on success, or a non-zero on failure.
 */
enum efct_hw_rtn_e
efct_hw_srrs_send(struct efct_hw_s *hw, enum efct_hw_io_type_e type,
		  struct efct_hw_io_s *io,
		  struct efc_dma_s *send, u32 len,
		  struct efc_dma_s *receive, struct efc_remote_node_s *rnode,
		  union efct_hw_io_param_u *iparam,
		  efct_hw_srrs_cb_t cb, void *arg)
{
	struct sli4_sge_s	*sge = NULL;
	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_SUCCESS;
	u16	local_flags = 0;
	u32 sge0_flags;
	u32 sge1_flags;

	if (!hw || !io || !rnode || !iparam) {
		efct_log_err(NULL,
			     "bad parm hw=%p io=%p s=%p r=%p rn=%p iparm=%p\n",
			    hw, io, send, receive, rnode, iparam);
		return EFCT_HW_RTN_ERROR;
	}

	if (hw->state != EFCT_HW_STATE_ACTIVE) {
		efct_log_test(hw->os,
			      "cannot send SRRS, HW state=%d\n", hw->state);
		return EFCT_HW_RTN_ERROR;
	}

	if (efct_hw_is_xri_port_owned(hw, io->indicator)) {
		/* We must set the XC bit for port owned XRIs */
		local_flags |= SLI4_IO_CONTINUATION;
	}
	io->rnode = rnode;
	io->type  = type;
	io->done = cb;
	io->arg  = arg;

	sge = io->sgl->virt;

	/* clear both SGE */
	memset(io->sgl->virt, 0, 2 * sizeof(struct sli4_sge_s));

	sge0_flags = sge[0].dw2_flags;
	sge1_flags = sge[1].dw2_flags;
	if (send) {
		sge[0].buffer_address_high =
			cpu_to_le32(upper_32_bits(send->phys));
		sge[0].buffer_address_low  =
			cpu_to_le32(lower_32_bits(send->phys));

		sge0_flags |= (SLI4_SGE_TYPE_DATA << 27);

		sge[0].buffer_length = cpu_to_le32(len);
	}

	if (type == EFCT_HW_ELS_REQ || type == EFCT_HW_FC_CT) {
		sge[1].buffer_address_high =
			cpu_to_le32(upper_32_bits(receive->phys));
		sge[1].buffer_address_low  =
			cpu_to_le32(lower_32_bits(receive->phys));

		sge1_flags |= (SLI4_SGE_TYPE_DATA << 27);
		sge1_flags |= SLI4_SGE_LAST;

		sge[1].buffer_length = cpu_to_le32(receive->size);
	} else {
		sge0_flags |= SLI4_SGE_LAST;
	}

	sge[0].dw2_flags = cpu_to_le32(sge0_flags);
	sge[1].dw2_flags = cpu_to_le32(sge1_flags);

	switch (type) {
	case EFCT_HW_ELS_REQ:
		if (!send ||
		    sli_els_request64_wqe(&hw->sli, io->wqe.wqebuf,
					  hw->sli.wqe_size, io->sgl,
					*((u8 *)send->virt),
					len, receive->size,
					iparam->els.timeout,
					io->indicator, io->reqtag,
					SLI4_CQ_DEFAULT, rnode->indicator,
					rnode->sport->indicator,
					rnode->node_group, rnode->attached,
					rnode->fc_id, rnode->sport->fc_id)) {
			efct_log_err(hw->os, "REQ WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_ELS_RSP:
		if (!send ||
		    sli_xmit_els_rsp64_wqe(&hw->sli, io->wqe.wqebuf,
					   hw->sli.wqe_size, send, len,
					io->indicator, io->reqtag,
					SLI4_CQ_DEFAULT, iparam->els.ox_id,
					rnode->indicator,
					rnode->sport->indicator,
					rnode->node_group, rnode->attached,
					rnode->fc_id,
					local_flags, U32_MAX)) {
			efct_log_err(hw->os, "RSP WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_ELS_RSP_SID:
		if (!send ||
		    sli_xmit_els_rsp64_wqe(&hw->sli, io->wqe.wqebuf,
					   hw->sli.wqe_size, send, len,
					io->indicator, io->reqtag,
					SLI4_CQ_DEFAULT,
					iparam->els_sid.ox_id,
					rnode->indicator,
					rnode->sport->indicator,
					rnode->node_group, rnode->attached,
					rnode->fc_id,
					local_flags, iparam->els_sid.s_id)) {
			efct_log_err(hw->os, "RSP (SID) WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_FC_CT:
		if (!send ||
		    sli_gen_request64_wqe(&hw->sli, io->wqe.wqebuf,
					  hw->sli.wqe_size, io->sgl,
					len, receive->size,
					iparam->fc_ct.timeout, io->indicator,
					io->reqtag, SLI4_CQ_DEFAULT,
					rnode->node_group, rnode->fc_id,
					rnode->indicator,
					iparam->fc_ct.r_ctl,
					iparam->fc_ct.type,
					iparam->fc_ct.df_ctl)) {
			efct_log_err(hw->os, "GEN WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_FC_CT_RSP:
		if (!send ||
		    sli_xmit_sequence64_wqe(&hw->sli, io->wqe.wqebuf,
					    hw->sli.wqe_size, io->sgl,
					len, iparam->fc_ct_rsp.timeout,
					iparam->fc_ct_rsp.ox_id,
					io->indicator, io->reqtag,
					rnode->node_group, rnode->fc_id,
					rnode->indicator,
					iparam->fc_ct_rsp.r_ctl,
					iparam->fc_ct_rsp.type,
					iparam->fc_ct_rsp.df_ctl)) {
			efct_log_err(hw->os, "XMIT SEQ WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_BLS_ACC:
	case EFCT_HW_BLS_RJT:
	{
		struct sli_bls_payload_s	bls;

		if (type == EFCT_HW_BLS_ACC) {
			bls.type = SLI4_SLI_BLS_ACC;
			memcpy(&bls.u.acc, iparam->bls.payload,
			       sizeof(bls.u.acc));
		} else {
			bls.type = SLI4_SLI_BLS_RJT;
			memcpy(&bls.u.rjt, iparam->bls.payload,
			       sizeof(bls.u.rjt));
		}

		bls.ox_id = cpu_to_le16(iparam->bls.ox_id);
		bls.rx_id = cpu_to_le16(iparam->bls.rx_id);

		if (sli_xmit_bls_rsp64_wqe(&hw->sli, io->wqe.wqebuf,
					   hw->sli.wqe_size, &bls,
					io->indicator, io->reqtag,
					SLI4_CQ_DEFAULT,
					rnode->attached, rnode->node_group,
					rnode->indicator,
					rnode->sport->indicator,
					rnode->fc_id, rnode->sport->fc_id,
					U32_MAX)) {
			efct_log_err(hw->os, "XMIT_BLS_RSP64 WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	}
	case EFCT_HW_BLS_ACC_SID:
	{
		struct sli_bls_payload_s	bls;

		bls.type = SLI4_SLI_BLS_ACC;
		memcpy(&bls.u.acc, iparam->bls_sid.payload,
		       sizeof(bls.u.acc));

		bls.ox_id = cpu_to_le16(iparam->bls_sid.ox_id);
		bls.rx_id = cpu_to_le16(iparam->bls_sid.rx_id);

		if (sli_xmit_bls_rsp64_wqe(&hw->sli, io->wqe.wqebuf,
					   hw->sli.wqe_size, &bls,
					io->indicator, io->reqtag,
					SLI4_CQ_DEFAULT,
					rnode->attached, rnode->node_group,
					rnode->indicator,
					rnode->sport->indicator,
					rnode->fc_id, rnode->sport->fc_id,
					iparam->bls_sid.s_id)) {
			efct_log_err(hw->os, "XMIT_BLS_RSP64 WQE SID error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	}
	default:
		efct_log_err(hw->os, "bad SRRS type %#x\n", type);
		rc = EFCT_HW_RTN_ERROR;
	}

	if (rc == EFCT_HW_RTN_SUCCESS) {
		if (!io->wq) {
			io->wq = efct_hw_queue_next_wq(hw, io);
			efct_hw_assert(io->wq);
		}
		io->xbusy = true;

		/*
		 * Add IO to active io wqe list before submitting, in case the
		 * wcqe processing preempts this thread.
		 */
		io->wq->use_count++;
		efct_hw_add_io_timed_wqe(hw, io);
		rc = efct_hw_wq_write(io->wq, &io->wqe);
		if (rc >= 0) {
			/* non-negative return is success */
			rc = 0;
		} else {
			/* failed to write wqe, remove from active wqe list */
			efct_log_err(hw->os,
				     "sli_queue_write failed: %d\n", rc);
			io->xbusy = false;
			efct_hw_remove_io_timed_wqe(hw, io);
		}
	}

	return rc;
}

/**
 * @ingroup io
 * @brief Send a read, write, or response IO.
 *
 * @par Description
 * This routine supports sending a higher-level IO (for example, FCP) between
 * two endpoints as a target or initiator. Examples include:
 *  - Sending read data and good response (target).
 *  - Sending a response (target with no data or after receiving write data).
 *  .
 * This routine assumes all IOs use the SGL associated with the HW IO. Prior to
 * calling this routine, the data should be loaded using efct_hw_io_add_sge().
 *
 * @param hw Hardware context.
 * @param type Type of IO (target read, target response, and so on).
 * @param io Previously-allocated HW IO object.
 * @param len Length, in bytes, of data to send.
 * @param iparam IO parameters.
 * @param rnode Destination of data (that is, a remote node).
 * @param cb Function call upon completion of sending data (may be NULL).
 * @param arg Argument to pass to IO completion function.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 *
 */
enum efct_hw_rtn_e
efct_hw_io_send(struct efct_hw_s *hw, enum efct_hw_io_type_e type,
		struct efct_hw_io_s *io,
		u32 len, union efct_hw_io_param_u *iparam,
		struct efc_remote_node_s *rnode, void *cb, void *arg)
{
	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_SUCCESS;
	u32	rpi;
	unsigned long	lock_flags = 0;
	bool send_wqe = true;

	CPUTRACE(hw->os, "io send");

	if (!hw || !io || !rnode || !iparam) {
		efct_log_err(NULL, "bad parm hw=%p io=%p iparam=%p rnode=%p\n",
			     hw, io, iparam, rnode);
		return EFCT_HW_RTN_ERROR;
	}

	if (hw->state != EFCT_HW_STATE_ACTIVE) {
		efct_log_err(hw->os, "cannot send IO, HW state=%d\n",
			     hw->state);
		return EFCT_HW_RTN_ERROR;
	}

	rpi = rnode->indicator;

	if (hw->workaround.use_unregistered_rpi && rpi == U32_MAX) {
		rpi = hw->workaround.unregistered_rid;
		efct_log_test(hw->os, "using unregistered RPI: %d\n", rpi);
	}

	/*
	 * Save state needed during later stages
	 */
	io->rnode = rnode;
	io->type  = type;
	io->done  = cb;
	io->arg   = arg;

	/*
	 * Format the work queue entry used to send the IO
	 */
	switch (type) {
	case EFCT_HW_IO_INITIATOR_READ:
		/*
		 * If use_dif_quarantine workaround is in effect, and
		 * dif_separates then mark the initiator read IO for quarantine
		 */
		if (hw->workaround.use_dif_quarantine &&
		    hw->config.dif_mode == EFCT_HW_DIF_MODE_SEPARATE &&
		    iparam->fcp_tgt.dif_oper != EFCT_HW_DIF_OPER_DISABLED)
			io->quarantine = true;

		efct_hw_io_ini_sge(hw, io, iparam->fcp_ini.cmnd,
				   iparam->fcp_ini.cmnd_size,
				   iparam->fcp_ini.rsp);

		if (sli_fcp_iread64_wqe(&hw->sli, io->wqe.wqebuf,
					hw->sli.wqe_size, &io->def_sgl,
					io->first_data_sge, len, io->indicator,
					io->reqtag, SLI4_CQ_DEFAULT, rpi,
					rnode->node_group, rnode->fc_id,
					iparam->fcp_ini.dif_oper,
					iparam->fcp_ini.blk_size,
					iparam->fcp_ini.timeout)) {
			efct_log_err(hw->os, "IREAD WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_IO_INITIATOR_WRITE:
		efct_hw_io_ini_sge(hw, io, iparam->fcp_ini.cmnd,
				   iparam->fcp_ini.cmnd_size,
				   iparam->fcp_ini.rsp);

		if (sli_fcp_iwrite64_wqe(&hw->sli, io->wqe.wqebuf,
					 hw->sli.wqe_size,
					&io->def_sgl, io->first_data_sge,
					len, iparam->fcp_ini.first_burst,
					io->indicator, io->reqtag,
					SLI4_CQ_DEFAULT, rpi,
					rnode->node_group, rnode->fc_id,
					iparam->fcp_ini.dif_oper,
					iparam->fcp_ini.blk_size,
					iparam->fcp_ini.timeout)) {
			efct_log_err(hw->os, "IWRITE WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_IO_INITIATOR_NODATA:
		efct_hw_io_ini_sge(hw, io, iparam->fcp_ini.cmnd,
				   iparam->fcp_ini.cmnd_size,
				   iparam->fcp_ini.rsp);

		if (sli_fcp_icmnd64_wqe(&hw->sli, io->wqe.wqebuf,
					hw->sli.wqe_size, &io->def_sgl,
					io->indicator, io->reqtag,
					SLI4_CQ_DEFAULT, rpi, rnode->node_group,
				rnode->fc_id, iparam->fcp_ini.timeout)) {
			efct_log_err(hw->os, "ICMND WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}
		break;
	case EFCT_HW_IO_TARGET_WRITE: {
		u16 flags = iparam->fcp_tgt.flags;
		struct fcp_xfer_rdy_iu_s *xfer = io->xfer_rdy.virt;

		/*
		 * Fill in the XFER_RDY for IF_TYPE 0 devices
		 */
		*((__be32 *)xfer->fcp_data_ro) =
					 cpu_to_be32(iparam->fcp_tgt.offset);
		*((__be32 *)xfer->fcp_burst_len) = cpu_to_be32(len);
		*((u32 *)xfer->rsvd) = 0;

		if (io->xbusy)
			flags |= SLI4_IO_CONTINUATION;
		else
			flags &= ~SLI4_IO_CONTINUATION;

		io->tgt_wqe_timeout = iparam->fcp_tgt.timeout;

		/*
		 * If use_dif_quarantine workaround is in effect, and this is a
		 * DIF enabled IO then mark the target write IO for quarantine
		 */
		if (hw->workaround.use_dif_quarantine &&
		    hw->config.dif_mode == EFCT_HW_DIF_MODE_SEPARATE &&
		    iparam->fcp_tgt.dif_oper != EFCT_HW_DIF_OPER_DISABLED)
			io->quarantine = true;

		/*
		 * Check for use_dif_sec_xri workaround.  Note, even though the
		 * first dataphase doesn't really need a secondary XRI, we
		 * allocate one anyway, as this avoids the potential for
		 * deadlock where all XRI's are allocated as primaries to IOs
		 * that are on hw->sec_hio_wait_list.   If this secondary XRI
		 * is not for the first data phase, it is marked for quarantine.
		 */
		if (hw->workaround.use_dif_sec_xri &&
		    iparam->fcp_tgt.dif_oper != EFCT_HW_DIF_OPER_DISABLED) {
			io->sec_hio = efct_hw_io_alloc(hw);
			if (!io->sec_hio) {
				/*
				 * Failed to allocate, so save full request
				 * context and put this IO on the wait list
				 */
				io->sec_iparam = *iparam;
				io->sec_len = len;
				spin_lock_irqsave(&hw->io_lock, lock_flags);
				list_del(&io->list_entry);
				INIT_LIST_HEAD(&io->list_entry);
				list_add_tail(&io->list_entry,
					      &hw->sec_hio_wait_list);
				io->state = EFCT_HW_IO_STATE_WAIT_SEC_HIO;
				hw->sec_hio_wait_count++;
				spin_unlock_irqrestore(&hw->io_lock,
						       lock_flags);
				send_wqe = false;
				/* Done */
				break;
			}
			/*
			 * We quarantine the secondary IO if this is the second
			 * or subsequent data phase
			 */
			if (io->xbusy)
				io->sec_hio->quarantine = true;
		}

		/*
		 * If not the first data phase, and io->sec_hio has been
		 * allocated, then issue FCP_CONT_TRECEIVE64 WQE, otherwise use
		 * the usual FCP_TRECEIVE64 WQE
		 */
		if (io->xbusy && io->sec_hio) {
			if (sli_fcp_cont_treceive64_wqe(&hw->sli,
							io->wqe.wqebuf,
						hw->sli.wqe_size,
						&io->def_sgl,
						io->first_data_sge,
						iparam->fcp_tgt.offset, len,
						io->indicator,
						io->sec_hio->indicator,
						io->reqtag, SLI4_CQ_DEFAULT,
						iparam->fcp_tgt.ox_id, rpi,
						rnode->node_group, rnode->fc_id,
						flags, iparam->fcp_tgt.dif_oper,
						iparam->fcp_tgt.blk_size,
						iparam->fcp_tgt.cs_ctl,
						iparam->fcp_tgt.app_id)) {
				efct_log_err(hw->os, "TRECEIVE WQE error\n");
				rc = EFCT_HW_RTN_ERROR;
			}
		} else {
			if (sli_fcp_treceive64_wqe(&hw->sli,
						   io->wqe.wqebuf,
						   hw->sli.wqe_size,
						   &io->def_sgl,
						   io->first_data_sge,
						   iparam->fcp_tgt.offset, len,
						   io->indicator, io->reqtag,
						   SLI4_CQ_DEFAULT,
						   iparam->fcp_tgt.ox_id, rpi,
						rnode->node_group,
						rnode->fc_id, flags,
						   iparam->fcp_tgt.dif_oper,
						   iparam->fcp_tgt.blk_size,
						   iparam->fcp_tgt.cs_ctl,
						   iparam->fcp_tgt.app_id)) {
				efct_log_err(hw->os, "TRECEIVE WQE error\n");
				rc = EFCT_HW_RTN_ERROR;
			}
		}
		break;
	}
	case EFCT_HW_IO_TARGET_READ: {
		u16 flags = iparam->fcp_tgt.flags;

		if (io->xbusy)
			flags |= SLI4_IO_CONTINUATION;
		else
			flags &= ~SLI4_IO_CONTINUATION;

		io->tgt_wqe_timeout = iparam->fcp_tgt.timeout;
		if (sli_fcp_tsend64_wqe(&hw->sli, io->wqe.wqebuf,
					hw->sli.wqe_size, &io->def_sgl,
					io->first_data_sge,
					iparam->fcp_tgt.offset, len,
					io->indicator, io->reqtag,
					SLI4_CQ_DEFAULT, iparam->fcp_tgt.ox_id,
					rpi, rnode->node_group,
					rnode->fc_id, flags,
					iparam->fcp_tgt.dif_oper,
					iparam->fcp_tgt.blk_size,
					iparam->fcp_tgt.cs_ctl,
					iparam->fcp_tgt.app_id)) {
			efct_log_err(hw->os, "TSEND WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		} else if (hw->workaround.retain_tsend_io_length) {
			io->length = len;
		}
		break;
	}
	case EFCT_HW_IO_TARGET_RSP: {
		u16 flags = iparam->fcp_tgt.flags;

		if (io->xbusy)
			flags |= SLI4_IO_CONTINUATION;
		else
			flags &= ~SLI4_IO_CONTINUATION;

		/* post a new auto xfer ready buffer */
		if (hw->auto_xfer_rdy_enabled && io->is_port_owned) {
			io->auto_xfer_rdy_dnrx =
				 efct_hw_rqpair_auto_xfer_rdy_buffer_post(hw,
									  io,
									  true);
			if (io->auto_xfer_rdy_dnrx)
				flags |= SLI4_IO_DNRX;
		}

		io->tgt_wqe_timeout = iparam->fcp_tgt.timeout;
		if (sli_fcp_trsp64_wqe(&hw->sli, io->wqe.wqebuf,
				       hw->sli.wqe_size, &io->def_sgl,
				       len, io->indicator, io->reqtag,
				       SLI4_CQ_DEFAULT, iparam->fcp_tgt.ox_id,
					rpi, rnode->node_group, rnode->fc_id,
					flags, iparam->fcp_tgt.cs_ctl,
				       io->is_port_owned,
				       iparam->fcp_tgt.app_id)) {
			efct_log_err(hw->os, "TRSP WQE error\n");
			rc = EFCT_HW_RTN_ERROR;
		}

		break;
	}
	default:
		efct_log_err(hw->os, "unsupported IO type %#x\n", type);
		rc = EFCT_HW_RTN_ERROR;
	}

	if (send_wqe && rc == EFCT_HW_RTN_SUCCESS) {
		if (!io->wq) {
			io->wq = efct_hw_queue_next_wq(hw, io);
			efct_hw_assert(io->wq);
		}

		io->xbusy = true;

		/*
		 * Add IO to active io wqe list before submitting, in case the
		 * wcqe processing preempts this thread.
		 */
		hw->tcmd_wq_submit[io->wq->instance]++;
		io->wq->use_count++;
		efct_hw_add_io_timed_wqe(hw, io);
		rc = efct_hw_wq_write(io->wq, &io->wqe);
		if (rc >= 0) {
			/* non-negative return is success */
			rc = 0;
		} else {
			/* failed to write wqe, remove from active wqe list */
			efct_log_err(hw->os,
				     "sli_queue_write failed: %d\n", rc);
			io->xbusy = false;
			efct_hw_remove_io_timed_wqe(hw, io);
		}
	}

	return rc;
}

/**
 * @brief Send a raw frame
 *
 * @par Description
 * Using the SEND_FRAME_WQE, a frame consisting of header and payload is sent.
 *
 * @param hw Pointer to HW object.
 * @param hdr Pointer to a little endian formatted FC header.
 * @param sof Value to use as the frame SOF.
 * @param eof Value to use as the frame EOF.
 * @param payload Pointer to payload DMA buffer.
 * @param ctx Pointer to caller provided send frame context.
 * @param callback Callback function.
 * @param arg Callback function argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
enum efct_hw_rtn_e
efct_hw_send_frame(struct efct_hw_s *hw, struct fc_header_le_s *hdr,
		   u8 sof, u8 eof, struct efc_dma_s *payload,
		   struct efct_hw_send_frame_context_s *ctx,
		   void (*callback)(void *arg, u8 *cqe, int status),
		   void *arg)
{
	int rc;
	struct efct_hw_wqe_s *wqe;
	u32 xri;
	struct hw_wq_s *wq;

	wqe = &ctx->wqe;

	/* populate the callback object */
	ctx->hw = hw;

	/* Fetch and populate request tag */
	ctx->wqcb = efct_hw_reqtag_alloc(hw, callback, arg);
	if (!ctx->wqcb) {
		efct_log_err(hw->os, "can't allocate request tag\n");
		return EFCT_HW_RTN_NO_RESOURCES;
	}

	/* Choose a work queue, first look for a class[1] wq, otherwise just
	 * use wq[0]
	 */
	wq = efct_varray_iter_next(hw->wq_class_array[1]);
	if (!wq)
		wq = hw->hw_wq[0];

	/* Set XRI and RX_ID in the header based on which WQ, and which
	 * send_frame_io we are using
	 */
	xri = wq->send_frame_io->indicator;

	/* Build the send frame WQE */
	rc = sli_send_frame_wqe(&hw->sli, wqe->wqebuf,
				hw->sli.wqe_size, sof, eof,
				(u32 *)hdr, payload, payload->len,
				EFCT_HW_SEND_FRAME_TIMEOUT, xri,
				ctx->wqcb->instance_index);
	if (rc) {
		efct_log_err(hw->os, "sli_send_frame_wqe failed: %d\n",
			     rc);
		return EFCT_HW_RTN_ERROR;
	}

	/* Write to WQ */
	rc = efct_hw_wq_write(wq, wqe);
	if (rc) {
		efct_log_err(hw->os, "efct_hw_wq_write failed: %d\n", rc);
		return EFCT_HW_RTN_ERROR;
	}

	wq->use_count++;

	return rc ? EFCT_HW_RTN_ERROR : EFCT_HW_RTN_SUCCESS;
}

enum efct_hw_rtn_e
efct_hw_io_register_sgl(struct efct_hw_s *hw, struct efct_hw_io_s *io,
			struct efc_dma_s *sgl,
			u32 sgl_count)
{
	if (hw->sli.sgl_pre_registered) {
		efct_log_err(hw->os,
			     "can't use temp SGL with pre-registered SGLs\n");
		return EFCT_HW_RTN_ERROR;
	}
	io->ovfl_sgl = sgl;
	io->ovfl_sgl_count = sgl_count;

	return EFCT_HW_RTN_SUCCESS;
}

static void
efct_hw_io_restore_sgl(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	/* Restore the default */
	io->sgl = &io->def_sgl;
	io->sgl_count = io->def_sgl_count;

	/* Clear the overflow SGL */
	io->ovfl_sgl = NULL;
	io->ovfl_sgl_count = 0;
	io->ovfl_lsp = NULL;
}

/**
 * @ingroup io
 * @brief Initialize the scatter gather list entries of an IO.
 *
 * @param hw Hardware context.
 * @param io Previously-allocated HW IO object.
 * @param type Type of IO (target read, target response, and so on).
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_io_init_sges(struct efct_hw_s *hw, struct efct_hw_io_s *io,
		     enum efct_hw_io_type_e type)
{
	struct sli4_sge_s	*data = NULL;
	u32	i = 0;
	u32	skips = 0;
	u32 sge_flags = 0;

	if (!hw || !io) {
		efct_log_err(hw->os,
			     "bad parameter hw=%p io=%p\n", hw, io);
		return EFCT_HW_RTN_ERROR;
	}

	/* Clear / reset the scatter-gather list */
	io->sgl = &io->def_sgl;
	io->sgl_count = io->def_sgl_count;
	io->first_data_sge = 0;

	memset(io->sgl->virt, 0, 2 * sizeof(struct sli4_sge_s));
	io->n_sge = 0;
	io->sge_offset = 0;

	io->type = type;

	data = io->sgl->virt;

	/*
	 * Some IO types have underlying hardware requirements on the order
	 * of SGEs. Process all special entries here.
	 */
	switch (type) {
	case EFCT_HW_IO_INITIATOR_READ:
	case EFCT_HW_IO_INITIATOR_WRITE:
	case EFCT_HW_IO_INITIATOR_NODATA:
		/*
		 * No skips, 2 special for initiator I/Os
		 * The addresses and length are written later
		 */
		/* setup command pointer */
		sge_flags = data->dw2_flags;
		sge_flags &= ~SLI4_SGE_TYPE;
		sge_flags |= (SLI4_SGE_TYPE_DATA << 27);
		data->dw2_flags = cpu_to_le32(sge_flags);
		data++;

		/* setup response pointer */
		sge_flags = data->dw2_flags;
		sge_flags &= ~SLI4_SGE_TYPE;
		sge_flags |= (SLI4_SGE_TYPE_DATA << 27);

		if (type == EFCT_HW_IO_INITIATOR_NODATA)
			sge_flags |= SLI4_SGE_LAST;

		data->dw2_flags = cpu_to_le32(sge_flags);
		data++;

		io->n_sge = 2;
		break;
	case EFCT_HW_IO_TARGET_WRITE:
#define EFCT_TARGET_WRITE_SKIPS	2
		skips = EFCT_TARGET_WRITE_SKIPS;

		/* populate host resident XFER_RDY buffer */
		sge_flags = data->dw2_flags;
		sge_flags &= (~SLI4_SGE_TYPE);
		sge_flags |= (SLI4_SGE_TYPE_DATA << 27);
		data->buffer_address_high =
			cpu_to_le32(upper_32_bits(io->xfer_rdy.phys));
		data->buffer_address_low  =
			cpu_to_le32(lower_32_bits(io->xfer_rdy.phys));
		data->buffer_length = cpu_to_le32(io->xfer_rdy.size);
		data->dw2_flags = cpu_to_le32(sge_flags);
		data++;

		skips--;

		io->n_sge = 1;
		break;
	case EFCT_HW_IO_TARGET_READ:
		/*
		 * For FCP_TSEND64, the first 2 entries are SKIP SGE's
		 */
#define EFCT_TARGET_READ_SKIPS	2
		skips = EFCT_TARGET_READ_SKIPS;
		break;
	case EFCT_HW_IO_TARGET_RSP:
		/*
		 * No skips, etc. for FCP_TRSP64
		 */
		break;
	default:
		efct_log_err(hw->os, "unsupported IO type %#x\n", type);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * Write skip entries
	 */
	for (i = 0; i < skips; i++) {
		sge_flags = data->dw2_flags;
		sge_flags &= (~SLI4_SGE_TYPE);
		sge_flags |= (SLI4_SGE_TYPE_SKIP << 27);
		data->dw2_flags = cpu_to_le32(sge_flags);
		data++;
	}

	io->n_sge += skips;

	/*
	 * Set last
	 */
	sge_flags = data->dw2_flags;
	sge_flags |= SLI4_SGE_LAST;
	data->dw2_flags = cpu_to_le32(sge_flags);

	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup io
 * @brief Add a T10 PI seed scatter gather list entry.
 *
 * @param hw Hardware context.
 * @param io Previously-allocated HW IO object.
 * @param dif_info Pointer to T10 DIF fields, or NULL if no DIF.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_io_add_seed_sge(struct efct_hw_s *hw, struct efct_hw_io_s *io,
			struct efct_hw_dif_info_s *dif_info)
{
	struct sli4_sge_s	*data = NULL;
	struct sli4_diseed_sge_s *dif_seed;
	u32 sge_flags;
	u16 dif_flags;

	/* If no dif_info, or dif_oper is disabled, then just return success */
	if (!dif_info ||
	    dif_info->dif_oper == EFCT_HW_DIF_OPER_DISABLED)
		return EFCT_HW_RTN_SUCCESS;

	if (!hw || !io) {
		efct_log_err(hw->os,
			     "bad parameter hw=%p io=%p dif_info=%p\n", hw,
			    io, dif_info);
		return EFCT_HW_RTN_ERROR;
	}

	data = io->sgl->virt;
	data += io->n_sge;

	/* If we are doing T10 DIF add the DIF Seed SGE */
	memset(data, 0, sizeof(struct sli4_diseed_sge_s));
	dif_seed = (struct sli4_diseed_sge_s *)data;

	dif_seed->ref_tag_cmp = cpu_to_le32(dif_info->ref_tag_cmp);
	dif_seed->ref_tag_repl = cpu_to_le32(dif_info->ref_tag_repl);
	dif_seed->app_tag_repl = cpu_to_le16(dif_info->app_tag_repl);

	dif_flags = 0;
	if (dif_info->repl_app_tag)
		dif_flags |= SLI4_DISEED_SGE_REPLAPPTAG;

	if (hw->sli.if_type != SLI4_INTF_IF_TYPE_2) {
		if (dif_info->disable_app_ref_ffff)
			dif_flags |= SLI4_DISEED_SGE_ATRT;

		if (dif_info->disable_app_ffff)
			dif_flags |= SLI4_DISEED_SGE_AT;
	}
	dif_flags |= SLI4_SGE_TYPE_DISEED << 11;

	if ((io->type == EFCT_HW_IO_TARGET_WRITE ||
	     io->type == EFCT_HW_IO_INITIATOR_READ) &&
	    hw->sli.if_type != SLI4_INTF_IF_TYPE_2 &&
	    dif_info->dif_separate) {
		dif_flags &= ~SLI4_DISEED_SGE_SGETYPE;
		dif_flags |= SLI4_SGE_TYPE_SKIP << 11;
	}

	dif_seed->dw2w1_flags = cpu_to_le16(dif_flags);
	dif_seed->app_tag_cmp = cpu_to_le16(dif_info->app_tag_cmp);

	dif_flags = 0;
	dif_flags |= (dif_info->blk_size & SLI4_DISEED_SGE_DIFBLKSIZE);
	if (dif_info->auto_incr_ref_tag)
		dif_flags |= SLI4_DISEED_SGE_AUTOINCRREFTAG;
	if (dif_info->check_app_tag)
		dif_flags |= SLI4_DISEED_SGE_CHKAPPTAG;
	if (dif_info->check_ref_tag)
		dif_flags |= SLI4_DISEED_SGE_CHKREFTAG;
	if (dif_info->check_guard)
		dif_flags |= SLI4_DISEED_SGE_CHKCRC;
	if (dif_info->repl_ref_tag)
		dif_flags |= SLI4_DISEED_SGE_NEWREFTAG;

	switch (dif_info->dif_oper) {
	case EFCT_HW_SGE_DIFOP_INNODIFOUTCRC:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_NODIF_OUT_CRC << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_NODIF_OUT_CRC << 12);
		break;
	case EFCT_HW_SGE_DIFOP_INCRCOUTNODIF:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CRC_OUT_NODIF << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CRC_OUT_NODIF << 12);
		break;
	case EFCT_HW_SGE_DIFOP_INNODIFOUTCHKSUM:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_NODIF_OUT_CHKSUM << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_NODIF_OUT_CHKSUM << 12);
		break;
	case EFCT_HW_SGE_DIFOP_INCHKSUMOUTNODIF:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_NODIF << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_NODIF << 12);
		break;
	case EFCT_HW_SGE_DIFOP_INCRCOUTCRC:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CRC_OUT_CRC << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CRC_OUT_CRC << 12);
		break;
	case EFCT_HW_SGE_DIFOP_INCHKSUMOUTCHKSUM:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_CHKSUM << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_CHKSUM << 12);
		break;
	case EFCT_HW_SGE_DIFOP_INCRCOUTCHKSUM:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CRC_OUT_CHKSUM << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CRC_OUT_CHKSUM << 12);
		break;
	case EFCT_HW_SGE_DIFOP_INCHKSUMOUTCRC:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_CRC << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_CRC << 12);
		break;
	case EFCT_HW_SGE_DIFOP_INRAWOUTRAW:
		dif_flags |= (SLI4_SGE_DIF_OP_IN_RAW_OUT_RAW << 8);
		dif_flags |= (SLI4_SGE_DIF_OP_IN_RAW_OUT_RAW << 12);
		break;
	default:
		efct_log_err(hw->os, "unsupported DIF operation %#x\n",
			     dif_info->dif_oper);
		return EFCT_HW_RTN_ERROR;
	}

	dif_seed->dw3w1_flags = cpu_to_le16(dif_flags);
	/*
	 * Set last, clear previous last
	 */
	sge_flags = data->dw2_flags;
	sge_flags |= SLI4_SGE_LAST;
	data->dw2_flags = cpu_to_le32(sge_flags);
	if (io->n_sge) {
		sge_flags = data[-1].dw2_flags;
		sge_flags &= ~SLI4_SGE_LAST;
		data[-1].dw2_flags = cpu_to_le32(sge_flags);
	}

	io->n_sge++;

	return EFCT_HW_RTN_SUCCESS;
}

static enum efct_hw_rtn_e
efct_hw_io_overflow_sgl(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	struct sli4_lsp_sge_s *lsp;
	u32 dw2_flags = 0;

	/* fail if we're already pointing to the overflow SGL */
	if (io->sgl == io->ovfl_sgl)
		return EFCT_HW_RTN_ERROR;

	/* fail if we don't have an overflow SGL registered */
	if (!io->ovfl_sgl)
		return EFCT_HW_RTN_ERROR;

	/*
	 * Overflow, we need to put a link SGE in the last location of the
	 * current SGL, after copying the the last SGE to the overflow SGL
	 */

	((struct sli4_sge_s *)io->ovfl_sgl->virt)[0] =
			 ((struct sli4_sge_s *)io->sgl->virt)[io->n_sge - 1];

	lsp = &((struct sli4_lsp_sge_s *)io->sgl->virt)[io->n_sge - 1];
	memset(lsp, 0, sizeof(*lsp));

	lsp->buffer_address_high =
		cpu_to_le32(upper_32_bits(io->ovfl_sgl->phys));
	lsp->buffer_address_low  =
		cpu_to_le32(lower_32_bits(io->ovfl_sgl->phys));
	dw2_flags = SLI4_SGE_TYPE_LSP << 27;
	dw2_flags &= ~SLI4_LSP_SGE_LAST;
	lsp->dw2_flags = cpu_to_le32(dw2_flags);

	io->ovfl_lsp = lsp;
	io->ovfl_lsp->dw3_seglen =
		cpu_to_le32(sizeof(struct sli4_sge_s) &
			    SLI4_LSP_SGE_SEGLEN);

	/* Update the current SGL pointer, and n_sgl */
	io->sgl = io->ovfl_sgl;
	io->sgl_count = io->ovfl_sgl_count;
	io->n_sge = 1;

	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup io
 * @brief Add a scatter gather list entry to an IO.
 *
 * @param hw Hardware context.
 * @param io Previously-allocated HW IO object.
 * @param addr Physical address.
 * @param length Length of memory pointed to by @c addr.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_io_add_sge(struct efct_hw_s *hw, struct efct_hw_io_s *io,
		   uintptr_t addr, u32 length)
{
	struct sli4_sge_s	*data = NULL;
	u32 sge_flags = 0;

	if (!hw || !io || !addr || !length) {
		efct_log_err(hw->os,
			     "bad parameter hw=%p io=%p addr=%lx length=%u\n",
			    hw, io, addr, length);
		return EFCT_HW_RTN_ERROR;
	}

	if (length && (io->n_sge + 1) > io->sgl_count) {
		if (efct_hw_io_overflow_sgl(hw, io) != EFCT_HW_RTN_SUCCESS) {
			efct_log_err(hw->os, "SGL full (%d)\n", io->n_sge);
			return EFCT_HW_RTN_ERROR;
		}
	}

	if (length > hw->sli.sge_supported_length) {
		efct_log_err(hw->os,
			     "length of SGE %d bigger than allowed %d\n",
			    length, hw->sli.sge_supported_length);
		return EFCT_HW_RTN_ERROR;
	}

	data = io->sgl->virt;
	data += io->n_sge;

	sge_flags = data->dw2_flags;
	sge_flags &= ~SLI4_SGE_TYPE;
	sge_flags |= SLI4_SGE_TYPE_DATA << 27;
	sge_flags &= ~SLI4_SGE_DATA_OFFSET;
	sge_flags |= SLI4_SGE_DATA_OFFSET & io->sge_offset;

	data->buffer_address_high = cpu_to_le32(upper_32_bits(addr));
	data->buffer_address_low  = cpu_to_le32(lower_32_bits(addr));
	data->buffer_length = cpu_to_le32(length);

	/*
	 * Always assume this is the last entry and mark as such.
	 * If this is not the first entry unset the "last SGE"
	 * indication for the previous entry
	 */
	sge_flags |= SLI4_SGE_LAST;
	data->dw2_flags = cpu_to_le32(sge_flags);

	if (io->n_sge) {
		sge_flags = data[-1].dw2_flags;
		sge_flags &= ~SLI4_SGE_LAST;
		data[-1].dw2_flags = cpu_to_le32(sge_flags);
	}

	/* Set first_data_bde if not previously set */
	if (io->first_data_sge == 0)
		io->first_data_sge = io->n_sge;

	io->sge_offset += length;
	io->n_sge++;

	/* Update the linked segment length (only executed after overflow has
	 * begun)
	 */
	if (io->ovfl_lsp)
		io->ovfl_lsp->dw3_seglen =
			cpu_to_le32(io->n_sge * sizeof(struct sli4_sge_s) &
				    SLI4_LSP_SGE_SEGLEN);

	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup io
 * @brief Add a T10 DIF scatter gather list entry to an IO.
 *
 * @param hw Hardware context.
 * @param io Previously-allocated HW IO object.
 * @param addr DIF physical address.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_io_add_dif_sge(struct efct_hw_s *hw,
		       struct efct_hw_io_s *io, uintptr_t addr)
{
	struct sli4_dif_sge_s	*data = NULL;
	u32 sge_flags = 0;

	if (!hw || !io || !addr) {
		efct_log_err(hw->os,
			     "bad parameter hw=%p io=%p addr=%lx\n",
			    hw, io, addr);
		return EFCT_HW_RTN_ERROR;
	}

	if ((io->n_sge + 1) > hw->config.n_sgl) {
		if (efct_hw_io_overflow_sgl(hw, io) != EFCT_HW_RTN_ERROR) {
			efct_log_err(hw->os, "SGL full (%d)\n", io->n_sge);
			return EFCT_HW_RTN_ERROR;
		}
	}

	data = io->sgl->virt;
	data += io->n_sge;

	sge_flags = data->dw2_flags;
	sge_flags &= ~SLI4_SGE_TYPE;
	sge_flags |= SLI4_SGE_TYPE_DIF << 27;

	if ((io->type == EFCT_HW_IO_TARGET_WRITE ||
	     io->type == EFCT_HW_IO_INITIATOR_READ) &&
	    hw->sli.if_type != SLI4_INTF_IF_TYPE_2) {
		sge_flags &= ~SLI4_SGE_TYPE;
		sge_flags |= SLI4_SGE_TYPE_SKIP << 27;
	}

	data->buffer_address_high = cpu_to_le32(upper_32_bits(addr));
	data->buffer_address_low  = cpu_to_le32(lower_32_bits(addr));

	/*
	 * Always assume this is the last entry and mark as such.
	 * If this is not the first entry unset the "last SGE"
	 * indication for the previous entry
	 */
	sge_flags |= SLI4_SGE_LAST;
	data->dw2_flags = cpu_to_le32(sge_flags);
	if (io->n_sge) {
		sge_flags = data[-1].dw2_flags;
		sge_flags &= ~SLI4_SGE_LAST;
		data[-1].dw2_flags &= cpu_to_le32(sge_flags);
	}

	io->n_sge++;

	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @ingroup io
 * @brief Abort all previously-started IO's.
 *
 * @param hw Hardware context.
 *
 * @return Returns None.
 */

void
efct_hw_io_abort_all(struct efct_hw_s *hw)
{
	struct efct_hw_io_s *io_to_abort	= NULL;
	struct efct_hw_io_s *next_io		= NULL;

	list_for_each_entry_safe(io_to_abort, next_io,
				 &hw->io_inuse, list_entry) {
		efct_hw_io_abort(hw, io_to_abort, true, NULL, NULL);
	}
}

/**
 * @ingroup io
 * @brief Abort a previously-started IO.
 *
 * @param hw Hardware context.
 * @param io_to_abort The IO to abort.
 * @param send_abts Boolean to have the hardware automatically
 * generate an ABTS.
 * @param cb Function call upon completion of the abort (may be NULL).
 * @param arg Argument to pass to abort completion function.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_io_abort(struct efct_hw_s *hw, struct efct_hw_io_s *io_to_abort,
		 bool send_abts, void *cb, void *arg)
{
	enum sli4_abort_type_e atype = SLI_ABORT_MAX;
	u32	id = 0, mask = 0;
	enum efct_hw_rtn_e	rc = EFCT_HW_RTN_SUCCESS;
	struct hw_wq_callback_s *wqcb;
	unsigned long flags = 0;

	if (!hw || !io_to_abort) {
		efct_log_err(hw->os,
			     "bad parameter hw=%p io=%p\n",
			    hw, io_to_abort);
		return EFCT_HW_RTN_ERROR;
	}

	if (hw->state != EFCT_HW_STATE_ACTIVE) {
		efct_log_err(hw->os, "cannot send IO abort, HW state=%d\n",
			     hw->state);
		return EFCT_HW_RTN_ERROR;
	}

	/* take a reference on IO being aborted */
	if (kref_get_unless_zero(&io_to_abort->ref) == 0) {
		/* command no longer active */
		efct_log_test(hw->os,
			      "io not active xri=0x%x tag=0x%x\n",
			     io_to_abort->indicator, io_to_abort->reqtag);
		return EFCT_HW_RTN_IO_NOT_ACTIVE;
	}

	/* non-port owned XRI checks */
	/* Must have a valid WQ reference */
	if (!io_to_abort->wq) {
		efct_log_test(hw->os, "io_to_abort xri=0x%x not active on WQ\n",
			      io_to_abort->indicator);
		/* efct_ref_get(): same function */
		kref_put(&io_to_abort->ref, io_to_abort->release);
		return EFCT_HW_RTN_IO_NOT_ACTIVE;
	}

	/*
	 * Validation checks complete; now check to see if already being
	 * aborted
	 */
	spin_lock_irqsave(&hw->io_abort_lock, flags);
	if (io_to_abort->abort_in_progress) {
		spin_unlock_irqrestore(&hw->io_abort_lock, flags);
		/* efct_ref_get(): same function */
		kref_put(&io_to_abort->ref, io_to_abort->release);
		efct_log_debug(hw->os,
			       "io already being aborted xri=0x%x tag=0x%x\n",
			      io_to_abort->indicator, io_to_abort->reqtag);
		return EFCT_HW_RTN_IO_ABORT_IN_PROGRESS;
	}

	/*
	 * This IO is not already being aborted. Set flag so we won't try to
	 * abort it again. After all, we only have one abort_done callback.
	 */
	io_to_abort->abort_in_progress = true;
	spin_unlock_irqrestore(&hw->io_abort_lock, flags);

	/*
	 * If we got here, the possibilities are:
	 * - host owned xri
	 *	- io_to_abort->wq_index != U32_MAX
	 *		- submit ABORT_WQE to same WQ
	 * - port owned xri:
	 *	- rxri: io_to_abort->wq_index == U32_MAX
	 *		- submit ABORT_WQE to any WQ
	 *	- non-rxri
	 *		- io_to_abort->index != U32_MAX
	 *			- submit ABORT_WQE to same WQ
	 *		- io_to_abort->index == U32_MAX
	 *			- submit ABORT_WQE to any WQ
	 */
	io_to_abort->abort_done = cb;
	io_to_abort->abort_arg  = arg;

	atype = SLI_ABORT_XRI;
	id = io_to_abort->indicator;

	/* Allocate a request tag for the abort portion of this IO */
	wqcb = efct_hw_reqtag_alloc(hw, efct_hw_wq_process_abort, io_to_abort);
	if (!wqcb) {
		efct_log_err(hw->os, "can't allocate request tag\n");
		return EFCT_HW_RTN_NO_RESOURCES;
	}
	io_to_abort->abort_reqtag = wqcb->instance_index;

	/*
	 * If the wqe is on the pending list, then set this wqe to be
	 * aborted when the IO's wqe is removed from the list.
	 */
	if (io_to_abort->wq) {
		spin_lock_irqsave(&io_to_abort->wq->queue->lock, flags);
		if (io_to_abort->wqe.list_entry.next) {
			io_to_abort->wqe.abort_wqe_submit_needed = true;
			io_to_abort->wqe.send_abts = send_abts;
			io_to_abort->wqe.id = id;
			io_to_abort->wqe.abort_reqtag =
						 io_to_abort->abort_reqtag;
			spin_unlock_irqrestore(&io_to_abort->wq->queue->lock,
					       flags);
			return 0;
		}
		spin_unlock_irqrestore(&io_to_abort->wq->queue->lock, flags);
	}

	if (sli_abort_wqe(&hw->sli, io_to_abort->wqe.wqebuf,
			  hw->sli.wqe_size, atype, send_abts, id, mask,
			  io_to_abort->abort_reqtag, SLI4_CQ_DEFAULT)) {
		efct_log_err(hw->os, "ABORT WQE error\n");
		io_to_abort->abort_reqtag = U32_MAX;
		efct_hw_reqtag_free(hw, wqcb);
		rc = EFCT_HW_RTN_ERROR;
	}

	if (rc == EFCT_HW_RTN_SUCCESS) {
		if (!io_to_abort->wq) {
			io_to_abort->wq = efct_hw_queue_next_wq(hw,
								io_to_abort);
			efct_hw_assert(io_to_abort->wq);
		}
		/* ABORT_WQE does not actually utilize an XRI on the Port,
		 * therefore, keep xbusy as-is to track the exchange's state,
		 * not the ABORT_WQE's state
		 */
		rc = efct_hw_wq_write(io_to_abort->wq, &io_to_abort->wqe);
		if (rc > 0)
			/* non-negative return is success */
			rc = 0;
			/*
			 * can't abort an abort so skip adding to timed wqe
			 * list
			 */
	}

	if (rc != EFCT_HW_RTN_SUCCESS) {
		spin_lock_irqsave(&hw->io_abort_lock, flags);
		io_to_abort->abort_in_progress = false;
		spin_unlock_irqrestore(&hw->io_abort_lock, flags);
		/* efct_ref_get(): same function */
		kref_put(&io_to_abort->ref, io_to_abort->release);
	}
	return rc;
}

/**
 * @ingroup io
 * @brief Return the OX_ID/RX_ID of the IO.
 *
 * @param hw Hardware context.
 * @param io HW IO object.
 *
 * @return Returns X_ID on success, or -1 on failure.
 */
int
efct_hw_io_get_xid(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	if (!hw || !io) {
		efct_log_err(hw->os,
			     "bad parameter hw=%p io=%p\n", hw, io);
		return -1;
	}

	return io->indicator;
}

struct efct_hw_fw_wr_cb_arg {
	void (*cb)(int status, u32 bytes_written,
		   u32 change_status, void *arg);
	void *arg;
};

struct efct_hw_sfp_cb_arg {
	void (*cb)(int status, u32 bytes_written,
		   u32 *data, void *arg);
	void *arg;
	struct efc_dma_s payload;
};

struct efct_hw_temp_cb_arg {
	void (*cb)(int status, u32 curr_temp,
		   u32 crit_temp_thrshld,
		u32 warn_temp_thrshld,
		u32 norm_temp_thrshld,
		u32 fan_off_thrshld,
		u32 fan_on_thrshld,
		void *arg);
	void *arg;
};

struct efct_hw_link_stat_cb_arg {
	void (*cb)(int status,
		   u32 num_counters,
		struct efct_hw_link_stat_counts_s *counters,
		void *arg);
	void *arg;
};

struct efct_hw_host_stat_cb_arg {
	void (*cb)(int status,
		   u32 num_counters,
		struct efct_hw_host_stat_counts_s *counters,
		void *arg);
	void *arg;
};

struct efct_hw_dump_get_cb_arg {
	void (*cb)(int status,
		   u32 bytes_read,
		u8 eof, void *arg);
	void *arg;
	void *mbox_cmd;
};

struct efct_hw_dump_clear_cb_arg {
	void (*cb)(int status, void *arg);
	void *arg;
	void *mbox_cmd;
};

/**
 * @brief Write a portion of a firmware image to the device.
 *
 * @par Description
 * Calls the correct firmware write function based on the device type.
 *
 * @param hw Hardware context.
 * @param dma DMA structure containing the firmware image chunk.
 * @param size Size of the firmware image chunk.
 * @param offset Offset, in bytes, from the beginning of the firmware image.
 * @param last True if this is the last chunk of the image.
 * Causes the image to be committed to flash.
 * @param cb Pointer to a callback function that is called when the command
 * completes.
 * The callback function prototype is
 * <tt>void cb(int status, u32 bytes_written, void *arg)</tt>.
 * @param arg Pointer to be passed to the callback function.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_firmware_write(struct efct_hw_s *hw, struct efc_dma_s *dma,
		       u32 size, u32 offset, int last,
			void (*cb)(int status, u32 bytes_written,
				   u32 change_status, void *arg),
			void *arg)
{
	return efct_hw_firmware_write_sli4_intf_2(hw, dma, size, offset,
						     last, cb, arg);
}

/**
 * @brief Write a portion of a firmware image to the Emulex XE201 ASIC Type=2.
 *
 * @par Description
 * Creates a SLI_CONFIG mailbox command, fills it with the correct values to
 * write a firmware image chunk, and then sends the command with
 * efct_hw_command(). On completion, the callback function
 * efct_hw_fw_write_cb() gets called to free the mailbox and to signal the
 * caller that the write has completed.
 *
 * @param hw Hardware context.
 * @param dma DMA structure containing the firmware image chunk.
 * @param size Size of the firmware image chunk.
 * @param offset Offset, in bytes, from the beginning of the firmware image.
 * @param last True if this is the last chunk of the image. Causes the image to
 * be committed to flash.
 * @param cb Pointer to a callback function that is called when the command
 * completes.
 * The callback function prototype is
 * <tt>void cb(int status, u32 bytes_written, void *arg)</tt>.
 * @param arg Pointer to be passed to the callback function.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static enum efct_hw_rtn_e
efct_hw_firmware_write_sli4_intf_2(struct efct_hw_s *hw, struct efc_dma_s *dma,
				   u32 size, u32 offset, int last,
			      void (*cb)(int status, u32 bytes_written,
					 u32 change_status, void *arg),
				void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	u8 *mbxdata;
	struct efct_hw_fw_wr_cb_arg *cb_arg;
	int noc = 0;

	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);

	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}
	memset(cb_arg, 0, sizeof(struct efct_hw_fw_wr_cb_arg));
	cb_arg->cb = cb;
	cb_arg->arg = arg;

	/* Send the HW command */
	if (sli_cmd_common_write_object(&hw->sli, mbxdata, SLI4_BMBX_SIZE,
					noc, last, size, offset, "/prg/",
					dma))
		rc = efct_hw_command(hw, mbxdata, EFCT_CMD_NOWAIT,
				     efct_hw_cb_fw_write, cb_arg);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os, "COMMON_WRITE_OBJECT failed\n");
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

/**
 * @brief Called when the WRITE OBJECT command completes.
 *
 * @par Description
 * Get the number of bytes actually written out of the response, free the
 * mailbox that was malloc'd by efct_hw_firmware_write(), then call the
 * callback and pass the status and bytes written.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 * The callback function prototype is <tt>void cb(int status,
 * u32 bytes_written).
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_fw_write(struct efct_hw_s *hw, int status,
		    u8 *mqe, void  *arg)
{
	struct sli4_cmd_sli_config_s *mbox_rsp =
					(struct sli4_cmd_sli_config_s *)mqe;
	struct sli4_res_common_write_object_s *wr_obj_rsp;
	struct efct_hw_fw_wr_cb_arg *cb_arg = arg;
	u32 bytes_written;
	u16 mbox_status;
	u32 change_status;

	wr_obj_rsp = (struct sli4_res_common_write_object_s *)
		      &mbox_rsp->payload.embed;
	bytes_written = le32_to_cpu(wr_obj_rsp->actual_write_length);
	mbox_status = le16_to_cpu(mbox_rsp->hdr.status);
	change_status = (le32_to_cpu(wr_obj_rsp->change_status_dword) &
			 SLI4_RES_CHANGE_STATUS);

	kfree(mqe);

	if (cb_arg) {
		if (cb_arg->cb) {
			if (!status && mbox_status)
				status = mbox_status;
			cb_arg->cb(status, bytes_written, change_status,
				   cb_arg->arg);
		}

		kfree(cb_arg);
	}

	return 0;
}

/**
 * @brief Called when the READ_TRANSCEIVER_DATA command completes.
 *
 * @par Description
 * Get the number of bytes read out of the response, free the mailbox that was
 * malloc'd by efct_hw_get_sfp(), then call the callback and pass the status
 * and bytes written.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 * The callback function prototype is
 * void cb(int status, u32 bytes_written, u32 *data, void *arg).
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_sfp(struct efct_hw_s *hw, int status, u8 *mqe, void  *arg)
{
	struct efct_hw_sfp_cb_arg *cb_arg = arg;
	struct efc_dma_s *payload = &cb_arg->payload;
	struct sli4_res_common_read_transceiver_data_s *mbox_rsp;
	struct efct_s *efct = hw->os;
	u32 bytes_written;

	mbox_rsp =
	(struct sli4_res_common_read_transceiver_data_s *)payload->virt;
	bytes_written = le32_to_cpu(mbox_rsp->hdr.response_length);
	if (cb_arg) {
		if (cb_arg->cb) {
			if (!status && mbox_rsp->hdr.status)
				status = mbox_rsp->hdr.status;
			cb_arg->cb(status, bytes_written, mbox_rsp->page_data,
				   cb_arg->arg);
		}

		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->payload.size, cb_arg->payload.virt,
				  cb_arg->payload.phys);
		memset(&cb_arg->payload, 0, sizeof(struct efc_dma_s));
		kfree(cb_arg);
	}

	kfree(mqe);
	return 0;
}

/**
 * @ingroup io
 * @brief Function to retrieve the SFP information.
 *
 * @param hw Hardware context.
 * @param page The page of SFP data to retrieve (0xa0 or 0xa2).
 * @param cb Function call upon completion of sending the data (may be NULL).
 * @param arg Argument to pass to IO completion function.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS, EFCT_HW_RTN_ERROR, or
 * EFCT_HW_RTN_NO_MEMORY.
 */
enum efct_hw_rtn_e
efct_hw_get_sfp(struct efct_hw_s *hw, u16 page,
		void (*cb)(int, u32, u32 *, void *), void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	struct efct_hw_sfp_cb_arg *cb_arg;
	u8 *mbxdata;
	struct efct_s *efct = hw->os;
	struct efc_dma_s *dma;

	/* mbxdata holds the header of the command */
	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);
	/*
	 * cb_arg holds the data that will be passed to the callback on
	 * completion
	 */
	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}
	memset(cb_arg, 0, sizeof(struct efct_hw_sfp_cb_arg));

	cb_arg->cb = cb;
	cb_arg->arg = arg;

	/* payload holds the non-embedded portion */
	dma = &cb_arg->payload;
	dma->size = sizeof(struct sli4_res_common_read_transceiver_data_s);
	dma->virt = dma_alloc_coherent(&efct->pdev->dev,
				       dma->size, &dma->phys, GFP_DMA);
	if (!dma->virt) {
		efct_log_err(hw->os, "Failed to allocate DMA buffer\n");
		kfree(cb_arg);
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	/* Send the HW command */
	if (sli_cmd_common_read_transceiver_data(&hw->sli, mbxdata,
						 SLI4_BMBX_SIZE, page,
						 &cb_arg->payload))
		rc = efct_hw_command(hw, mbxdata, EFCT_CMD_NOWAIT,
				     efct_hw_cb_sfp, cb_arg);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os,
			      "READ_TRANSCEIVER_DATA failed with status %d\n",
			     rc);
		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->payload.size, cb_arg->payload.virt,
				  cb_arg->payload.phys);
		memset(&cb_arg->payload, 0, sizeof(struct efc_dma_s));
		kfree(cb_arg);
		kfree(mbxdata);
	}

	return rc;
}

/**
 * @brief Function to retrieve the temperature information.
 *
 * @param hw Hardware context.
 * @param cb Function call upon completion of sending the data (may be NULL).
 * @param arg Argument to pass to IO completion function.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS, EFCT_HW_RTN_ERROR, or
 * EFCT_HW_RTN_NO_MEMORY.
 */
enum efct_hw_rtn_e
efct_hw_get_temperature(struct efct_hw_s *hw,
			void (*cb)(int status,
				   u32 curr_temp,
				u32 crit_temp_thrshld,
				u32 warn_temp_thrshld,
				u32 norm_temp_thrshld,
				u32 fan_off_thrshld,
				u32 fan_on_thrshld,
				void *arg),
			void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	struct efct_hw_temp_cb_arg *cb_arg;
	u8 *mbxdata;

	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);

	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	cb_arg->cb = cb;
	cb_arg->arg = arg;

	/* Send the HW command */
	if (sli_cmd_dump_type4(&hw->sli, mbxdata, SLI4_BMBX_SIZE,
			       SLI4_WKI_TAG_SAT_TEM))
		rc = efct_hw_command(hw, mbxdata, EFCT_CMD_NOWAIT,
				     efct_hw_cb_temp, cb_arg);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os, "DUMP_TYPE4 failed\n");
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

/**
 * @brief Called when the DUMP command completes.
 *
 * @par Description
 * Get the temperature data out of the response, free the mailbox that was
 * malloc'd by efct_hw_get_temperature(), then call the callback and pass the
 * status and data.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 * The callback function prototype is defined by efct_hw_temp_cb_t.
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_temp(struct efct_hw_s *hw, int status, u8 *mqe, void  *arg)
{
	struct sli4_cmd_dump4_s *mbox_rsp = (struct sli4_cmd_dump4_s *)mqe;
	struct efct_hw_temp_cb_arg *cb_arg = arg;
	u32 curr_temp = le32_to_cpu(mbox_rsp->resp_data[0]); /* word 5 */
	u32 crit_temp_thrshld =
			le32_to_cpu(mbox_rsp->resp_data[1]); /* word 6 */
	u32 warn_temp_thrshld =
			le32_to_cpu(mbox_rsp->resp_data[2]); /* word 7 */
	u32 norm_temp_thrshld =
			le32_to_cpu(mbox_rsp->resp_data[3]); /* word 8 */
	u32 fan_off_thrshld =
			le32_to_cpu(mbox_rsp->resp_data[4]);   /* word 9 */
	u32 fan_on_thrshld =
			le32_to_cpu(mbox_rsp->resp_data[5]);    /* word 10 */

	if (cb_arg) {
		if (cb_arg->cb) {
			if (status == 0 && le16_to_cpu(mbox_rsp->hdr.status))
				status = le16_to_cpu(mbox_rsp->hdr.status);
			cb_arg->cb(status,
				   curr_temp,
				   crit_temp_thrshld,
				   warn_temp_thrshld,
				   norm_temp_thrshld,
				   fan_off_thrshld,
				   fan_on_thrshld,
				   cb_arg->arg);
		}

		kfree(cb_arg);
	}
	kfree(mqe);

	return 0;
}

/**
 * @brief Function to retrieve the link statistics.
 *
 * @param hw Hardware context.
 * @param req_ext_counters If TRUE, then the extended counters will be
 * requested.
 * @param clear_overflow_flags If TRUE, then overflow flags will be cleared.
 * @param clear_all_counters If TRUE, the counters will be cleared.
 * @param cb Function call upon completion of sending the data (may be NULL).
 * @param arg Argument to pass to IO completion function.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS, EFCT_HW_RTN_ERROR, ori
 * EFCT_HW_RTN_NO_MEMORY.
 */
enum efct_hw_rtn_e
efct_hw_get_link_stats(struct efct_hw_s *hw,
		       u8 req_ext_counters,
		       u8 clear_overflow_flags,
		       u8 clear_all_counters,
		       void (*cb)(int status,
				  u32 num_counters,
			struct efct_hw_link_stat_counts_s *counters,
			void *arg),
		       void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	struct efct_hw_link_stat_cb_arg *cb_arg;
	u8 *mbxdata;

	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);

	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	cb_arg->cb = cb;
	cb_arg->arg = arg;

	/* Send the HW command */
	if (sli_cmd_read_link_stats(&hw->sli, mbxdata, SLI4_BMBX_SIZE,
				    req_ext_counters,
				    clear_overflow_flags,
				    clear_all_counters))
		rc = efct_hw_command(hw, mbxdata, EFCT_CMD_NOWAIT,
				     efct_hw_cb_link_stat, cb_arg);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

/**
 * @brief Called when the READ_LINK_STAT command completes.
 *
 * @par Description
 * Get the counters out of the response, free the mailbox that was malloc'd
 * by efct_hw_get_link_stats(), then call the callback and pass the status and
 * data.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 * The callback function prototype is defined by efct_hw_link_stat_cb_t.
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_link_stat(struct efct_hw_s *hw, int status,
		     u8 *mqe, void  *arg)
{
	struct sli4_cmd_read_link_stats_s *mbox_rsp;
	struct efct_hw_link_stat_cb_arg *cb_arg = arg;
	struct efct_hw_link_stat_counts_s counts[EFCT_HW_LINK_STAT_MAX];
	u32 num_counters;
	u32 mbox_rsp_flags = 0;

	mbox_rsp = (struct sli4_cmd_read_link_stats_s *)mqe;
	mbox_rsp_flags = le32_to_cpu(mbox_rsp->dw1_flags);
	num_counters = (mbox_rsp_flags & SLI4_READ_LNKSTAT_GEC) ? 20 : 13;
	memset(counts, 0, sizeof(struct efct_hw_link_stat_counts_s) *
				 EFCT_HW_LINK_STAT_MAX);

	counts[EFCT_HW_LINK_STAT_LINK_FAILURE_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W02OF);
	counts[EFCT_HW_LINK_STAT_LOSS_OF_SYNC_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W03OF);
	counts[EFCT_HW_LINK_STAT_LOSS_OF_SIGNAL_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W04OF);
	counts[EFCT_HW_LINK_STAT_PRIMITIVE_SEQ_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W05OF);
	counts[EFCT_HW_LINK_STAT_INVALID_XMIT_WORD_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W06OF);
	counts[EFCT_HW_LINK_STAT_CRC_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W07OF);
	counts[EFCT_HW_LINK_STAT_PRIMITIVE_SEQ_TIMEOUT_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W08OF);
	counts[EFCT_HW_LINK_STAT_ELASTIC_BUFFER_OVERRUN_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W09OF);
	counts[EFCT_HW_LINK_STAT_ARB_TIMEOUT_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W10OF);
	counts[EFCT_HW_LINK_STAT_ADVERTISED_RCV_B2B_CREDIT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W11OF);
	counts[EFCT_HW_LINK_STAT_CURR_RCV_B2B_CREDIT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W12OF);
	counts[EFCT_HW_LINK_STAT_ADVERTISED_XMIT_B2B_CREDIT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W13OF);
	counts[EFCT_HW_LINK_STAT_CURR_XMIT_B2B_CREDIT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W14OF);
	counts[EFCT_HW_LINK_STAT_RCV_EOFA_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W15OF);
	counts[EFCT_HW_LINK_STAT_RCV_EOFDTI_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W16OF);
	counts[EFCT_HW_LINK_STAT_RCV_EOFNI_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W17OF);
	counts[EFCT_HW_LINK_STAT_RCV_SOFF_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W18OF);
	counts[EFCT_HW_LINK_STAT_RCV_DROPPED_NO_AER_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W19OF);
	counts[EFCT_HW_LINK_STAT_RCV_DROPPED_NO_RPI_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W20OF);
	counts[EFCT_HW_LINK_STAT_RCV_DROPPED_NO_XRI_COUNT].overflow =
		(mbox_rsp_flags & SLI4_READ_LNKSTAT_W21OF);
	counts[EFCT_HW_LINK_STAT_LINK_FAILURE_COUNT].counter =
		 le32_to_cpu(mbox_rsp->linkfail_errcnt);
	counts[EFCT_HW_LINK_STAT_LOSS_OF_SYNC_COUNT].counter =
		 le32_to_cpu(mbox_rsp->losssync_errcnt);
	counts[EFCT_HW_LINK_STAT_LOSS_OF_SIGNAL_COUNT].counter =
		 le32_to_cpu(mbox_rsp->losssignal_errcnt);
	counts[EFCT_HW_LINK_STAT_PRIMITIVE_SEQ_COUNT].counter =
		 le32_to_cpu(mbox_rsp->primseq_errcnt);
	counts[EFCT_HW_LINK_STAT_INVALID_XMIT_WORD_COUNT].counter =
		 le32_to_cpu(mbox_rsp->inval_txword_errcnt);
	counts[EFCT_HW_LINK_STAT_CRC_COUNT].counter =
		le32_to_cpu(mbox_rsp->crc_errcnt);
	counts[EFCT_HW_LINK_STAT_PRIMITIVE_SEQ_TIMEOUT_COUNT].counter =
		le32_to_cpu(mbox_rsp->primseq_eventtimeout_cnt);
	counts[EFCT_HW_LINK_STAT_ELASTIC_BUFFER_OVERRUN_COUNT].counter =
		 le32_to_cpu(mbox_rsp->elastic_bufoverrun_errcnt);
	counts[EFCT_HW_LINK_STAT_ARB_TIMEOUT_COUNT].counter =
		 le32_to_cpu(mbox_rsp->arbit_fc_al_timeout_cnt);
	counts[EFCT_HW_LINK_STAT_ADVERTISED_RCV_B2B_CREDIT].counter =
		 le32_to_cpu(mbox_rsp->adv_rx_buftor_to_buf_credit);
	counts[EFCT_HW_LINK_STAT_CURR_RCV_B2B_CREDIT].counter =
		 le32_to_cpu(mbox_rsp->curr_rx_buf_to_buf_credit);
	counts[EFCT_HW_LINK_STAT_ADVERTISED_XMIT_B2B_CREDIT].counter =
		 le32_to_cpu(mbox_rsp->adv_tx_buf_to_buf_credit);
	counts[EFCT_HW_LINK_STAT_CURR_XMIT_B2B_CREDIT].counter =
		 le32_to_cpu(mbox_rsp->curr_tx_buf_to_buf_credit);
	counts[EFCT_HW_LINK_STAT_RCV_EOFA_COUNT].counter =
		 le32_to_cpu(mbox_rsp->rx_eofa_cnt);
	counts[EFCT_HW_LINK_STAT_RCV_EOFDTI_COUNT].counter =
		 le32_to_cpu(mbox_rsp->rx_eofdti_cnt);
	counts[EFCT_HW_LINK_STAT_RCV_EOFNI_COUNT].counter =
		 le32_to_cpu(mbox_rsp->rx_eofni_cnt);
	counts[EFCT_HW_LINK_STAT_RCV_SOFF_COUNT].counter =
		 le32_to_cpu(mbox_rsp->rx_soff_cnt);
	counts[EFCT_HW_LINK_STAT_RCV_DROPPED_NO_AER_COUNT].counter =
		 le32_to_cpu(mbox_rsp->rx_dropped_no_aer_cnt);
	counts[EFCT_HW_LINK_STAT_RCV_DROPPED_NO_RPI_COUNT].counter =
		 le32_to_cpu(mbox_rsp->rx_dropped_no_avail_rpi_rescnt);
	counts[EFCT_HW_LINK_STAT_RCV_DROPPED_NO_XRI_COUNT].counter =
		 le32_to_cpu(mbox_rsp->rx_dropped_no_avail_xri_rescnt);

	if (cb_arg) {
		if (cb_arg->cb) {
			if (status == 0 && le16_to_cpu(mbox_rsp->hdr.status))
				status = le16_to_cpu(mbox_rsp->hdr.status);
			cb_arg->cb(status, num_counters, counts, cb_arg->arg);
		}

		kfree(cb_arg);
	}
	kfree(mqe);

	return 0;
}

/**
 * @brief Function to retrieve the link and host statistics.
 *
 * @param hw Hardware context.
 * @param cc clear counters, if TRUE all counters will be cleared.
 * @param cb Function call upon completion of receiving the data.
 * @param arg Argument to pass to pointer fc hosts statistics structure.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS, EFCT_HW_RTN_ERROR, or
 * EFCT_HW_RTN_NO_MEMORY.
 */
enum efct_hw_rtn_e
efct_hw_get_host_stats(struct efct_hw_s *hw, u8 cc,
		       void (*cb)(int status,
				  u32 num_counters,
				  struct efct_hw_host_stat_counts_s *counters,
				  void *arg),
		       void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	struct efct_hw_host_stat_cb_arg *cb_arg;
	u8 *mbxdata;

	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_KERNEL);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);

	cb_arg = kmalloc(sizeof(*cb_arg), GFP_KERNEL);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	 cb_arg->cb = cb;
	 cb_arg->arg = arg;

	 /* Send the HW command to get the host stats */
	if (sli_cmd_read_status(&hw->sli, mbxdata, SLI4_BMBX_SIZE, cc))
		rc = efct_hw_command(hw, mbxdata, EFCT_CMD_NOWAIT,
				     efct_hw_cb_host_stat, cb_arg);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os, "READ_HOST_STATS failed\n");
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

/**
 * @brief Called when the READ_STATUS command completes.
 *
 * @par Description
 * Get the counters out of the response, free the mailbox that was malloc'd
 * by efct_hw_get_host_stats(), then call the callback and pass
 * the status and data.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 * The callback function prototype is defined by
 * efct_hw_host_stat_cb_t.
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_host_stat(struct efct_hw_s *hw, int status,
		     u8 *mqe, void  *arg)
{
	struct sli4_cmd_read_status_s *mbox_rsp =
					(struct sli4_cmd_read_status_s *)mqe;
	struct efct_hw_host_stat_cb_arg *cb_arg = arg;
	struct efct_hw_host_stat_counts_s counts[EFCT_HW_HOST_STAT_MAX];
	u32 num_counters = EFCT_HW_HOST_STAT_MAX;

	memset(counts, 0, sizeof(struct efct_hw_host_stat_counts_s) *
		   EFCT_HW_HOST_STAT_MAX);

	counts[EFCT_HW_HOST_STAT_TX_KBYTE_COUNT].counter =
		 le32_to_cpu(mbox_rsp->trans_kbyte_cnt);
	counts[EFCT_HW_HOST_STAT_RX_KBYTE_COUNT].counter =
		 le32_to_cpu(mbox_rsp->recv_kbyte_cnt);
	counts[EFCT_HW_HOST_STAT_TX_FRAME_COUNT].counter =
		 le32_to_cpu(mbox_rsp->trans_frame_cnt);
	counts[EFCT_HW_HOST_STAT_RX_FRAME_COUNT].counter =
		 le32_to_cpu(mbox_rsp->recv_frame_cnt);
	counts[EFCT_HW_HOST_STAT_TX_SEQ_COUNT].counter =
		 le32_to_cpu(mbox_rsp->trans_seq_cnt);
	counts[EFCT_HW_HOST_STAT_RX_SEQ_COUNT].counter =
		 le32_to_cpu(mbox_rsp->recv_seq_cnt);
	counts[EFCT_HW_HOST_STAT_TOTAL_EXCH_ORIG].counter =
		 le32_to_cpu(mbox_rsp->tot_exchanges_orig);
	counts[EFCT_HW_HOST_STAT_TOTAL_EXCH_RESP].counter =
		 le32_to_cpu(mbox_rsp->tot_exchanges_resp);
	counts[EFCT_HW_HOSY_STAT_RX_P_BSY_COUNT].counter =
		 le32_to_cpu(mbox_rsp->recv_p_bsy_cnt);
	counts[EFCT_HW_HOST_STAT_RX_F_BSY_COUNT].counter =
		 le32_to_cpu(mbox_rsp->recv_f_bsy_cnt);
	counts[EFCT_HW_HOST_STAT_DROP_FRM_DUE_TO_NO_RQ_BUF_COUNT].counter =
		 le32_to_cpu(mbox_rsp->no_rq_buf_dropped_frames_cnt);
	counts[EFCT_HW_HOST_STAT_EMPTY_RQ_TIMEOUT_COUNT].counter =
		 le32_to_cpu(mbox_rsp->empty_rq_timeout_cnt);
	counts[EFCT_HW_HOST_STAT_DROP_FRM_DUE_TO_NO_XRI_COUNT].counter =
		 le32_to_cpu(mbox_rsp->no_xri_dropped_frames_cnt);
	counts[EFCT_HW_HOST_STAT_EMPTY_XRI_POOL_COUNT].counter =
		 le32_to_cpu(mbox_rsp->empty_xri_pool_cnt);

	if (cb_arg) {
		if (cb_arg->cb) {
			if (status == 0 && le16_to_cpu(mbox_rsp->hdr.status))
				status = le16_to_cpu(mbox_rsp->hdr.status);
			cb_arg->cb(status, num_counters, counts, cb_arg->arg);
		}

		kfree(cb_arg);
	}
	kfree(mqe);

	return 0;
}

/**
 * @brief HW link configuration enum to the CLP string value mapping.
 *
 * This structure provides a mapping from the efct_hw_linkcfg_e
 * enum (enum exposed for the EFCT_HW_PORT_SET_LINK_CONFIG port
 * control) to the CLP string that is used
 * in the DMTF_CLP_CMD mailbox command.
 */
struct efct_hw_linkcfg_map_s {
	enum efct_hw_linkcfg_e linkcfg;
	const char *clp_str;
};

/**
 * @brief Mapping from the HW linkcfg enum to the CLP command value
 * string.
 */
static struct efct_hw_linkcfg_map_s linkcfg_map[] = {
	{EFCT_HW_LINKCFG_4X10G, "ELX_4x10G"},
	{EFCT_HW_LINKCFG_1X40G, "ELX_1x40G"},
	{EFCT_HW_LINKCFG_2X16G, "ELX_2x16G"},
	{EFCT_HW_LINKCFG_4X8G, "ELX_4x8G"},
	{EFCT_HW_LINKCFG_4X1G, "ELX_4x1G"},
	{EFCT_HW_LINKCFG_2X10G, "ELX_2x10G"},
	{EFCT_HW_LINKCFG_2X10G_2X8G, "ELX_2x10G_2x8G"} };

/**
 * @brief Helper function for getting the HW linkcfg enum from the CLP
 * string value
 *
 * @param clp_str CLP string value from OEMELX_LinkConfig.
 *
 * @return Returns the HW linkcfg enum corresponding to clp_str.
 */
static enum efct_hw_linkcfg_e
efct_hw_linkcfg_from_clp(const char *clp_str)
{
	u32 i;

	for (i = 0; i < ARRAY_SIZE(linkcfg_map); i++) {
		if (strncmp(linkcfg_map[i].clp_str, clp_str,
			    strlen(clp_str)) == 0)
			return linkcfg_map[i].linkcfg;
	}
	return EFCT_HW_LINKCFG_NA;
}

/**
 * @brief Helper function for getting the CLP string value from the HW
 * linkcfg enum.
 *
 * @param linkcfg HW linkcfg enum.
 *
 * @return Returns the OEMELX_LinkConfig CLP string value corresponding to
 * given linkcfg.
 */
static const char *
efct_hw_clp_from_linkcfg(enum efct_hw_linkcfg_e linkcfg)
{
	u32 i;

	for (i = 0; i < ARRAY_SIZE(linkcfg_map); i++) {
		if (linkcfg_map[i].linkcfg == linkcfg)
			return linkcfg_map[i].clp_str;
	}
	return NULL;
}

/**
 * @brief Link configuration callback argument.
 */
struct efct_hw_linkcfg_cb_arg_s {
	void (*cb)(int status,
		   uintptr_t value, void *arg);
	void *arg;
	u32 opts;
	int status;
	struct efc_dma_s dma_cmd;
	struct efc_dma_s dma_resp;
	u32 result_len;
};

/**
 * @brief Set link configuration.
 *
 * @param hw Hardware context.
 * @param value Link configuration enum to which the link configuration is
 * set.
 * @param opts Mailbox command options (EFCT_CMD_NOWAIT/POLL).
 * @param cb Callback function to invoke following mbx command.
 * @param arg Callback argument.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static enum efct_hw_rtn_e
efct_hw_set_linkcfg(struct efct_hw_s *hw,
		    enum efct_hw_linkcfg_e value, u32 opts,
		void (*cb)(int status, uintptr_t value, void *arg),
		void *arg)
{
	if (!sli_link_is_configurable(&hw->sli)) {
		efct_log_debug(hw->os, "Function not supported\n");
		return EFCT_HW_RTN_ERROR;
	}

	if (hw->sli.if_type == SLI4_INTF_IF_TYPE_2)
		return efct_hw_set_linkcfg_sli4_intf_2(hw, value,
						       opts, cb, arg);
	efct_log_test(hw->os,
		      "Function not supported for this IF_TYPE\n");
	return EFCT_HW_RTN_ERROR;
}

/**
 * @brief Set link configuration for Lancer
 *
 * @param hw Hardware context.
 * @param value Link configuration enum to which the link configuration is
 * set.
 * @param opts Mailbox command options (EFCT_CMD_NOWAIT/POLL).
 * @param cb Callback function to invoke following mbx command.
 * @param arg Callback argument.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static enum efct_hw_rtn_e
efct_hw_set_linkcfg_sli4_intf_2(struct efct_hw_s *hw,
				enum efct_hw_linkcfg_e value,
			u32 opts,
			void (*cb)(int status, uintptr_t value, void *arg),
			void *arg)
{
	char cmd[EFCT_HW_DMTF_CLP_CMD_MAX];
	struct efct_hw_linkcfg_cb_arg_s *cb_arg;
	const char *value_str = NULL;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	struct efct_s *efct = hw->os;
	struct efc_dma_s *dma;

	/* translate efct_hw_linkcfg_e to CLP string */
	value_str = efct_hw_clp_from_linkcfg(value);

	/* allocate memory for callback argument */
	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg)
		return EFCT_HW_RTN_NO_MEMORY;

	snprintf(cmd, EFCT_HW_DMTF_CLP_CMD_MAX,
		 "set / OEMELX_LinkConfig=%s", value_str);
	/* allocate DMA for command  */
	dma = &cb_arg->dma_cmd;
	dma->size = strlen(cmd) + 1;
	dma->virt = dma_alloc_coherent(&efct->pdev->dev,
				       dma->size, &dma->phys, GFP_DMA);
	if (!dma->virt) {
		efct_log_err(hw->os, "malloc failed\n");
		kfree(cb_arg);
		return EFCT_HW_RTN_NO_MEMORY;
	}
	memset(cb_arg->dma_cmd.virt, 0, strlen(cmd) + 1);
	memcpy(cb_arg->dma_cmd.virt, cmd, strlen(cmd));

	/* allocate DMA for response */
	dma = &cb_arg->dma_resp;
	dma->size = EFCT_HW_DMTF_CLP_RSP_MAX;
	dma->virt = dma_alloc_coherent(&efct->pdev->dev,
				       dma->size, &dma->phys, GFP_DMA);
	if (!dma->virt) {
		efct_log_err(hw->os, "malloc failed\n");
		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->dma_cmd.size, cb_arg->dma_cmd.virt,
				  cb_arg->dma_cmd.phys);
		memset(&cb_arg->dma_cmd, 0, sizeof(struct efc_dma_s));
		kfree(cb_arg);
		return EFCT_HW_RTN_NO_MEMORY;
	}
	cb_arg->cb = cb;
	cb_arg->arg = arg;
	cb_arg->opts = opts;

	rc = efct_hw_exec_dmtf_clp_cmd(hw, &cb_arg->dma_cmd,
				       &cb_arg->dma_resp, opts,
				       efct_hw_linkcfg_dmtf_clp_cb, cb_arg);

	if (opts == EFCT_CMD_POLL || rc != EFCT_HW_RTN_SUCCESS) {
		/* if failed, or polling, free memory here; if success and not
		 * polling, will free in callback function
		 */
		if (rc)
			efct_log_test(hw->os, "CLP cmd=\"%s\" failed\n",
				      (char *)cb_arg->dma_cmd.virt);

		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->dma_cmd.size, cb_arg->dma_cmd.virt,
				  cb_arg->dma_cmd.phys);
		memset(&cb_arg->dma_cmd, 0, sizeof(struct efc_dma_s));
		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->dma_resp.size, cb_arg->dma_resp.virt,
				  cb_arg->dma_resp.phys);
		memset(&cb_arg->dma_resp, 0, sizeof(struct efc_dma_s));
		kfree(cb_arg);
	}
	return rc;
}

/**
 * @brief Get link configuration.
 *
 * @param hw Hardware context.
 * @param opts Mailbox command options (EFCT_CMD_NOWAIT/POLL).
 * @param cb Callback function to invoke following mbx command.
 * @param arg Callback argument.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static enum efct_hw_rtn_e
efct_hw_get_linkcfg(struct efct_hw_s *hw, u32 opts,
		    void (*cb)(int status, uintptr_t value, void *arg),
		void *arg)
{
	if (!sli_link_is_configurable(&hw->sli)) {
		efct_log_debug(hw->os, "Function not supported\n");
		return EFCT_HW_RTN_ERROR;
	}

	return efct_hw_get_linkcfg_sli4_intf_2(hw, opts, cb, arg);
}

/**
 * @brief Get link configuration for a Lancer
 *
 * @param hw Hardware context.
 * @param opts Mailbox command options (EFCT_CMD_NOWAIT/POLL).
 * @param cb Callback function to invoke following mbx command.
 * @param arg Callback argument.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static enum efct_hw_rtn_e
efct_hw_get_linkcfg_sli4_intf_2(struct efct_hw_s *hw, u32 opts,
				void (*cb)(int status, uintptr_t value,
					   void *arg),
				void *arg)
{
	char cmd[EFCT_HW_DMTF_CLP_CMD_MAX];
	struct efct_hw_linkcfg_cb_arg_s *cb_arg;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	struct efc_dma_s *dma;
	struct efct_s *efct = hw->os;

	/* allocate memory for callback argument */
	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg)
		return EFCT_HW_RTN_NO_MEMORY;

	snprintf((char *)cmd, EFCT_HW_DMTF_CLP_CMD_MAX,
		 "show / OEMELX_LinkConfig");

	/* allocate DMA for command  */
	dma = &cb_arg->dma_cmd;
	dma->size = strlen(cmd) + 1;
	dma->virt = dma_alloc_coherent(&efct->pdev->dev,
				       dma->size, &dma->phys, GFP_DMA);
	if (!dma->virt) {
		efct_log_err(hw->os, "malloc failed\n");
		kfree(cb_arg);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	/* copy CLP command to DMA command */
	memset(cb_arg->dma_cmd.virt, 0, strlen(cmd) + 1);
	memcpy(cb_arg->dma_cmd.virt, cmd, strlen(cmd));

	/* allocate DMA for response */
	dma = &cb_arg->dma_resp;
	dma->size = EFCT_HW_DMTF_CLP_RSP_MAX;
	dma->virt = dma_alloc_coherent(&efct->pdev->dev,
				       dma->size, &dma->phys, GFP_DMA);
	if (!dma->virt) {
		efct_log_err(hw->os, "malloc failed\n");
		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->dma_cmd.size, cb_arg->dma_cmd.virt,
				  cb_arg->dma_cmd.phys);
		memset(&cb_arg->dma_cmd, 0, sizeof(struct efc_dma_s));
		kfree(cb_arg);
		return EFCT_HW_RTN_NO_MEMORY;
	}
	cb_arg->cb = cb;
	cb_arg->arg = arg;
	cb_arg->opts = opts;

	rc = efct_hw_exec_dmtf_clp_cmd(hw, &cb_arg->dma_cmd, &cb_arg->dma_resp,
				       opts, efct_hw_linkcfg_dmtf_clp_cb,
				       cb_arg);

	if (opts == EFCT_CMD_POLL || rc != EFCT_HW_RTN_SUCCESS) {
		/*
		 * if failed or polling, free memory here; if not polling and
		 * success, will free in callback function
		 */
		if (rc)
			efct_log_test(hw->os, "CLP cmd=\"%s\" failed\n",
				      (char *)cb_arg->dma_cmd.virt);
		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->dma_cmd.size, cb_arg->dma_cmd.virt,
				  cb_arg->dma_cmd.phys);
		memset(&cb_arg->dma_cmd, 0, sizeof(struct efc_dma_s));
		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->dma_resp.size, cb_arg->dma_resp.virt,
				  cb_arg->dma_resp.phys);
		memset(&cb_arg->dma_resp, 0, sizeof(struct efc_dma_s));
		kfree(cb_arg);
	}
	return rc;
}

/**
 * @brief Sets the DIF seed value.
 *
 * @param hw Hardware context.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static enum efct_hw_rtn_e
efct_hw_set_dif_seed(struct efct_hw_s *hw)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	u8 buf[SLI4_BMBX_SIZE];
	struct sli4_req_common_set_features_dif_seed_s seed_param;

	memset(&seed_param, 0, sizeof(seed_param));
	seed_param.seed = cpu_to_le16(hw->config.dif_seed);

	/* send set_features command */
	if (sli_cmd_common_set_features(&hw->sli, buf, SLI4_BMBX_SIZE,
					SLI4_SET_FEATURES_DIF_SEED,
					4,
					(u32 *)&seed_param)) {
		rc = efct_hw_command(hw, buf, EFCT_CMD_POLL, NULL, NULL);
		if (rc)
			efct_log_err(hw->os,
				     "efct_hw_command returns %d\n", rc);
		else
			efct_log_debug(hw->os, "DIF seed set to 0x%x\n",
				       hw->config.dif_seed);
	} else {
		efct_log_err(hw->os,
			     "sli_cmd_common_set_features failed\n");
		rc = EFCT_HW_RTN_ERROR;
	}
	return rc;
}

static void
efct_hw_watchdog_timer_cb(struct timer_list *t)
{
	struct efct_hw_s *hw = from_timer(hw, t, watchdog_timer);

	efct_hw_config_watchdog_timer(hw);
}

static void
efct_hw_cb_cfg_watchdog(struct efct_hw_s *hw, int status, u8 *mqe,
			void  *arg)
{
	u16 timeout = hw->watchdog_timeout;

	if (status != 0) {
		efct_log_err(hw->os, "config watchdog timer failed, rc = %d\n",
			     status);
	} else {
		if (timeout != 0) {
			/*
			 * keeping callback 500ms before timeout to keep
			 * heartbeat alive
			 */
			timer_setup(&hw->watchdog_timer,
				    &efct_hw_watchdog_timer_cb, 0);

			mod_timer(&hw->watchdog_timer,
				  jiffies +
				  msecs_to_jiffies(timeout * 1000 - 500));
		} else {
			del_timer(&hw->watchdog_timer);
		}
	}

	kfree(mqe);
}

/**
 * @brief Set configuration parameters for watchdog timer feature.
 *
 * @param hw Hardware context.
 * @param timeout Timeout for watchdog timer in seconds
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static enum efct_hw_rtn_e
efct_hw_config_watchdog_timer(struct efct_hw_s *hw)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	u8 *buf = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);

	sli4_cmd_lowlevel_set_watchdog(&hw->sli, buf, SLI4_BMBX_SIZE,
				       hw->watchdog_timeout);
	rc = efct_hw_command(hw, buf, EFCT_CMD_NOWAIT, efct_hw_cb_cfg_watchdog,
			     NULL);
	if (rc) {
		kfree(buf);
		efct_log_err(hw->os, "config watchdog timer failed, rc = %d\n",
			     rc);
	}
	return rc;
}

/**
 * @brief enable sli port health check
 *
 * @param hw Hardware context.
 * @param buf Pointer to a mailbox buffer area.
 * @param query current status of the health check feature enabled/disabled
 * @param enable if 1: enable 0: disable
 * @param buf Pointer to a mailbox buffer area.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static enum efct_hw_rtn_e
efct_hw_config_sli_port_health_check(struct efct_hw_s *hw, u8 query,
				     u8 enable)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	u8 buf[SLI4_BMBX_SIZE];
	struct sli4_req_common_set_features_health_check_s param;
	u32	health_check_flag = 0;

	memset(&param, 0, sizeof(param));

	if (enable)
		health_check_flag |= SLI4_RQ_HEALTH_CHECK_ENABLE;

	if (query)
		health_check_flag |= SLI4_RQ_HEALTH_CHECK_QUERY;

	param.health_check_dword = cpu_to_le32(health_check_flag);

	/* build the set_features command */
	sli_cmd_common_set_features(&hw->sli, buf, SLI4_BMBX_SIZE,
				    SLI4_SET_FEATURES_SLI_PORT_HEALTH_CHECK,
				    sizeof(param),
				    &param);

	rc = efct_hw_command(hw, buf, EFCT_CMD_POLL, NULL, NULL);
	if (rc)
		efct_log_err(hw->os, "efct_hw_command returns %d\n", rc);
	else
		efct_log_test(hw->os, "SLI Port Health Check is enabled\n");

	return rc;
}

/**
 * @brief Set FTD transfer hint feature
 *
 * @param hw Hardware context.
 * @param fdt_xfer_hint size in bytes where read requests are segmented.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static enum efct_hw_rtn_e
efct_hw_config_set_fdt_xfer_hint(struct efct_hw_s *hw, u32 fdt_xfer_hint)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	u8 buf[SLI4_BMBX_SIZE];
	struct sli4_req_common_set_features_set_fdt_xfer_hint_s param;

	memset(&param, 0, sizeof(param));
	param.fdt_xfer_hint = cpu_to_le32(fdt_xfer_hint);
	/* build the set_features command */
	sli_cmd_common_set_features(&hw->sli, buf, SLI4_BMBX_SIZE,
				    SLI4_SET_FEATURES_SET_FTD_XFER_HINT,
				    sizeof(param),
				    &param);

	rc = efct_hw_command(hw, buf, EFCT_CMD_POLL, NULL, NULL);
	if (rc)
		efct_log_warn(hw->os, "set FDT hint %d failed: %d\n",
			      fdt_xfer_hint, rc);
	else
		efct_log_info(hw->os, "Set FTD transfer hint to %d\n",
			      le32_to_cpu(param.fdt_xfer_hint));

	return rc;
}

/**
 * @brief Get the link configuration callback.
 *
 * @param hw Hardware context.
 * @param status Status from the DMTF CLP command.
 * @param result_len Length, in bytes, of the DMTF CLP result.
 * @param arg Pointer to a callback argument.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
static void
efct_hw_linkcfg_dmtf_clp_cb(struct efct_hw_s *hw, int status,
			    u32 result_len, void *arg)
{
	int rval;
	char retdata_str[64];
	struct efct_hw_linkcfg_cb_arg_s *cb_arg;
	enum efct_hw_linkcfg_e linkcfg = EFCT_HW_LINKCFG_NA;
	struct efct_s *efct = hw->os;

	cb_arg = (struct efct_hw_linkcfg_cb_arg_s *)arg;

	if (status) {
		efct_log_test(hw->os, "CLP cmd failed, status=%d\n", status);
	} else {
		/* parse CLP response to get return data */
		rval = efct_hw_clp_resp_get_value(hw, "retdata", retdata_str,
						  sizeof(retdata_str),
						  cb_arg->dma_resp.virt,
						  result_len);

		if (rval <= 0)
			efct_log_err(hw->os, "failed to get retdata %d\n",
				     result_len);
		else
			/* translate string into hw enum */
			linkcfg = efct_hw_linkcfg_from_clp(retdata_str);
	}

	/* invoke callback */
	if (cb_arg->cb)
		cb_arg->cb(status, linkcfg, cb_arg->arg);

	/* if polling, will free memory in calling function */
	if (cb_arg->opts != EFCT_CMD_POLL) {
		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->dma_cmd.size, cb_arg->dma_cmd.virt,
				  cb_arg->dma_cmd.phys);
		memset(&cb_arg->dma_cmd, 0, sizeof(struct efc_dma_s));
		dma_free_coherent(&efct->pdev->dev,
				  cb_arg->dma_resp.size, cb_arg->dma_resp.virt,
				  cb_arg->dma_resp.phys);
		memset(&cb_arg->dma_resp, 0, sizeof(struct efc_dma_s));
		kfree(cb_arg);
	}
}

/**
 * @brief Set the Lancer dump location
 * @par Description
 * This function tells a Lancer chip to use a specific DMA
 * buffer as a dump location rather than the internal flash.
 *
 * @param hw Hardware context.
 * @param num_buffers The number of DMA buffers to hold the dump (1..n).
 * @param dump_buffers DMA buffers to hold the dump.
 *
 * @return Returns EFCT_HW_RTN_SUCCESS on success.
 */
enum efct_hw_rtn_e
efct_hw_set_dump_location(struct efct_hw_s *hw, u32 num_buffers,
			  struct efc_dma_s *dump_buffers, u8 fdb)
{
	u8 func;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	u8	buf[SLI4_BMBX_SIZE];
	struct efct_s *efct = hw->os;

	/*
	 * Make sure the FW is new enough to support this command. If the FW
	 * is too old, the FW will UE.
	 */
	if (hw->workaround.disable_dump_loc) {
		efct_log_test(hw->os,
			      "FW version is too old for this feature\n");
		return EFCT_HW_RTN_ERROR;
	}

	/* This command is only valid for physical port 0 */
	func = PCI_FUNC(efct->pdev->devfn);
	if (func != 0) {
		efct_log_test(hw->os, "%s%d passed\n",
			      "function only valid for pci function 0, ",
			     func);
		return EFCT_HW_RTN_ERROR;
	}

	/*
	 * If a single buffer is used, then it may be passed as is to the chip.
	 * For multiple buffers, We must allocate a SGL list and then pass the
	 * address of the list to the chip.
	 */
	if (num_buffers > 1) {
		u32 sge_size = num_buffers * sizeof(struct sli4_sge_s);
		struct sli4_sge_s *sge;
		u32 sge_flags = 0;
		u32 i;
		struct efc_dma_s *dma;

		if (hw->dump_sges.size < sge_size) {
			dma_free_coherent(&efct->pdev->dev, hw->dump_sges.size,
					  hw->dump_sges.virt,
					  hw->dump_sges.phys);
			memset(&hw->dump_sges, 0, sizeof(struct efc_dma_s));
			dma = &hw->dump_sges;
			dma->size = sge_size;
			dma->virt = dma_alloc_coherent(&efct->pdev->dev,
						       dma->size, &dma->phys,
						       GFP_DMA);
			if (!dma->virt) {
				efct_log_err(hw->os,
					     "SGE DMA allocation failed\n");
				return EFCT_HW_RTN_NO_MEMORY;
			}
		}
		/* build the SGE list */
		memset(hw->dump_sges.virt, 0, hw->dump_sges.size);
		hw->dump_sges.len = sge_size;
		sge = hw->dump_sges.virt;
		for (i = 0; i < num_buffers; i++) {
			sge[i].buffer_address_high =
			  cpu_to_le32(upper_32_bits(dump_buffers[i].phys));
			sge[i].buffer_address_low =
			  cpu_to_le32(lower_32_bits(dump_buffers[i].phys));

			sge_flags = sge[i].dw2_flags;
			if (i == num_buffers - 1)
				sge_flags |= SLI4_SGE_LAST;
			else
				sge_flags &= ~SLI4_SGE_LAST;
			sge[i].dw2_flags = cpu_to_le32(sge_flags);

			sge[i].buffer_length =
				cpu_to_le32(dump_buffers[i].size);
		}
		rc = sli_cmd_common_set_dump_location(&hw->sli,
						      (void *)buf,
						      SLI4_BMBX_SIZE, false,
						      true, &hw->dump_sges,
						      fdb);
	} else {
		dump_buffers->len = dump_buffers->size;
		rc = sli_cmd_common_set_dump_location(&hw->sli,
						      (void *)buf,
						SLI4_BMBX_SIZE, false,
						false, dump_buffers, fdb);
	}

	if (rc) {
		rc = efct_hw_command(hw, buf, EFCT_CMD_POLL,
				     NULL, NULL);
		if (rc)
			efct_log_err(hw->os, "efct_hw_command returns %d\n",
				     rc);
	} else {
		efct_log_err(hw->os,
			     "sli_cmd_common_set_dump_location failed\n");
		rc = EFCT_HW_RTN_ERROR;
	}

	return rc;
}

/**
 * @brief Callback argument structure for the DMTF CLP commands.
 */
struct efct_hw_clp_cb_arg_s {
	efct_hw_dmtf_clp_cb_t cb;
	struct efc_dma_s *dma_resp;
	int status;
	u32 opts;
	void *arg;
};

/**
 * @brief Execute the DMTF CLP command.
 *
 * @param hw Hardware context.
 * @param dma_cmd DMA buffer containing the CLP command.
 * @param dma_resp DMA buffer that will contain the response (if successful).
 * @param opts Mailbox command options (such as EFCT_CMD_NOWAIT and POLL).
 * @param cb Callback function.
 * @param arg Callback argument.
 *
 * @return Returns the number of bytes written to the response
 * buffer on success, or a negative value if failed.
 */
static enum efct_hw_rtn_e
efct_hw_exec_dmtf_clp_cmd(struct efct_hw_s *hw, struct efc_dma_s *dma_cmd,
			  struct efc_dma_s *dma_resp, u32 opts,
			  efct_hw_dmtf_clp_cb_t cb, void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	struct efct_hw_clp_cb_arg_s *cb_arg;
	u8 *mbxdata;

	/* allocate DMA for mailbox */
	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);

	/* allocate memory for callback argument */
	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	cb_arg->cb = cb;
	cb_arg->arg = arg;
	cb_arg->dma_resp = dma_resp;
	cb_arg->opts = opts;

	/* Send the HW command */
	if (sli_cmd_dmtf_exec_clp_cmd(&hw->sli, mbxdata, SLI4_BMBX_SIZE,
				      dma_cmd, dma_resp)) {
		rc = efct_hw_command(hw, mbxdata, opts, efct_hw_dmtf_clp_cb,
				     cb_arg);

		if (opts == EFCT_CMD_POLL && rc == EFCT_HW_RTN_SUCCESS) {
			/*
			 * if we're polling, copy response and invoke callback
			 * to parse result
			 */
			memcpy(mbxdata, hw->sli.bmbx.virt, SLI4_BMBX_SIZE);
			efct_hw_dmtf_clp_cb(hw, 0, mbxdata, cb_arg);

			/* set rc to resulting or "parsed" status */
			rc = cb_arg->status;
		}

		/* if failed, or polling, free memory here */
		if (opts == EFCT_CMD_POLL || rc != EFCT_HW_RTN_SUCCESS) {
			if (rc != EFCT_HW_RTN_SUCCESS)
				efct_log_test(hw->os,
					      "efct_hw_command failed\n");
			kfree(mbxdata);
			kfree(cb_arg);
		}
	} else {
		efct_log_test(hw->os,
			      "sli_cmd_dmtf_exec_clp_cmd failed\n");
		rc = EFCT_HW_RTN_ERROR;
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

/**
 * @brief Called when the DMTF CLP command completes.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback argument.
 *
 * @return None.
 *
 */
static void
efct_hw_dmtf_clp_cb(struct efct_hw_s *hw, int status,
		    u8 *mqe, void  *arg)
{
	int cb_status = 0;
	struct sli4_cmd_sli_config_s *mbox_rsp =
					(struct sli4_cmd_sli_config_s *)mqe;
	struct sli4_res_dmtf_exec_clp_cmd_s *clp_rsp;
	struct efct_hw_clp_cb_arg_s *cb_arg = arg;
	u32 result_len = 0;
	int stat_len;
	char stat_str[8];

	clp_rsp =
		(struct sli4_res_dmtf_exec_clp_cmd_s *)mbox_rsp->payload.embed;

	/* there are several status codes here, check them all and condense
	 * into a single callback status
	 */
	if (status || le16_to_cpu(mbox_rsp->hdr.status) ||
	    le32_to_cpu(clp_rsp->clp_status)) {
		efct_log_debug(hw->os,
			       "stats=x%x/x%x/x%x  addl=x%x clp=x%x dtail=x%x\n",
			status, le16_to_cpu(mbox_rsp->hdr.status),
			clp_rsp->hdr.status,
			clp_rsp->hdr.additional_status,
			le32_to_cpu(clp_rsp->clp_status),
			le32_to_cpu(clp_rsp->clp_detailed_status));
		if (status)
			cb_status = status;
		else if (le16_to_cpu(mbox_rsp->hdr.status))
			cb_status = le16_to_cpu(mbox_rsp->hdr.status);
		else
			cb_status = le32_to_cpu(clp_rsp->clp_status);
	} else {
		result_len = le32_to_cpu(clp_rsp->resp_length);
	}

	if (cb_status)
		goto efct_hw_cb_dmtf_clp_done;

	if (result_len == 0 || cb_arg->dma_resp->size < result_len) {
		efct_log_test(hw->os, "%s%zu result len=%d\n",
			      "Invalid response length: resp_len=",
			     cb_arg->dma_resp->size, result_len);
		cb_status = -1;
		goto efct_hw_cb_dmtf_clp_done;
	}

	/* parse CLP response to get status */
	stat_len = efct_hw_clp_resp_get_value(hw, "status", stat_str,
					      sizeof(stat_str),
					      cb_arg->dma_resp->virt,
					      result_len);

	if (stat_len <= 0) {
		efct_log_test(hw->os, "failed to get status %d\n", stat_len);
		cb_status = -1;
		goto efct_hw_cb_dmtf_clp_done;
	}

	if (strcmp(stat_str, "0") != 0) {
		efct_log_test(hw->os,
			      "CLP status indicates failure=%s\n", stat_str);
		cb_status = -1;
		goto efct_hw_cb_dmtf_clp_done;
	}

efct_hw_cb_dmtf_clp_done:

	/* save status in cb_arg for callers with NULL cb's + polling */
	cb_arg->status = cb_status;
	if (cb_arg->cb)
		cb_arg->cb(hw, cb_status, result_len, cb_arg->arg);
	/* if polling, caller will free memory */
	if (cb_arg->opts != EFCT_CMD_POLL) {
		kfree(cb_arg);
		kfree(mqe);
	}
}

/**
 * @brief Parse the CLP result and get the value corresponding to the given
 * keyword.
 *
 * @param hw Hardware context.
 * @param keyword CLP keyword for which the value is returned.
 * @param value Location to which the resulting value is copied.
 * @param value_len Length of the value parameter.
 * @param resp Pointer to the response buffer that is searched
 * for the keyword and value.
 * @param resp_len Length of response buffer passed in.
 *
 * @return Returns the number of bytes written to the value
 * buffer on success, or a negative vaue on failure.
 */
static int
efct_hw_clp_resp_get_value(struct efct_hw_s *hw, const char *keyword,
			   char *value, u32 value_len, const char *resp,
			   u32 resp_len)
{
	char *start = NULL;
	char *end = NULL;

	/* look for specified keyword in string */
	start = strstr(resp, keyword);
	if (!start) {
		efct_log_test(hw->os,
			      "could not find keyword=%s in CLP response\n",
			     keyword);
		return -1;
	}

	/* now look for '=' and go one past */
	start = strchr(start, '=');
	if (!start) {
		efct_log_test(hw->os,
			      "could not find \'=\' in CLP resp for keyword=%s\n",
			keyword);
		return -1;
	}
	start++;

	/* \r\n terminates value */
	end = strstr(start, "\r\n");
	if (!end) {
		efct_log_test(hw->os,
			      "could not find \\r\\n for keyword=%s%s",
			     keyword, " in CLP response\n");
		return -1;
	}

	/* make sure given result array is big enough */
	if ((end - start + 1) > value_len) {
		efct_log_test(hw->os,
			      "value len=%d not large enough for actual=%ld\n",
			     value_len, (end - start));
		return -1;
	}

	strncpy(value, start, (end - start));
	value[end - start] = '\0';
	return (end - start + 1);
}

/**
 * @brief Cause chip to enter an unrecoverable error state.
 *
 * @par Description
 * Cause chip to enter an unrecoverable error state. This is
 * used when detecting unexpected FW behavior so that the FW can be
 * hwted from the driver as soon as the error is detected.
 *
 * @param hw Hardware context.
 * @param dump Generate dump as part of reset.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 *
 */
enum efct_hw_rtn_e
efct_hw_raise_ue(struct efct_hw_s *hw, u8 dump)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;

	if (sli_raise_ue(&hw->sli, dump) != 0) {
		rc = EFCT_HW_RTN_ERROR;
	} else {
		if (hw->state != EFCT_HW_STATE_UNINITIALIZED)
			hw->state = EFCT_HW_STATE_QUEUES_ALLOCATED;
	}

	return rc;
}

/**
 * @brief Called when the OBJECT_GET command completes.
 *
 * @par Description
 * Get the number of bytes actually written out of the response, free the
 * mailbox that was malloc'd by efct_hw_dump_get(), then call the callback
 * and pass the status and bytes read.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 * The callback function prototype is <tt>void cb(int status,
 * u32 bytes_read)</tt>.
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_dump_get(struct efct_hw_s *hw, int status,
		    u8 *mqe, void  *arg)
{
	struct sli4_cmd_sli_config_s *mbox_rsp =
					(struct sli4_cmd_sli_config_s *)mqe;
	struct sli4_res_common_read_object_s *rd_obj_rsp;
	struct efct_hw_dump_get_cb_arg *cb_arg = arg;
	u32 bytes_read;
	u8 eof;

	rd_obj_rsp =
	(struct sli4_res_common_read_object_s *)mbox_rsp->payload.embed;

	bytes_read = le32_to_cpu(rd_obj_rsp->actual_read_length);

	eof = (le32_to_cpu(rd_obj_rsp->eof_dword) &
			   SLI4_RES_COM_READ_OBJ_EOF);

	if (cb_arg) {
		if (cb_arg->cb) {
			if (status == 0 && le16_to_cpu(mbox_rsp->hdr.status))
				status = le16_to_cpu(mbox_rsp->hdr.status);
			cb_arg->cb(status, bytes_read, eof, cb_arg->arg);
		}

		kfree(cb_arg->mbox_cmd);
		kfree(cb_arg);
	}

	return 0;
}

/**
 * @brief Read a dump image to the host.
 *
 * @par Description
 * Creates a SLI_CONFIG mailbox command, fills in the correct values to read a
 * dump image chunk, then sends the command with the efct_hw_command(). On
 * completion, the callback function efct_hw_cb_dump_get() gets called to free
 * the mailbox and signal the caller that the read has completed.
 *
 * @param hw Hardware context.
 * @param dma DMA structure to transfer the dump chunk into.
 * @param size Size of the dump chunk.
 * @param offset Offset, in bytes, from the beginning of the dump.
 * @param cb Pointer to a callback function that is called when the command
 * completes. The callback function prototype is
 * void cb(int status, u32 bytes_read, u8 eof, void *arg).
 * @param arg Pointer to be passed to the callback function.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_dump_get(struct efct_hw_s *hw, struct efc_dma_s *dma, u32 size,
		 u32 offset,
		void (*cb)(int status, u32 bytes_read,
			   u8 eof, void *arg),
		void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	u8 *mbxdata;
	struct efct_hw_dump_get_cb_arg *cb_arg;
	u32 opts;

	opts  = (hw->state == EFCT_HW_STATE_ACTIVE ?
		 EFCT_CMD_NOWAIT : EFCT_CMD_POLL);

	if (sli_dump_is_present(&hw->sli) != 1) {
		efct_log_test(hw->os, "No dump is present\n");
		return EFCT_HW_RTN_ERROR;
	}

	if (sli_reset_required(&hw->sli) == 1) {
		efct_log_test(hw->os, "device reset required\n");
		return EFCT_HW_RTN_ERROR;
	}

	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);

	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	cb_arg->cb = cb;
	cb_arg->arg = arg;
	cb_arg->mbox_cmd = mbxdata;

	/* Send the HW command */
	if (sli_cmd_common_read_object(&hw->sli, mbxdata, SLI4_BMBX_SIZE,
				       size, offset, "/dbg/dump.bin", dma)) {
		rc = efct_hw_command(hw, mbxdata, opts, efct_hw_cb_dump_get,
				     cb_arg);
		if (rc == 0 && opts == EFCT_CMD_POLL) {
			memcpy(mbxdata, hw->sli.bmbx.virt, SLI4_BMBX_SIZE);
			rc = efct_hw_cb_dump_get(hw, 0, mbxdata, cb_arg);
		}
	}

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os, "COMMON_READ_OBJECT failed\n");
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

/**
 * @brief Called when the OBJECT_DELETE command completes.
 *
 * @par Description
 * Free the mailbox that was malloc'd
 * by efct_hw_dump_clear(), then call the callback and pass the status.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 * The callback function prototype is void cb(int status, void *arg).
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_dump_clear(struct efct_hw_s *hw, int status,
		      u8 *mqe, void  *arg)
{
	struct efct_hw_dump_clear_cb_arg *cb_arg = arg;
	struct sli4_cmd_sli_config_s *mbox_rsp =
				(struct sli4_cmd_sli_config_s *)mqe;

	if (cb_arg) {
		if (cb_arg->cb) {
			if (status == 0 && le16_to_cpu(mbox_rsp->hdr.status))
				status = le16_to_cpu(mbox_rsp->hdr.status);
			cb_arg->cb(status, cb_arg->arg);
		}

		kfree(cb_arg->mbox_cmd);
		kfree(cb_arg);
	}

	return 0;
}

/**
 * @brief Clear a dump image from the device.
 *
 * @par Description
 * Creates a SLI_CONFIG mailbox command, fills it with the correct values to
 * clear the dump, then sends the command with efct_hw_command().
 * On completion, the callback function efct_hw_cb_dump_clear() gets
 * called to free the mailbox and to signal the caller that
 * the write has completed.
 *
 * @param hw Hardware context.
 * @param cb Pointer to a callback function that is called when the command
 * completes.
 * The callback function prototype is
 * <tt>void cb(int status, u32 bytes_written, void *arg)</tt>.
 * @param arg Pointer to be passed to the callback function.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_dump_clear(struct efct_hw_s *hw,
		   void (*cb)(int status, void *arg),
		void *arg)
{
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;
	u8 *mbxdata;
	struct efct_hw_dump_clear_cb_arg *cb_arg;
	u32 opts;

	opts = (hw->state == EFCT_HW_STATE_ACTIVE ?
				 EFCT_CMD_NOWAIT : EFCT_CMD_POLL);
	if (hw->sli.if_type != SLI4_INTF_IF_TYPE_2) {
		efct_log_test(hw->os,
			      "Function only supported for I/F type 2\n");
		return EFCT_HW_RTN_ERROR;
	}

	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);

	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	cb_arg->cb = cb;
	cb_arg->arg = arg;
	cb_arg->mbox_cmd = mbxdata;

	/* Send the HW command */
	if (sli_cmd_common_delete_object(&hw->sli, mbxdata,
					 SLI4_BMBX_SIZE,
					      "/dbg/dump.bin")) {
		rc = efct_hw_command(hw, mbxdata, opts, efct_hw_cb_dump_clear,
				     cb_arg);
		if (rc == 0 && opts == EFCT_CMD_POLL) {
			memcpy(mbxdata, hw->sli.bmbx.virt, SLI4_BMBX_SIZE);
			rc = efct_hw_cb_dump_clear(hw, 0, mbxdata, cb_arg);
		}
	}

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os, "COMMON_DELETE_OBJECT failed\n");
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

struct efct_hw_get_port_protocol_cb_arg_s {
	void (*cb)(int status,
		   enum efct_hw_port_protocol_e port_protocol,
		void *arg);
	void *arg;
	u32 pci_func;
	struct efc_dma_s payload;
};

struct efct_hw_set_port_protocol_cb_arg_s {
	void (*cb)(int status,  void *arg);
	void *arg;
	struct efc_dma_s payload;
	u32 new_protocol;
	u32 pci_func;
};

struct efct_hw_get_nvparms_cb_arg_s {
	void (*cb)(int status,
		   u8 *wwpn, u8 *wwnn,
		u8 hard_alpa, u32 preferred_d_id,
		void *arg);
	void *arg;
};

/**
 * @brief Called for the completion of get_nvparms for a
 *        user request.
 *
 * @param hw Hardware context.
 * @param status The status from the MQE.
 * @param mqe Pointer to mailbox command buffer.
 * @param arg Pointer to a callback argument.
 *
 * @return 0 on success, non-zero otherwise
 */
static int
efct_hw_get_nvparms_cb(struct efct_hw_s *hw, int status,
		       u8 *mqe, void *arg)
{
	struct efct_hw_get_nvparms_cb_arg_s *cb_arg = arg;
	struct sli4_cmd_read_nvparms_s *mbox_rsp =
			(struct sli4_cmd_read_nvparms_s *)mqe;
	u8 hard_alpa;
	u32 preferred_d_id;

	hard_alpa = le32_to_cpu(mbox_rsp->hard_alpa_d_id) &
				SLI4_READ_NVPARAMS_HARD_ALPA;
	preferred_d_id = (le32_to_cpu(mbox_rsp->hard_alpa_d_id) &
			  SLI4_READ_NVPARAMS_PREFERRED_D_ID) >> 8;
	if (cb_arg->cb)
		cb_arg->cb(status, mbox_rsp->wwpn, mbox_rsp->wwnn,
			   hard_alpa, preferred_d_id,
			   cb_arg->arg);

	kfree(mqe);
	kfree(cb_arg);

	return 0;
}

/**
 * @ingroup io
 * @brief  Read non-volatile parms.
 * @par Description
 * Issues a SLI-4 READ_NVPARMS mailbox. When the
 * command completes the provided mgmt callback function is
 * called.
 *
 * @param hw Hardware context.
 * @param cb Callback function to be called when the
 *	  command completes.
 * @param ul_arg An argument that is passed to the callback
 *	  function.
 *
 * @return
 * - EFCT_HW_RTN_SUCCESS on success.
 * - EFCT_HW_RTN_NO_MEMORY if a malloc fails.
 * - EFCT_HW_RTN_NO_RESOURCES if unable to get a command
 *   context.
 * - EFCT_HW_RTN_ERROR on any other error.
 */
int
efct_hw_get_nvparms(struct efct_hw_s *hw,
		    void (*cb)(int status, u8 *wwpn,
			       u8 *wwnn, u8 hard_alpa,
			       u32 preferred_d_id, void *arg),
		    void *ul_arg)
{
	u8 *mbxdata;
	struct efct_hw_get_nvparms_cb_arg_s *cb_arg;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;

	/* mbxdata holds the header of the command */
	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(mbxdata, 0, SLI4_BMBX_SIZE);

	/*
	 * cb_arg holds the data that will be passed to the callback on
	 * completion
	 */
	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	cb_arg->cb = cb;
	cb_arg->arg = ul_arg;

	/* Send the HW command */
	if (sli_cmd_read_nvparms(&hw->sli, mbxdata, SLI4_BMBX_SIZE))
		rc = efct_hw_command(hw, mbxdata, EFCT_CMD_NOWAIT,
				     efct_hw_get_nvparms_cb, cb_arg);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os, "READ_NVPARMS failed\n");
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

struct efct_hw_set_nvparms_cb_arg_s {
	void (*cb)(int status, void *arg);
	void *arg;
};

/**
 * @brief Called for the completion of set_nvparms for a
 *        user request.
 *
 * @param hw Hardware context.
 * @param status The status from the MQE.
 * @param mqe Pointer to mailbox command buffer.
 * @param arg Pointer to a callback argument.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
efct_hw_set_nvparms_cb(struct efct_hw_s *hw, int status,
		       u8 *mqe, void *arg)
{
	struct efct_hw_set_nvparms_cb_arg_s *cb_arg = arg;

	if (cb_arg->cb)
		cb_arg->cb(status, cb_arg->arg);

	kfree(mqe);
	kfree(cb_arg);

	return 0;
}

/**
 * @ingroup io
 * @brief  Write non-volatile parms.
 * @par Description
 * Issues a SLI-4 WRITE_NVPARMS mailbox. When the
 * command completes the provided mgmt callback function is
 * called.
 *
 * @param hw Hardware context.
 * @param cb Callback function to be called when the
 *	  command completes.
 * @param wwpn Port's WWPN in big-endian order, or NULL to use default.
 * @param wwnn Port's WWNN in big-endian order, or NULL to use default.
 * @param hard_alpa A hard AL_PA address setting used during loop
 * initialization. If no hard AL_PA is required, set to 0.
 * @param preferred_d_id A preferred D_ID address setting
 * that may be overridden with the CONFIG_LINK mailbox command.
 * If there is no preference, set to 0.
 * @param ul_arg An argument that is passed to the callback
 *	  function.
 *
 * @return
 * - EFCT_HW_RTN_SUCCESS on success.
 * - EFCT_HW_RTN_NO_MEMORY if a malloc fails.
 * - EFCT_HW_RTN_NO_RESOURCES if unable to get a command
 *   context.
 * - EFCT_HW_RTN_ERROR on any other error.
 */
int
efct_hw_set_nvparms(struct efct_hw_s *hw,
		    void (*cb)(int status, void *arg),
		u8 *wwpn, u8 *wwnn, u8 hard_alpa,
		u32 preferred_d_id,
		void *ul_arg)
{
	u8 *mbxdata;
	struct efct_hw_set_nvparms_cb_arg_s *cb_arg;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;

	/* mbxdata holds the header of the command */
	mbxdata = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!mbxdata)
		return EFCT_HW_RTN_NO_MEMORY;

	/*
	 * cb_arg holds the data that will be passed to the callback on
	 * completion
	 */
	cb_arg = kmalloc(sizeof(*cb_arg), GFP_ATOMIC);
	if (!cb_arg) {
		kfree(mbxdata);
		return EFCT_HW_RTN_NO_MEMORY;
	}

	cb_arg->cb = cb;
	cb_arg->arg = ul_arg;

	/* Send the HW command */
	if (sli_cmd_write_nvparms(&hw->sli, mbxdata, SLI4_BMBX_SIZE, wwpn,
				  wwnn, hard_alpa, preferred_d_id))
		rc = efct_hw_command(hw, mbxdata, EFCT_CMD_NOWAIT,
				     efct_hw_set_nvparms_cb, cb_arg);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os, "SET_NVPARMS failed\n");
		kfree(mbxdata);
		kfree(cb_arg);
	}

	return rc;
}

/**
 * @brief Called to obtain the count for the specified type.
 *
 * @param hw Hardware context.
 * @param io_count_type IO count type (inuse, free, wait_free).
 *
 * @return Returns the number of IOs on the specified list type.
 */
u32
efct_hw_io_get_count(struct efct_hw_s *hw,
		     enum efct_hw_io_count_type_e io_count_type)
{
	struct efct_hw_io_s *io = NULL;
	u32 count = 0;
	unsigned long flags = 0;

	spin_lock_irqsave(&hw->io_lock, flags);

	switch (io_count_type) {
	case EFCT_HW_IO_INUSE_COUNT:
		list_for_each_entry(io, &hw->io_inuse, list_entry) {
			count = count + 1;
		}
		break;
	case EFCT_HW_IO_FREE_COUNT:
		list_for_each_entry(io, &hw->io_free, list_entry) {
			count = count + 1;
		}
		break;
	case EFCT_HW_IO_WAIT_FREE_COUNT:
		list_for_each_entry(io, &hw->io_wait_free, list_entry) {
			count = count + 1;
		}
		break;
	case EFCT_HW_IO_PORT_OWNED_COUNT:
		list_for_each_entry(io, &hw->io_port_owned, list_entry) {
			count = count + 1;
		}
		break;
	case EFCT_HW_IO_N_TOTAL_IO_COUNT:
		count = hw->config.n_io;
		break;
	}

	spin_unlock_irqrestore(&hw->io_lock, flags);

	return count;
}

/**
 * @brief Called to obtain the count of produced RQs.
 *
 * @param hw Hardware context.
 *
 * @return Returns the number of RQs produced.
 */
u32
efct_hw_get_rqes_produced_count(struct efct_hw_s *hw)
{
	u32 count = 0;
	u32 i;
	u32 j;

	for (i = 0; i < hw->rq_count; i++) {
		struct hw_rq_s *rq = hw->hw_rq[i];

		if (rq->rq_tracker) {
			for (j = 0; j < rq->entry_count; j++) {
				if (rq->rq_tracker[j])
					count++;
			}
		}
	}

	return count;
}

struct efct_hw_set_active_profile_cb_arg_s {
	void (*cb)(int status, void *arg);
	void *arg;
};

/*
 * Private functions
 */

/**
 * @brief Update the queue hash with the ID and index.
 *
 * @param hash Pointer to hash table.
 * @param id ID that was created.
 * @param index The index into the hash object.
 */
static void
efct_hw_queue_hash_add(struct efct_queue_hash_s *hash,
		       u16 id, u16 index)
{
	u32	hash_index = id & (EFCT_HW_Q_HASH_SIZE - 1);

	/*
	 * Since the hash is always bigger than the number of queues, then we
	 * never have to worry about an infinite loop.
	 */
	while (hash[hash_index].in_use)
		hash_index = (hash_index + 1) & (EFCT_HW_Q_HASH_SIZE - 1);

	/* not used, claim the entry */
	hash[hash_index].id = id;
	hash[hash_index].in_use = true;
	hash[hash_index].index = index;
}

/**
 * @brief Find index given queue ID.
 *
 * @param hash Pointer to hash table.
 * @param id ID to find.
 *
 * @return Returns the index into the HW cq array or -1 if not found.
 */
int
efct_hw_queue_hash_find(struct efct_queue_hash_s *hash, u16 id)
{
	int	rc = -1;
	int	index = id & (EFCT_HW_Q_HASH_SIZE - 1);

	/*
	 * Since the hash is always bigger than the maximum number of Qs, then
	 * we never have to worry about an infinite loop. We will always find
	 * an unused entry.
	 */
	do {
		if (hash[index].in_use &&
		    hash[index].id == id)
			rc = hash[index].index;
		else
			index = (index + 1) & (EFCT_HW_Q_HASH_SIZE - 1);
	} while (rc == -1 && hash[index].in_use);

	return rc;
}

static int
efct_hw_domain_add(struct efct_hw_s *hw, struct efc_domain_s *domain)
{
	int		rc = EFCT_HW_RTN_ERROR;
	u16	fcfi = U16_MAX;

	if (!hw || !domain) {
		efct_log_err(NULL, "bad parameter hw=%p domain=%p\n",
			     hw, domain);
		return EFCT_HW_RTN_ERROR;
	}

	fcfi = domain->fcf_indicator;

	if (fcfi < SLI4_MAX_FCFI) {
		u16	fcf_index = U16_MAX;

		efct_log_debug(hw->os, "adding domain %p @ %#x\n",
			       domain, fcfi);
		hw->domains[fcfi] = domain;

		/* HW_WORKAROUND_OVERRIDE_FCFI_IN_SRB */
		if (hw->workaround.override_fcfi) {
			if (hw->first_domain_idx < 0)
				hw->first_domain_idx = fcfi;
		}

		fcf_index = domain->fcf;

		if (fcf_index < SLI4_MAX_FCF_INDEX) {
			efct_log_debug(hw->os,
				       "adding map of FCF index %d to FCFI %d\n",
				fcf_index, fcfi);
			hw->fcf_index_fcfi[fcf_index] = fcfi;
			rc = EFCT_HW_RTN_SUCCESS;
		} else {
			efct_log_test(hw->os,
				      "FCF index %d out of range (max %d)\n",
				     fcf_index, SLI4_MAX_FCF_INDEX);
			hw->domains[fcfi] = NULL;
		}
	} else {
		efct_log_test(hw->os, "FCFI %#x out of range (max %#x)\n",
			      fcfi, SLI4_MAX_FCFI);
	}

	return rc;
}

static int
efct_hw_domain_del(struct efct_hw_s *hw, struct efc_domain_s *domain)
{
	int		rc = EFCT_HW_RTN_ERROR;
	u16	fcfi = U16_MAX;

	if (!hw || !domain) {
		efct_log_err(NULL, "bad parameter hw=%p domain=%p\n",
			     hw, domain);
		return EFCT_HW_RTN_ERROR;
	}

	fcfi = domain->fcf_indicator;

	if (fcfi < SLI4_MAX_FCFI) {
		u16	fcf_index = U16_MAX;

		efct_log_debug(hw->os, "deleting domain %p @ %#x\n",
			       domain, fcfi);

		if (domain != hw->domains[fcfi]) {
			efct_log_test(hw->os,
				      "%s%p does not match stored domain %p\n",
				     "provided domain ",
				     domain, hw->domains[fcfi]);
			return EFCT_HW_RTN_ERROR;
		}

		hw->domains[fcfi] = NULL;

		/* HW_WORKAROUND_OVERRIDE_FCFI_IN_SRB */
		if (hw->workaround.override_fcfi) {
			if (hw->first_domain_idx == fcfi)
				hw->first_domain_idx = -1;
		}

		fcf_index = domain->fcf;

		if (fcf_index < SLI4_MAX_FCF_INDEX) {
			if (hw->fcf_index_fcfi[fcf_index] == fcfi) {
				hw->fcf_index_fcfi[fcf_index] = 0;
				rc = EFCT_HW_RTN_SUCCESS;
			} else {
				efct_log_test(hw->os,
					      "%#x%#x @ %d indexed FCFI doesn't match provided\n",
					hw->fcf_index_fcfi[fcf_index],
					fcfi, fcf_index);
			}
		} else {
			efct_log_test(hw->os,
				      "FCF index %d out of range (max %d)\n",
				     fcf_index, SLI4_MAX_FCF_INDEX);
		}
	} else {
		efct_log_test(hw->os, "FCFI %#x out of range (max %#x)\n",
			      fcfi, SLI4_MAX_FCFI);
	}

	return rc;
}

struct efc_domain_s *
efct_hw_domain_get(struct efc_lport *efc, u16 fcfi)
{
	struct efct_s *efct = efc->base;
	struct efct_hw_s *hw = &efct->hw;

	if (!hw) {
		efct_log_err(NULL, "bad parameter hw=%p\n", hw);
		return NULL;
	}

	if (fcfi < SLI4_MAX_FCFI)
		return hw->domains[fcfi];

	efct_log_test(hw->os, "FCFI %#x out of range (max %#x)\n",
		      fcfi, SLI4_MAX_FCFI);
	return NULL;
}

/**
 * @brief Quaratine an IO by taking a reference count and adding it to the
 *        quarantine list. When the IO is popped from the list then the
 *        count is released and the IO MAY be freed depending on whether
 *        it is still referenced by the IO.
 *
 *        If this is a target write or an initiator read using
 *        DIF, then we must add the XRI to a quarantine list until we receive
 *        4 more completions of this same type.
 *
 * @param hw Hardware context.
 * @param wq Pointer to the WQ associated with the IO object to quarantine.
 * @param io Pointer to the io object to quarantine.
 */
static void
efct_hw_io_quarantine(struct efct_hw_s *hw, struct hw_wq_s *wq,
		      struct efct_hw_io_s *io)
{
	struct efct_quarantine_info_s *q_info = &wq->quarantine_info;
	u32	index;
	struct efct_hw_io_s	*free_io = NULL;
	unsigned long flags = 0;

	/* return if the QX bit was clear */
	if (!io->quarantine)
		return;

	/*
	 * increment the IO refcount to prevent it from being freed before the
	 * quarantine is over
	 */
	if (kref_get_unless_zero(&io->ref) == 0) {
		/* command no longer active */
		efct_log_debug(hw->os,
			       "io not active xri=0x%x tag=0x%x\n",
			      io->indicator, io->reqtag);
		return;
	}

	spin_lock_irqsave(&wq->queue->lock, flags);
	index = q_info->quarantine_index;
	free_io = q_info->quarantine_ios[index];
	q_info->quarantine_ios[index] = io;
	q_info->quarantine_index =
			(index + 1) % EFCT_HW_QUARANTINE_QUEUE_DEPTH;
	spin_unlock_irqrestore(&wq->queue->lock, flags);

	if (free_io)
		kref_put(&free_io->ref, free_io->release);
}

/**
 * @brief Process entries on the given completion queue.
 *
 * @param hw Hardware context.
 * @param cq Pointer to the HW completion queue object.
 *
 * @return None.
 */
void
efct_hw_cq_process(struct efct_hw_s *hw, struct hw_cq_s *cq)
{
	u8		cqe[sizeof(struct sli4_mcqe_s)];
	u16	rid = U16_MAX;
	enum sli4_qentry_e	ctype;		/* completion type */
	int		status;
	u32	n_processed = 0;
	u32	tstart, telapsed;

	tstart = jiffies_to_msecs(jiffies);

	while (!sli_cq_read(&hw->sli, cq->queue, cqe)) {
		status = sli_cq_parse(&hw->sli, cq->queue,
				      cqe, &ctype, &rid);
		/*
		 * The sign of status is significant. If status is:
		 * == 0 : call completed correctly and
		 * the CQE indicated success
		 * > 0 : call completed correctly and
		 * the CQE indicated an error
		 * < 0 : call failed and no information is available about the
		 * CQE
		 */
		if (status < 0) {
			if (status == -2)
				/*
				 * Notification that an entry was consumed,
				 * but not completed
				 */
				continue;

			break;
		}

		switch (ctype) {
		case SLI_QENTRY_ASYNC:
			CPUTRACE(efct, "async");
			sli_cqe_async(&hw->sli, cqe);
			break;
		case SLI_QENTRY_MQ:
			/*
			 * Process MQ entry. Note there is no way to determine
			 * the MQ_ID from the completion entry.
			 */
			CPUTRACE(efct, "mq");
			efct_hw_mq_process(hw, status, hw->mq);
			break;
		case SLI_QENTRY_OPT_WRITE_CMD:
			efct_hw_rqpair_process_auto_xfr_rdy_cmd(hw, cq, cqe);
			break;
		case SLI_QENTRY_OPT_WRITE_DATA:
			efct_hw_rqpair_process_auto_xfr_rdy_data(hw, cq, cqe);
			break;
		case SLI_QENTRY_WQ:
			CPUTRACE(efct, "wq");
			efct_hw_wq_process(hw, cq, cqe, status, rid);
			break;
		case SLI_QENTRY_WQ_RELEASE: {
			u32 wq_id = rid;
			u32 index;
			struct hw_wq_s *wq;

			index = efct_hw_queue_hash_find(hw->wq_hash, wq_id);
			wq = hw->hw_wq[index];
			/* Submit any HW IOs that are on the WQ pending list */
			hw_wq_submit_pending(wq, wq->wqec_set_count);

			break;
		}

		case SLI_QENTRY_RQ:
			CPUTRACE(efct, "rq");
			efct_hw_rqpair_process_rq(hw, cq, cqe);
			break;
		case SLI_QENTRY_XABT: {
			CPUTRACE(efct, "xabt");
			efct_hw_xabt_process(hw, cq, cqe, rid);
			break;
		}
		default:
			efct_log_test(hw->os,
				      "unhandled ctype=%#x rid=%#x\n",
				     ctype, rid);
			break;
		}

		n_processed++;
		if (n_processed == cq->queue->proc_limit)
			break;

		if (cq->queue->n_posted >= cq->queue->posted_limit)
			sli_queue_arm(&hw->sli, cq->queue, false);
	}

	sli_queue_arm(&hw->sli, cq->queue, true);

	if (n_processed > cq->queue->max_num_processed)
		cq->queue->max_num_processed = n_processed;
	telapsed = jiffies_to_msecs(jiffies) - tstart;
	if (telapsed > cq->queue->max_process_time)
		cq->queue->max_process_time = telapsed;
}

/**
 * @brief Process WQ completion queue entries.
 *
 * @param hw Hardware context.
 * @param cq Pointer to the HW completion queue object.
 * @param cqe Pointer to WQ completion queue.
 * @param status Completion status.
 * @param rid Resource ID (IO tag).
 *
 * @return none
 */
void
efct_hw_wq_process(struct efct_hw_s *hw, struct hw_cq_s *cq,
		   u8 *cqe, int status, u16 rid)
{
	struct hw_wq_callback_s *wqcb;

	if (rid == EFCT_HW_REQUE_XRI_REGTAG) {
		if (status)
			efct_log_err(hw->os, "reque xri failed, status = %d\n",
				     status);
		return;
	}

	wqcb = efct_hw_reqtag_get_instance(hw, rid);
	if (!wqcb) {
		efct_log_err(hw->os, "invalid request tag: x%x\n", rid);
		return;
	}

	if (!wqcb->callback) {
		efct_log_err(hw->os, "wqcb callback is NULL\n");
		return;
	}

	(*wqcb->callback)(wqcb->arg, cqe, status);
}

/**
 * @brief Process WQ completions for IO requests
 *
 * @param arg Generic callback argument
 * @param cqe Pointer to completion queue entry
 * @param status Completion status
 *
 * @par Description
 * Note:  Regarding io->reqtag, the reqtag is assigned once when HW IOs are
 * initialized in efct_hw_setup_io(), and don't need to be returned to the
 * hw->wq_reqtag_pool.
 *
 * @return None.
 */
static void
efct_hw_wq_process_io(void *arg, u8 *cqe, int status)
{
	struct efct_hw_io_s *io = arg;
	struct efct_hw_s *hw = io->hw;
	struct sli4_fc_wcqe_s *wcqe = (void *)cqe;
	struct efct_s *efct = hw->os;
	u32	len = 0;
	u32 ext = 0;
	u8 out_of_order_axr_cmd = 0;
	u8 out_of_order_axr_data = 0;
	u8 lock_taken = 0;
	unsigned long flags = 0;

	/*
	 * For the primary IO, this will also be used for the
	 * response. So it is important to only set/clear this
	 * flag on the first data phase of the IO because
	 * subsequent phases will be done on the secondary XRI.
	 */
	if (io->quarantine && io->quarantine_first_phase) {
		io->quarantine = ((wcqe->qx_byte & SLI4_WCQE_QX) == 1);
		efct_hw_io_quarantine(hw, io->wq, io);
	}
	io->quarantine_first_phase = false;

	if (io->sec_hio &&
	    io->sec_hio->quarantine) {
		/*
		 * If the quarantine flag is set on the
		 * IO, then set it on the secondary IO
		 * based on the quarantine XRI (QX) bit
		 * sent by the FW.
		 */
		io->sec_hio->quarantine =
				((wcqe->qx_byte & SLI4_WCQE_QX) == 1);
		/*
		 * use the primary io->wq because it is not set on the
		 * secondary IO.
		 */
		efct_hw_io_quarantine(hw, io->wq, io->sec_hio);
	}

	efct_hw_remove_io_timed_wqe(hw, io);

	/* clear xbusy flag if WCQE[XB] is clear */
	if (io->xbusy && (wcqe->flags & SLI4_WCQE_XB) == 0)
		io->xbusy = false;

	/* get extended CQE status */
	switch (io->type) {
	case EFCT_HW_BLS_ACC:
	case EFCT_HW_BLS_ACC_SID:
		break;
	case EFCT_HW_ELS_REQ:
		sli_fc_els_did(&hw->sli, cqe, &ext);
		len = sli_fc_response_length(&hw->sli, cqe);
		break;
	case EFCT_HW_ELS_RSP:
	case EFCT_HW_ELS_RSP_SID:
	case EFCT_HW_FC_CT_RSP:
		break;
	case EFCT_HW_FC_CT:
		len = sli_fc_response_length(&hw->sli, cqe);
		break;
	case EFCT_HW_IO_TARGET_WRITE:
		len = sli_fc_io_length(&hw->sli, cqe);
		break;
	case EFCT_HW_IO_TARGET_READ:
		len = sli_fc_io_length(&hw->sli, cqe);
		/*
		 * if_type == 2 seems to return 0 "total length placed" on
		 * FCP_TSEND64_WQE completions. If this appears to happen,
		 * use the CTIO data transfer length instead.
		 */
		if (hw->workaround.retain_tsend_io_length && !len && !status)
			len = io->length;

		break;
	case EFCT_HW_IO_TARGET_RSP:
		if (io->is_port_owned) {
			spin_lock_irqsave(&io->axr_lock, flags);
			lock_taken = 1;
			if (io->axr_buf->call_axr_cmd)
				out_of_order_axr_cmd = 1;

			if (io->axr_buf->call_axr_data)
				out_of_order_axr_data = 1;
		}
		break;
	case EFCT_HW_IO_INITIATOR_READ:
		len = sli_fc_io_length(&hw->sli, cqe);
		break;
	case EFCT_HW_IO_INITIATOR_WRITE:
		len = sli_fc_io_length(&hw->sli, cqe);
		break;
	case EFCT_HW_IO_INITIATOR_NODATA:
		break;
	case EFCT_HW_IO_DNRX_REQUEUE:
		/* release the count for re-posting the buffer */
		/* efct_hw_io_free(hw, io); */
		break;
	default:
		efct_log_test(hw->os, "unhandled io type %#x for XRI 0x%x\n",
			      io->type, io->indicator);
		break;
	}
	if (status) {
		ext = sli_fc_ext_status(&hw->sli, cqe);
		/*
		 * Emulate IAAB=0 for initiator WQEs only; i.e. automatically
		 * abort exchange if an error occurred and exchange is still
		 * busy.
		 */
		if (hw->config.i_only_aab &&
		    (efct_hw_iotype_is_originator(io->type)) &&
		    (efct_hw_wcqe_abort_needed(status, ext,
				wcqe->flags & SLI4_WCQE_XB))) {
			enum efct_hw_rtn_e rc;

			efct_log_debug(hw->os, "aborting xri=%#x tag=%#x\n",
				       io->indicator, io->reqtag);
			/*
			 * Because the initiator will not issue another IO
			 * phase, then it is OK to to issue the callback on
			 * the abort completion, but for consistency with the
			 * target, wait for the XRI_ABORTED CQE to issue the
			 * IO callback.
			 */
			rc = efct_hw_io_abort(hw, io, true, NULL, NULL);

			if (rc == EFCT_HW_RTN_SUCCESS) {
				/*
				 * latch status to return after abort is
				 * complete
				 */
				io->status_saved = true;
				io->saved_status = status;
				io->saved_ext = ext;
				io->saved_len = len;
				goto exit_efct_hw_wq_process_io;
			} else if (rc == EFCT_HW_RTN_IO_ABORT_IN_PROGRESS) {
				/*
				 * Already being aborted by someone else (ABTS
				 * perhaps). Just fall through and return
				 * original error.
				 */
				efct_log_debug(hw->os, "%s=%#x tag=%#x\n",
					       "abort in progress xri",
					      io->indicator, io->reqtag);

			} else {
				/*
				 * Failed to abort for some other reason, log
				 * error
				 */
				efct_log_test(hw->os, "%s%#x tag=%#x rc=%d\n",
					      "Failed to abort xri=",
					     io->indicator, io->reqtag, rc);
			}
		}

		/*
		 * If we're not an originator IO, and XB is set, then issue
		 * abort for the IO from within the HW
		 */
		if ((!efct_hw_iotype_is_originator(io->type)) &&
		    wcqe->flags & SLI4_WCQE_XB) {
			enum efct_hw_rtn_e rc;

			efct_log_debug(hw->os, "aborting xri=%#x tag=%#x\n",
				       io->indicator, io->reqtag);

			/*
			 * Because targets may send a response when the IO
			 * completes using the same XRI, we must wait for the
			 * XRI_ABORTED CQE to issue the IO callback
			 */
			rc = efct_hw_io_abort(hw, io, false, NULL, NULL);
			if (rc == EFCT_HW_RTN_SUCCESS) {
				/*
				 * latch status to return after abort is
				 * complete
				 */
				io->status_saved = true;
				io->saved_status = status;
				io->saved_ext = ext;
				io->saved_len = len;
				goto exit_efct_hw_wq_process_io;
			} else if (rc == EFCT_HW_RTN_IO_ABORT_IN_PROGRESS) {
				/*
				 * Already being aborted by someone else (ABTS
				 * perhaps). Just fall thru and return original
				 * error.
				 */
				efct_log_debug(hw->os, "%s%#x tag=%#x\n",
					       "abort in progress xri=",
					      io->indicator, io->reqtag);

			} else {
				/* Failed to abort for some other reason, log
				 * error
				 */
				efct_log_test(hw->os, "%s%#x tag=%#x rc=%d\n",
					      "Failed to abort xri=",
					     io->indicator, io->reqtag, rc);
			}
		}
	}
	/* free secondary HW IO */
	if (io->sec_hio) {
		efct_hw_io_free(hw, io->sec_hio);
		io->sec_hio = NULL;
	}

	if (io->done) {
		efct_hw_done_t done = io->done;
		void		*arg = io->arg;

		io->done = NULL;

		if (io->status_saved) {
			/* use latched status if exists */
			status = io->saved_status;
			len = io->saved_len;
			ext = io->saved_ext;
			io->status_saved = false;
		}

		/* Restore default SGL */
		efct_hw_io_restore_sgl(hw, io);
		done(io, io->rnode, len, status, ext, arg);
	}

	if (out_of_order_axr_cmd) {
		efct_unsolicited_cb(efct, io->axr_buf->cmd_seq);

		if (out_of_order_axr_data)
			efct_unsolicited_cb(efct, &io->axr_buf->seq);
	}

exit_efct_hw_wq_process_io:
	if (lock_taken)
		spin_unlock_irqrestore(&io->axr_lock, flags);
}

/**
 * @brief Process WQ completions for abort requests.
 *
 * @param arg Generic callback argument.
 * @param cqe Pointer to completion queue entry.
 * @param status Completion status.
 *
 * @return None.
 */
static void
efct_hw_wq_process_abort(void *arg, u8 *cqe, int status)
{
	struct efct_hw_io_s *io = arg;
	struct efct_hw_s *hw = io->hw;
	u32 ext = 0;
	u32 len = 0;
	struct hw_wq_callback_s *wqcb;
	unsigned long flags = 0;

	/*
	 * For IOs that were aborted internally, we may need to issue the
	 * callback here depending on whether a XRI_ABORTED CQE is expected ot
	 * not. If the status is Local Reject/No XRI, then
	 * issue the callback now.
	 */
	ext = sli_fc_ext_status(&hw->sli, cqe);
	if (status == SLI4_FC_WCQE_STATUS_LOCAL_REJECT &&
	    ext == SLI4_FC_LOCAL_REJECT_NO_XRI &&
		io->done) {
		efct_hw_done_t done = io->done;
		void		*arg = io->arg;

		io->done = NULL;

		/*
		 * Use latched status as this is always saved for an internal
		 * abort Note: We wont have both a done and abort_done
		 * function, so don't worry about
		 *       clobbering the len, status and ext fields.
		 */
		status = io->saved_status;
		len = io->saved_len;
		ext = io->saved_ext;
		io->status_saved = false;
		done(io, io->rnode, len, status, ext, arg);
	}

	if (io->abort_done) {
		efct_hw_done_t done = io->abort_done;
		void *arg = io->abort_arg;

		io->abort_done = NULL;

		done(io, io->rnode, len, status, ext, arg);
	}
	spin_lock_irqsave(&hw->io_abort_lock, flags);
	/* clear abort bit to indicate abort is complete */
	io->abort_in_progress = false;
	spin_unlock_irqrestore(&hw->io_abort_lock, flags);

	/* Free the WQ callback */
	efct_hw_assert(io->abort_reqtag != U32_MAX);
	wqcb = efct_hw_reqtag_get_instance(hw, io->abort_reqtag);
	efct_hw_reqtag_free(hw, wqcb);

	/*
	 * Call efct_hw_io_free() because this releases the WQ reservation as
	 * well as doing the refcount put. Don't duplicate the code here.
	 */
	(void)efct_hw_io_free(hw, io);
}

/**
 * @brief Process XABT completions
 *
 * @param hw Hardware context.
 * @param cq Pointer to the HW completion queue object.
 * @param cqe Pointer to WQ completion queue.
 * @param rid Resource ID (IO tag).
 *
 *
 * @return None.
 */
void
efct_hw_xabt_process(struct efct_hw_s *hw, struct hw_cq_s *cq,
		     u8 *cqe, u16 rid)
{
	/* search IOs wait free list */
	struct efct_hw_io_s *io = NULL;
	unsigned long flags = 0;

	io = efct_hw_io_lookup(hw, rid);
	if (!io) {
		/* IO lookup failure should never happen */
		efct_log_err(hw->os,
			     "Error: xabt io lookup failed rid=%#x\n", rid);
		return;
	}

	if (!io->xbusy)
		efct_log_debug(hw->os, "xabt io not busy rid=%#x\n", rid);
	else
		/* mark IO as no longer busy */
		io->xbusy = false;

	if (io->is_port_owned) {
		spin_lock_irqsave(&hw->io_lock, flags);
		/*
		 * Take reference so that below callback will
		 * not free io before reque
		 */
		kref_get(&io->ref);
		spin_unlock_irqrestore(&hw->io_lock, flags);
	}

	/*
	 * For IOs that were aborted internally, we need to issue any pending
	 * callback here.
	 */
	if (io->done) {
		efct_hw_done_t done = io->done;
		void		*arg = io->arg;

		/*
		 * Use latched status as this is always saved for an internal
		 * abort
		 */
		int status = io->saved_status;
		u32 len = io->saved_len;
		u32 ext = io->saved_ext;

		io->done = NULL;
		io->status_saved = false;

		done(io, io->rnode, len, status, ext, arg);
	}

	/* Check to see if this is a port owned XRI */
	if (io->is_port_owned) {
		spin_lock_irqsave(&hw->io_lock, flags);
		efct_hw_reque_xri(hw, io);
		spin_unlock_irqrestore(&hw->io_lock, flags);
		/* Not hanlding reque xri completion, free io */
		efct_hw_io_free(hw, io);
		return;
	}

	spin_lock_irqsave(&hw->io_lock, flags);
	if (io->state == EFCT_HW_IO_STATE_INUSE ||
	    io->state == EFCT_HW_IO_STATE_WAIT_FREE) {
		/* if on wait_free list, caller has already freed IO;
		 * remove from wait_free list and add to free list.
		 * if on in-use list, already marked as no longer busy;
		 * just leave there and wait for caller to free.
		 */
		if (io->state == EFCT_HW_IO_STATE_WAIT_FREE) {
			io->state = EFCT_HW_IO_STATE_FREE;
			list_del(&io->list_entry);
			efct_hw_io_free_move_correct_list(hw, io);
		}
	}
	spin_unlock_irqrestore(&hw->io_lock, flags);
}

/**
 * @brief Adjust the number of WQs and CQs within the HW.
 *
 * @par Description
 * Calculates the number of WQs and associated CQs needed in the HW based on
 * the number of IOs. Calculates the starting CQ index for each WQ, RQ and
 * MQ.
 *
 * @param hw Hardware context allocated by the caller.
 */
static void
efct_hw_adjust_wqs(struct efct_hw_s *hw)
{
	u32 max_wq_num = hw->sli.max_qcount[SLI_QTYPE_WQ];
	u32 max_wq_entries = hw->num_qentries[SLI_QTYPE_WQ];
	u32 max_cq_entries = hw->num_qentries[SLI_QTYPE_CQ];

	/*
	 * possibly adjust the the size of the WQs so that the CQ is twice as
	 * big as the WQ to allow for 2 completions per IO. This allows us to
	 * handle multi-phase as well as aborts.
	 */
	if (max_cq_entries < max_wq_entries * 2) {
		hw->num_qentries[SLI_QTYPE_WQ] = max_cq_entries / 2;
		max_wq_entries =  hw->num_qentries[SLI_QTYPE_WQ];
	}

	/*
	 * Calculate the number of WQs to use base on the number of IOs.
	 *
	 * Note: We need to reserve room for aborts which must be sent down
	 *       the same WQ as the IO. So we allocate enough WQ space to
	 *       handle 2 times the number of IOs. Half of the space will be
	 *       used for normal IOs and the other hwf is reserved for aborts.
	 */
	hw->config.n_wq = ((hw->config.n_io * 2) + (max_wq_entries - 1))
			    / max_wq_entries;

	/*
	 * For dual-chute support, we need to have at least one WQ per chute.
	 */
	if (hw->config.n_wq < 2 &&
	    efct_hw_get_num_chutes(hw) > 1)
		hw->config.n_wq = 2;

	/* make sure we haven't exceeded the max supported in the HW */
	if (hw->config.n_wq > EFCT_HW_MAX_NUM_WQ)
		hw->config.n_wq = EFCT_HW_MAX_NUM_WQ;

	/* make sure we haven't exceeded the chip maximum */
	if (hw->config.n_wq > max_wq_num)
		hw->config.n_wq = max_wq_num;

	/*
	 * Using Queue Topology string, we divide by number of chutes
	 */
	hw->config.n_wq /= efct_hw_get_num_chutes(hw);
}

static int
efct_hw_command_process(struct efct_hw_s *hw, int status, u8 *mqe,
			size_t size)
{
	struct efct_command_ctx_s *ctx = NULL;
	unsigned long flags = 0;

	spin_lock_irqsave(&hw->cmd_lock, flags);
	if (!list_empty(&hw->cmd_head)) {
		ctx = list_first_entry(&hw->cmd_head,
				       struct efct_command_ctx_s, list_entry);
		list_del(&ctx->list_entry);
	}
	if (!ctx) {
		efct_log_err(hw->os, "no command context?!?\n");
		spin_unlock_irqrestore(&hw->cmd_lock, flags);
		return -1;
	}

	hw->cmd_head_count--;

	/* Post any pending requests */
	efct_hw_cmd_submit_pending(hw);

	spin_unlock_irqrestore(&hw->cmd_lock, flags);

	if (ctx->cb) {
		if (ctx->buf)
			memcpy(ctx->buf, mqe, size);

		ctx->cb(hw, status, ctx->buf, ctx->arg);
	}

	memset(ctx, 0, sizeof(struct efct_command_ctx_s));
	kfree(ctx);

	return 0;
}

/**
 * @brief Process entries on the given mailbox queue.
 *
 * @param hw Hardware context.
 * @param status CQE status.
 * @param mq Pointer to the mailbox queue object.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
efct_hw_mq_process(struct efct_hw_s *hw,
		   int status, struct sli4_queue_s *mq)
{
	u8		mqe[SLI4_BMBX_SIZE];

	if (!sli_mq_read(&hw->sli, mq, mqe))
		efct_hw_command_process(hw, status, mqe, mq->size);

	return 0;
}

/**
 * @brief Read a FCF table entry.
 *
 * @param hw Hardware context.
 * @param index Table index to read. Use SLI4_FCF_TABLE_FIRST for
 * the first read and the next_index field from the READ_FCF_TABLE command
 * for subsequent reads.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static enum efct_hw_rtn_e
efct_hw_read_fcf(struct efct_hw_s *hw, u32 index)
{
	struct efc_dma_s	*dma = NULL;
	u8		*buf = NULL;
	int		rc = EFCT_HW_RTN_ERROR;
	struct efct_s *efct = hw->os;

	buf = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!buf)
		return EFCT_HW_RTN_NO_MEMORY;

	dma = kmalloc(sizeof(*dma), GFP_ATOMIC);
	if (!dma) {
		kfree(buf);
		return EFCT_HW_RTN_NO_MEMORY;
	}
	memset(dma, 0, sizeof(struct efc_dma_s));

	dma->size = EFCT_HW_READ_FCF_SIZE;
	dma->virt = dma_alloc_coherent(&efct->pdev->dev,
				       dma->size, &dma->phys, GFP_DMA);
	if (!dma->virt) {
		efct_log_err(hw->os, "dma_alloc fail\n");
		kfree(buf);
		kfree(dma);
		return EFCT_HW_RTN_ERROR;
	}

	if (sli_cmd_read_fcf_table(&hw->sli, buf,
				   SLI4_BMBX_SIZE, dma, index))
		rc = efct_hw_command(hw, buf, EFCT_CMD_NOWAIT,
				     efct_hw_cb_read_fcf, dma);

	if (rc != EFCT_HW_RTN_SUCCESS) {
		efct_log_test(hw->os, "READ_FCF_TABLE failed\n");
		dma_free_coherent(&efct->pdev->dev,
				  dma->size, dma->virt, dma->phys);
		memset(dma, 0, sizeof(struct efc_dma_s));
		kfree(buf);
		kfree(dma);
	}

	return rc;
}

/**
 * @brief Callback function for the READ_FCF_TABLE command.
 *
 * @par Description
 * Note that the caller has allocated:
 *  - DMA memory to hold the table contents
 *  - DMA memory structure
 *  - Command/results buffer
 *  .
 * Each of these must be freed here.
 *
 * @param hw Hardware context.
 * @param status Hardware status.
 * @param mqe Pointer to the mailbox command/results buffer.
 * @param arg Pointer to the DMA memory structure.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
efct_hw_cb_read_fcf(struct efct_hw_s *hw,
		    int status, u8 *mqe, void *arg)
{
	struct efc_dma_s *dma = arg;
	struct sli4_mbox_command_header_s *hdr =
				(struct sli4_mbox_command_header_s *)mqe;

	struct efct_s *efct = hw->os;

	if (status || le16_to_cpu(hdr->status)) {
		efct_log_test(hw->os, "bad status cqe=%#x mqe=%#x\n",
			      status, le16_to_cpu(hdr->status));
	} else if (dma->virt) {
		struct sli4_res_read_fcf_table_s *read_fcf = dma->virt;

		/* if FC and FCF entry valid, process it */
		if ((read_fcf->fcf_entry.val_fc_sol & SLI4_FCF_FC) ||
		    ((read_fcf->fcf_entry.val_fc_sol & SLI4_FCF_VAL) &&
		     !(read_fcf->fcf_entry.val_fc_sol & SLI4_FCF_SOL))) {
			struct efc_domain_record_s drec = {0};

			if (read_fcf->fcf_entry.val_fc_sol &
			    SLI4_FCF_FC) {
				/*
				 * This is a pseudo FCF entry. Create a
				 * domain record based on the read
				 * topology information
				 */
				drec.speed = hw->link.speed;
				drec.fc_id = hw->link.fc_id;
				drec.is_fc = true;
				if (hw->link.topology ==
				    SLI_LINK_TOPO_LOOP) {
					drec.is_loop = true;
					memcpy(drec.map.loop,
					       hw->link.loop_map,
					       sizeof(drec.map.loop));
				} else if (hw->link.topology ==
					   SLI_LINK_TOPO_NPORT) {
					drec.is_nport = true;
				}
			} else {
				drec.index =
				le16_to_cpu(read_fcf->fcf_entry.fcf_index);
				drec.priority =
				le32_to_cpu(read_fcf->fcf_entry.fip_priority);

				/* copy address, wwn and vlan_bitmap */
				memcpy(drec.address,
				       read_fcf->fcf_entry.fcf_mac_address,
				  sizeof(drec.address));
				memcpy(drec.wwn,
				       read_fcf->fcf_entry.fabric_name_id,
				  sizeof(drec.wwn));
				memcpy(drec.map.vlan,
				       read_fcf->fcf_entry.vlan_bitmap,
				  sizeof(drec.map.vlan));

				drec.is_ethernet = true;
				drec.is_nport = true;
			}

			efc_domain_cb(efct->efcport, EFC_HW_DOMAIN_FOUND,
				      &drec);
		} else {
			/* if FCF is not valid, ignore it */
			efct_log_test(hw->os, "ignore invalid FCF entry\n");
		}

		if (le16_to_cpu(read_fcf->next_index) !=
		    SLI4_FCF_TABLE_LAST)
			efct_hw_read_fcf(hw,
					 le16_to_cpu(read_fcf->next_index));
	}

	kfree(mqe);
	dma_free_coherent(&efct->pdev->dev,
			  dma->size, dma->virt, dma->phys);
	memset(dma, 0, sizeof(struct efc_dma_s));
	kfree(dma);

	return 0;
}

/**
 * @brief Callback function for the SLI link events.
 *
 * @par Description
 * This function allocates memory which must be freed in its callback.
 *
 * @param ctx Hardware context pointer (that is, struct efct_hw_s *).
 * @param e Event structure pointer (that is, struct sli4_link_event_s *).
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
efct_hw_cb_link(void *ctx, void *e)
{
	struct efct_hw_s	*hw = ctx;
	struct sli4_link_event_s *event = e;
	struct efc_domain_s	*d = NULL;
	u32	i = 0;
	int		rc = EFCT_HW_RTN_ERROR;
	struct efct_s	*efct = hw->os;
	struct efc_dma_s *dma;

	efct_hw_link_event_init(hw);

	switch (event->status) {
	case SLI_LINK_STATUS_UP:

		hw->link = *event;
		efct->efcport->link_status = EFC_LINK_STATUS_UP;

		if (event->topology == SLI_LINK_TOPO_NPORT) {
			efct_log_info(hw->os, "Link Up, NPORT, speed is %d\n",
				      event->speed);
			efct_hw_read_fcf(hw, SLI4_FCF_TABLE_FIRST);
		} else if (event->topology == SLI_LINK_TOPO_LOOP) {
			u8	*buf = NULL;

			efct_log_info(hw->os, "Link Up, LOOP, speed is %d\n",
				      event->speed);
			dma = &hw->loop_map;
			dma->size = SLI4_MIN_LOOP_MAP_BYTES;
			dma->virt = dma_alloc_coherent(&efct->pdev->dev,
						       dma->size, &dma->phys,
						       GFP_DMA);
			if (!dma->virt)
				efct_log_err(hw->os, "efct_dma_alloc_fail\n");

			buf = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
			if (!buf)
				break;

			if (sli_cmd_read_topology(&hw->sli, buf,
						  SLI4_BMBX_SIZE,
						       &hw->loop_map)) {
				rc = efct_hw_command(hw, buf, EFCT_CMD_NOWAIT,
						     __efct_read_topology_cb,
						     NULL);
			}

			if (rc != EFCT_HW_RTN_SUCCESS) {
				efct_log_test(hw->os, "READ_TOPOLOGY failed\n");
				kfree(buf);
			}
		} else {
			efct_log_info(hw->os, "%s(%#x), speed is %d\n",
				      "Link Up, unsupported topology ",
				     event->topology, event->speed);
		}
		break;
	case SLI_LINK_STATUS_DOWN:
		efct_log_info(hw->os, "Link down\n");

		hw->link.status = event->status;
		efct->efcport->link_status = EFC_LINK_STATUS_DOWN;

		for (i = 0; d = hw->domains[i], i < SLI4_MAX_FCFI; i++) {
			if (d)
				efc_domain_cb(efct->efcport,
					      EFC_HW_DOMAIN_LOST, d);
		}
		break;
	default:
		efct_log_test(hw->os, "unhandled link status %#x\n",
			      event->status);
		break;
	}

	return 0;
}

static int
efct_hw_cb_node_attach(struct efct_hw_s *hw, int status,
		       u8 *mqe, void *arg)
{
	struct efc_remote_node_s *rnode = arg;
	struct sli4_mbox_command_header_s *hdr =
				(struct sli4_mbox_command_header_s *)mqe;
	enum efc_hw_remote_node_event_e	evt = 0;

	struct efct_s   *efct = hw->os;

	if (status || le16_to_cpu(hdr->status)) {
		efct_log_debug(hw->os, "bad status cqe=%#x mqe=%#x\n", status,
			       le16_to_cpu(hdr->status));
		atomic_sub_return(1, &hw->rpi_ref[rnode->index].rpi_count);
		rnode->attached = false;
		atomic_set(&hw->rpi_ref[rnode->index].rpi_attached, 0);
		evt = EFC_HW_NODE_ATTACH_FAIL;
	} else {
		rnode->attached = true;
		atomic_set(&hw->rpi_ref[rnode->index].rpi_attached, 1);
		evt = EFC_HW_NODE_ATTACH_OK;
	}

	efc_remote_node_cb(efct->efcport, evt, rnode);

	kfree(mqe);

	return 0;
}

static int
efct_hw_cb_node_free(struct efct_hw_s *hw,
		     int status, u8 *mqe, void *arg)
{
	struct efc_remote_node_s *rnode = arg;
	struct sli4_mbox_command_header_s *hdr =
				(struct sli4_mbox_command_header_s *)mqe;
	enum efc_hw_remote_node_event_e evt = EFC_HW_NODE_FREE_FAIL;
	int		rc = 0;
	struct efct_s   *efct = hw->os;

	if (status || le16_to_cpu(hdr->status)) {
		efct_log_debug(hw->os, "bad status cqe=%#x mqe=%#x\n", status,
			       le16_to_cpu(hdr->status));

		/*
		 * In certain cases, a non-zero MQE status is OK (all must be
		 * true):
		 *   - node is attached
		 *   - if High Login Mode is enabled, node is part of a node
		 * group
		 *   - status is 0x1400
		 */
		if (!rnode->attached ||
		    (hw->sli.high_login_mode && !rnode->node_group) ||
				(le16_to_cpu(hdr->status) !=
				 SLI4_MBOX_STATUS_RPI_NOT_REG))
			rc = -1;
	}

	if (rc == 0) {
		rnode->node_group = false;
		rnode->attached = false;

		if (atomic_read(&hw->rpi_ref[rnode->index].rpi_count) == 0)
			atomic_set(&hw->rpi_ref[rnode->index].rpi_attached,
				   0);
		 evt = EFC_HW_NODE_FREE_OK;
	}

	efc_remote_node_cb(efct->efcport, evt, rnode);

	kfree(mqe);

	return rc;
}

static int
efct_hw_cb_node_free_all(struct efct_hw_s *hw, int status, u8 *mqe,
			 void *arg)
{
	struct sli4_mbox_command_header_s *hdr =
				(struct sli4_mbox_command_header_s *)mqe;
	enum efc_hw_remote_node_event_e evt = EFC_HW_NODE_FREE_FAIL;
	int		rc = 0;
	u32	i;
	struct efct_s   *efct = hw->os;

	if (status || le16_to_cpu(hdr->status)) {
		efct_log_debug(hw->os, "bad status cqe=%#x mqe=%#x\n", status,
			       le16_to_cpu(hdr->status));
	} else {
		evt = EFC_HW_NODE_FREE_ALL_OK;
	}

	if (evt == EFC_HW_NODE_FREE_ALL_OK) {
		for (i = 0; i < hw->sli.extent[SLI_RSRC_RPI].size;
		     i++)
			atomic_set(&hw->rpi_ref[i].rpi_count, 0);

		if (sli_resource_reset(&hw->sli, SLI_RSRC_RPI)) {
			efct_log_test(hw->os, "RPI free all failure\n");
			rc = -1;
		}
	}

	efc_remote_node_cb(efct->efcport, evt, NULL);

	kfree(mqe);

	return rc;
}

/**
 * @brief Initialize the pool of HW IO objects.
 *
 * @param hw Hardware context.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static enum efct_hw_rtn_e
efct_hw_setup_io(struct efct_hw_s *hw)
{
	u32	i = 0;
	struct efct_hw_io_s	*io = NULL;
	uintptr_t	xfer_virt = 0;
	uintptr_t	xfer_phys = 0;
	u32	index;
	bool new_alloc = true;
	struct efc_dma_s *dma;
	struct efct_s *efct = hw->os;

	if (!hw->io) {
		hw->io = kmalloc(hw->config.n_io * sizeof(io),
				 GFP_ATOMIC);

		if (!hw->io)
			return EFCT_HW_RTN_NO_MEMORY;

		memset(hw->io, 0, hw->config.n_io * sizeof(io));

		for (i = 0; i < hw->config.n_io; i++) {
			hw->io[i] = kmalloc(sizeof(*io), GFP_ATOMIC);
			if (!hw->io[i])
				goto error;

			memset(hw->io[i], 0, sizeof(struct efct_hw_io_s));
		}

		/* Create WQE buffs for IO */
		hw->wqe_buffs = kmalloc((hw->config.n_io *
					     hw->sli.wqe_size),
					     GFP_ATOMIC);
		if (!hw->wqe_buffs) {
			kfree(hw->io);
			return EFCT_HW_RTN_NO_MEMORY;
		}
		memset(hw->wqe_buffs, 0, (hw->config.n_io *
					hw->sli.wqe_size));

	} else {
		/* re-use existing IOs, including SGLs */
		new_alloc = false;
	}

	if (new_alloc) {
		dma = &hw->xfer_rdy;
		dma->size = sizeof(struct fcp_xfer_rdy_iu_s) * hw->config.n_io;
		dma->virt = dma_alloc_coherent(&efct->pdev->dev,
					       dma->size, &dma->phys, GFP_DMA);
		if (!dma->virt) {
			efct_log_err(hw->os,
				     "XFER_RDY buffer allocation failed\n");
			return EFCT_HW_RTN_NO_MEMORY;
		}
	}
	xfer_virt = (uintptr_t)hw->xfer_rdy.virt;
	xfer_phys = hw->xfer_rdy.phys;

	for (i = 0; i < hw->config.n_io; i++) {
		struct hw_wq_callback_s *wqcb;

		io = hw->io[i];

		/* initialize IO fields */
		io->hw = hw;

		/* Assign a WQE buff */
		io->wqe.wqebuf = &hw->wqe_buffs[i * hw->sli.wqe_size];

		/* Allocate the request tag for this IO */
		wqcb = efct_hw_reqtag_alloc(hw, efct_hw_wq_process_io, io);
		if (!wqcb) {
			efct_log_err(hw->os, "can't allocate request tag\n");
			return EFCT_HW_RTN_NO_RESOURCES;
		}
		io->reqtag = wqcb->instance_index;

		/* Now for the fields that are initialized on each free */
		efct_hw_init_free_io(io);

		/* The XB flag isn't cleared on IO free, so init to zero */
		io->xbusy = 0;

		if (sli_resource_alloc(&hw->sli, SLI_RSRC_XRI,
				       &io->indicator, &index)) {
			efct_log_err(hw->os,
				     "sli_resource_alloc failed @ %d\n", i);
			return EFCT_HW_RTN_NO_MEMORY;
		}
		if (new_alloc) {
			dma = &io->def_sgl;
			dma->size = hw->config.n_sgl *
					sizeof(struct sli4_sge_s);
			dma->virt = dma_alloc_coherent(&efct->pdev->dev,
						       dma->size, &dma->phys,
						       GFP_DMA);
			if (!dma->virt) {
				efct_log_err(hw->os, "dma_alloc fail %d\n", i);
				memset(&io->def_sgl, 0,
				       sizeof(struct efc_dma_s));
				return EFCT_HW_RTN_NO_MEMORY;
			}
		}
		io->def_sgl_count = hw->config.n_sgl;
		io->sgl = &io->def_sgl;
		io->sgl_count = io->def_sgl_count;

		if (hw->xfer_rdy.size) {
			io->xfer_rdy.virt = (void *)xfer_virt;
			io->xfer_rdy.phys = xfer_phys;
			io->xfer_rdy.size = sizeof(struct fcp_xfer_rdy_iu_s);

			xfer_virt += sizeof(struct fcp_xfer_rdy_iu_s);
			xfer_phys += sizeof(struct fcp_xfer_rdy_iu_s);
		}
	}

	return EFCT_HW_RTN_SUCCESS;
error:
	for (i = 0; i < hw->config.n_io && hw->io[i]; i++) {
		kfree(hw->io[i]);
		hw->io[i] = NULL;
	}
	
	if (hw->io) {
		kfree(hw->io);
		hw->io = NULL;
	}

	return EFCT_HW_RTN_NO_MEMORY;
}

static enum efct_hw_rtn_e
efct_hw_init_io(struct efct_hw_s *hw)
{
	u32	i = 0, io_index = 0;
	bool prereg = false;
	struct efct_hw_io_s	*io = NULL;
	u8		cmd[SLI4_BMBX_SIZE];
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_SUCCESS;
	u32	nremaining;
	u32	n = 0;
	u32	sgls_per_request = 256;
	struct efc_dma_s	**sgls = NULL;
	struct efc_dma_s	reqbuf;
	struct efct_s *efct = hw->os;

	prereg = hw->sli.sgl_pre_registered;

	memset(&reqbuf, 0, sizeof(struct efc_dma_s));
	if (prereg) {
		sgls = kmalloc(sizeof(*sgls) * sgls_per_request,
			       GFP_ATOMIC);
		if (!sgls)
			return EFCT_HW_RTN_NO_MEMORY;

		reqbuf.size = 32 + sgls_per_request * 16;
		reqbuf.virt = dma_alloc_coherent(&efct->pdev->dev,
						 reqbuf.size, &reqbuf.phys,
						 GFP_DMA);
		if (!reqbuf.virt) {
			efct_log_err(hw->os, "dma_alloc reqbuf failed\n");
			kfree(sgls);
			return EFCT_HW_RTN_NO_MEMORY;
		}
	}

	for (nremaining = hw->config.n_io; nremaining; nremaining -= n) {
		if (prereg) {
			/* Copy address of SGL's into local sgls[] array, break
			 * out if the xri is not contiguous.
			 */
			u32 min = (sgls_per_request < nremaining)
					? sgls_per_request : nremaining;
			for (n = 0; n < min; n++) {
				/* Check that we have contiguous xri values */
				if (n > 0) {
					if (hw->io[io_index + n]->indicator !=
					    hw->io[io_index + n - 1]->indicator
					    + 1)
						break;
				}
				sgls[n] = hw->io[io_index + n]->sgl;
			}

			if (sli_cmd_post_sgl_pages(&hw->sli, cmd,
						   sizeof(cmd),
						hw->io[io_index]->indicator,
						n, sgls, NULL, &reqbuf)) {
				if (efct_hw_command(hw, cmd, EFCT_CMD_POLL,
						    NULL, NULL)) {
					rc = EFCT_HW_RTN_ERROR;
					efct_log_err(hw->os,
						     "SGL post failed\n");
					break;
				}
			}
		} else {
			n = nremaining;
		}

		/* Add to tail if successful */
		for (i = 0; i < n; i++, io_index++) {
			io = hw->io[io_index];
			io->is_port_owned = false;
			io->state = EFCT_HW_IO_STATE_FREE;
			INIT_LIST_HEAD(&io->list_entry);
			list_add_tail(&io->list_entry, &hw->io_free);
		}
	}

	if (prereg) {
		dma_free_coherent(&efct->pdev->dev,
				  reqbuf.size, reqbuf.virt, reqbuf.phys);
		memset(&reqbuf, 0, sizeof(struct efc_dma_s));
		kfree(sgls);
	}

	return rc;
}

static int
efct_hw_flush(struct efct_hw_s *hw)
{
	u32	i = 0;

	/* Process any remaining completions */
	for (i = 0; i < hw->eq_count; i++)
		efct_hw_process(hw, i, ~0);

	return 0;
}

static int
efct_hw_command_cancel(struct efct_hw_s *hw)
{
	unsigned long flags = 0;

	spin_lock_irqsave(&hw->cmd_lock, flags);

	/*
	 * Manually clean up remaining commands. Note: since this calls
	 * efct_hw_command_process(), we'll also process the cmd_pending
	 * list, so no need to manually clean that out.
	 */
	while (!list_empty(&hw->cmd_head)) {
		u8		mqe[SLI4_BMBX_SIZE] = { 0 };
		struct efct_command_ctx_s *ctx =
	list_first_entry(&hw->cmd_head, struct efct_command_ctx_s, list_entry);

		efct_log_test(hw->os, "hung command %08x\n",
			      !ctx ? U32_MAX :
			      (!ctx->buf ? U32_MAX :
			       *((u32 *)ctx->buf)));
		spin_unlock_irqrestore(&hw->cmd_lock, flags);
		efct_hw_command_process(hw, -1, mqe, SLI4_BMBX_SIZE);
		spin_lock_irqsave(&hw->cmd_lock, flags);
	}

	spin_unlock_irqrestore(&hw->cmd_lock, flags);

	return 0;
}

/**
 * @brief Find IO given indicator (xri).
 *
 * @param hw context.
 * @param indicator Indicator (xri) to look for.
 *
 * @return Returns io if found, NULL otherwise.
 */
struct efct_hw_io_s *
efct_hw_io_lookup(struct efct_hw_s *hw, u32 xri)
{
	u32 ioindex;

	ioindex = xri - hw->sli.extent[SLI_RSRC_XRI].base[0];
	return hw->io[ioindex];
}

/**
 * @brief Issue any pending callbacks for an IO and remove off the timer and
 * pending lists.
 *
 * @param hw context.
 * @param io Pointer to the IO to cleanup.
 */
static void
efct_hw_io_cancel_cleanup(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	efct_hw_done_t done = io->done;
	efct_hw_done_t abort_done = io->abort_done;
	unsigned long flags = 0;

	/* first check active_wqe list and remove if there */
	if (io->wqe_link.next)
		list_del(&io->wqe_link);

	/* Remove from WQ pending list */
	if (io->wq && io->wq->pending_list.next)
		list_del(&io->list_entry);

	if (io->done) {
		void *arg = io->arg;

		io->done = NULL;
		spin_unlock_irqrestore(&hw->io_lock, flags);
		done(io, io->rnode, 0, SLI4_FC_WCQE_STATUS_SHUTDOWN, 0, arg);
		spin_lock_irqsave(&hw->io_lock, flags);
	}

	if (io->abort_done) {
		void		*abort_arg = io->abort_arg;

		io->abort_done = NULL;
		spin_unlock_irqrestore(&hw->io_lock, flags);
		abort_done(io, io->rnode, 0, SLI4_FC_WCQE_STATUS_SHUTDOWN, 0,
			   abort_arg);
		spin_lock_irqsave(&hw->io_lock, flags);
	}
}

static int
efct_hw_io_cancel(struct efct_hw_s *hw)
{
	struct efct_hw_io_s	*io = NULL;
	struct efct_hw_io_s	*tmp_io = NULL;
	u32	iters = 100; /* One second limit */
	unsigned long flags = 0;

	/*
	 * Manually clean up outstanding IO.
	 * Only walk through list once: the backend will cleanup any IOs when
	 * done/abort_done is called.
	 */
	spin_lock_irqsave(&hw->io_lock, flags);
	list_for_each_entry_safe(io, tmp_io, &hw->io_inuse, list_entry) {
		efct_hw_done_t  done = io->done;
		efct_hw_done_t  abort_done = io->abort_done;

		efct_hw_io_cancel_cleanup(hw, io);

		/*
		 * Since this is called in a reset/shutdown
		 * case, If there is no callback, then just
		 * free the IO.
		 *
		 * Note: A port owned XRI cannot be on
		 *       the in use list. We cannot call
		 *       efct_hw_io_free() because we already
		 *       hold the io_lock.
		 */
		if (!done &&
		    !abort_done) {
			/*
			 * Since this is called in a reset/shutdown
			 * case, If there is no callback, then just
			 * free the IO.
			 */
			efct_hw_io_free_common(hw, io);
			list_del(&io->list_entry);
			efct_hw_io_free_move_correct_list(hw, io);
		}
	}

	/*
	 * For port owned XRIs, they are not on the in use list, so
	 * walk though XRIs and issue any callbacks.
	 */
	list_for_each_entry_safe(io, tmp_io, &hw->io_port_owned, list_entry) {
		/* check  list and remove if there */
		if (io->dnrx_link.next) {
			list_del(&io->dnrx_link);
			kref_put(&io->ref, io->release);
		}
		efct_hw_io_cancel_cleanup(hw, io);
		list_del(&io->list_entry);
		efct_hw_io_free_common(hw, io);
	}
	spin_unlock_irqrestore(&hw->io_lock, flags);

	/* Give time for the callbacks to complete */
	do {
		mdelay(10);
		iters--;
	} while (!list_empty(&hw->io_inuse) && iters);

	/* Leave a breadcrumb that cleanup is not yet complete. */
	if (!list_empty(&hw->io_inuse))
		efct_log_test(hw->os, "io_inuse list is not empty\n");

	return 0;
}

static int
efct_hw_io_ini_sge(struct efct_hw_s *hw, struct efct_hw_io_s *io,
		   struct efc_dma_s *cmnd,
		   u32 cmnd_size, struct efc_dma_s *rsp)
{
	struct sli4_sge_s *data = NULL;

	if (!hw || !io) {
		efct_log_err(NULL, "bad parm hw=%p io=%p\n", hw, io);
		return EFCT_HW_RTN_ERROR;
	}

	data = io->def_sgl.virt;

	/* setup command pointer */
	data->buffer_address_high =
		cpu_to_le32(upper_32_bits(cmnd->phys));
	data->buffer_address_low  =
		cpu_to_le32(lower_32_bits(cmnd->phys));
	data->buffer_length = cpu_to_le32(cmnd_size);
	data++;

	/* setup response pointer */
	data->buffer_address_high =
		cpu_to_le32(upper_32_bits(rsp->phys));
	data->buffer_address_low  =
		cpu_to_le32(lower_32_bits(rsp->phys));
	data->buffer_length = cpu_to_le32(rsp->size);

	return 0;
}

static int
__efct_read_topology_cb(struct efct_hw_s *hw, int status,
			u8 *mqe, void *arg)
{
	struct sli4_cmd_read_topology_s *read_topo =
				(struct sli4_cmd_read_topology_s *)mqe;
	u8 speed;

	if (status || le16_to_cpu(read_topo->hdr.status)) {
		efct_log_debug(hw->os, "bad status cqe=%#x mqe=%#x\n",
			       status,
			       le16_to_cpu(read_topo->hdr.status));
		kfree(mqe);
		return -1;
	}

	switch (le32_to_cpu(read_topo->dw2_attentype) &
		SLI4_READTOPO_ATTEN_TYPE) {
	case SLI4_READ_TOPOLOGY_LINK_UP:
		hw->link.status = SLI_LINK_STATUS_UP;
		break;
	case SLI4_READ_TOPOLOGY_LINK_DOWN:
		hw->link.status = SLI_LINK_STATUS_DOWN;
		break;
	case SLI4_READ_TOPOLOGY_LINK_NO_ALPA:
		hw->link.status = SLI_LINK_STATUS_NO_ALPA;
		break;
	default:
		hw->link.status = SLI_LINK_STATUS_MAX;
		break;
	}

	switch (read_topo->topology) {
	case SLI4_READ_TOPOLOGY_NPORT:
		hw->link.topology = SLI_LINK_TOPO_NPORT;
		break;
	case SLI4_READ_TOPOLOGY_FC_AL:
		hw->link.topology = SLI_LINK_TOPO_LOOP;
		if (hw->link.status == SLI_LINK_STATUS_UP)
			hw->link.loop_map = hw->loop_map.virt;
		hw->link.fc_id = read_topo->acquired_al_pa;
		break;
	default:
		hw->link.topology = SLI_LINK_TOPO_MAX;
		break;
	}

	hw->link.medium = SLI_LINK_MEDIUM_FC;

	speed = (le32_to_cpu(read_topo->currlink_state) &
		 SLI4_READTOPO_LINKSTATE_SPEED) >> 8;
	switch (speed) {
	case SLI4_READ_TOPOLOGY_SPEED_1G:
		hw->link.speed =  1 * 1000;
		break;
	case SLI4_READ_TOPOLOGY_SPEED_2G:
		hw->link.speed =  2 * 1000;
		break;
	case SLI4_READ_TOPOLOGY_SPEED_4G:
		hw->link.speed =  4 * 1000;
		break;
	case SLI4_READ_TOPOLOGY_SPEED_8G:
		hw->link.speed =  8 * 1000;
		break;
	case SLI4_READ_TOPOLOGY_SPEED_16G:
		hw->link.speed = 16 * 1000;
		hw->link.loop_map = NULL;
		break;
	case SLI4_READ_TOPOLOGY_SPEED_32G:
		hw->link.speed = 32 * 1000;
		hw->link.loop_map = NULL;
		break;
	}

	kfree(mqe);

	efct_hw_read_fcf(hw, SLI4_FCF_TABLE_FIRST);

	return 0;
}

static int
efct_hw_port_get_mbox_status(struct efc_sli_port_s *sport,
			     u8 *mqe, int status)
{
	struct efct_hw_s *hw = sport->hw;
	struct sli4_mbox_command_header_s *hdr =
			(struct sli4_mbox_command_header_s *)mqe;
	int rc = 0;

	if (status || le16_to_cpu(hdr->status)) {
		efct_log_debug(hw->os, "bad status vpi=%#x st=%x hdr=%x\n",
			       sport->indicator, status,
			       le16_to_cpu(hdr->status));
		rc = -1;
	}

	return rc;
}

static void
efct_hw_port_free_resources(struct efc_sli_port_s *sport, int evt, void *data)
{
	struct efct_hw_s *hw = sport->hw;
	struct efct_s *efct = hw->os;

	/* Clear the sport attached flag */
	sport->attached = false;

	/* Free the service parameters buffer */
	if (sport->dma.virt) {
		dma_free_coherent(&efct->pdev->dev,
				  sport->dma.size, sport->dma.virt,
				  sport->dma.phys);
		memset(&sport->dma, 0, sizeof(struct efc_dma_s));
	}

	/* Free the command buffer */
	kfree(data);

	/* Free the SLI resources */
	sli_resource_free(&hw->sli, SLI_RSRC_VPI, sport->indicator);

	efc_lport_cb(efct->efcport, evt, sport);
}

static void
efct_hw_port_send_evt(struct efc_sli_port_s *sport, int evt, void *data)
{
	struct efct_hw_s *hw = sport->hw;
	struct efct_s *efct = hw->os;

	/* Free the mbox buffer */
	kfree(data);

	/* Now inform the registered callbacks */
	efc_lport_cb(efct->efcport, evt, sport);

	/* Set the sport attached flag */
	if (evt == EFC_HW_PORT_ATTACH_OK)
		sport->attached = true;

	/* If there is a pending free request, then handle it now */
	if (sport->free_req_pending)
		efct_hw_port_free_unreg_vpi(sport, NULL);
}

static int
efct_hw_port_alloc_init_vpi_cb(struct efct_hw_s *hw,
			       int status, u8 *mqe, void *arg)
{
	struct efc_sli_port_s *sport = arg;
	int rc;

	rc = efct_hw_port_get_mbox_status(sport, mqe, status);
	if (rc) {
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ALLOC_FAIL, mqe);
		return -1;
	}

	efct_hw_port_send_evt(sport, EFC_HW_PORT_ALLOC_OK, mqe);
	return 0;
}

static void
efct_hw_port_alloc_init_vpi(struct efc_sli_port_s *sport, void *data)
{
	struct efct_hw_s *hw = sport->hw;
	int rc;

	/* If there is a pending free request, then handle it now */
	if (sport->free_req_pending) {
		efct_hw_port_free_resources(sport, EFC_HW_PORT_FREE_OK, data);
		return;
	}

	rc = sli_cmd_init_vpi(&hw->sli, data, SLI4_BMBX_SIZE,
			      sport->indicator, sport->domain->indicator);
	if (rc == 0) {
		efct_log_err(hw->os, "INIT_VPI format failure\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ALLOC_FAIL, data);
		return;
	}

	rc = efct_hw_command(hw, data, EFCT_CMD_NOWAIT,
			     efct_hw_port_alloc_init_vpi_cb, sport);
	if (rc) {
		efct_log_err(hw->os, "INIT_VPI command failure\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ALLOC_FAIL, data);
	}
}

static int
efct_hw_port_alloc_read_sparm64_cb(struct efct_hw_s *hw,
				   int status, u8 *mqe, void *arg)
{
	struct efc_sli_port_s *sport = arg;
	u8 *payload = NULL;
	struct efct_s *efct = hw->os;
	int rc;

	rc = efct_hw_port_get_mbox_status(sport, mqe, status);
	if (rc) {
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ALLOC_FAIL, mqe);
		return -1;
	}

	payload = sport->dma.virt;

	memcpy(&sport->sli_wwpn,
	       payload + SLI4_READ_SPARM64_WWPN_OFFSET,
		sizeof(sport->sli_wwpn));
	memcpy(&sport->sli_wwnn,
	       payload + SLI4_READ_SPARM64_WWNN_OFFSET,
		sizeof(sport->sli_wwnn));

	dma_free_coherent(&efct->pdev->dev,
			  sport->dma.size, sport->dma.virt, sport->dma.phys);
	memset(&sport->dma, 0, sizeof(struct efc_dma_s));
	efct_hw_port_alloc_init_vpi(sport, mqe);
	return 0;
}

static void
efct_hw_port_alloc_read_sparm64(struct efc_sli_port_s *sport, void *data)
{
	struct efct_hw_s *hw = sport->hw;
	struct efct_s *efct = hw->os;
	int rc;

	/* Allocate memory for the service parameters */
	sport->dma.size = 112;
	sport->dma.virt = dma_alloc_coherent(&efct->pdev->dev,
					     sport->dma.size, &sport->dma.phys,
					     GFP_DMA);
	if (!sport->dma.virt) {
		efct_log_err(hw->os, "Failed to allocate DMA memory\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ALLOC_FAIL, data);
		return;
	}

	rc = sli_cmd_read_sparm64(&hw->sli, data, SLI4_BMBX_SIZE,
				  &sport->dma, sport->indicator);
	if (rc == 0) {
		efct_log_err(hw->os, "READ_SPARM64 format failure\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ALLOC_FAIL, data);
		return;
	}

	rc = efct_hw_command(hw, data, EFCT_CMD_NOWAIT,
			     efct_hw_port_alloc_read_sparm64_cb, sport);
	if (rc) {
		efct_log_err(hw->os, "READ_SPARM64 command failure\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ALLOC_FAIL, data);
	}
}

static int
efct_hw_port_attach_reg_vpi_cb(struct efct_hw_s *hw,
			       int status, u8 *mqe, void *arg)
{
	struct efc_sli_port_s *sport = arg;
	int rc;

	rc = efct_hw_port_get_mbox_status(sport, mqe, status);
	if (rc) {
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ATTACH_FAIL, mqe);
		return -1;
	}

	efct_hw_port_send_evt(sport, EFC_HW_PORT_ATTACH_OK, mqe);
	return 0;
}

static void
efct_hw_port_attach_reg_vpi(struct efc_sli_port_s *sport, void *data)
{
	struct efct_hw_s *hw = sport->hw;
	int rc;

	if (sli_cmd_reg_vpi(&hw->sli, data, SLI4_BMBX_SIZE,
			    sport->fc_id, sport->sli_wwpn,
			sport->indicator, sport->domain->indicator,
			false) == 0) {
		efct_log_err(hw->os, "REG_VPI format failure\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ATTACH_FAIL, data);
		return;
	}

	rc = efct_hw_command(hw, data, EFCT_CMD_NOWAIT,
			     efct_hw_port_attach_reg_vpi_cb, sport);
	if (rc) {
		efct_log_err(hw->os, "REG_VPI command failure\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_ATTACH_FAIL, data);
	}
}

static int
efct_hw_port_free_unreg_vpi_cb(struct efct_hw_s *hw,
			       int status, u8 *mqe, void *arg)
{
	struct efc_sli_port_s *sport = arg;
	int evt = EFC_HW_PORT_FREE_OK;
	int rc = 0;

	rc = efct_hw_port_get_mbox_status(sport, mqe, status);
	if (rc) {
		evt = EFC_HW_PORT_FREE_FAIL;
		rc = -1;
	}

	efct_hw_port_free_resources(sport, evt, mqe);
	return rc;
}

static void
efct_hw_port_free_unreg_vpi(struct efc_sli_port_s *sport, void *data)
{
	struct efct_hw_s *hw = sport->hw;
	int rc;

	/* Allocate memory and send unreg_vpi */
	if (!data) {
		data = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
		if (!data) {
			efct_hw_port_free_resources(sport,
						    EFC_HW_PORT_FREE_FAIL,
						    data);
			return;
		}
		memset(data, 0, SLI4_BMBX_SIZE);
	}

	rc = sli_cmd_unreg_vpi(&hw->sli, data, SLI4_BMBX_SIZE,
			       sport->indicator, SLI4_UNREG_TYPE_PORT);
	if (rc == 0) {
		efct_log_err(hw->os, "UNREG_VPI format failure\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_FREE_FAIL, data);
		return;
	}

	rc = efct_hw_command(hw, data, EFCT_CMD_NOWAIT,
			     efct_hw_port_free_unreg_vpi_cb, sport);
	if (rc) {
		efct_log_err(hw->os, "UNREG_VPI command failure\n");
		efct_hw_port_free_resources(sport,
					    EFC_HW_PORT_FREE_FAIL, data);
	}
}

static int
efct_hw_domain_get_mbox_status(struct efc_domain_s *domain,
			       u8 *mqe, int status)
{
	struct efct_hw_s *hw = domain->hw;
	struct sli4_mbox_command_header_s *hdr =
			(struct sli4_mbox_command_header_s *)mqe;
	int rc = 0;

	if (status || le16_to_cpu(hdr->status)) {
		efct_log_debug(hw->os, "bad status vfi=%#x st=%x hdr=%x\n",
			       domain->indicator, status,
			       le16_to_cpu(hdr->status));
		rc = -1;
	}

	return rc;
}

static void
efct_hw_domain_free_resources(struct efc_domain_s *domain,
			      int evt, void *data)
{
	struct efct_hw_s *hw = domain->hw;
	struct efct_s *efct = hw->os;

	/* Free the service parameters buffer */
	if (domain->dma.virt) {
		dma_free_coherent(&efct->pdev->dev,
				  domain->dma.size, domain->dma.virt,
				  domain->dma.phys);
		memset(&domain->dma, 0, sizeof(struct efc_dma_s));
	}

	/* Free the command buffer */
	kfree(data);

	/* Free the SLI resources */
	sli_resource_free(&hw->sli, SLI_RSRC_VFI, domain->indicator);

	efc_domain_cb(efct->efcport, evt, domain);
}

static void
efct_hw_domain_send_sport_evt(struct efc_domain_s *domain,
			      int port_evt, int domain_evt, void *data)
{
	struct efct_hw_s *hw = domain->hw;
	struct efct_s *efct = hw->os;

	/* Free the mbox buffer */
	kfree(data);

	/* Send alloc/attach ok to the physical sport */
	efct_hw_port_send_evt(domain->sport, port_evt, NULL);

	/* Now inform the registered callbacks */
	efc_domain_cb(efct->efcport, domain_evt, domain);
}

static int
efct_hw_domain_alloc_read_sparm64_cb(struct efct_hw_s *hw,
				     int status, u8 *mqe, void *arg)
{
	struct efc_domain_s *domain = arg;
	int rc;

	rc = efct_hw_domain_get_mbox_status(domain, mqe, status);
	if (rc) {
		efct_hw_domain_free_resources(domain,
					      EFC_HW_DOMAIN_ALLOC_FAIL, mqe);
		return -1;
	}

	efct_hw_domain_add(hw, domain);
	efct_hw_domain_send_sport_evt(domain, EFC_HW_PORT_ALLOC_OK,
				      EFC_HW_DOMAIN_ALLOC_OK, mqe);
	return 0;
}

static void
efct_hw_domain_alloc_read_sparm64(struct efc_domain_s *domain, void *data)
{
	struct efct_hw_s *hw = domain->hw;
	int rc;

	rc = sli_cmd_read_sparm64(&hw->sli, data, SLI4_BMBX_SIZE,
				  &domain->dma, SLI4_READ_SPARM64_VPI_DEFAULT);
	if (rc == 0) {
		efct_log_err(hw->os, "READ_SPARM64 format failure\n");
		efct_hw_domain_free_resources(domain,
					      EFC_HW_DOMAIN_ALLOC_FAIL, data);
		return;
	}

	rc = efct_hw_command(hw, data, EFCT_CMD_NOWAIT,
			     efct_hw_domain_alloc_read_sparm64_cb, domain);
	if (rc) {
		efct_log_err(hw->os, "READ_SPARM64 command failure\n");
		efct_hw_domain_free_resources(domain,
					      EFC_HW_DOMAIN_ALLOC_FAIL, data);
	}
}

static int
efct_hw_domain_alloc_init_vfi_cb(struct efct_hw_s *hw,
				 int status, u8 *mqe, void *arg)
{
	struct efc_domain_s *domain = arg;
	int rc;

	rc = efct_hw_domain_get_mbox_status(domain, mqe, status);
	if (rc) {
		efct_hw_domain_free_resources(domain,
					      EFC_HW_DOMAIN_ALLOC_FAIL, mqe);
		return -1;
	}

	efct_hw_domain_alloc_read_sparm64(domain, mqe);
	return 0;
}

static void
efct_hw_domain_alloc_init_vfi(struct efc_domain_s *domain, void *data)
{
	struct efct_hw_s *hw = domain->hw;
	struct efc_sli_port_s *sport = domain->sport;
	int rc;

	/*
	 * For FC, the HW alread registered an FCFI.
	 * Copy FCF information into the domain and jump to INIT_VFI.
	 */
	domain->fcf_indicator = hw->fcf_indicator;
	rc = sli_cmd_init_vfi(&hw->sli, data, SLI4_BMBX_SIZE,
			      domain->indicator, domain->fcf_indicator,
			sport->indicator);
	if (rc == 0) {
		efct_log_err(hw->os, "INIT_VFI format failure\n");
		efct_hw_domain_free_resources(domain,
					      EFC_HW_DOMAIN_ALLOC_FAIL, data);
		return;
	}

	rc = efct_hw_command(hw, data, EFCT_CMD_NOWAIT,
			     efct_hw_domain_alloc_init_vfi_cb, domain);
	if (rc) {
		efct_log_err(hw->os, "INIT_VFI command failure\n");
		efct_hw_domain_free_resources(domain,
					      EFC_HW_DOMAIN_ALLOC_FAIL, data);
	}
}

static int
efct_hw_domain_attach_reg_vfi_cb(struct efct_hw_s *hw,
				 int status, u8 *mqe, void *arg)
{
	struct efc_domain_s *domain = arg;
	int rc;

	rc = efct_hw_domain_get_mbox_status(domain, mqe, status);
	if (rc) {
		efct_hw_domain_del(hw, domain);
		efct_hw_domain_free_resources(domain,
					      EFC_HW_DOMAIN_ATTACH_FAIL, mqe);
		return -1;
	}

	efct_hw_domain_send_sport_evt(domain, EFC_HW_PORT_ATTACH_OK,
				      EFC_HW_DOMAIN_ATTACH_OK, mqe);
	return 0;
}

static void
efct_hw_domain_attach_reg_vfi(struct efc_domain_s *domain, void *data)
{
	struct efct_hw_s *hw = domain->hw;
	int rc;

	if (sli_cmd_reg_vfi(&hw->sli, data, SLI4_BMBX_SIZE,
			    domain->indicator, domain->fcf_indicator,
			domain->dma, domain->sport->indicator,
			domain->sport->sli_wwpn,
			domain->sport->fc_id) == 0) {
		efct_log_err(hw->os, "REG_VFI format failure\n");
		goto cleanup;
	}

	rc = efct_hw_command(hw, data, EFCT_CMD_NOWAIT,
			     efct_hw_domain_attach_reg_vfi_cb, domain);
	if (rc) {
		efct_log_err(hw->os, "REG_VFI command failure\n");
		goto cleanup;
	}

	return;

cleanup:
	efct_hw_domain_del(hw, domain);
	efct_hw_domain_free_resources(domain,
				      EFC_HW_DOMAIN_ATTACH_FAIL, data);
}

static int
efct_hw_domain_free_unreg_vfi_cb(struct efct_hw_s *hw,
				 int status, u8 *mqe, void *arg)
{
	struct efc_domain_s *domain = arg;
	int evt = EFC_HW_DOMAIN_FREE_OK;
	int rc = 0;

	rc = efct_hw_domain_get_mbox_status(domain, mqe, status);
	if (rc) {
		evt = EFC_HW_DOMAIN_FREE_FAIL;
		rc = -1;
	}

	efct_hw_domain_del(hw, domain);
	efct_hw_domain_free_resources(domain, evt, mqe);
	return rc;
}

static void
efct_hw_domain_free_unreg_vfi(struct efc_domain_s *domain, void *data)
{
	struct efct_hw_s *hw = domain->hw;
	int rc;

	if (!data) {
		data = kzalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
		if (!data)
			goto cleanup;
	}

	rc = sli_cmd_unreg_vfi(&hw->sli, data, SLI4_BMBX_SIZE,
			       domain->indicator, SLI4_UNREG_TYPE_DOMAIN);
	if (rc == 0) {
		efct_log_err(hw->os, "UNREG_VFI format failure\n");
		goto cleanup;
	}

	rc = efct_hw_command(hw, data, EFCT_CMD_NOWAIT,
			     efct_hw_domain_free_unreg_vfi_cb, domain);
	if (rc) {
		efct_log_err(hw->os, "UNREG_VFI command failure\n");
		goto cleanup;
	}

	return;

cleanup:
	efct_hw_domain_del(hw, domain);
	efct_hw_domain_free_resources(domain, EFC_HW_DOMAIN_FREE_FAIL, data);
}

static int
target_wqe_timer_nop_cb(struct efct_hw_s *hw, int status,
			u8 *mqe, void *arg)
{
	struct efct_hw_io_s *io = NULL;
	struct efct_hw_io_s *io_next = NULL;
	u64 ticks_current = jiffies_64;
	u32 sec_elapsed;
	struct sli4_mbox_command_header_s *hdr =
				(struct sli4_mbox_command_header_s *)mqe;
	unsigned long flags = 0;

	if (status || le16_to_cpu(hdr->status)) {
		efct_log_debug(hw->os, "bad status st=%x hdr=%x\n",
			       status,
			       le16_to_cpu(hdr->status));
		/* go ahead and proceed with wqe timer checks... */
	}

	/* loop through active WQE list and check for timeouts */
	spin_lock_irqsave(&hw->io_lock, flags);
	list_for_each_entry_safe(io, io_next, &hw->io_timed_wqe, wqe_link) {
		sec_elapsed = ((u32)(ticks_current - io->submit_ticks) / HZ);

		/*
		 * If elapsed time > timeout, abort it. No need to check type
		 * since it wouldn't be on this list unless it was a target WQE
		 */
		if (sec_elapsed > io->tgt_wqe_timeout) {
			efct_log_test(hw->os,
				      "IO timeout xri=0x%x tag=0x%x type=%d\n",
				     io->indicator, io->reqtag, io->type);

			/*
			 * remove from active_wqe list so won't try to abort
			 * again
			 */
			list_del(&io->list_entry);

			/* save status of timed_out for when abort completes */
			io->status_saved = true;
			io->saved_status =
					 SLI4_FC_WCQE_STATUS_TARGET_WQE_TIMEOUT;
			io->saved_ext = 0;
			io->saved_len = 0;

			/* now abort outstanding IO */
			efct_hw_io_abort(hw, io, false, NULL, NULL);
		}
		/*
		 * need to go through entire list since each IO could have a
		 * different timeout value
		 */
	}
	spin_unlock_irqrestore(&hw->io_lock, flags);

	/* if we're not in the middle of shutting down, schedule next timer */
	if (!hw->active_wqe_timer_shutdown) {
		timer_setup(&hw->wqe_timer,
			    &target_wqe_timer_cb, 0);

		mod_timer(&hw->wqe_timer,
			  jiffies +
			  msecs_to_jiffies(EFCT_HW_WQ_TIMER_PERIOD_MS));
	}
	hw->in_active_wqe_timer = false;
	return 0;
}

static void
target_wqe_timer_cb(struct timer_list *t)
{
	struct efct_hw_s *hw = from_timer(hw, t, wqe_timer);

	/*
	 * delete existing timer; will kick off new timer after checking wqe
	 * timeouts
	 */
	hw->in_active_wqe_timer = true;
	del_timer(&hw->wqe_timer);

	/*
	 * Forward timer callback to execute in the mailbox completion
	 * processing context
	 */
	if (efct_hw_async_call(hw, target_wqe_timer_nop_cb, hw))
		efct_log_test(hw->os, "efct_hw_async_call failed\n");
}

static void
shutdown_target_wqe_timer(struct efct_hw_s *hw)
{
	u32	iters = 100;

	if (hw->config.emulate_tgt_wqe_timeout) {
		/*
		 * request active wqe timer shutdown, then wait for it to
		 * complete
		 */
		hw->active_wqe_timer_shutdown = true;

		/*
		 * delete WQE timer and wait for timer handler to complete
		 * (if necessary)
		 */
		del_timer(&hw->wqe_timer);

		/* now wait for timer handler to complete (if necessary) */
		while (hw->in_active_wqe_timer && iters) {
			/*
			 * if we happen to have just sent NOP mbox cmn, make
			 * sure completions are being processed
			 */
			efct_hw_flush(hw);
			iters--;
		}

		if (iters == 0)
			efct_log_test(hw->os,
				      "Failed to shutdown active wqe timer\n");
	}
}

/**
 * @brief Determine if HW IO is owned by the port.
 *
 * @par Description
 * Determines if the given HW IO has been posted to the chip.
 *
 * @param hw Hardware context allocated by the caller.
 * @param io HW IO.
 *
 * @return Returns TRUE if given HW IO is port-owned.
 */
u8
efct_hw_is_io_port_owned(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	/* Check to see if this is a port owned XRI */
	return io->is_port_owned;
}

/**
 * @brief Return TRUE if exchange is port-owned.
 *
 * @par Description
 * Test to see if the xri is a port-owned xri.
 *
 * @param hw Hardware context.
 * @param xri Exchange indicator.
 *
 * @return Returns TRUE if XRI is a port owned XRI.
 */

bool
efct_hw_is_xri_port_owned(struct efct_hw_s *hw, u32 xri)
{
	struct efct_hw_io_s *io = efct_hw_io_lookup(hw, xri);

	return (!io ? false : io->is_port_owned);
}

/**
 * @brief Returns an XRI from the port owned list to the host.
 *
 * @par Description
 * Used when the POST_XRI command fails as well as when the RELEASE_XRI cmpls.
 *
 * @param hw Hardware context.
 * @param xri_base The starting XRI number.
 * @param xri_count The number of XRIs to free from the base.
 */
static void
efct_hw_reclaim_xri(struct efct_hw_s *hw, u16 xri_base,
		    u16 xri_count)
{
	struct efct_hw_io_s	*io;
	u32 i;
	unsigned long flags = 0;

	for (i = 0; i < xri_count; i++) {
		io = efct_hw_io_lookup(hw, xri_base + i);

		/*
		 * if this is an auto xfer rdy XRI, then we need to release any
		 * buffer attached to the XRI before moving the XRI back to the
		 * free pool.
		 */
		if (hw->auto_xfer_rdy_enabled)
			efct_hw_rqpair_auto_xfer_rdy_move_to_host(hw, io);

		spin_lock_irqsave(&hw->io_lock, flags);
		list_del(&io->list_entry);
		io->is_port_owned = false;
		INIT_LIST_HEAD(&io->list_entry);
		list_add_tail(&io->list_entry, &hw->io_free);
		spin_unlock_irqrestore(&hw->io_lock, flags);
	}
}

/**
 * @brief Called when the POST_XRI command completes.
 *
 * @par Description
 * Free the mailbox command buffer and reclaim the XRIs on failure.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_post_xri(struct efct_hw_s *hw, int status,
		    u8 *mqe, void  *arg)
{
	struct sli4_cmd_post_xri_s *post_xri =
				(struct sli4_cmd_post_xri_s *)mqe;
	u16 xri_count = 0;
	/* Reclaim the XRIs as host owned if the command fails */
	if (status != 0) {
		xri_count = le16_to_cpu(post_xri->xri_count_flags) &
					SLI4_POST_XRI_COUNT;
		efct_log_debug(hw->os,
			       "Status 0x%x for XRI base 0x%x, cnt =x%x\n",
			       status, post_xri->xri_base,
			       xri_count);
		efct_hw_reclaim_xri(hw,
				    le16_to_cpu(post_xri->xri_base),
				    xri_count);
	}

	kfree(mqe);
	return 0;
}

/**
 * @brief Issues a mailbox command to move XRIs from the host-controlled pool
 * to the port.
 *
 * @param hw Hardware context.
 * @param xri_start The starting XRI to post.
 * @param num_to_post The number of XRIs to post.
 *
 * @return Returns EFCT_HW_RTN_NO_MEMORY, EFCT_HW_RTN_ERROR, or
 * EFCT_HW_RTN_SUCCESS.
 */

static enum efct_hw_rtn_e
efct_hw_post_xri(struct efct_hw_s *hw, u32 xri_start,
		 u32 num_to_post)
{
	u8	*post_xri;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;

	/* Since we need to allocate for mailbox queue, just always allocate */
	post_xri = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!post_xri)
		return EFCT_HW_RTN_NO_MEMORY;

	/* Register the XRIs */
	if (sli_cmd_post_xri(&hw->sli, post_xri, SLI4_BMBX_SIZE,
			     xri_start, num_to_post)) {
		rc = efct_hw_command(hw, post_xri, EFCT_CMD_NOWAIT,
				     efct_hw_cb_post_xri, NULL);
		if (rc != EFCT_HW_RTN_SUCCESS) {
			kfree(post_xri);
			efct_log_err(hw->os, "post_xri failed\n");
		}
	}
	return rc;
}

/**
 * @brief Move XRIs from the host-controlled pool to the port.
 *
 * @par Description
 * Removes IOs from the free list and moves them to the port.
 *
 * @param hw Hardware context.
 * @param num_xri The number of XRIs being requested to move to the chip.
 *
 * @return Returns the number of XRIs that were moved.
 */

u32
efct_hw_xri_move_to_port_owned(struct efct_hw_s *hw, u32 num_xri)
{
	struct efct_hw_io_s	*io = NULL;
	u32 i;
	u32 num_posted = 0;
	unsigned long flags = 0;

	/*
	 * Note: We cannot use efct_hw_io_alloc() because that would place the
	 *       IO on the io_inuse list. We need to move from the io_free to
	 *       the io_port_owned list.
	 */
	spin_lock_irqsave(&hw->io_lock, flags);

	for (i = 0; i < num_xri; i++) {
		enum efct_hw_rtn_e rc;

		if (!list_empty(&hw->io_free))
			io = list_first_entry(&hw->io_free,
					      struct efct_hw_io_s, list_entry);
		if (!io)
			break;
		list_del(&io->list_entry);

		/*
		 * if this is an auto xfer rdy XRI, then we need to
		 * attach a buffer to the XRI before submitting it to
		 * the chip. If a buffer is unavailable, then we
		 * cannot post it, so return it to the free pool.
		 */
		if (hw->auto_xfer_rdy_enabled) {
			/*
			 * Note: uses the IO lock to get the auto xfer
			 * rdy buffer
			 */
			spin_unlock_irqrestore(&hw->io_lock, flags);
			rc = efct_hw_rqpair_auto_xfer_rdy_move_to_port(hw, io);
			spin_lock_irqsave(&hw->io_lock, flags);
			if (rc != EFCT_HW_RTN_SUCCESS) {
				list_add(&hw->io_free, &io->list_entry);
				break;
			}
		}
		spin_lock_init(&io->axr_lock);
		io->is_port_owned = true;
		INIT_LIST_HEAD(&io->list_entry);
		list_add_tail(&io->list_entry, &hw->io_port_owned);

		/* Post XRI */
		if (efct_hw_post_xri(hw, io->indicator, 1) !=
		    EFCT_HW_RTN_SUCCESS) {
			efct_hw_reclaim_xri(hw, io->indicator, i);
			break;
		}
		num_posted++;
	}
	spin_unlock_irqrestore(&hw->io_lock, flags);

	return num_posted;
}

/**
 * @brief Called when the RELEASE_XRI command completes.
 *
 * @par Description
 * Move the IOs back to the free pool on success.
 *
 * @param hw Hardware context.
 * @param status Status field from the mbox completion.
 * @param mqe Mailbox response structure.
 * @param arg Pointer to a callback function that signals the caller that the
 * command is done.
 *
 * @return Returns 0.
 */
static int
efct_hw_cb_release_xri(struct efct_hw_s *hw, int status,
		       u8 *mqe, void  *arg)
{
	struct sli4_cmd_release_xri_s *release_xri =
					(struct sli4_cmd_release_xri_s *)mqe;
	u16 released_xri_count;
	u8 i;

	released_xri_count = le16_to_cpu(release_xri->rel_xri_count_word) &
					 SLI4_RELEASE_XRI_REL_XRI_CNT;
	/* Reclaim the XRIs as host owned if the command fails */
	if (status != 0) {
		efct_log_err(hw->os, "Status 0x%x\n", status);
	} else {
		for (i = 0; i < released_xri_count; i++) {
			u16 xri;

			xri  =
			((i & 1) == 0 ?
			 le16_to_cpu(release_xri->xri_tbl[i / 2].xri_tag0) :
			 le16_to_cpu(release_xri->xri_tbl[i / 2].xri_tag1));
			efct_hw_reclaim_xri(hw, xri, 1);
		}
	}

	kfree(mqe);
	return 0;
}

/**
 * @brief Move XRIs from the port-controlled pool to the host.
 *
 * Requests XRIs from the FW to return to the host-owned pool.
 *
 * @param hw Hardware context.
 * @param num_xri The number of XRIs being requested to moved from the chip.
 *
 * @return Returns 0 for success, or a negative error code value for failure.
 */

enum efct_hw_rtn_e
efct_hw_xri_move_to_host_owned(struct efct_hw_s *hw, u8 num_xri)
{
	u8	*release_xri;
	enum efct_hw_rtn_e rc = EFCT_HW_RTN_ERROR;

	/* non-local buffer required for mailbox queue */
	release_xri = kmalloc(SLI4_BMBX_SIZE, GFP_ATOMIC);
	if (!release_xri)
		return EFCT_HW_RTN_NO_MEMORY;

	/* release the XRIs */
	if (sli_cmd_release_xri(&hw->sli, release_xri, SLI4_BMBX_SIZE,
				num_xri)) {
		rc = efct_hw_command(hw, release_xri, EFCT_CMD_NOWAIT,
				     efct_hw_cb_release_xri, NULL);
		if (rc != EFCT_HW_RTN_SUCCESS)
			efct_log_err(hw->os, "release_xri failed\n");
	}
	/* If we are polling or an error occurred, then free the mbox buffer */
	if (release_xri && rc != EFCT_HW_RTN_SUCCESS)
		kfree(release_xri);
	return rc;
}

/**
 * @brief Allocate an efct_hw_rx_buffer_t array.
 *
 * @par Description
 * An efct_hw_rx_buffer_t array is allocated, along with the required DMA mem.
 *
 * @param hw Pointer to HW object.
 * @param rqindex RQ index for this buffer.
 * @param count Count of buffers in array.
 * @param size Size of buffer.
 *
 * @return Returns the pointer to the allocated efc_hw_rq_buffer_s array.
 */
static struct efc_hw_rq_buffer_s *
efct_hw_rx_buffer_alloc(struct efct_hw_s *hw, u32 rqindex, u32 count,
			u32 size)
{
	struct efct_s *efct = hw->os;
	struct efc_hw_rq_buffer_s *rq_buf = NULL;
	struct efc_hw_rq_buffer_s *prq;
	u32 i;

	if (count != 0) {
		rq_buf = kmalloc_array(count, sizeof(*rq_buf), GFP_ATOMIC);
		if (!rq_buf)
			return NULL;
		memset(rq_buf, 0, sizeof(*rq_buf) * count);

		for (i = 0, prq = rq_buf; i < count; i ++, prq++) {
			prq->rqindex = rqindex;
			prq->dma.size = size;
			prq->dma.virt = dma_alloc_coherent(&efct->pdev->dev,
							   prq->dma.size,
							   &prq->dma.phys,
							   GFP_DMA);
			if (!prq->dma.virt) {
				efct_log_err(hw->os, "DMA allocation failed\n");
				kfree(rq_buf);
				rq_buf = NULL;
				break;
			}
		}
	}
	return rq_buf;
}

/**
 * @brief Free an efct_hw_rx_buffer_t array.
 *
 * @par Description
 * The efct_hw_rx_buffer_t array is freed, along with allocated DMA memory.
 *
 * @param hw Pointer to HW object.
 * @param rq_buf Pointer to efct_hw_rx_buffer_t array.
 * @param count Count of buffers in array.
 *
 * @return None.
 */
static void
efct_hw_rx_buffer_free(struct efct_hw_s *hw,
		       struct efc_hw_rq_buffer_s *rq_buf,
			u32 count)
{
	struct efct_s *efct = hw->os;
	u32 i;
	struct efc_hw_rq_buffer_s *prq;

	if (rq_buf) {
		for (i = 0, prq = rq_buf; i < count; i++, prq++) {
			dma_free_coherent(&efct->pdev->dev,
					  prq->dma.size, prq->dma.virt,
					  prq->dma.phys);
			memset(&prq->dma, 0, sizeof(struct efc_dma_s));
		}

		kfree(rq_buf);
	}
}

/**
 * @brief Allocate the RQ data buffers.
 *
 * @param hw Pointer to HW object.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_rx_allocate(struct efct_hw_s *hw)
{
	struct efct_s *efct = hw->os;
	u32 i;
	int rc = EFCT_HW_RTN_SUCCESS;
	u32 rqindex = 0;
	struct hw_rq_s *rq;
	u32 hdr_size = EFCT_HW_RQ_SIZE_HDR;
	u32 payload_size = hw->config.rq_default_buffer_size;

	rqindex = 0;

	for (i = 0; i < hw->hw_rq_count; i++) {
		rq = hw->hw_rq[i];

		/* Allocate header buffers */
		rq->hdr_buf = efct_hw_rx_buffer_alloc(hw, rqindex,
						      rq->entry_count,
						      hdr_size);
		if (!rq->hdr_buf) {
			efct_log_err(efct,
				     "efct_hw_rx_buffer_alloc hdr_buf failed\n");
			rc = EFCT_HW_RTN_ERROR;
			break;
		}

		efct_log_debug(hw->os,
			       "rq[%2d] rq_id %02d header  %4d by %4d bytes\n",
			      i, rq->hdr->id, rq->entry_count, hdr_size);

		rqindex++;

		/* Allocate payload buffers */
		rq->payload_buf = efct_hw_rx_buffer_alloc(hw, rqindex,
							  rq->entry_count,
							  payload_size);
		if (!rq->payload_buf) {
			efct_log_err(efct,
				     "efct_hw_rx_buffer_alloc fb_buf failed\n");
			rc = EFCT_HW_RTN_ERROR;
			break;
		}
		efct_log_debug(hw->os,
			       "rq[%2d] rq_id %02d default %4d by %4d bytes\n",
			      i, rq->data->id, rq->entry_count, payload_size);
		rqindex++;
	}

	return rc ? EFCT_HW_RTN_ERROR : EFCT_HW_RTN_SUCCESS;
}

/**
 * @brief Post the RQ data buffers to the chip.
 *
 * @param hw Pointer to HW object.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
enum efct_hw_rtn_e
efct_hw_rx_post(struct efct_hw_s *hw)
{
	u32 i;
	u32 idx;
	u32 rq_idx;
	int rc = 0;

	/*
	 * In RQ pair mode, we MUST post the header and payload buffer at the
	 * same time.
	 */
	for (rq_idx = 0, idx = 0; rq_idx < hw->hw_rq_count; rq_idx++) {
		struct hw_rq_s *rq = hw->hw_rq[rq_idx];

		for (i = 0; i < rq->entry_count - 1; i++) {
			struct efc_hw_sequence_s *seq;

			seq = efct_array_get(hw->seq_pool, idx++);
			efct_hw_assert(seq);
			seq->header = &rq->hdr_buf[i];
			seq->payload = &rq->payload_buf[i];
			rc = efct_hw_sequence_free(hw, seq);
			if (rc)
				break;
		}
		if (rc)
			break;
	}

	return rc;
}

/**
 * @brief Free the RQ data buffers.
 *
 * @param hw Pointer to HW object.
 *
 */
void
efct_hw_rx_free(struct efct_hw_s *hw)
{
	struct hw_rq_s *rq;
	u32 i;

	/* Free hw_rq buffers */
	for (i = 0; i < hw->hw_rq_count; i++) {
		rq = hw->hw_rq[i];
		if (rq) {
			efct_hw_rx_buffer_free(hw, rq->hdr_buf,
					       rq->entry_count);
			rq->hdr_buf = NULL;
			efct_hw_rx_buffer_free(hw, rq->payload_buf,
					       rq->entry_count);
			rq->payload_buf = NULL;
		}
	}
}

/**
 * @brief HW async call context structure.
 */
struct efct_hw_async_call_ctx_s {
	efct_hw_async_cb_t callback;
	void *arg;
	u8 cmd[SLI4_BMBX_SIZE];
};

/**
 * @brief HW async callback handler
 *
 * @par Description
 * This function is called when the NOP mbox cmd completes.  The callback stored
 * in the requesting context is invoked.
 *
 * @param hw Pointer to HW object.
 * @param status Completion status.
 * @param mqe Pointer to mailbox completion queue entry.
 * @param arg Caller-provided argument.
 *
 * @return None.
 */
static void
efct_hw_async_cb(struct efct_hw_s *hw, int status, u8 *mqe, void *arg)
{
	struct efct_hw_async_call_ctx_s *ctx = arg;

	if (ctx) {
		if (ctx->callback)
			(*ctx->callback)(hw, status, mqe, ctx->arg);

		kfree(ctx);
	}
}

/**
 * @brief Make an async callback using NOP mailbox command
 *
 * @par Description
 * Post a NOP mbox cmd; the callback with argument is invoked upon completion
 * while in the event processing context.
 *
 * @param hw Pointer to HW object.
 * @param callback Pointer to callback function.
 * @param arg Caller-provided callback.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int
efct_hw_async_call(struct efct_hw_s *hw,
		   efct_hw_async_cb_t callback, void *arg)
{
	int rc = 0;
	struct efct_hw_async_call_ctx_s *ctx;

	/*
	 * Allocate a callback context (which includes the mbox cmd buffer),
	 * we need this to be persistent as the mbox cmd submission may be
	 * queued and executed later execution.
	 */
	ctx = kmalloc(sizeof(*ctx), GFP_ATOMIC);
	if (!ctx)
		return EFCT_HW_RTN_NO_MEMORY;

	memset(ctx, 0, sizeof(*ctx));
	ctx->callback = callback;
	ctx->arg = arg;

	/* Build and send a NOP mailbox command */
	if (sli_cmd_common_nop(&hw->sli, ctx->cmd,
			       sizeof(ctx->cmd), 0) == 0) {
		efct_log_err(hw->os, "COMMON_NOP format failure\n");
		kfree(ctx);
		rc = -1;
	}

	if (efct_hw_command(hw, ctx->cmd, EFCT_CMD_NOWAIT, efct_hw_async_cb,
			    ctx)) {
		efct_log_err(hw->os, "COMMON_NOP command failure\n");
		kfree(ctx);
		rc = -1;
	}
	return rc;
}

/**
 * @brief Initialize the reqtag pool.
 *
 * @par Description
 * The WQ request tag pool is initialized.
 *
 * @param hw Pointer to HW object.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
enum efct_hw_rtn_e
efct_hw_reqtag_init(struct efct_hw_s *hw)
{
	if (!hw->wq_reqtag_pool) {
		hw->wq_reqtag_pool = efct_pool_alloc(hw->os,
					sizeof(struct hw_wq_callback_s),
					65536, true);
		if (!hw->wq_reqtag_pool) {
			efct_log_err(hw->os,
				     "efct_pool_alloc struct hw_wq_callback_s fail\n");
			return EFCT_HW_RTN_NO_MEMORY;
		}
	}
	efct_hw_reqtag_reset(hw);
	return EFCT_HW_RTN_SUCCESS;
}

/**
 * @brief Allocate a WQ request tag.
 *
 * Allocate and populate a WQ request tag from the WQ request tag pool.
 *
 * @param hw Pointer to HW object.
 * @param callback Callback function.
 * @param arg Pointer to callback argument.
 *
 * @return Returns pointer to allocated WQ request tag, or NULL if object
 * cannot be allocated.
 */
struct hw_wq_callback_s *
efct_hw_reqtag_alloc(struct efct_hw_s *hw,
		     void (*callback)(void *arg, u8 *cqe, int status),
		     void *arg)
{
	struct hw_wq_callback_s *wqcb;

	efct_hw_assert(callback);

	wqcb = efct_pool_get(hw->wq_reqtag_pool);
	if (wqcb) {
		efct_hw_assert(!wqcb->callback);
		wqcb->callback = callback;
		wqcb->arg = arg;
	}
	return wqcb;
}

/**
 * @brief Free a WQ request tag.
 *
 * Free the passed in WQ request tag.
 *
 * @param hw Pointer to HW object.
 * @param wqcb Pointer to WQ request tag object to free.
 *
 * @return None.
 */
void
efct_hw_reqtag_free(struct efct_hw_s *hw, struct hw_wq_callback_s *wqcb)
{
	efct_hw_assert(wqcb->callback);
	wqcb->callback = NULL;
	wqcb->arg = NULL;
	efct_pool_put(hw->wq_reqtag_pool, wqcb);
}

/**
 * @brief Return WQ request tag by index.
 *
 * @par Description
 * Return pointer to WQ request tag object given an index.
 *
 * @param hw Pointer to HW object.
 * @param instance_index Index of WQ request tag to return.
 *
 * @return Pointer to WQ request tag, or NULL.
 */
struct hw_wq_callback_s *
efct_hw_reqtag_get_instance(struct efct_hw_s *hw, u32 instance_index)
{
	struct hw_wq_callback_s *wqcb;

	wqcb = efct_pool_get_instance(hw->wq_reqtag_pool, instance_index);
	if (!wqcb)
		efct_log_err(hw->os, "wqcb for instance %d is null\n",
			     instance_index);

	return wqcb;
}

/**
 * @brief Reset the WQ request tag pool.
 *
 * @par Description
 * Reset the WQ request tag pool, returning all to the free list.
 *
 * @param hw pointer to HW object.
 *
 * @return None.
 */
void
efct_hw_reqtag_reset(struct efct_hw_s *hw)
{
	struct hw_wq_callback_s *wqcb;
	u32 i;

	/* Remove all from freelist */
	while (efct_pool_get(hw->wq_reqtag_pool))
		;

	/* Put them all back */
	for (i = 0;
	     ((wqcb = efct_pool_get_instance(hw->wq_reqtag_pool, i)) != NULL);
	     i++) {
		wqcb->instance_index = i;
		wqcb->callback = NULL;
		wqcb->arg = NULL;
		efct_pool_put(hw->wq_reqtag_pool, wqcb);
	}
}

/**
 * @brief Handle HW assertion
 *
 * HW assert, display diagnostic message, and abort.
 *
 * @param cond string describing failing assertion condition
 * @param filename file name
 * @param linenum line number
 *
 * @return none
 */
void
_efct_hw_assert(const char *cond, const char *filename, int linenum)
{
	efct_log_err(NULL,
		     "(%d): HW assertion (%s) failed\n", linenum,
		    cond);
	WARN_ON(1);
}

/**
 * @brief Handle HW verify
 *
 * HW verify, display diagnostic message, dump stack and return.
 *
 * @param cond string describing failing verify condition
 * @param filename file name
 * @param linenum line number
 *
 * @return none
 */
int
_efct_hw_verify(const char *cond, const char *filename, int linenum)
{
	if (!cond) {
		efct_log_err(NULL,
			     "(%d): HW verify (%s) failed\n", linenum, cond);
		dump_stack();
		return -1;
	}
	return 0;
}

/**
 * @brief Reque XRI
 *
 * @par Description
 * Reque XRI
 *
 * @param hw Pointer to HW object.
 * @param io Pointer to HW IO
 *
 * @return Return 0 if successful else returns -1
 */
int
efct_hw_reque_xri(struct efct_hw_s *hw, struct efct_hw_io_s *io)
{
	int rc = 0;

	rc = efct_hw_rqpair_auto_xfer_rdy_buffer_post(hw, io, true);
	if (rc) {
		INIT_LIST_HEAD(&io->dnrx_link);
		list_add_tail(&io->dnrx_link, &hw->io_port_dnrx);
		rc = -1;
		goto exit_efct_hw_reque_xri;
	}

	io->auto_xfer_rdy_dnrx = false;
	io->type = EFCT_HW_IO_DNRX_REQUEUE;
	if (sli_requeue_xri_wqe(&hw->sli, io->wqe.wqebuf,
				hw->sli.wqe_size, io->indicator,
				EFCT_HW_REQUE_XRI_REGTAG, SLI4_CQ_DEFAULT)) {
		/* Clear buffer from XRI */
		efct_pool_put(hw->auto_xfer_rdy_buf_pool, io->axr_buf);
		io->axr_buf = NULL;

		efct_log_err(hw->os, "requeue_xri WQE error\n");
		INIT_LIST_HEAD(&io->dnrx_link);
		list_add_tail(&io->dnrx_link, &hw->io_port_dnrx);

		rc = -1;
		goto exit_efct_hw_reque_xri;
	}

	if (!io->wq) {
		io->wq = efct_hw_queue_next_wq(hw, io);
		efct_hw_assert(io->wq);
	}

	/*
	 * Add IO to active io wqe list before submitting, in case the
	 * wcqe processing preempts this thread.
	 */
	hw->tcmd_wq_submit[io->wq->instance]++;
	io->wq->use_count++;

	rc = efct_hw_wq_write(io->wq, &io->wqe);
	if (rc < 0) {
		efct_log_err(hw->os, "sli_queue_write reque xri failed: %d\n",
			     rc);
		rc = -1;
	}

exit_efct_hw_reque_xri:
	return 0;
}

/**
 *
 * A workaround may consist of overriding a particular HW/SLI4 value that was
 * initialized during efct_hw_setup() (for example the MAX_QUEUE overrides for
 * mis-reported queue sizes). Or if required, elements of the
 * efct_hw_workaround_s structure may be set to control specific runtime
 * behavior.
 *
 * It is intended that the controls in efct_hw_workaround_s be defined
 * functionally. So we would have the driver look like:
 * "if (hw->workaround.enable_xxx) then ...", rather than what we might
 * previously"
 *
 **/

#define HW_FWREV_ZERO		(0ull)
#define HW_FWREV_MAX		(~0ull)

#define SLI4_ASIC_TYPE_ANY	0
#define SLI4_ASIC_REV_ANY	0
#define HWWRK "HW Workaround:"
/**
 * @brief Internal definition of workarounds
 */

enum hw_workaround_e {
	HW_WORKAROUND_TEST = 1,
	/* Limits all queues */
	HW_WORKAROUND_MAX_QUEUE,
	/* Limits only the RQ */
	HW_WORKAROUND_MAX_RQ,
	HW_WORKAROUND_RETAIN_TSEND_IO_LENGTH,
	HW_WORKAROUND_WQE_COUNT_METHOD,
	HW_WORKAROUND_RQE_COUNT_METHOD,
	HW_WORKAROUND_USE_UNREGISTERD_RPI,
	/* Disable of auto-rsp tgt DIF */
	HW_WORKAROUND_DISABLE_AR_TGT_DIF,
	HW_WORKAROUND_DISABLE_SET_DUMP_LOC,
	HW_WORKAROUND_USE_DIF_QUARANTINE,
	HW_WORKAROUND_USE_DIF_SEC_XRI,	/*Use 2nd xri for multiple dataphases */
	/* FCFI reported in SRB not correct, use "first" registered domain */
	HW_WORKAROUND_OVERRIDE_FCFI_IN_SRB,
	/* The FW version is not the min version supported by this driver */
	HW_WORKAROUND_FW_VERSION_TOO_LOW,
	/* Chip supports SGL Chaining but SGLC is not set in SLI4_PARAMS */
	HW_WORKAROUND_SGLC_MISREPORTED,
	/* Don't use SEND_FRAME capable if FW version is too old */
	HW_WORKAROUND_IGNORE_SEND_FRAME_CAPABLE,
};

/**
 * @brief Internal workaround structure instance
 */

struct hw_workaround_s {
	enum sli4_asic_type_e asic_type;
	enum sli4_asic_rev_e asic_rev;
	u64 fwrev_low;
	u64 fwrev_high;

	enum hw_workaround_e workaround;
	u32 value;
};

static struct hw_workaround_s hw_workarounds[] = {
	{SLI4_ASIC_TYPE_ANY, SLI4_ASIC_REV_ANY, HW_FWREV_ZERO, HW_FWREV_MAX,
	 HW_WORKAROUND_TEST, 999},

	/*
	 * if_type == 2 returns 0 for total length placed on
	 * FCP_TSEND64_WQE completions.   Note, original driver code enables
	 * this workaround for all asic types
	 */
	{SLI4_ASIC_TYPE_ANY, SLI4_ASIC_REV_ANY, HW_FWREV_ZERO, HW_FWREV_MAX,
	 HW_WORKAROUND_RETAIN_TSEND_IO_LENGTH, 0},

	/* mis-reported max queue depth */
	{SLI4_ASIC_INTF_2,	SLI4_ASIC_REV_A0, HW_FWREV_ZERO, HW_FWREV_MAX,
	 HW_WORKAROUND_MAX_QUEUE, 2048},

	{SLI4_ASIC_INTF_2,	SLI4_ASIC_REV_ANY, HW_FWREV_ZERO,
	 HW_FWREV(1, 1, 65, 0), HW_WORKAROUND_DISABLE_SET_DUMP_LOC, 0},

	/* FCFI reported in SRB not corrrect */
	{SLI4_ASIC_INTF_2, SLI4_ASIC_REV_ANY, HW_FWREV_ZERO,
	 HW_FWREV_MAX, HW_WORKAROUND_OVERRIDE_FCFI_IN_SRB, 0},

	/* version check for driver */
	{SLI4_ASIC_INTF_2, SLI4_ASIC_REV_ANY, HW_FWREV_ZERO,
	 HW_FWREV(10, 4, 255, 0), HW_WORKAROUND_FW_VERSION_TOO_LOW,
	 0},

	/* FW does not set the SGLC bit */
	{SLI4_ASIC_INTF_2, SLI4_ASIC_REV_ANY, HW_FWREV_ZERO,
	 HW_FWREV_MAX, HW_WORKAROUND_SGLC_MISREPORTED, 0},

	/* enable this workaround for ALL revisions */
	{SLI4_ASIC_TYPE_ANY, SLI4_ASIC_REV_ANY, HW_FWREV_ZERO, HW_FWREV_MAX,
	 HW_WORKAROUND_IGNORE_SEND_FRAME_CAPABLE, 0},
};

/**
 * @brief Function prototypes
 */

static int efct_hw_workaround_match(struct efct_hw_s *hw,
				    struct hw_workaround_s *w);

/**
 * @brief Parse the firmware version (name)
 *
 * Parse a string of the form a.b.c.d, returning a uint64_t packed as defined
 * by the HW_FWREV() macro
 *
 * @param fwrev_string pointer to the firmware string
 *
 * @return packed firmware revision value
 */

static uint64_t
parse_fw_version(const char *fwrev_string)
{
	int v[4] = {0};
	char *p, *token;
	int i = 0;

	p = kstrdup(fwrev_string, GFP_KERNEL);

	while ((token = strsep(&p, ".")) && *token) {
		if (kstrtoint(token, 0, &v[i++]))
			efct_log_err(NULL, "kstrtoint failed\n");

		if (!p || !*p)
			break;
	}

	kfree(p);

	/*
	 * Special case for bootleg releases with f/w rev 0.0.9999.0,
	 * set to max value
	 */
	if (v[2] == 9999)
		return HW_FWREV_MAX;
	else
		return HW_FWREV(v[0], v[1], v[2], v[3]);
}

/**
 * @brief Test for a workaround match
 *
 * Looks at the asic type, asic revision, and fw revision, and returns TRUE if
 * match.
 *
 * @param hw Pointer to the HW structure
 * @param w Pointer to a workaround structure entry
 *
 * @return Return TRUE for a match
 */

static int
efct_hw_workaround_match(struct efct_hw_s *hw, struct hw_workaround_s *w)
{
	return (((w->asic_type == SLI4_ASIC_TYPE_ANY) ||
		 (w->asic_type == hw->sli.asic_type)) &&
		    ((w->asic_rev == SLI4_ASIC_REV_ANY) ||
		     (w->asic_rev == hw->sli.asic_rev)) &&
		    (w->fwrev_low <= hw->workaround.fwrev) &&
		    ((w->fwrev_high == HW_FWREV_MAX) ||
		     (hw->workaround.fwrev < w->fwrev_high)));
}

/**
 * @brief Setup HW runtime workarounds
 *
 * The function is called at the end of efct_hw_setup() to setup any runtime
 * workarounds based on the HW/SLI setup.
 *
 * @param hw Pointer to HW structure
 *
 * @return none
 */

void
efct_hw_workaround_setup(struct efct_hw_s *hw)
{
	struct hw_workaround_s *w;
	struct sli4_s *sli4 = &hw->sli;
	u32 i;
	int t_idx;

	/* Initialize the workaround settings */
	memset(&hw->workaround, 0, sizeof(hw->workaround));

	/*
	 * If hw_war_version is non-null, then its a value that was set by a
	 * module parameter
	 */

	if (hw->hw_war_version)
		hw->workaround.fwrev = parse_fw_version(hw->hw_war_version);
	else
		hw->workaround.fwrev =
			 parse_fw_version((char *)sli4->fw_name[0]);

	/* Walk the workaround list, if a match is found, then handle it */
	for (i = 0, w = hw_workarounds; i < ARRAY_SIZE(hw_workarounds);
	     i++, w++) {
		if (efct_hw_workaround_match(hw, w)) {
			switch (w->workaround) {
			case HW_WORKAROUND_TEST: {
				efct_log_debug(hw->os, "%s Override: tst:%d\n",
					       HWWRK,
					      w->value);
				break;
			}

			case HW_WORKAROUND_RETAIN_TSEND_IO_LENGTH: {
				efct_log_debug(hw->os,
					       "%s retain TSEND IO len\n",
					      HWWRK);
				hw->workaround.retain_tsend_io_length = 1;
				break;
			}
			case HW_WORKAROUND_MAX_QUEUE: {
				enum sli4_qtype_e q;

				efct_log_debug(hw->os,
					       "%s max_qentries:%d\n",
					      HWWRK,
					      w->value);
				for (q = SLI_QTYPE_EQ; q < SLI_QTYPE_MAX; q++) {
					if (hw->num_qentries[q] > w->value)
						hw->num_qentries[q] = w->value;
				}
				break;
			}
			case HW_WORKAROUND_MAX_RQ: {
				efct_log_debug(hw->os,
					       "%s RQ max_qentries: %d\n",
					      HWWRK,
					      w->value);
				t_idx = SLI_QTYPE_RQ;
				if (hw->num_qentries[t_idx] > w->value)
					hw->num_qentries[t_idx] = w->value;
				break;
			}
			case HW_WORKAROUND_WQE_COUNT_METHOD: {
				efct_log_debug(hw->os,
					       "%s WQE count method=%d\n",
					      HWWRK,
					      w->value);
				t_idx = SLI_QTYPE_WQ;
				sli4->count_method[t_idx] = w->value;
				sli_calc_max_qentries(sli4);
				break;
			}
			case HW_WORKAROUND_RQE_COUNT_METHOD: {
				efct_log_debug(hw->os,
					       "%s RQE count method=%d\n",
					      HWWRK,
					      w->value);
				t_idx = SLI_QTYPE_RQ;
				sli4->count_method[t_idx] = w->value;
				sli_calc_max_qentries(sli4);
				break;
			}
			case HW_WORKAROUND_USE_UNREGISTERD_RPI:
				efct_log_debug(hw->os,
					       "%s use unreg'd RPI\n",
					      HWWRK);
				hw->workaround.use_unregistered_rpi = true;
				/*
				 * Allocate an RPI that is never registered, to
				 * be used in the case where a node has been
				 * unregistered, and its indicator (RPI) value
				 * is set to 0xFFFF
				 */
				if (sli_resource_alloc(&hw->sli,
						       SLI_RSRC_RPI,
					 &hw->workaround.unregistered_rid,
					&hw->workaround.unregistered_index)) {
					efct_log_err(hw->os,
						     "sli_resource_alloc unregd RPI failed\n");
					hw->workaround.use_unregistered_rpi =
									 false;
				}
				break;
			case HW_WORKAROUND_DISABLE_AR_TGT_DIF:
				efct_log_debug(hw->os,
					       "%s disable AR on T10-PI TSEND\n",
					      HWWRK);
				hw->workaround.disable_ar_tgt_dif = true;
				break;
			case HW_WORKAROUND_DISABLE_SET_DUMP_LOC:
				efct_log_debug(hw->os,
					       "%s disable set_dump_loc\n",
					      HWWRK);
				hw->workaround.disable_dump_loc = true;
				break;
			case HW_WORKAROUND_USE_DIF_QUARANTINE:
				efct_log_debug(hw->os,
					       "%s use DIF quarantine\n",
					     HWWRK);
				hw->workaround.use_dif_quarantine = true;
				break;
			case HW_WORKAROUND_USE_DIF_SEC_XRI:
				efct_log_debug(hw->os,
					       "%s use DIF secondary xri\n",
					      HWWRK);
				hw->workaround.use_dif_sec_xri = true;
				break;
			case HW_WORKAROUND_OVERRIDE_FCFI_IN_SRB:
				efct_log_debug(hw->os,
					       "%s override FCFI in SRB\n",
					      HWWRK);
				hw->workaround.override_fcfi = true;
				break;

			case HW_WORKAROUND_FW_VERSION_TOO_LOW:
				efct_log_debug(hw->os,
					       "%s fwrev below min for drvr\n",
					      HWWRK);
				hw->workaround.fw_version_too_low = true;
				break;
			case HW_WORKAROUND_SGLC_MISREPORTED:
				efct_log_debug(hw->os,
					       "%s SGLC wrong - chaining on\n",
					      HWWRK);
				hw->workaround.sglc_misreported = true;
				break;
			case HW_WORKAROUND_IGNORE_SEND_FRAME_CAPABLE:
				efct_log_debug(hw->os,
					       "%s SEND_FRAME disabled\n",
					      HWWRK);
				hw->workaround.ignore_send_frame = true;
				break;
			} /* switch(w->workaround) */
		}
	}
}

/*
 * @brief Return the WWN as a uint64_t.
 *
 * <h3 class="desc">Description</h3>
 * Calls the HW property function for the WWNN or WWPN, and returns the value
 * as a uint64_t.
 *
 * @param hw Pointer to the HW object.
 * @param prop HW property.
 *
 * @return Returns uint64_t request value.
 */

uint64_t
efct_get_wwn(struct efct_hw_s *hw, enum efct_hw_property_e prop)
{
	u8 *p = efct_hw_get_ptr(hw, prop);
	u64 value = 0;

	if (p) {
		u32 i;

		for (i = 0; i < sizeof(value); i++)
			value = (value << 8) | p[i];
	}

	return value;
}

