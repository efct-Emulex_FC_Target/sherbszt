/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#include "efct_driver.h"
#include "efct_utils.h"

#include "efct_els.h"
#include "efct_hw.h"
#include "efct_unsol.h"
#include "efct_scsi.h"

/* Copyright notice */
static const char efct_copyright[] =
			"Copyright (C) 2017 Broadcom. All Rights Reserved";

static int efct_proc_open(struct inode *inode, struct file *file);
static int efct_proc_get(struct seq_file *m, void *v);

static void efct_device_interrupt_handler(struct efct_s *efct, u32 vector);
static void efct_teardown_msix(struct efct_os_s *efct_os);
static int efct_fw_reset(struct efct_s *efct);
static int
efct_firmware_write(struct efct_s *efct, const u8 *buf, size_t buf_len,
		    u8 *change_status);
static int
efct_efclib_config(struct efct_s *efct, struct libefc_function_template *tt);

struct efct_s *efct_devices[MAX_EFCT_DEVICES];

static const struct file_operations efct_proc_fops = {
	.owner = THIS_MODULE,
	.open = efct_proc_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

int loglevel = 3;
module_param(loglevel, int, 0444);
MODULE_PARM_DESC(loglevel, "logging level. 0-5 default 3");

static int logmask;
module_param(logmask, int, 0444);
MODULE_PARM_DESC(logmask, "logging bitmask (default 0)");

#define FW_WRITE_BUFSIZE (64 * 1024)
struct efct_fw_write_result {
	struct semaphore sem;
	int status;
	u32 actual_xfer;
	u32 change_status;
};

struct libefc_function_template efct_libefc_templ = {
	.hw_domain_alloc = efct_hw_domain_alloc,
	.hw_domain_attach = efct_hw_domain_attach,
	.hw_domain_free = efct_hw_domain_free,
	.hw_domain_force_free = efct_hw_domain_force_free,
	.hw_domain_get = efct_hw_domain_get,
	.domain_hold_frames = efct_domain_hold_frames,
	.domain_accept_frames = efct_domain_accept_frames,

	.hw_port_alloc = efct_hw_port_alloc,
	.hw_port_attach = efct_hw_port_attach,
	.hw_port_free = efct_hw_port_free,

	.hw_node_alloc = efct_hw_node_alloc,
	.hw_node_attach = efct_hw_node_attach,
	.hw_node_detach = efct_hw_node_detach,
	.hw_node_free_resources = efct_hw_node_free_resources,
	.node_purge_pending = efct_node_purge_pending,

	.scsi_io_alloc_disable = efct_scsi_io_alloc_disable,
	.scsi_io_alloc_enable = efct_scsi_io_alloc_enable,
	.scsi_validate_initiator = efct_scsi_validate_initiator,
	.scsi_del_initiator = efct_scsi_del_initiator,
	.tgt_del_sport = efct_scsi_tgt_del_sport,
	.tgt_new_domain = efct_scsi_tgt_new_domain,
	.tgt_del_domain = efct_scsi_tgt_del_domain,
	.tgt_new_sport = efct_scsi_tgt_new_sport,
	.scsi_new_initiator = efct_scsi_new_initiator,

	.els_send = efct_els_req_send,
	.els_send_ct = efct_els_send_ct,
	.els_send_resp = efct_els_resp_send,
	.bls_send_acc_hdr = efct_bls_send_acc_hdr,
	.send_flogi_p2p_acc = efct_send_flogi_p2p_acc,
	.send_ct_rsp = efct_send_ct_rsp,
	.send_ls_rjt = efct_send_ls_rjt,

	.node_io_cleanup = efct_node_io_cleanup,
	.node_els_cleanup = efct_node_els_cleanup,
	.node_abort_all_els = efct_node_abort_all_els,

	.dispatch_fcp_cmd = efct_dispatch_fcp_cmd,
	.dispatch_fcp_cmd_auto_xfer_rdy = efct_dispatch_fcp_cmd_auto_xfer_rdy,
	.dispatch_fcp_data = efct_dispatch_fcp_data,
	.recv_abts_frame = efct_node_recv_abts_frame,
};

static char *queue_topology =
	"eq cq rq cq mq $nulp($nwq(cq wq:ulp=$rpt1)) cq wq:len=256:class=1";
/**
 * @brief Perform driver wide initialization
 *
 * This function is called prior to enumerating PCI devices, with subsequent
 * calls to efct_device_attach.  For EFCT, this function invokes the back end
 * functions efct_scsi_tgt_driver_init(), and efct_scsi_ini_driver_init()
 *
 * @return returns 0 for success, a negative error code value for failure.
 */
int
efct_device_init(void)
{
	int rc;

	hw_global.queue_topology_string = queue_topology;

	/* driver-wide init for target-server */
	rc = efct_scsi_tgt_driver_init();
	if (rc) {
		efct_log_err(NULL, "efct_scsi_tgt_init failed rc=%d\n",
			     rc);
		return -1;
	}

	rc = efct_scsi_reg_fc_transport();
	if (rc) {
		efct_log_err(NULL, "failed to register to FC host\n");
		return -1;
	}

	return 0;
}

/**
 * @brief Perform driver wide shutdown complete actions
 *
 * This function is called shutdown for all devices has completed
 *
 * @return none
 */
void
efct_device_shutdown(void)
{
	efct_scsi_release_fc_transport();

	efct_scsi_tgt_driver_exit();
}

/*
 * @brief allocate efct device
 *
 * @param nid Numa node ID
 *
 * @return pointer to EFCT structure
 */

void *efct_device_alloc(u32 nid)
{
	struct efct_s *efct = NULL;
	u32 i;

	efct = kmalloc_node(sizeof(*efct), GFP_ATOMIC, nid);

	if (efct) {
		memset(efct, 0, sizeof(*efct));
		for (i = 0; i < ARRAY_SIZE(efct_devices); i++) {
			if (!efct_devices[i]) {
				efct->instance_index = i;
				efct_devices[i] = efct;
				break;
			}
		}

		if (i == ARRAY_SIZE(efct_devices)) {
			efct_log_err(NULL, "failed\n");
			kfree(efct);
			efct = NULL;
		} else {
			efct->attached = false;
		}
	}
	return efct;
}

static int
efct_fw_reset(struct efct_s *efct)
{
	int rc = 0;
	int index = 0;
	u8 bus, dev, func;
	struct efct_s *other_efct;

	bus = efct->pdev->bus->number;
	dev = PCI_SLOT(efct->pdev->devfn);
	func = PCI_FUNC(efct->pdev->devfn);

	while ((other_efct = efct_get_instance(index++)) != NULL) {
		u8 other_bus, other_dev, other_func;

		other_bus = other_efct->pdev->bus->number;
		other_dev = PCI_SLOT(other_efct->pdev->devfn);
		other_func = PCI_FUNC(other_efct->pdev->devfn);

		if (bus == other_bus && dev == other_dev &&
		    timer_pending(&other_efct->xport->stats_timer)) {
			efct_log_debug(other_efct,
				       "removing link stats timer\n");
			del_timer(&other_efct->xport->stats_timer);
		}
	}

	if (efct_hw_reset(&efct->hw, EFCT_HW_RESET_FIRMWARE)) {
		efct_log_test(efct, "failed to reset firmware\n");
		rc = -1;
	} else {
		efct_log_debug(efct,
			       "successfully reset firmware.Now resetting port\n");
		/* now flag all functions on the same device
		 * as this port as uninitialized
		 */
		index = 0;

		while ((other_efct = efct_get_instance(index++)) != NULL) {
			u8 other_bus, other_dev, other_func;

			other_bus = other_efct->pdev->bus->number;
			other_dev = PCI_SLOT(other_efct->pdev->devfn);
			other_func = PCI_FUNC(other_efct->pdev->devfn);

			if (bus == other_bus && dev == other_dev) {
				if (other_efct->hw.state !=
						EFCT_HW_STATE_UNINITIALIZED) {
					other_efct->hw.state =
						EFCT_HW_STATE_QUEUES_ALLOCATED;
				}
				efct_device_detach(efct);
				rc = efct_device_attach(efct);

				efct_log_debug(other_efct,
					       "re-start driver with new firmware\n");
			}
		}
	}
	return rc;
}

static void
efct_fw_write_cb(int status, u32 actual_write_length,
		 u32 change_status, void *arg)
{
	struct efct_fw_write_result *result = arg;

	result->status = status;
	result->actual_xfer = actual_write_length;
	result->change_status = change_status;

	up(&result->sem);
}

static int
efct_firmware_write(struct efct_s *efct, const u8 *buf, size_t buf_len,
		    u8 *change_status)
{
	int rc = 0;
	u32 bytes_left;
	u32 xfer_size;
	u32 offset;
	struct efc_dma_s dma;
	int last = 0;
	struct efct_fw_write_result result;

	sema_init(&result.sem, 0);

	bytes_left = buf_len;
	offset = 0;

	dma.size = FW_WRITE_BUFSIZE;
	dma.virt = dma_alloc_coherent(&efct->pdev->dev,
				      dma.size, &dma.phys, GFP_DMA);
	if (!dma.virt) {
		efct_log_err(efct, "dma_alloc failed\n");
		return -ENOMEM;
	}

	while (bytes_left > 0) {
		if (bytes_left > FW_WRITE_BUFSIZE)
			xfer_size = FW_WRITE_BUFSIZE;
		else
			xfer_size = bytes_left;

		memcpy(dma.virt, buf + offset, xfer_size);

		if (bytes_left == xfer_size)
			last = 1;

		efct_hw_firmware_write(&efct->hw, &dma, xfer_size, offset,
				       last, efct_fw_write_cb, &result);

		if (down_interruptible(&result.sem) != 0) {
			rc = -ENXIO;
			break;
		}

		if (result.actual_xfer == 0 || result.status != 0) {
			rc = -EFAULT;
			break;
		}

		if (last)
			*change_status = result.change_status;

		bytes_left -= result.actual_xfer;
		offset += result.actual_xfer;
	}

	dma_free_coherent(&efct->pdev->dev, dma.size, dma.virt, dma.phys);
	return rc;
}

	int
efct_request_firmware_update(struct efct_s *efct)
{
	int rc = 0;
	u8 file_name[256], fw_change_status;
	const struct firmware *fw;
	struct efct_hw_grp_hdr *fw_image;

	snprintf(file_name, 256, "%s.grp", efct->model);
	rc = request_firmware(&fw, file_name, &efct->efct_os.pdev->dev);
	if (rc) {
		efct_log_err(efct, "Firmware file(%s) not found.\n", file_name);
		return rc;
	}
	fw_image = (struct efct_hw_grp_hdr *)fw->data;

	/* Check if firmware provided is compatible with this particular
	 * Adapter of not
	 */
	if ((be32_to_cpu(fw_image->magic_number) != EFCT_HW_OBJECT_G5) &&
	    (be32_to_cpu(fw_image->magic_number) != EFCT_HW_OBJECT_G6)) {
		efct_log_warn(efct,
			      "Invalid FW image found Magic: 0x%x Size: %ld\n",
			be32_to_cpu(fw_image->magic_number), fw->size);
		rc = -1;
		goto exit;
	}

	if (!strncmp(efct->fw_version, fw_image->revision,
		     strnlen(fw_image->revision, 16))) {
		efct_log_debug(efct,
			       "No update req. Firmware is already up to date.\n");
		rc = 0;
		goto exit;
	}
	rc = efct_firmware_write(efct, fw->data, fw->size, &fw_change_status);
	if (rc) {
		efct_log_err(efct,
			     "Firmware update failed. Return code = %d\n", rc);
	} else {
		efct_log_info(efct, "Firmware updated successfully\n");
		switch (fw_change_status) {
		case 0x00:
			efct_log_debug(efct,
				       "No reset needed, new firmware is active.\n");
			break;
		case 0x01:
			efct_log_warn(efct,
				      "A physical device reset (host reboot) is needed to activate the new firmware\n");
			break;
		case 0x02:
		case 0x03:
			efct_log_debug(efct,
				       "firmware is resetting to activate the new firmware\n");
			efct_fw_reset(efct);
			break;
		default:
			efct_log_debug(efct,
				       "Unexected value change_status: %d\n",
				fw_change_status);
			break;
		}
	}

exit:
	release_firmware(fw);

	return rc;
}

/**
 * @brief free efct device
 *
 * @param efct pointer to efct structure
 *
 * @return none
 */

void efct_device_free(struct efct_s *efct)
{
	if (efct) {
		efct_devices[efct->instance_index] = NULL;

		kfree(efct);
	}
}

/**
 * @brief return the number of interrupts required per HBA
 *
 * @param efct pointer to efct structure
 *
 * @return the number of interrupts or a negative value on error.
 */
int
efct_device_interrupts_required(struct efct_s *efct)
{
	if (efct_hw_setup(&efct->hw, efct, efct->efct_os.pdev)
				!= EFCT_HW_RTN_SUCCESS) {
		return -1;
	}
	return efct_hw_qtop_eq_count(&efct->hw);
}

static int
efct_efclib_config(struct efct_s *efct, struct libefc_function_template *tt)
{
	struct efc_lport *efc;
	struct sli4_s	*sli;

	efc = kmalloc(sizeof(*efc), GFP_KERNEL);
	if (!efc)
		return -1;

	memset(efc, 0, sizeof(struct efc_lport));
	efct->efcport = efc;

	memcpy(&efc->tt, tt, sizeof(*tt));
	efc->base = efct;
	efc->pcidev = efct->efct_os.pdev;

	efc->def_wwnn = efct_get_wwn(&efct->hw, EFCT_HW_WWN_NODE);
	efc->def_wwpn = efct_get_wwn(&efct->hw, EFCT_HW_WWN_PORT);
	efc->enable_tgt = 1;
	
	sli = &efct->hw.sli;	
	efc->max_xfer_size = sli->sge_supported_length *
			     sli_get_max_sgl(&efct->hw.sli);

	efcport_init(efc);

	return 0;
}

/**
 * @brief Initialize resources when pci devices attach
 *
 * @param efct pointer to efct structure
 *
 * @return 0 for success, a negative error code value for failure.
 */

int
efct_device_attach(struct efct_s *efct)
{
	u32 rc = 0, i = 0;

	if (efct->attached) {
		efct_log_warn(efct, "Device is already attached\n");
		rc = -1;
	} else {
		snprintf(efct->display_name, sizeof(efct->display_name),
			 "[%s%d] ", "fc",  efct->instance_index);

		efct->logmask = logmask;
		efct->ctrlmask = 10;
		efct->enable_numa_support = 1;
		efct->filter_def = "0,0,0,0";
		efct->max_isr_time_msec = EFCT_OS_MAX_ISR_TIME_MSEC;
		efct->model = (efct->pci_device == EFCT_DEVICE_ID_LPE31004) ?
					"LPE31004" : "unknown";
		efct->fw_version = (const char *)efct_hw_get_ptr(&efct->hw,
							EFCT_HW_FW_REV);
		efct->driver_version = EFCT_DRIVER_VERSION;

		efct->efct_req_fw_upgrade = true;

		/* Allocate transport object and bring online */
		efct->xport = efct_xport_alloc(efct);
		if (!efct->xport) {
			efct_log_err(efct, "failed to allocate transport object\n");
			rc = -1;
		} else if (efct_xport_attach(efct->xport) != 0) {
			efct_log_err(efct, "failed to attach transport object\n");
			rc = -1;
		} else if (efct_xport_initialize(efct->xport) != 0) {
			efct_log_err(efct, "failed to initialize transport object\n");
			rc = -1;
		} else if (efct_efclib_config(efct, &efct_libefc_templ)) {
			efct_log_err(efct, "failed to init efclib\n");
			rc = -1;
		} else if (efct_start_event_processing(&efct->efct_os)) {
			efct_log_err(efct, "failed to start event processing\n");
			rc = -1;
		} else {
			for (i = 0; i < efct->efct_os.n_msix_vec; i++) {
				efct_log_debug(efct, "irq %d enabled\n",
					efct->efct_os.msix_vec[i].vector);
				enable_irq(efct->efct_os.msix_vec[i].vector);
			}
		}

		efct->desc = efct->hw.sli.modeldesc;
		efct_log_info(efct, "adapter model description: %s\n",
			      efct->desc);

		if (rc == 0) {
			efct->attached = true;
		} else {
			efct_teardown_msix((struct efct_os_s *)efct);
			if (efct->xport) {
				efct_xport_free(efct->xport);
				efct->xport = NULL;
			}
		}

		if (efct->efct_req_fw_upgrade) {
			efct_log_debug(efct, "firmware update is in progress\n");
			efct_request_firmware_update(efct);
		}
	}

	return rc;
}

/**
 * @brief interrupt handler
 *
 * Interrupt handler
 *
 * @param efct pointer to efct structure
 * @param vector Zero-based interrupt vector number.
 *
 * @return none
 */

static void
efct_device_interrupt_handler(struct efct_s *efct, u32 vector)
{
	efct_hw_process(&efct->hw, vector, efct->max_isr_time_msec);
}

/**
 * @brief free resources when pci device detach
 *
 * @param efct pointer to efct structure
 *
 * @return 0 for success, a negative error code value for failure.
 */

int
efct_device_detach(struct efct_s *efct)
{
	int rc = 0;

	if (efct) {
		if (!efct->attached) {
			efct_log_warn(efct, "Device is not attached\n");
			return -1;
		}

		rc = efct_xport_control(efct->xport, EFCT_XPORT_SHUTDOWN);
		if (rc)
			efct_log_err(efct, "Transport Shutdown timed out\n");

		efct_stop_event_processing(&efct->efct_os);

		if (efct_xport_detach(efct->xport) != 0)
			efct_log_err(efct, "Transport detach failed\n");

		efct_xport_free(efct->xport);
		efct->xport = NULL;

		efcport_destroy(efct->efcport);
		kfree(efct->efcport);

		efct->attached = false;
	}

	return 0;
}

/**
 * @brief return EFCT instance display name
 *
 * returns const char string of EFCT instance display name
 *
 * @param os pointer to OS (efct_t) object
 *
 * @return return display name
 */

const char *efct_display_name(void *os)
{
	struct efct_s *efct = os;

	efct_assert(efct, "[?]");
	return efct->display_name;
}

/**
 * @brief handle MSIX interrupts
 *
 * Interrupt entry point for MSIX interrupts.
 * Simply schedule the interrupt tasklet
 *
 * @param irq interrupt request number
 * @param handle pointer to interrupt context structure
 *
 * @return IRQ_HANDLED (always handled)
 */

static irqreturn_t
efct_intr_msix(int irq, void *handle)
{
	struct efct_os_intr_context_s *intr_context = handle;

	up(&intr_context->intsem);
	return IRQ_HANDLED;
}

/**
 * @brief Process interrupt events
 *
 * Process events in a kernel thread context.  A counting semaphore is used,
 * this function waits on the semaphore, the interrupt handler increments it.
 *
 * @param thread pointer to the thread object
 *
 * @return returns 0 for success, a negative error code value for failure.
 */

static int
efct_intr_thread(struct efct_os_intr_context_s *intr_context)
{
	struct efct_s *efct = intr_context->efct;
	int rc;
	u32 tstart, tnow;

	tstart = jiffies_to_msecs(jiffies);

	while (!kthread_should_stop()) {
		rc = down_timeout(&intr_context->intsem,
				  usecs_to_jiffies(100000));
		if (rc) {
			tnow = jiffies_to_msecs(jiffies);
			continue;
		}

		efct_device_interrupt_handler(efct, intr_context->index);

		/* If we've been running for too long, then yield */
		tnow = jiffies_to_msecs(jiffies);
		if ((tnow - tstart) > 5000) {
			cond_resched();
			tstart = tnow;
		}
	}

	return 0;
}

/**
 * @brief setup MSIX interrupts
 *
 * Sets up MSIX interrupts.  Currently a single vector is requested and used.
 *
 * @param efct_os pointer to efct_os structure
 * @param num_interrupts The number of MSI-X interrupts to acquire
 *
 * @return returns 0 for success, a negative error code value for failure.
 */

static int
efct_setup_msix(struct efct_os_s *efct_os, u32 num_interrupts)
{
	int	rc = 0;
	u32 i;

	if (!pci_find_capability(efct_os->pdev, PCI_CAP_ID_MSIX)) {
		dev_err(&efct_os->pdev->dev,
			"%s : MSI-X not available\n", __func__);
		return -EINVAL;
	}

	efct_assert(num_interrupts <= ARRAY_SIZE(efct_os->msix_vec), -1);

	efct_os->n_msix_vec = num_interrupts;
	for (i = 0; i < num_interrupts; i++)
		efct_os->msix_vec[i].entry = i;

	rc = pci_enable_msix_exact(efct_os->pdev,
				   efct_os->msix_vec, efct_os->n_msix_vec);
	if (!rc) {
		for (i = 0; i < num_interrupts; i++) {
			rc = request_irq(efct_os->msix_vec[i].vector,
					 efct_intr_msix,
					 0, EFCT_DRIVER_NAME,
					 &efct_os->intr_context[i]);
			if (rc)
				break;
		}
	} else {
		dev_err(&efct_os->pdev->dev,
			"%s : rc % d returned, IRQ allocation failed\n",
			   __func__, rc);
	}

	return rc;
}

/**
 * @brief tear down MSIX interrupts
 *
 * Previously setup MSIX interrupts are taken down
 *
 * @param efct_os pointer to efct_os structure
 *
 * @return none
 */

static void
efct_teardown_msix(struct efct_os_s *efct_os)
{
	u32 i;

	for (i = 0; i < efct_os->n_msix_vec; i++) {
		synchronize_irq(efct_os->msix_vec[i].vector);
		free_irq(efct_os->msix_vec[i].vector,
			 &efct_os->intr_context[i]);
	}
	pci_disable_msix(efct_os->pdev);
}

static struct pci_device_id efct_pci_table[] = {
	{PCI_DEVICE(EFCT_VENDOR_ID, EFCT_DEVICE_ID_LPE31004), 0},
	{PCI_DEVICE(EFCT_VENDOR_ID, EFCT_DEVICE_ID_G7), 0},
	{}	/* terminate list */
};

/**
 * @brief return pointer to efct structure given instance index
 *
 * A pointer to an efct structure is returned given an instance index.
 *
 * @param index index to efct_devices array
 *
 * @return efct pointer
 */

struct efct_s *efct_get_instance(u32 index)
{
	if (index < ARRAY_SIZE(efct_devices))
		return efct_devices[index];

	return NULL;
}

/**
 * @brief instantiate PCI device
 *
 * This is the PCI device probe entry point,
 * called by the Linux PCI subsystem once for
 * each matching device/function.
 *
 * The efct structure is allocated, and initiatlized
 *
 * @param pdev pointer PCI device structure
 * @param ent poitner to PCI device Id structure
 *
 * @return returns 0 for success, a negative error code value for failure.
 */

static int
efct_pci_probe(struct pci_dev *pdev, const struct pci_device_id *ent)
{
	struct efct_s		*efct = NULL;
	struct efct_os_s	*efct_os = NULL;
	int		rc;
	u32	i, r;
	int		num_interrupts = 0;
	int		nid;				/* Numa node id */
	struct task_struct	*thread = NULL;

	dev_info(&pdev->dev, "%s:%s\n", EFCT_DRIVER_NAME, efct_copyright);

	rc = pci_enable_device_mem(pdev);
	if (rc)
		goto efct_pci_probe_err_enable;

	pci_set_master(pdev);

	rc = pci_set_mwi(pdev);
	if (rc) {
		dev_info(&pdev->dev,
			 "pci_set_mwi returned %d\n", rc);
		goto efct_pci_probe_err_set_mwi;
	}

	rc = pci_request_regions(pdev, EFCT_DRIVER_NAME);
	if (rc) {
		dev_err(&pdev->dev, "pci_request_regions failed\n");
		goto efct_pci_probe_err_request_regions;
	}

	/* Fetch the Numa node id for this device */
	nid = dev_to_node(&pdev->dev);
	if (nid < 0) {
		dev_err(&pdev->dev,
			"Warning Numa node ID is %d\n", nid);
		nid = 0;
	}

	/* Allocate efct */
	efct = efct_device_alloc(nid);
	if (!efct) {
		dev_err(&pdev->dev, "Failed to allocate efct_t\n");
		rc = -ENOMEM;
		goto efct_pci_probe_err_efct_device_alloc;
	}

	efct_os = &efct->efct_os;

	efct_os->pdev = pdev;
	efct->pdev = pdev;

	if (efct->enable_numa_support)
		efct_os->numa_node = nid;

	efct->pci_vendor = efct->efct_os.pdev->vendor;
	efct->pci_device = efct->efct_os.pdev->device;
	efct->pci_subsystem_vendor = efct->efct_os.pdev->subsystem_vendor;
	efct->pci_subsystem_device = efct->efct_os.pdev->subsystem_device;
	strncpy(efct->businfo,
		dev_name(&efct->efct_os.pdev->dev), sizeof(efct->businfo));

	/* Map all memory BARs */
	for (i = 0, r = 0; i < EFCT_PCI_MAX_BAR; i++) {
		if (pci_resource_flags(pdev, i) & IORESOURCE_MEM) {
			efct_os->reg[r] = ioremap(pci_resource_start(pdev, i),
						  pci_resource_len(pdev, i));
			r++;
		}

		/*
		 * If the 64-bit attribute is set, both this BAR and the
		 * next form the complete address. Skip processing the
		 * next BAR.
		 */
		if (pci_resource_flags(pdev, i) & IORESOURCE_MEM_64)
			i++;
	}

	pci_set_drvdata(pdev, efct_os);

	if (pci_set_dma_mask(pdev, DMA_BIT_MASK(64)) != 0 ||
	    pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(64)) != 0) {
		dev_warn(&pdev->dev,
			 "trying DMA_BIT_MASK(32)\n");
		if (pci_set_dma_mask(pdev, DMA_BIT_MASK(32)) != 0 ||
		    pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(32)) != 0) {
			dev_err(&pdev->dev,
				"setting DMA_BIT_MASK failed\n");
			rc = -1;
			goto efct_pci_probe_err_setup_thread;
		}
	}

	num_interrupts = efct_device_interrupts_required(efct);
	if (num_interrupts < 0) {
		efct_log_err(efct, "efct_device_interrupts_required failed\n");
		rc = -1;
		goto efct_pci_probe_err_setup_thread;
	}

	/*
	 * Initialize MSIX interrupts, note,
	 * efct_setup_msix() enables the interrupt
	 */
	rc = efct_setup_msix(efct_os, num_interrupts);
	if (rc) {
		dev_err(&pdev->dev, "Can't setup msix\n");
		goto efct_pci_probe_err_setup_msix;
	}
	/* Disable interrupt for now */
	for (i = 0; i < efct->efct_os.n_msix_vec; i++) {
		efct_log_debug(efct, "irq %d disabled\n",
			       efct->efct_os.msix_vec[i].vector);
		disable_irq(efct_os->msix_vec[i].vector);
	}

	rc = efct_device_attach((struct efct_s *)efct_os);
	if (rc)
		goto efct_pci_probe_err_setup_msix;

	return 0;

efct_pci_probe_err_setup_msix:
	for (i = 0; i < (u32)num_interrupts; i++) {
		thread = efct->efct_os.intr_context[i].thread;
		if (!thread)
			continue;

		/* Call stop */
		kthread_stop(thread);
	}

efct_pci_probe_err_setup_thread:
	pci_set_drvdata(pdev, NULL);

	for (i = 0; i < EFCT_PCI_MAX_BAR; i++) {
		if (efct_os->reg[i])
			iounmap(efct_os->reg[i]);
	}
	efct_device_free((struct efct_s *)efct_os);
efct_pci_probe_err_efct_device_alloc:
	pci_release_regions(pdev);
efct_pci_probe_err_request_regions:
	pci_clear_mwi(pdev);
efct_pci_probe_err_set_mwi:
	pci_disable_device(pdev);
efct_pci_probe_err_enable:
	return rc;
}

/**
 * @brief remove PCI device instance
 *
 * Called when driver is unloaded, once for each PCI device/function instance.
 *
 * @param pdev pointer to PCI device structure
 *
 * @return none
 */

static void
efct_pci_remove(struct pci_dev *pdev)
{
	struct efct_s *efct = pci_get_drvdata(pdev);
	struct efct_os_s	*efct_os;
	u32	i;

	if (!efct)
		return;

	efct_os = &efct->efct_os;

	if (efct_os) {
		efct_device_detach((struct efct_s *)efct_os);

		efct_teardown_msix(efct_os);

		for (i = 0; i < EFCT_PCI_MAX_BAR; i++) {
			if (efct_os->reg[i])
				iounmap(efct_os->reg[i]);
		}

		pci_set_drvdata(pdev, NULL);

		efct_devices[efct->instance_index] = NULL;

		efct_device_free((struct efct_s *)efct_os);
	}

	pci_release_regions(pdev);

	pci_disable_device(pdev);
}

/**
 * efct_device_prep_dev_for_reset - Prepare EFCT device for pci slot reset
 * @phba: pointer to EFCT data structure.
 *
 * This routine is called to prepare the EFCT device for PCI slot reset. It
 * disables the device interrupt and pci device, and aborts the internal FCP
 * pending I/Os
 */
static void
efct_device_prep_for_reset(struct efct_s *efct, struct pci_dev *pdev)
{
	if (efct) {
		efct_log_debug(efct,
			       "PCI channel disable preparing for reset\n");
		efct_device_detach(efct);
		/* Disable interrupt and pci device */
		efct_teardown_msix(&efct->efct_os);
	}
	pci_disable_device(pdev);
}

/**
 * efct_device_prep_for_recover - Prepare EFCT device for pci slot recover
 * @phba: pointer to EFCT hba data structure.
 *
 * This routine is called to prepare the SLI4 device for PCI slot recover. It
 * aborts all the outstanding SCSI I/Os to the pci device.
 */
static void
efct_device_prep_for_recover(struct efct_s *efct)
{
	if (efct) {
		efct_log_debug(efct, "PCI channel preparing for recovery\n");
		efct_hw_io_abort_all(&efct->hw);
	}
}

/**
 * efct_pci_io_error_detected - method for handling PCI I/O error
 * @pdev: pointer to PCI device.
 * @state: the current PCI connection state.
 *
 * This routine is registered to the PCI subsystem for error handling. This
 * function is called by the PCI subsystem after a PCI bus error affecting
 * this device has been detected. When this routine is invoked, it dispatches
 * device error detected handling routine, which will perform the proper
 * error detected operation.
 *
 * Return codes
 * PCI_ERS_RESULT_NEED_RESET - need to reset before recovery
 * PCI_ERS_RESULT_DISCONNECT - device could not be recovered
 */
static pci_ers_result_t
efct_pci_io_error_detected(struct pci_dev *pdev, pci_channel_state_t state)
{
	struct efct_s *efct = pci_get_drvdata(pdev);
	struct efct_os_s        *efct_os;
	pci_ers_result_t rc;

	efct_os = &efct->efct_os;
	switch (state) {
	case pci_channel_io_normal:
		efct_device_prep_for_recover((struct efct_s *)efct_os);
		rc = PCI_ERS_RESULT_CAN_RECOVER;
		break;
	case pci_channel_io_frozen:
		efct_device_prep_for_reset((struct efct_s *)efct_os, pdev);
		rc = PCI_ERS_RESULT_NEED_RESET;
		break;
	case pci_channel_io_perm_failure:
		efct_device_detach((struct efct_s *)efct_os);
		rc = PCI_ERS_RESULT_DISCONNECT;
		break;
	default:
		efct_log_debug(efct, "Unknown PCI error state:0x%x\n",
			       state);
		efct_device_prep_for_reset((struct efct_s *)efct_os, pdev);
		rc = PCI_ERS_RESULT_NEED_RESET;
		break;
	}

	return rc;
}

static pci_ers_result_t
efct_pci_io_slot_reset(struct pci_dev *pdev)
{
	int rc;
	struct efct_s *efct = pci_get_drvdata(pdev);
	struct efct_os_s *efct_os = &efct->efct_os;

	rc = pci_enable_device_mem(pdev);
	if (rc) {
		efct_log_err(efct,
			     "failed to re-enable PCI device after reset.\n");
		return PCI_ERS_RESULT_DISCONNECT;
	}

	/*
	 * As the new kernel behavior of pci_restore_state() API call clears
	 * device saved_state flag, need to save the restored state again.
	 */

	pci_save_state(pdev);

	pci_set_master(pdev);

	rc = efct_setup_msix(efct_os, efct_os->n_msix_vec);
	if (rc) {
		efct_log_err(NULL,
			     "rc %d returned, IRQ allocation failed\n",
			    rc);
	}
	/* Perform device reset */
	efct_device_detach((struct efct_s *)efct_os);
	/* Bring device to online*/
	efct_device_attach((struct efct_s *)efct_os);

	return PCI_ERS_RESULT_RECOVERED;
}

static void
efct_pci_io_resume(struct pci_dev *pdev)
{
	struct efct_s *efct = pci_get_drvdata(pdev);
	struct efct_os_s *efct_os = &efct->efct_os;

	/* Perform device reset */
	efct_device_detach((struct efct_s *)efct_os);
	/* Bring device to online*/
	efct_device_attach((struct efct_s *)efct_os);
}

/**
 * @brief Start event processing
 *
 * Start up the threads for event processing
 *
 * @param efct_os pointer to EFCT OS structure
 *
 * @return returns 0 for success, a negative error code value for failure.
 */

int
efct_start_event_processing(struct efct_os_s *efct_os)
{
	struct efct_s *efct = (struct efct_s *)efct_os;
	u32 i;

	for (i = 0; i < efct_os->n_msix_vec; i++) {
		char label[32];
		struct efct_os_intr_context_s *intr_ctx = NULL;

		intr_ctx = &efct->efct_os.intr_context[i];

		intr_ctx->efct = efct;
		intr_ctx->index = i;

		sema_init(&intr_ctx->intsem, 0);

		snprintf(label, sizeof(label),
			 "efct:%d:%d", efct->instance_index, i);

		intr_ctx->thread =
			kthread_create((int(*)(void *)) efct_intr_thread,
				       intr_ctx, label);

		if (IS_ERR(intr_ctx->thread)) {
			efct_log_err(NULL, "kthread_create failed: %ld\n",
				     PTR_ERR(intr_ctx->thread));
			intr_ctx->thread = NULL;

			return -1;
		}

		wake_up_process(intr_ctx->thread);
	}

	return 0;
}

/**
 * @brief Stop event processing
 *
 * Interrupts are disabled, and any asynchronous thread (or tasklet) is stopped.
 *
 * @param efct_os pointer to EFCT OS structure
 *
 * @return none
 */

void
efct_stop_event_processing(struct efct_os_s *efct_os)
{
	u32 i;
	struct task_struct *thread = NULL;

	for (i = 0; i < efct_os->n_msix_vec; i++) {
		disable_irq(efct_os->msix_vec[i].vector);

		thread = efct_os->intr_context[i].thread;
		if (!thread)
			continue;

		/* Call stop */
		kthread_stop(thread);
	}
}

MODULE_DEVICE_TABLE(pci, efct_pci_table);

static struct pci_error_handlers efct_pci_err_handler = {
	.error_detected = efct_pci_io_error_detected,
	.slot_reset = efct_pci_io_slot_reset,
	.resume = efct_pci_io_resume,
};

static struct pci_driver efct_pci_driver = {
	.name		= EFCT_DRIVER_NAME,
	.id_table	= efct_pci_table,
	.probe		= efct_pci_probe,
	.remove		= efct_pci_remove,
	.err_handler	= &efct_pci_err_handler,
};

static int efct_proc_open(struct inode *indoe, struct file *file)
{
	return single_open(file, efct_proc_get, NULL);
}

static int efct_proc_get(struct seq_file *m, void *v)
{
	u32 i;
	u32 j;
	u32 device_count = 0;

	for (i = 0; i < ARRAY_SIZE(efct_devices); i++) {
		if (efct_devices[i])
			device_count++;
	}

	seq_printf(m, "%d\n", device_count);

	for (i = 0; i < ARRAY_SIZE(efct_devices); i++) {
		if (efct_devices[i]) {
			struct efct_s *efct = efct_devices[i];
			struct efct_os_s *efct_os = &efct->efct_os;

			for (j = 0; j < efct_os->n_msix_vec; j++) {
				seq_printf(m, "%d,%d,%d\n", i,
					   efct_os->msix_vec[j].vector,
					-1);
			}
		}
	}

	return 0;
}

/**
 * @brief driver load entry point
 *
 * Called when driver is loaded, device class EFCT_DRIVER_NAME is created, and
 * PCI devices are enumerated.
 *
 * @return returns 0 for success, a negative error code value for failure.
 */

static
int __init efct_init(void)
{
	int	rc = -ENODEV;

	rc = efct_device_init();
	if (rc) {
		pr_err("efct_device_init failed rc=%d\n", rc);
		return -ENOMEM;
	}

	/* pci_register_driver() will enumerate all of the PCI device/functions
	 * before returning.
	 */
	rc = pci_register_driver(&efct_pci_driver);
	if (rc)
		goto l1;

	proc_create(EFCT_DRIVER_NAME, 0444, NULL, &efct_proc_fops);
	return rc;

l1:
	efct_device_shutdown();
	return rc;
}

/**
 * @brief driver unload entry point
 *
 * Called when driver is unloaded.
 * PCI devices are removed, and device class objects
 * removed.
 *
 * @return none
 */

static void __exit efct_exit(void)
{
	pci_unregister_driver(&efct_pci_driver);
	remove_proc_entry(EFCT_DRIVER_NAME, NULL);
	efct_device_shutdown();
}

module_init(efct_init);
module_exit(efct_exit);
MODULE_VERSION(EFCT_DRIVER_VERSION);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Broadcom");
