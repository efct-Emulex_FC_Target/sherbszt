/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#include "efct_driver.h"
#include "efct_utils.h"
#include "efct_hw.h"
#include "efct_io.h"

/**
 * @brief IO pool.
 *
 * Structure encapsulating a pool of IO objects.
 *
 */

struct efct_io_pool_s {
	struct efct_s *efct;			/* Pointer to device object */
	spinlock_t lock;		/* IO pool lock */
	u32 io_num_ios;		/* Total IOs allocated */
	struct efct_pool_s *pool;
};

/**
 * @brief Create a pool of IO objects.
 *
 * @par Description
 * This function allocates memory in larger chucks called
 * "slabs" which are a fixed size. It calculates the number of IO objects that
 * fit within each "slab" and determines the number of "slabs" required to
 * allocate the number of IOs requested. Each of the slabs is allocated and
 * then it grabs each IO object within the slab and adds it to the free list.
 * Individual command, response and SGL DMA buffers are allocated for each IO.
 *
 *           "Slabs"
 *      +----------------+
 *      |                |
 *   +----------------+  |
 *   |    IO          |  |
 *   +----------------+  |
 *   |    ...         |  |
 *   +----------------+__+
 *   |    IO          |
 *   +----------------+
 *
 * @param efct Driver instance's software context.
 * @param num_io Number of IO contexts to allocate.
 * @param num_sgl Number of SGL entries to allocate for each IO.
 *
 * @return Returns a pointer to a new efct_io_pool_s on success,
 *         or NULL on failure.
 */

struct efct_io_pool_s *
efct_io_pool_create(struct efct_s *efct, u32 num_io, u32 num_sgl)
{
	u32 i = 0;
	struct efct_io_pool_s *io_pool;

	/* Allocate the IO pool */
	io_pool = kmalloc(sizeof(*io_pool), GFP_ATOMIC);
	if (!io_pool)
		return NULL;

	memset(io_pool, 0, sizeof(*io_pool));
	io_pool->efct = efct;
	io_pool->io_num_ios = num_io;

	/* initialize IO pool lock */
	spin_lock_init(&io_pool->lock);

	io_pool->pool = efct_pool_alloc(efct, sizeof(struct efct_io_s),
					io_pool->io_num_ios, false);

	for (i = 0; i < io_pool->io_num_ios; i++) {
		struct efct_io_s *io = efct_pool_get_instance(io_pool->pool, i);

		io->tag = i;
		io->instance_index = i;
		io->efct = efct;

		/* Allocate a response buffer */
		io->rspbuf.size = SCSI_RSP_BUF_LENGTH;
		io->rspbuf.virt = dma_alloc_coherent(&efct->pdev->dev,
						     io->rspbuf.size,
						     &io->rspbuf.phys, GFP_DMA);
		if (!io->rspbuf.virt) {
			efct_log_err(efct, "dma_alloc cmdbuf failed\n");
			efct_io_pool_free(io_pool);
			return NULL;
		}

		/* Allocate SGL */
		io->sgl = kzalloc(sizeof(*io->sgl) * num_sgl, GFP_ATOMIC);
		if (!io->sgl) {
			efct_io_pool_free(io_pool);
			return NULL;
		}

		memset(io->sgl, 0, sizeof(*io->sgl) * num_sgl);
		io->sgl_allocated = num_sgl;
		io->sgl_count = 0;

		/* Make IO backend call to initialize IO */
		efct_scsi_tgt_io_init(io);
	}

	return io_pool;
}

/**
 * @brief Free IO objects pool
 *
 * @par Description
 * The pool of IO objects are freed.
 *
 * @param io_pool Pointer to IO pool object.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int
efct_io_pool_free(struct efct_io_pool_s *io_pool)
{
	struct efct_s *efct;
	u32 i;
	struct efct_io_s *io;

	if (io_pool) {
		efct = io_pool->efct;

		for (i = 0; i < io_pool->io_num_ios; i++) {
			io = efct_pool_get_instance(io_pool->pool, i);
			if (!io)
				continue;

			efct_scsi_tgt_io_exit(io);
			kfree(io->sgl);
			dma_free_coherent(&efct->pdev->dev,
					  io->cmdbuf.size, io->cmdbuf.virt,
					  io->cmdbuf.phys);
			memset(&io->cmdbuf, 0, sizeof(struct efc_dma_s));
			dma_free_coherent(&efct->pdev->dev,
					  io->rspbuf.size, io->rspbuf.virt,
					  io->rspbuf.phys);
			memset(&io->rspbuf, 0, sizeof(struct efc_dma_s));
		}

		if (io_pool->pool)
			efct_pool_free(io_pool->pool);

		kfree(io_pool);
		efct->xport->io_pool = NULL;
	}

	return 0;
}

u32 efct_io_pool_allocated(struct efct_io_pool_s *io_pool)
{
	return io_pool->io_num_ios;
}

/**
 * @ingroup io_alloc
 * @brief Allocate an object used to track an IO.
 *
 * @param io_pool Pointer to the IO pool.
 *
 * @return Returns the pointer to a new object, or NULL if none available.
 */
struct efct_io_s *
efct_io_pool_io_alloc(struct efct_io_pool_s *io_pool)
{
	struct efct_io_s *io = NULL;
	struct efct_s *efct;
	unsigned long flags = 0;

	efct_assert(io_pool, NULL);

	efct = io_pool->efct;

	spin_lock_irqsave(&io_pool->lock, flags);
	io = efct_pool_get(io_pool->pool);
	if (io) {
		spin_unlock_irqrestore(&io_pool->lock, flags);

		io->io_type = EFCT_IO_TYPE_MAX;
		io->hio_type = EFCT_HW_IO_MAX;
		io->hio = NULL;
		io->transferred = 0;
		io->efct = efct;
		io->timeout = 0;
		io->sgl_count = 0;
		io->tgt_task_tag = 0;
		io->init_task_tag = 0;
		io->hw_tag = 0;
		io->display_name = "pending";
		io->seq_init = 0;
		io->els_req_free = false;
		io->io_free = 0;
		io->release = NULL;
		atomic_add_return(1, &efct->xport->io_active_count);
		atomic_add_return(1, &efct->xport->io_total_alloc);
	} else {
		spin_unlock_irqrestore(&io_pool->lock, flags);
	}
	return io;
}

/**
 * @ingroup io_alloc
 * @brief Free an object used to track an IO.
 *
 * @param io_pool Pointer to IO pool object.
 * @param io Pointer to the IO object.
 */
void
efct_io_pool_io_free(struct efct_io_pool_s *io_pool, struct efct_io_s *io)
{
	struct efct_s *efct;
	struct efct_hw_io_s *hio = NULL;
	unsigned long flags = 0;

	efct_assert(io_pool);

	efct = io_pool->efct;

	spin_lock_irqsave(&io_pool->lock, flags);
	hio = io->hio;
	io->hio = NULL;
	io->io_free = 1;
	efct_pool_put_head(io_pool->pool, io);
	spin_unlock_irqrestore(&io_pool->lock, flags);

	if (hio)
		efct_hw_io_free(&efct->hw, hio);

	atomic_sub_return(1, &efct->xport->io_active_count);
	atomic_add_return(1, &efct->xport->io_total_free);
}

/**
 * @ingroup io_alloc
 * @brief Find an I/O given it's node and ox_id.
 *
 * @param efct Driver instance's software context.
 * @param node Pointer to node.
 * @param ox_id OX_ID to find.
 * @param rx_id RX_ID to find (0xffff for unassigned).
 */
struct efct_io_s *
efct_io_find_tgt_io(struct efct_s *efct, struct efc_node_s *node,
		    u16 ox_id, u16 rx_id)
{
	struct efct_io_s	*io = NULL;
	unsigned long flags = 0;

	spin_lock_irqsave(&node->active_ios_lock, flags);
	list_for_each_entry(io, &node->active_ios, list_entry) {
		if ((io->cmd_tgt && io->init_task_tag == ox_id) &&
		    (rx_id == 0xffff || io->tgt_task_tag == rx_id)) {
			break;
		}
	}
	spin_unlock_irqrestore(&node->active_ios_lock, flags);
	return io;
}

/**
 * @ingroup io_alloc
 * @brief Return IO context given the instance index.
 *
 * @par Description
 * Returns a pointer to the IO context given by the instance index.
 *
 * @param efct Pointer to driver structure.
 * @param index IO instance index to return.
 *
 * @return Returns a pointer to the IO context, or NULL if not found.
 */
struct efct_io_s *
efct_io_get_instance(struct efct_s *efct, u32 index)
{
	struct efct_xport_s *xport = efct->xport;
	struct efct_io_pool_s *io_pool = xport->io_pool;

	return efct_pool_get_instance(io_pool->pool, index);
}
