/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/*
 * All common SLI-4 structures and function prototypes.
 */

#ifndef _SLI4_H
#define _SLI4_H

#include "scsi/fc/fc_els.h"
#include "scsi/fc/fc_fs.h"
#include "scsi/fc/fc_ns.h"

#define SLI_PAGE_SIZE		BIT(12)	/* 4096 */
#define SLI_SUB_PAGE_MASK	(SLI_PAGE_SIZE - 1)
#define SLI_ROUND_PAGE(b)	(((b) + SLI_SUB_PAGE_MASK) & ~SLI_SUB_PAGE_MASK)

#define SLI4_BMBX_TIMEOUT_MSEC		30000
#define SLI4_FW_READY_TIMEOUT_MSEC	30000

static inline u32
sli_page_count(size_t bytes, u32 page_size)
{
	u32	mask = page_size - 1;
	u32	shift = 0;

	switch (page_size) {
	case 4096:
		shift = 12;
		break;
	case 8192:
		shift = 13;
		break;
	case 16384:
		shift = 14;
		break;
	case 32768:
		shift = 15;
		break;
	case 65536:
		shift = 16;
		break;
	default:
		return 0;
	}

	return (bytes + mask) >> shift;
}

/*************************************************************************
 * Common SLI-4 register offsets and field definitions
 */

/* SLI_INTF - SLI Interface Definition Register */
#define SLI4_INTF_REGOFFSET			0x0058
enum {
	SLI4_INTF_SLI_REV_SHIFT = 4,
	SLI4_INTF_SLI_REV_MASK = 0x000000F0,

	SLI4_INTF_SLI_REV_S3 = 3 << SLI4_INTF_SLI_REV_SHIFT,
	SLI4_INTF_SLI_REV_S4 = 4 << SLI4_INTF_SLI_REV_SHIFT,

	SLI4_INTF_SLI_FAMILY_SHIFT = 8,
	SLI4_INTF_SLI_FAMILY_MASK  = 0x00000F00,

	SLI4_FAMILY_CHECK_ASIC_TYPE = 0xf << SLI4_INTF_SLI_FAMILY_SHIFT,

	SLI4_INTF_IF_TYPE_SHIFT = 12,
	SLI4_INTF_IF_TYPE_MASK = 0x0000F000,

	SLI4_INTF_IF_TYPE_2 = 2 << SLI4_INTF_IF_TYPE_SHIFT,
	SLI4_INTF_IF_TYPE_6 = 6 << SLI4_INTF_IF_TYPE_SHIFT,

	SLI4_INTF_VALID_SHIFT = 29,
	SLI4_INTF_VALID_MASK = 0xF0000000,

	SLI4_INTF_VALID_VALUE = 6 << SLI4_INTF_VALID_SHIFT,
};

/* ASIC_ID - SLI ASIC Type and Revision Register */
#define SLI4_ASIC_ID_REGOFFSET			0x009c
enum {
	SLI4_ASIC_GEN_SHIFT = 8,
	SLI4_ASIC_GEN_MASK = 0x0000FF00,
	SLI4_ASIC_GEN_5 = 0x0b << SLI4_ASIC_GEN_SHIFT,
	SLI4_ASIC_GEN_6 = 0x0c << SLI4_ASIC_GEN_SHIFT,
	SLI4_ASIC_GEN_7 = 0x0d << SLI4_ASIC_GEN_SHIFT,
};

/* Define macros for 64 bit support */
//#define addr32_lo(addr)    ((u32)(0xFFFFFFFF & (u64)(addr)))
//#define upper_32_bits(addr)   ((u32)(0xFFFFFFFF & (((u64)(addr)) >> 32)))

/* BMBX - Bootstrap Mailbox Register */
#define SLI4_BMBX_REGOFFSET			0x0160
#define SLI4_BMBX_MASK_HI			0x3
#define SLI4_BMBX_MASK_LO			0xf
#define SLI4_BMBX_RDY				BIT(0)
#define SLI4_BMBX_HI				BIT(1)
#define SLI4_BMBX_WRITE_HI(r)	((upper_32_bits(r) & ~SLI4_BMBX_MASK_HI) | \
					SLI4_BMBX_HI)
#define SLI4_BMBX_WRITE_LO(r)	(((upper_32_bits(r) & SLI4_BMBX_MASK_HI) \
				<< 30) | (((r) & ~SLI4_BMBX_MASK_LO) >> 2))

#define SLI4_BMBX_SIZE			256

/* SLIPORT_CONTROL - SLI Port Control Register */
#define SLI4_SLIPORT_CONTROL_REGOFFSET		0x0408
#define SLI4_SLIPORT_CONTROL_IP			BIT(27)
#define SLI4_SLIPORT_CONTROL_IDIS		BIT(22)
#define SLI4_SLIPORT_CONTROL_FDD		BIT(31)

/* SLI4_SLIPORT_ERROR - SLI Port Error Register */
#define SLI4_SLIPORT_ERROR1		0x040c
#define SLI4_SLIPORT_ERROR2		0x0410

/* EQCQ_DOORBELL - EQ and CQ Doorbell Register */
#define SLI4_EQCQ_DOORBELL_REGOFFSET		0x120
enum {
	SLI4_EQ_ID_LO_MASK = 0x01FF,

	SLI4_CQ_ID_LO_MASK = 0x03FF,

	SLI4_EQ_CI_SHIFT = 9,
	SLI4_EQ_CI_MASK = 0x0200,

	SLI4_EQCQ_QT_SHIFT = 10,
	SLI4_EQCQ_QT_MASK = 0x0400,

	SLI4_EQCQ_ID_HI_SHIFT = 11,
	SLI4_EQCQ_ID_HI_MASK = 0xF800,

	SLI4_EQCQ_NUM_POST_SHIFT = 16,
	SLI4_EQCQ_NUM_POST_MASK = 0x1FFF0000,

	SLI4_EQCQ_ARM_SHIFT = 29,
	SLI4_EQCQ_ARM_MASK = 0x20000000,

};

#define SLI4_EQ_DOORBELL(n, id, a)\
	((id & SLI4_EQ_ID_LO_MASK) | \
	((1 << SLI4_EQCQ_QT_SHIFT) & SLI4_EQCQ_QT_MASK) |\
	(((id >> 9) << SLI4_EQCQ_ID_HI_SHIFT) & SLI4_EQCQ_ID_HI_MASK) | \
	((n << SLI4_EQCQ_NUM_POST_SHIFT) & SLI4_EQCQ_NUM_POST_MASK) | \
	((a << SLI4_EQCQ_ARM_SHIFT) & SLI4_EQCQ_ARM_MASK) | \
	((1 << SLI4_EQ_CI_SHIFT) & SLI4_EQ_CI_MASK))

#define SLI4_CQ_DOORBELL(n, id, a)\
	((id & SLI4_CQ_ID_LO_MASK) | \
	((0 << SLI4_EQCQ_QT_SHIFT) & SLI4_EQCQ_QT_MASK) |\
	(((id >> 10) << SLI4_EQCQ_ID_HI_SHIFT) & SLI4_EQCQ_ID_HI_MASK) | \
	((n << SLI4_EQCQ_NUM_POST_SHIFT) & SLI4_EQCQ_NUM_POST_MASK) | \
	((a << SLI4_EQCQ_ARM_SHIFT) & SLI4_EQCQ_ARM_MASK))

/* EQ_DOORBELL - EQ Doorbell Register for IF_TYPE = 6*/
#define SLI4_IF6_EQ_DOORBELL_REGOFFSET		0x120
enum {
	SLI4_IF6_EQ_ID_MASK = 0x0FFF,

	SLI4_IF6_EQ_NUM_POST_SHIFT = 16,
	SLI4_IF6_EQ_NUM_POST_MASK = 0x1FFF0000,

	SLI4_IF6_EQ_ARM_SHIFT = 29,
	SLI4_IF6_EQ_ARM_MASK = 0x20000000,
};

#define SLI4_IF6_EQ_DOORBELL(n, id, a)\
	((id & SLI4_IF6_EQ_ID_MASK) | \
	((n << SLI4_IF6_EQ_NUM_POST_SHIFT) & SLI4_IF6_EQ_NUM_POST_MASK) | \
	((a << SLI4_IF6_EQ_ARM_SHIFT) & SLI4_IF6_EQ_ARM_MASK))

/* CQ_DOORBELL - CQ Doorbell Register for IF_TYPE = 6*/
#define SLI4_IF6_CQ_DOORBELL_REGOFFSET		0xC0
enum {
	SLI4_IF6_CQ_ID_MASK = 0xFFFF,

	SLI4_IF6_CQ_NUM_POST_SHIFT = 16,
	SLI4_IF6_CQ_NUM_POST_MASK = 0x1FFF0000,

	SLI4_IF6_CQ_ARM_SHIFT = 29,
	SLI4_IF6_CQ_ARM_MASK = 0x20000000,
};

#define SLI4_IF6_CQ_DOORBELL(n, id, a)\
	((id & SLI4_IF6_CQ_ID_MASK) | \
	((n << SLI4_IF6_CQ_NUM_POST_SHIFT) & SLI4_IF6_CQ_NUM_POST_MASK) | \
	((a << SLI4_IF6_CQ_ARM_SHIFT) & SLI4_IF6_CQ_ARM_MASK))

/**
 * @brief MQ_DOORBELL - MQ Doorbell Register
 */
#define SLI4_MQ_DOORBELL_REGOFFSET		0x0140	/* register offset */
#define SLI4_IF6_MQ_DOORBELL_REGOFFSET	0x0160	/* if_type = 6*/
enum {
	SLI4_MQ_DOORBELL_ID_MASK = 0xFFFF,

	SLI4_MQ_DOORBELL_NUM_SHIFT = 16,
	SLI4_MQ_DOORBELL_NUM_MASK = 0x3FFF0000,
};

#define SLI4_MQ_DOORBELL(n, i)\
	((i & SLI4_MQ_DOORBELL_ID_MASK) | \
	((n << SLI4_MQ_DOORBELL_NUM_SHIFT) & SLI4_MQ_DOORBELL_NUM_MASK))

/**
 * @brief RQ_DOORBELL - RQ Doorbell Register
 */
#define SLI4_RQ_DOORBELL_REGOFFSET		0x0a0	/* register offset */
#define SLI4_IF6_RQ_DOORBELL_REGOFFSET	0x0080	/* if_type = 6 */
enum {
	SLI4_RQ_DOORBELL_ID_MASK = 0xFFFF,

	SLI4_RQ_DOORBELL_NUM_SHIFT = 16,
	SLI4_RQ_DOORBELL_NUM_MASK = 0x3FFF0000,
};

#define SLI4_RQ_DOORBELL(n, i)\
	((i & SLI4_RQ_DOORBELL_ID_MASK) | \
	((n << SLI4_RQ_DOORBELL_NUM_SHIFT) & SLI4_RQ_DOORBELL_NUM_MASK))

/**
 * @brief WQ_DOORBELL - WQ Doorbell Register
 */
#define SLI4_IO_WQ_DOORBELL_REGOFFSET		0x040	/* register offset */
#define SLI4_IF6_WQ_DOORBELL_REGOFFSET	0x040	/* if_type = 6 */
enum {
	SLI4_WQ_DOORBELL_ID_MASK = 0xFFFF,

	SLI4_WQ_DOORBELL_IDX_SHIFT = 16,
	SLI4_WQ_DOORBELL_IDX_MASK = 0x00FF0000,

	SLI4_WQ_DOORBELL_NUM_SHIFT = 24,
	SLI4_WQ_DOORBELL_NUM_MASK = 0xFF000000,
};

#define SLI4_WQ_DOORBELL(n, x, i)\
	((i & SLI4_WQ_DOORBELL_ID_MASK) | \
	((x << SLI4_WQ_DOORBELL_IDX_SHIFT) & SLI4_WQ_DOORBELL_IDX_MASK) | \
	((n << SLI4_WQ_DOORBELL_NUM_SHIFT) & SLI4_WQ_DOORBELL_NUM_MASK))

/**
 * @brief SLIPORT_SEMAPHORE - SLI Port Host and Port Status Register
 */
#define SLI4_PORT_SEMAPHORE_REGOFFSET		0x0400	/* Type 2 + 3 + 6*/
enum {
	SLI4_PORT_SEMAPHORE_ERR_MASK = 0xF000,
	SLI4_PORT_SEMAPHORE_UNRECOV_ERR = 0xF000,
};

/**
 * @brief SLIPORT_STATUS - SLI Port Status Register
 */

				/* register offset Interface */
#define SLI4_PORT_STATUS_REGOFFSET	0x0404	/* Type 2 + 3 + 6*/
#define SLI4_PORT_STATUS_FDP	BIT(21)	/** func specific dump present */
#define SLI4_PORT_STATUS_RDY	BIT(23)	/** ready */
#define SLI4_PORT_STATUS_RN	BIT(24)	/** reset needed */
#define SLI4_PORT_STATUS_DIP	BIT(25)	/** dump present */
#define SLI4_PORT_STATUS_OTI	BIT(29) /** over temp indicator */
#define SLI4_PORT_STATUS_END	BIT(30)	/** endianness */
#define SLI4_PORT_STATUS_ERR	BIT(31)	/** SLI port error */

				/* register offset Interface */
#define SLI4_PHYDEV_CONTROL_REGOFFSET		0x0414	/* Type 2 + 3 + 6 */
#define SLI4_PHYDEV_CONTROL_FRST	BIT(1)	/** firmware reset */
#define SLI4_PHYDEV_CONTROL_DD		BIT(2)	/** diagnostic dump */
/*************************************************************************
 * SLI-4 mailbox command formats and definitions
 */

struct sli4_mbox_command_header_s {
	u8	resvd0;
	u8	command;
	__le16	status;	/* Port writes to indicate success/fail */
};

#define SLI4_MBOX_COMMAND_CONFIG_LINK	0x07
#define SLI4_MBOX_COMMAND_DUMP		0x17
#define SLI4_MBOX_COMMAND_DOWN_LINK	0x06
#define SLI4_MBOX_COMMAND_INIT_LINK	0x05
#define SLI4_MBOX_COMMAND_INIT_VFI	0xa3
#define SLI4_MBOX_COMMAND_INIT_VPI	0xa4
#define SLI4_MBOX_COMMAND_POST_XRI	0xa7
#define SLI4_MBOX_COMMAND_RELEASE_XRI	0xac
#define SLI4_MBOX_COMMAND_READ_CONFIG	0x0b
#define SLI4_MBOX_COMMAND_READ_STATUS	0x0e
#define SLI4_MBOX_COMMAND_READ_NVPARMS	0x02
#define SLI4_MBOX_COMMAND_READ_REV	0x11
#define SLI4_MBOX_COMMAND_READ_LNK_STAT	0x12
#define SLI4_MBOX_COMMAND_READ_SPARM64	0x8d
#define SLI4_MBOX_COMMAND_READ_TOPOLOGY	0x95
#define SLI4_MBOX_COMMAND_REG_FCFI	0xa0
#define SLI4_MBOX_COMMAND_REG_FCFI_MRQ	0xaf
#define SLI4_MBOX_COMMAND_REG_RPI	0x93
#define SLI4_MBOX_COMMAND_REG_RX_RQ	0xa6
#define SLI4_MBOX_COMMAND_REG_VFI	0x9f
#define SLI4_MBOX_COMMAND_REG_VPI	0x96
#define SLI4_MBOX_COMMAND_REQUEST_FEATURES 0x9d
#define SLI4_MBOX_COMMAND_SLI_CONFIG	0x9b
#define SLI4_MBOX_COMMAND_UNREG_FCFI	0xa2
#define SLI4_MBOX_COMMAND_UNREG_RPI	0x14
#define SLI4_MBOX_COMMAND_UNREG_VFI	0xa1
#define SLI4_MBOX_COMMAND_UNREG_VPI	0x97
#define SLI4_MBOX_COMMAND_WRITE_NVPARMS	0x03
#define SLI4_MBOX_COMMAND_CONFIG_AUTO_XFER_RDY	0xAD

#define SLI4_MBOX_STATUS_SUCCESS	0x0000
#define SLI4_MBOX_STATUS_FAILURE	0x0001
#define SLI4_MBOX_STATUS_RPI_NOT_REG	0x1400

/**
 * @brief Buffer Descriptor Entry (BDE)
 */
enum {
	SLI4_BDE_MASK_BUFFER_LEN	= 0xffffff,
	SLI4_BDE_MASK_BDE_TYPE		= 0xff000000,
};

struct sli4_bde_s {
	__le32		bde_type_buflen;
	union {
		struct {
			__le32	buffer_address_low;
			__le32	buffer_address_high;
		} data;
		struct {
			__le32	offset;
			__le32	rsvd2;
		} imm;
		struct {
			__le32	sgl_segment_address_low;
			__le32	sgl_segment_address_high;
		} blp;
	} u;
};

#define SLI4_BDE_TYPE_BDE_64		0x00	/** Generic 64-bit data */
#define SLI4_BDE_TYPE_BDE_IMM		0x01	/** Immediate data */
#define SLI4_BDE_TYPE_BLP		0x40	/** Buffer List Pointer */

/**
 * @brief Scatter-Gather Entry (SGE)
 */
enum {
	SLI4_SGE_DATA_OFFSET		= 0x07FFFFFF,	/* DW2 */
	SLI4_SGE_TYPE			= 0x78000000,
	SLI4_SGE_LAST			= 0x80000000,
};

struct sli4_sge_s {
	__le32		buffer_address_high;
	__le32		buffer_address_low;
	__le32		dw2_flags;
	__le32		buffer_length;
};

/**
 * @brief T10 DIF Scatter-Gather Entry (SGE)
 */
struct sli4_dif_sge_s {
	__le32		buffer_address_high;
	__le32		buffer_address_low;
	__le32		dw2_flags;
	__le32		rsvd12;
};

/**
 * @brief T10 DIF Seed Scatter-Gather Entry (SGE)
 */
enum {
	SLI4_DISEED_SGE_HS		= (1 << 2),	/* DW2W1 */
	SLI4_DISEED_SGE_WS		= (1 << 3),
	SLI4_DISEED_SGE_IC		= (1 << 4),
	SLI4_DISEED_SGE_ICS		= (1 << 5),
	SLI4_DISEED_SGE_ATRT		= (1 << 6),
	SLI4_DISEED_SGE_AT		= (1 << 7),
	SLI4_DISEED_SGE_FWDAPPTAG	= (1 << 8),
	SLI4_DISEED_SGE_REPLAPPTAG	= (1 << 9),
	SLI4_DISEED_SGE_HEADINSERT	= (1 << 10),
	SLI4_DISEED_SGE_SGETYPE		= (0xf << 11),
	SLI4_DISEED_SGE_LAST		= (1 << 31),

	SLI4_DISEED_SGE_DIFBLKSIZE	= (0x7 << 0),	/* DW3W1 */
	SLI4_DISEED_SGE_AUTOINCRREFTAG	= (1 << 3),
	SLI4_DISEED_SGE_CHKAPPTAG	= (1 << 4),
	SLI4_DISEED_SGE_CHKREFTAG	= (1 << 5),
	SLI4_DISEED_SGE_CHKCRC		= (1 << 6),
	SLI4_DISEED_SGE_NEWREFTAG	= (1 << 7),
	SLI4_DISEED_SGE_DIFOPRX		= (0xf << 8),
	SLI4_DISEED_SGE_DIFOPTX		= (0xf << 12),
};

struct sli4_diseed_sge_s {
	__le32		ref_tag_cmp;
	__le32		ref_tag_repl;
	__le16		app_tag_repl;
	__le16		dw2w1_flags;
	__le16		app_tag_cmp;
	__le16		dw3w1_flags;
};

/**
 * @brief List Segment Pointer Scatter-Gather Entry (SGE)
 */
enum {
	SLI4_LSP_SGE_TYPE	= (0xf << 27),		/* DW2 */
	SLI4_LSP_SGE_LAST	= (1 << 31),

	SLI4_LSP_SGE_SEGLEN	= 0xffffff,		/* DW3 */
};

struct sli4_lsp_sge_s {
	__le32		buffer_address_high;
	__le32		buffer_address_low;
	__le32		dw2_flags;
	__le32		dw3_seglen;
};

#define SLI4_SGE_MAX_RESERVED			3

#define SLI4_SGE_DIF_OP_IN_NODIF_OUT_CRC     0x00
#define SLI4_SGE_DIF_OP_IN_CRC_OUT_NODIF     0x01
#define SLI4_SGE_DIF_OP_IN_NODIF_OUT_CHKSUM  0x02
#define SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_NODIF  0x03
#define SLI4_SGE_DIF_OP_IN_CRC_OUT_CRC       0x04
#define SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_CHKSUM 0x05
#define SLI4_SGE_DIF_OP_IN_CRC_OUT_CHKSUM    0x06
#define SLI4_SGE_DIF_OP_IN_CHKSUM_OUT_CRC    0x07
#define SLI4_SGE_DIF_OP_IN_RAW_OUT_RAW       0x08

#define SLI4_SGE_TYPE_DATA	0x00
#define SLI4_SGE_TYPE_DIF	0x04	/** Data Integrity Field */
#define SLI4_SGE_TYPE_LSP	0x05	/** List Segment Pointer */
#define SLI4_SGE_TYPE_PEDIF	0x06	/** Post Encryption Engine DIF */
#define SLI4_SGE_TYPE_PESEED	0x07	/** Post Encryption Engine DIF Seed */
#define SLI4_SGE_TYPE_DISEED	0x08	/** DIF Seed */
#define SLI4_SGE_TYPE_ENC	0x09	/** Encryption */
#define SLI4_SGE_TYPE_ATM	0x0a	/** DIF Application Tag Mask */
#define SLI4_SGE_TYPE_SKIP	0x0c	/** SKIP */

/**
 * @brief CONFIG_LINK
 */
enum {
	SLI4_CFG_LINK_BBSCN = 0xf00,
	SLI4_CFG_LINK_CSCN  = 0x1000,
};

struct sli4_cmd_config_link_s {
	struct sli4_mbox_command_header_s	hdr;
	u8		maxbbc;		/* Max buffer-to-buffer credit */
	u8		rsvd5;
	u8		rsvd6;
	u8		rsvd7;
	u8		alpa;
	__le16		n_port_id;
	u8		rsvd11;
	__le32		rsvd12;
	__le32		e_d_tov;
	__le32		lp_tov;
	__le32		r_a_tov;
	__le32		r_t_tov;
	__le32		al_tov;
	__le32		rsvd36;
	/*
	 * Buffer-to-buffer state change number
	 * Configure BBSCN
	 */
	__le32		bbscn_dword;
};

/**
 * @brief DUMP Type 4
 */
enum {
	SLI4_DUMP4_TYPE = 0xf,
};

#define SLI4_WKI_TAG_SAT_TEM 0x1040

struct sli4_cmd_dump4_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		type_dword;
	__le16		wki_selection;
	__le16		rsvd10;
	__le32		rsvd12;
	__le32		returned_byte_cnt;
	__le32		resp_data[59];
};

/* INIT_LINK - initialize the link for a FC port */
#define FC_TOPOLOGY_FCAL	0
#define FC_TOPOLOGY_P2P		1

#define SLI4_INIT_LINK_F_LOOP_BACK	BIT(0)
#define SLI4_INIT_LINK_F_UNFAIR		BIT(6)
#define SLI4_INIT_LINK_F_NO_LIRP	BIT(7)
#define SLI4_INIT_LINK_F_LOOP_VALID_CHK	BIT(8)
#define SLI4_INIT_LINK_F_NO_LISA	BIT(9)
#define SLI4_INIT_LINK_F_FAIL_OVER	BIT(10)
#define SLI4_INIT_LINK_F_NO_AUTOSPEED	BIT(11)
#define SLI4_INIT_LINK_F_PICK_HI_ALPA	BIT(15)

#define SLI4_INIT_LINK_F_P2P_ONLY	1
#define SLI4_INIT_LINK_F_FCAL_ONLY	2

#define SLI4_INIT_LINK_F_FCAL_FAIL_OVER	0
#define SLI4_INIT_LINK_F_P2P_FAIL_OVER	1

enum {
	SLI4_INIT_LINK_SEL_RESET_AL_PA = 0xff,
	SLI4_INIT_LINK_FLAG_LOOPBACK = 0x1,
	SLI4_INIT_LINK_FLAG_TOPOLOGY = 0x6,
	SLI4_INIT_LINK_FLAG_UNFAIR   = 0x40,
	SLI4_INIT_LINK_FLAG_SKIP_LIRP_LILP = 0x80,
	SLI4_INIT_LINK_FLAG_LOOP_VALIDITY = 0x100,
	SLI4_INIT_LINK_FLAG_SKIP_LISA = 0x200,
	SLI4_INIT_LINK_FLAG_EN_TOPO_FAILOVER = 0x400,
	SLI4_INIT_LINK_FLAG_FIXED_SPEED = 0x800,
	SLI4_INIT_LINK_FLAG_SEL_HIGHTEST_AL_PA = 0x8000,
};

struct sli4_cmd_init_link_s {
	struct sli4_mbox_command_header_s       hdr;
	__le32	sel_reset_al_pa_dword;
	__le32	flags0;
	__le32	link_speed_sel_code;
#define FC_LINK_SPEED_1G		1
#define FC_LINK_SPEED_2G		2
#define FC_LINK_SPEED_AUTO_1_2		3
#define FC_LINK_SPEED_4G		4
#define FC_LINK_SPEED_AUTO_4_1		5
#define FC_LINK_SPEED_AUTO_4_2		6
#define FC_LINK_SPEED_AUTO_4_2_1	7
#define FC_LINK_SPEED_8G		8
#define FC_LINK_SPEED_AUTO_8_1		9
#define FC_LINK_SPEED_AUTO_8_2		10
#define FC_LINK_SPEED_AUTO_8_2_1	11
#define FC_LINK_SPEED_AUTO_8_4		12
#define FC_LINK_SPEED_AUTO_8_4_1	13
#define FC_LINK_SPEED_AUTO_8_4_2	14
#define FC_LINK_SPEED_10G		16
#define FC_LINK_SPEED_16G		17
#define FC_LINK_SPEED_AUTO_16_8_4	18
#define FC_LINK_SPEED_AUTO_16_8		19
#define FC_LINK_SPEED_32G		20
#define FC_LINK_SPEED_AUTO_32_16_8	21
#define FC_LINK_SPEED_AUTO_32_16	22
};

/**
 * @brief INIT_VFI - initialize the VFI resource
 */
enum {
	SLI4_INIT_VFI_FLAG_VP = 0x1000,		/* DW1W1 */
	SLI4_INIT_VFI_FLAG_VF = 0x2000,
	SLI4_INIT_VFI_FLAG_VT = 0x4000,
	SLI4_INIT_VFI_FLAG_VR = 0x8000,

	SLI4_INIT_VFI_VFID	 = 0x1fff,	/* DW3W0 */
	SLI4_INIT_VFI_PRI	 = 0xe000,

	SLI4_INIT_VFI_HOP_COUNT = 0xff000000,	/* DW4 */
};

struct sli4_cmd_init_vfi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		vfi;
	__le16		flags0_word;
	__le16		fcfi;
	__le16		vpi;
	__le32		vf_id_pri_dword;
	__le32		hop_cnt_dword;
};

/**
 * @brief INIT_VPI - initialize the VPI resource
 */
struct sli4_cmd_init_vpi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		vpi;
	__le16		vfi;
};

/**
 * @brief POST_XRI - post XRI resources to the SLI Port
 */
enum {
	SLI4_POST_XRI_COUNT	= 0xfff,	/* DW1W1 */
	SLI4_POST_XRI_FLAG_ENX	= 0x1000,
	SLI4_POST_XRI_FLAG_DL	= 0x2000,
	SLI4_POST_XRI_FLAG_DI	= 0x4000,
	SLI4_POST_XRI_FLAG_VAL	= 0x8000,
};

struct sli4_cmd_post_xri_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		xri_base;
	__le16		xri_count_flags;
};

/**
 * @brief RELEASE_XRI - Release XRI resources from the SLI Port
 */
enum {
	SLI4_RELEASE_XRI_REL_XRI_CNT	= 0x1f,	/* DW1W0 */
	SLI4_RELEASE_XRI_COUNT		= 0x1f,	/* DW1W1 */
};

struct sli4_cmd_release_xri_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		rel_xri_count_word;
	__le16		xri_count_word;

	struct {
		__le16	xri_tag0;
		__le16	xri_tag1;
	} xri_tbl[62];
};

/**
 * @brief READ_CONFIG - read SLI port configuration parameters
 */
struct sli4_cmd_read_config_s {
	struct sli4_mbox_command_header_s	hdr;
};

enum {
	SLI4_READ_CFG_RESP_RESOURCE_EXT = 0x80000000,	/* DW1 */
	SLI4_READ_CFG_RESP_TOPOLOGY = 0xff000000,	/* DW2 */
};

struct sli4_res_read_config_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		ext_dword;
	__le32		topology_dword;
	__le32		resvd8;
	__le16		e_d_tov;
	__le16		resvd14;
	__le32		resvd16;
	__le16		r_a_tov;
	__le16		resvd22;
	__le32		resvd24;
	__le32		resvd28;
	__le16		lmt;
	__le16		resvd34;
	__le32		resvd36;
	__le32		resvd40;
	__le16		xri_base;
	__le16		xri_count;
	__le16		rpi_base;
	__le16		rpi_count;
	__le16		vpi_base;
	__le16		vpi_count;
	__le16		vfi_base;
	__le16		vfi_count;
	__le16		resvd60;
	__le16		fcfi_count;
	__le16		rq_count;
	__le16		eq_count;
	__le16		wq_count;
	__le16		cq_count;
	__le32		pad[45];
};

#define SLI4_READ_CFG_TOPO_FC		0x1	/** FC topology unknown */
#define SLI4_READ_CFG_TOPO_FC_DA	0x2 /* FC Direct Attach (non FC-AL) */
#define SLI4_READ_CFG_TOPO_FC_AL	0x3	/** FC-AL topology */

/**
 * @brief READ_NVPARMS - read SLI port configuration parameters
 */

enum {
	SLI4_READ_NVPARAMS_HARD_ALPA	  = 0xff,
	SLI4_READ_NVPARAMS_PREFERRED_D_ID = 0xffffff00,
};

struct sli4_cmd_read_nvparms_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		resvd0;
	__le32		resvd4;
	__le32		resvd8;
	__le32		resvd12;
	u8		wwpn[8];
	u8		wwnn[8];
	__le32		hard_alpa_d_id;
};

/**
 * @brief WRITE_NVPARMS - write SLI port configuration parameters
 */
struct sli4_cmd_write_nvparms_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		resvd0;
	__le32		resvd4;
	__le32		resvd8;
	__le32		resvd12;
	u8		wwpn[8];
	u8		wwnn[8];
	__le32		hard_alpa_d_id;
};

/**
 * @brief READ_REV - read the Port revision levels
 */
enum {
	SLI4_READ_REV_FLAG_SLI_LEVEL = 0xf,
	SLI4_READ_REV_FLAG_FCOEM	= 0x10,
	SLI4_READ_REV_FLAG_CEEV	= 0x60,
	SLI4_READ_REV_FLAG_VPD	= 0x2000,

	SLI4_READ_REV_AVAILABLE_LENGTH = 0xffffff,
};

struct sli4_cmd_read_rev_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		resvd0;
	__le16		flags0_word;
	__le32		first_hw_rev;
	__le32		second_hw_rev;
	__le32		resvd12;
	__le32		third_hw_rev;
	u8		fc_ph_low;
	u8		fc_ph_high;
	u8		feature_level_low;
	u8		feature_level_high;
	__le32		resvd24;
	__le32		first_fw_id;
	u8		first_fw_name[16];
	__le32		second_fw_id;
	u8		second_fw_name[16];
	__le32		rsvd18[30];
	__le32		available_length_dword;
	__le32		phy_addr_low;
	__le32		phy_addr_high;
	__le32		returned_vpd_length;
	__le32		actual_vpd_length;
};

/**
 * @brief READ_SPARM64 - read the Port service parameters
 */
struct sli4_cmd_read_sparm64_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		resvd0;
	__le32		resvd4;
	struct sli4_bde_s	bde_64;
	__le16		vpi;
	__le16		resvd22;
	__le16		port_name_start;
	__le16		port_name_len;
	__le16		node_name_start;
	__le16		node_name_len;
};

#define SLI4_READ_SPARM64_VPI_DEFAULT	0
#define SLI4_READ_SPARM64_VPI_SPECIAL	U16_MAX

#define SLI4_READ_SPARM64_WWPN_OFFSET	(4 * sizeof(u32))
#define SLI4_READ_SPARM64_WWNN_OFFSET	(SLI4_READ_SPARM64_WWPN_OFFSET \
					+ sizeof(uint64_t))
/**
 * @brief READ_TOPOLOGY - read the link event information
 */
enum {
	SLI4_READTOPO_ATTEN_TYPE	= 0xff,		/* DW2 */
	SLI4_READTOPO_FLAG_IL		= 0x100,
	SLI4_READTOPO_FLAG_PB_RECVD	= 0x200,

	SLI4_READTOPO_LINKSTATE_RECV	= 0x3,
	SLI4_READTOPO_LINKSTATE_TRANS	= 0xc,
	SLI4_READTOPO_LINKSTATE_MACHINE	= 0xf0,
	SLI4_READTOPO_LINKSTATE_SPEED	= 0xff00,
	SLI4_READTOPO_LINKSTATE_TF	= 0x40000000,
	SLI4_READTOPO_LINKSTATE_LU	= 0x80000000,

	SLI4_READTOPO_SCN_BBSCN		= 0xf,		/* DW9W1B0 */
	SLI4_READTOPO_SCN_CBBSCN	= 0xf0,

	SLI4_READTOPO_R_T_TOV		= 0x1ff,	/* DW10WO */
	SLI4_READTOPO_AL_TOV		= 0xf000,

	SLI4_READTOPO_PB_FLAG		= 0x80,

	SLI4_READTOPO_INIT_N_PORTID	= 0xffffff,
};

struct sli4_cmd_read_topology_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		event_tag;
	__le32		dw2_attentype;
	u8		topology;
	u8		lip_type;
	u8		lip_al_ps;
	u8		al_pa_granted;
	struct sli4_bde_s	bde_loop_map;
	__le32		linkdown_state;
	__le32		currlink_state;
	u8		max_bbc;
	u8		init_bbc;
	u8		scn_flags;
	u8		rsvd39;
	__le16		dw10w0_al_rt_tov;
	__le16		lp_tov;
	u8		acquired_al_pa;
	u8		pb_flags;
	__le16		specified_al_pa;
	__le32		dw12_init_n_port_id;
};

#define SLI4_MIN_LOOP_MAP_BYTES	128

#define SLI4_READ_TOPOLOGY_LINK_UP	0x1
#define SLI4_READ_TOPOLOGY_LINK_DOWN	0x2
#define SLI4_READ_TOPOLOGY_LINK_NO_ALPA	0x3

#define SLI4_READ_TOPOLOGY_UNKNOWN	0x0
#define SLI4_READ_TOPOLOGY_NPORT	0x1
#define SLI4_READ_TOPOLOGY_FC_AL	0x2

#define SLI4_READ_TOPOLOGY_SPEED_NONE	0x00
#define SLI4_READ_TOPOLOGY_SPEED_1G	0x04
#define SLI4_READ_TOPOLOGY_SPEED_2G	0x08
#define SLI4_READ_TOPOLOGY_SPEED_4G	0x10
#define SLI4_READ_TOPOLOGY_SPEED_8G	0x20
#define SLI4_READ_TOPOLOGY_SPEED_10G	0x40
#define SLI4_READ_TOPOLOGY_SPEED_16G	0x80
#define SLI4_READ_TOPOLOGY_SPEED_32G	0x90

/**
 * @brief REG_FCFI - activate a FC Forwarder
 */
struct sli4_cmd_reg_fcfi_rq_cfg {
	u8	r_ctl_mask;
	u8	r_ctl_match;
	u8	type_mask;
	u8	type_match;
};

enum {
	SLI4_REGFCFI_VLAN_TAG		= 0xfff,
	SLI4_REGFCFI_VLANTAG_VALID	= 0x1000,
};

#define SLI4_CMD_REG_FCFI_NUM_RQ_CFG	4
struct sli4_cmd_reg_fcfi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		fcf_index;
	__le16		fcfi;
	__le16		rqid1;
	__le16		rqid0;
	__le16		rqid3;
	__le16		rqid2;
	struct sli4_cmd_reg_fcfi_rq_cfg rq_cfg[SLI4_CMD_REG_FCFI_NUM_RQ_CFG];
	__le32		dw8_vlan;
};

#define SLI4_CMD_REG_FCFI_MRQ_NUM_RQ_CFG	4
#define SLI4_CMD_REG_FCFI_MRQ_MAX_NUM_RQ	32
#define SLI4_CMD_REG_FCFI_SET_FCFI_MODE		0
#define SLI4_CMD_REG_FCFI_SET_MRQ_MODE		1

enum {
	SLI4_REGFCFI_MRQ_VLAN_TAG	= 0xfff,
	SLI4_REGFCFI_MRQ_VLANTAG_VALID	= 0x1000,
	SLI4_REGFCFI_MRQ_MODE		= 0x2000,

	SLI4_REGFCFI_MRQ_MASK_NUM_PAIRS	= 0xff,
	SLI4_REGFCFI_MRQ_FILTER_BITMASK = 0xf00,
	SLI4_REGFCFI_MRQ_RQ_SEL_POLICY	= 0xf000,
};

struct sli4_cmd_reg_fcfi_mrq_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		fcf_index;
	__le16		fcfi;
	__le16		rqid1;
	__le16		rqid0;
	__le16		rqid3;
	__le16		rqid2;
	struct sli4_cmd_reg_fcfi_rq_cfg
				rq_cfg[SLI4_CMD_REG_FCFI_MRQ_NUM_RQ_CFG];
	__le32		dw8_vlan;
	__le32		dw9_mrqflags;
};

/**
 * @brief REG_RPI - register a Remote Port Indicator
 */
enum {
	SLI4_REGRPI_REMOTE_N_PORTID	= 0xffffff,	/* DW2 */
	SLI4_REGRPI_UPD			= 0x1000000,
	SLI4_REGRPI_ETOW		= 0x8000000,
	SLI4_REGRPI_TERP		= 0x20000000,
	SLI4_REGRPI_CI			= 0x80000000,
};

struct sli4_cmd_reg_rpi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		rpi;
	__le16		rsvd2;
	__le32		dw2_rportid_flags;
	struct sli4_bde_s	bde_64;
	__le16		vpi;
	__le16		rsvd26;
};

#define SLI4_REG_RPI_BUF_LEN			0x70

/**
 * @brief REG_VFI - register a Virtual Fabric Indicator
 */
enum {
	SLI4_REGVFI_VP		= 0x1000,	/* DW1 */
	SLI4_REGVFI_UPD		= 0x2000,

	SLI4_REGVFI_LOCAL_N_PORTID = 0xffffff,	/* DW10 */
};

struct sli4_cmd_reg_vfi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		vfi;
	__le16		dw0w1_flags;
	__le16		fcfi;
	__le16		vpi;			/* vp=TRUE */
	u8		wwpn[8];
	struct sli4_bde_s sparm;
	__le32		e_d_tov;
	__le32		r_a_tov;
	__le32		dw10_lportid_flags;
};

/**
 * @brief REG_VPI - register a Virtual Port Indicator
 */
enum {
	SLI4_REGVPI_LOCAL_N_PORTID	= 0xffffff,
	SLI4_REGVPI_UPD			= 0x1000000,
};

struct sli4_cmd_reg_vpi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		rsvd0;
	__le32		dw2_lportid_flags;
	u8		wwpn[8];
	__le32		rsvd12;
	__le16		vpi;
	__le16		vfi;
};

/**
 * @brief REQUEST_FEATURES - request / query SLI features
 */
enum {
	SLI4_REQFEAT_QRY	= 0x1,		/* Dw1 */

	SLI4_REQFEAT_IAAB	= (1 << 0),	/* DW2 & DW3 */
	SLI4_REQFEAT_NPIV	= (1 << 1),
	SLI4_REQFEAT_DIF	= (1 << 2),
	SLI4_REQFEAT_VF		= (1 << 3),
	SLI4_REQFEAT_FCPI	= (1 << 4),
	SLI4_REQFEAT_FCPT	= (1 << 5),
	SLI4_REQFEAT_FCPC	= (1 << 6),
	SLI4_REQFEAT_RSVD	= (1 << 7),
	SLI4_REQFEAT_RQD	= (1 << 8),
	SLI4_REQFEAT_IAAR	= (1 << 9),
	SLI4_REQFEAT_HLM	= (1 << 10),
	SLI4_REQFEAT_PERFH	= (1 << 11),
	SLI4_REQFEAT_RXSEQ	= (1 << 12),
	SLI4_REQFEAT_RXRI	= (1 << 13),
	SLI4_REQFEAT_DCL2	= (1 << 14),
	SLI4_REQFEAT_RSCO	= (1 << 15),
	SLI4_REQFEAT_MRQP	= (1 << 16),
};

struct sli4_cmd_request_features_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		dw1_qry;
	__le32		cmd;
	__le32		resp;
};

/**
 * @brief SLI_CONFIG - submit a configuration command to Port
 *
 * Command is either embedded as part of the payload (embed) or located
 * in a separate memory buffer (mem)
 */
enum {
	SLI4_SLICONFIG_EMB	= 0x1,		/* DW1 */
	SLI4_SLICONFIG_PMDCNT	= 0xf8,

	SLI4_SLICONFIG_PMD_LEN	= 0xffffff,	/* Config PMD length */
};

struct sli4_sli_config_pmd_s {
	__le32		addr_low;
	__le32		addr_high;
	__le32		length_dword;
};

struct sli4_cmd_sli_config_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		dw1_flags;
	__le32		payload_len;
	__le32		rsvd12;
	__le32		rsvd16;
	__le32		rsvd20;
	union {
		u8 embed[58 * sizeof(u32)];
		struct sli4_sli_config_pmd_s mem;
	} payload;
};

/**
 * @brief READ_STATUS - read tx/rx status of a particular port
 *
 */
enum {
	SLI4_READSTATUS_CLEAR_COUNTERS	= 0x1,	/* DW1 */
};

struct sli4_cmd_read_status_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		dw1_flags;
	__le32		rsvd4;
	__le32		trans_kbyte_cnt;
	__le32		recv_kbyte_cnt;
	__le32		trans_frame_cnt;
	__le32		recv_frame_cnt;
	__le32		trans_seq_cnt;
	__le32		recv_seq_cnt;
	__le32		tot_exchanges_orig;
	__le32		tot_exchanges_resp;
	__le32		recv_p_bsy_cnt;
	__le32		recv_f_bsy_cnt;
	__le32		no_rq_buf_dropped_frames_cnt;
	__le32		empty_rq_timeout_cnt;
	__le32		no_xri_dropped_frames_cnt;
	__le32		empty_xri_pool_cnt;
};

/**
 * @brief READ_LNK_STAT - read link status of a particular port
 *
 */
enum {
	SLI4_READ_LNKSTAT_REC	= (1 << 0),
	SLI4_READ_LNKSTAT_GEC	= (1 << 1),
	SLI4_READ_LNKSTAT_W02OF	= (1 << 2),
	SLI4_READ_LNKSTAT_W03OF	= (1 << 3),
	SLI4_READ_LNKSTAT_W04OF	= (1 << 4),
	SLI4_READ_LNKSTAT_W05OF	= (1 << 5),
	SLI4_READ_LNKSTAT_W06OF	= (1 << 6),
	SLI4_READ_LNKSTAT_W07OF	= (1 << 7),
	SLI4_READ_LNKSTAT_W08OF	= (1 << 8),
	SLI4_READ_LNKSTAT_W09OF	= (1 << 9),
	SLI4_READ_LNKSTAT_W10OF = (1 << 10),
	SLI4_READ_LNKSTAT_W11OF = (1 << 11),
	SLI4_READ_LNKSTAT_W12OF	= (1 << 12),
	SLI4_READ_LNKSTAT_W13OF	= (1 << 13),
	SLI4_READ_LNKSTAT_W14OF	= (1 << 14),
	SLI4_READ_LNKSTAT_W15OF	= (1 << 15),
	SLI4_READ_LNKSTAT_W16OF	= (1 << 16),
	SLI4_READ_LNKSTAT_W17OF	= (1 << 17),
	SLI4_READ_LNKSTAT_W18OF	= (1 << 18),
	SLI4_READ_LNKSTAT_W19OF	= (1 << 19),
	SLI4_READ_LNKSTAT_W20OF	= (1 << 20),
	SLI4_READ_LNKSTAT_W21OF	= (1 << 21),
	SLI4_READ_LNKSTAT_CLRC	= (1 << 30),
	SLI4_READ_LNKSTAT_CLOF	= (1 << 31),
};

struct sli4_cmd_read_link_stats_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32	dw1_flags;
	__le32	linkfail_errcnt;
	__le32	losssync_errcnt;
	__le32	losssignal_errcnt;
	__le32	primseq_errcnt;
	__le32	inval_txword_errcnt;
	__le32	crc_errcnt;
	__le32	primseq_eventtimeout_cnt;
	__le32	elastic_bufoverrun_errcnt;
	__le32	arbit_fc_al_timeout_cnt;
	__le32	adv_rx_buftor_to_buf_credit;
	__le32	curr_rx_buf_to_buf_credit;
	__le32	adv_tx_buf_to_buf_credit;
	__le32	curr_tx_buf_to_buf_credit;
	__le32	rx_eofa_cnt;
	__le32	rx_eofdti_cnt;
	__le32	rx_eofni_cnt;
	__le32	rx_soff_cnt;
	__le32	rx_dropped_no_aer_cnt;
	__le32	rx_dropped_no_avail_rpi_rescnt;
	__le32	rx_dropped_no_avail_xri_rescnt;
};

/**
 * @brief Format a WQE with WQ_ID Association performance hint
 *
 * @par Description
 * PHWQ works by over-writing part of Word 10 in the WQE with the WQ ID.
 *
 * @param entry Pointer to the WQE.
 * @param q_id Queue ID.
 *
 * @return None.
 */
static inline void
sli_set_wq_id_association(void *entry, u16 q_id)
{
	u32 *wqe = entry;

	/*
	 * Set Word 10, bit 0 to zero
	 * Set Word 10, bits 15:1 to the WQ ID
	 */
	wqe[10] &= cpu_to_le32(~0xffff);
	wqe[10] |= cpu_to_le16(q_id << 1);
}

/**
 * @brief UNREG_FCFI - unregister a FCFI
 */
struct sli4_cmd_unreg_fcfi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		rsvd0;
	__le16		fcfi;
	__le16		rsvd6;
};

/**
 * @brief UNREG_RPI - unregister one or more RPI
 */
enum {
	SLI4_UNREG_RPI_DP	= 0x2000,
	SLI4_UNREG_RPI_II	= 0xc000,

	SLI4_UNREG_RPI_DEST_N_PORTID = 0xffffff,
};

struct sli4_cmd_unreg_rpi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le16		index;
	__le16		dw1w0_flags;
	__le32		dw2_dest_n_portid;
};

#define SLI4_UNREG_RPI_II_RPI			0x0
#define SLI4_UNREG_RPI_II_VPI			0x1
#define SLI4_UNREG_RPI_II_VFI			0x2
#define SLI4_UNREG_RPI_II_FCFI			0x3

/**
 * @brief UNREG_VFI - unregister one or more VFI
 */
enum {
	SLI4_UNREG_VFI_II	= 0xc000,
};

struct sli4_cmd_unreg_vfi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		rsvd0;
	__le16		index;
	__le16		dw2_flags;
};

#define SLI4_UNREG_VFI_II_VFI			0x0
#define SLI4_UNREG_VFI_II_FCFI			0x3

enum sli4_unreg_type_e {
	SLI4_UNREG_TYPE_PORT,
	SLI4_UNREG_TYPE_DOMAIN,
	SLI4_UNREG_TYPE_FCF,
	SLI4_UNREG_TYPE_ALL
};

/**
 * @brief UNREG_VPI - unregister one or more VPI
 */
enum {
	SLI4_UNREG_VPI_II	= 0xc000,
};

struct sli4_cmd_unreg_vpi_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		rsvd0;
	__le16		index;
	__le16		dw2w0_flags;
};

#define SLI4_UNREG_VPI_II_VPI			0x0
#define SLI4_UNREG_VPI_II_VFI			0x2
#define SLI4_UNREG_VPI_II_FCFI			0x3

/**
 * @brief AUTO_XFER_RDY - Configure the auto-generate XFER-RDY feature.
 */
struct sli4_cmd_config_auto_xfer_rdy_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		rsvd0;
	__le32		max_burst_len;
};

#define SLI4_CONFIG_AUTO_XFERRDY_BLKSIZE	0xffff

struct sli4_cmd_config_auto_xfer_rdy_hp_s {
	struct sli4_mbox_command_header_s	hdr;
	__le32		rsvd0;
	__le32		max_burst_len;
	__le32		dw3_esoc_flags;
	__le16		block_size;
	__le16		rsvd14;
};

/*************************************************************************
 * SLI-4 common configuration command formats and definitions
 */

#define SLI4_CFG_STATUS_SUCCESS			0x00
#define SLI4_CFG_STATUS_FAILED			0x01
#define SLI4_CFG_STATUS_ILLEGAL_REQUEST		0x02
#define SLI4_CFG_STATUS_ILLEGAL_FIELD		0x03

#define SLI4_MGMT_STATUS_FLASHROM_READ_FAILED	0xcb

#define SLI4_CFG_ADD_STATUS_NO_STATUS		0x00
#define SLI4_CFG_ADD_STATUS_INVALID_OPCODE	0x1e

/**
 * Subsystem values.
 */
#define SLI4_SUBSYSTEM_COMMON			0x01
#define SLI4_SUBSYSTEM_LOWLEVEL			0x0B
#define SLI4_SUBSYSTEM_FC			0x0c
#define SLI4_SUBSYSTEM_DMTF			0x11

#define	SLI4_OPC_LOWLEVEL_SET_WATCHDOG		0X36

/**
 * Common opcode (OPC) values.
 */
#define SLI4_OPC_COMMON_FUNCTION_RESET			0x3d
#define SLI4_OPC_COMMON_CREATE_CQ			0x0c
#define SLI4_OPC_COMMON_CREATE_CQ_SET			0x1d
#define SLI4_OPC_COMMON_DESTROY_CQ			0x36
#define SLI4_OPC_COMMON_MODIFY_EQ_DELAY			0x29
#define SLI4_OPC_COMMON_CREATE_EQ			0x0d
#define SLI4_OPC_COMMON_DESTROY_EQ			0x37
#define SLI4_OPC_COMMON_CREATE_MQ_EXT			0x5a
#define SLI4_OPC_COMMON_DESTROY_MQ			0x35
#define SLI4_OPC_COMMON_GET_CNTL_ATTRIBUTES		0x20
#define SLI4_OPC_COMMON_NOP				0x21
#define SLI4_OPC_COMMON_GET_RESOURCE_EXTENT_INFO	0x9a
#define SLI4_OPC_COMMON_GET_SLI4_PARAMETERS		0xb5
#define SLI4_OPC_COMMON_QUERY_FW_CONFIG			0x3a
#define SLI4_OPC_COMMON_GET_PORT_NAME			0x4d

#define SLI4_OPC_COMMON_WRITE_FLASHROM			0x07
#define SLI4_OPC_COMMON_READ_TRANSCEIVER_DATA		0x49
#define SLI4_OPC_COMMON_GET_CNTL_ADDL_ATTRIBUTES	0x79
#define SLI4_OPC_COMMON_GET_FUNCTION_CONFIG		0xa0
#define SLI4_OPC_COMMON_GET_PROFILE_CONFIG		0xa4
#define SLI4_OPC_COMMON_SET_PROFILE_CONFIG		0xa5
#define SLI4_OPC_COMMON_GET_PROFILE_LIST		0xa6
#define SLI4_OPC_COMMON_GET_ACTIVE_PROFILE		0xa7
#define SLI4_OPC_COMMON_SET_ACTIVE_PROFILE		0xa8
#define SLI4_OPC_COMMON_READ_OBJECT			0xab
#define SLI4_OPC_COMMON_WRITE_OBJECT			0xac
#define SLI4_OPC_COMMON_DELETE_OBJECT			0xae
#define SLI4_OPC_COMMON_READ_OBJECT_LIST		0xad
#define SLI4_OPC_COMMON_SET_DUMP_LOCATION		0xb8
#define SLI4_OPC_COMMON_SET_FEATURES			0xbf
#define SLI4_OPC_COMMON_GET_RECONFIG_LINK_INFO		0xc9
#define SLI4_OPC_COMMON_SET_RECONFIG_LINK_ID		0xca

/**
 * DMTF opcode (OPC) values.
 */
#define SLI4_OPC_DMTF_EXEC_CLP_CMD			0x01

/**
 * @brief Generic Command Request header
 */
enum {
	SLI4_REQHDR_VERSION = 0xff,
};

struct sli4_req_hdr_s {
	u8		opcode;
	u8		subsystem;
	__le16		rsvd2;
	__le32		timeout;
	__le32		request_length;
	u32		dw3_version;
};

/**
 * @brief Generic Command Response header
 */
struct sli4_res_hdr_s {
	u8		opcode;
	u8		subsystem;
	__le16		rsvd2;
	u8		status;
	u8		additional_status;
	__le16		rsvd6;
	__le32		response_length;
	__le32		actual_response_length;
};

/**
 * @brief COMMON_FUNCTION_RESET
 *
 * Resets the Port, returning it to a power-on state. This configuration
 * command does not have a payload and should set/expect the lengths to
 * be zero.
 */
struct sli4_req_common_function_reset_s {
	struct sli4_req_hdr_s	hdr;
};

struct sli4_res_common_function_reset_s {
	struct sli4_res_hdr_s	hdr;
};

/**
 * @brief COMMON_CREATE_CQ_V0
 *
 * Create a Completion Queue.
 */
struct sli4_req_common_create_cq_v0_s {
	struct sli4_req_hdr_s	hdr;
	__le16		num_pages;
	__le16		rsvd18;
	__le32		dw5_flags;
	__le32		dw6_flags;
	__le32		rsvd28;
	__le32		rsvd32;
	struct {
		__le32	low;
		__le32	high;
	} page_physical_address[0];
};

/**
 * @brief COMMON_CREATE_CQ_V2
 *
 * Create a Completion Queue.
 */
enum {
	SLI4_REQCREATE_CQV2_CLSWM	= 0x3000,	/* DW5 */
	SLI4_REQCREATE_CQV2_NODELAY	= 0x4000,
	SLI4_REQCREATE_CQV2_AUTOVALID	= 0x8000,
	SLI4_REQCREATE_CQV2_CQECNT	= 0x18000000,
	SLI4_REQCREATE_CQV2_VALID	= 0x20000000,
	SLI4_REQCREATE_CQV2_EVT		= 0x80000000,
	SLI4_REQCREATE_CQV2_ARM		= 0x8000,
};

struct sli4_req_common_create_cq_v2_s {
	struct sli4_req_hdr_s	hdr;
	__le16		num_pages;
	u8		page_size;
	u8		rsvd19;
	__le32		dw5_flags;
	__le16		eq_id;
	__le16		dw6w1_arm;
	__le16		cqe_count;
	__le16		rsvd30;
	__le32		rsvd32;
	struct {
		__le32	low;
		__le32	high;
	} page_physical_address[0];
};

/**
 * @brief COMMON_CREATE_CQ_SET_V0
 *
 * Create a set of Completion Queues.
 */
enum {
	SLI4_REQCREATE_CQSETV0_CLSWM	= (3 << 12),	/* DW5 */
	SLI4_REQCREATE_CQSETV0_NODELAY	= (1 << 14),
	SLI4_REQCREATE_CQSETV0_AUTOVALID = (1 << 15),
	SLI4_REQCREATE_CQSETV0_CQECNT	= (3 << 27),
	SLI4_REQCREATE_CQSETV0_VALID	= (1 << 29),
	SLI4_REQCREATE_CQSETV0_EVT	= (1 << 31),

	SLI4_REQCREATE_CQSETV0_CQE_COUNT = 0x7fff,	/* DW5W1 */
	SLI4_REQCREATE_CQSETV0_ARM	= 0x8000,
};

struct sli4_req_common_create_cq_set_v0_s {
	struct sli4_req_hdr_s	hdr;
	__le16		num_pages;
	u8		page_size;
	u8		rsvd19;
	__le32		dw5_flags;
	__le16		num_cq_req;
	__le16		dw6w1_flags;
	__le16		eq_id[16];
	struct {
		__le32	low;
		__le32	high;
	} page_physical_address[0];
};

/**
 * CQE count.
 */
#define SLI4_CQ_CNT_256			0
#define SLI4_CQ_CNT_512			1
#define SLI4_CQ_CNT_1024		2
#define SLI4_CQ_CNT_LARGE		3

#define SLI4_CQE_BYTES			(4 * sizeof(u32))

#define SLI4_COMMON_CREATE_CQ_V2_MAX_PAGES 8

/**
 * @brief Generic Common Create EQ/CQ/MQ/WQ/RQ Queue completion
 */
struct sli4_res_common_create_queue_s {
	struct sli4_res_hdr_s	hdr;
	__le16	q_id;
	u8	rsvd18;
	u8	ulp;
	__le32	db_offset;
	__le16	db_rs;
	__le16	db_fmt;
};

struct sli4_res_common_create_queue_set_s {
	struct sli4_res_hdr_s	hdr;
	__le16	q_id;
	__le16	num_q_allocated;
};

/**
 * @brief Common Destroy CQ
 */
struct sli4_req_common_destroy_cq_s {
	struct sli4_req_hdr_s	hdr;
	__le16	cq_id;
	__le16	rsvd14;
};

/**
 * @brief COMMON_MODIFY_EQ_DELAY
 *
 * Modify the delay multiplier for EQs
 */
struct sli4_req_common_modify_eq_delay_s {
	struct sli4_req_hdr_s	hdr;
	__le32	num_eq;
	struct {
		__le32	eq_id;
		__le32	phase;
		__le32	delay_multiplier;
	} eq_delay_record[8];
};

/**
 * @brief COMMON_CREATE_EQ
 *
 * Create an Event Queue.
 */
enum {
	SLI4_REQCREATE_EQ_AUTOVALID	= (1 << 28),	/* DW5 */
	SLI4_REQCREATE_EQ_VALID		= (1 << 29),
	SLI4_REQCREATE_EQ_EQESZ		= (1 << 31),

	SLI4_REQCREATE_EQ_COUNT		= (7 << 26),	/* DW6 */
	SLI4_REQCREATE_EQ_ARM		= (1 << 31),

	SLI4_REQCREATE_EQ_DELAYMULTI	= 0x7FE000,	/* DW7 */
};

struct sli4_req_common_create_eq_s {
	struct sli4_req_hdr_s	hdr;
	__le16	num_pages;
	__le16	rsvd18;
	__le32	dw5_flags;
	__le32	dw6_flags;
	__le32	dw7_delaymulti;
	__le32	rsvd32;
	struct {
		__le32	low;
		__le32	high;
	} page_address[8];
};

#define SLI4_EQ_CNT_256			0
#define SLI4_EQ_CNT_512			1
#define SLI4_EQ_CNT_1024		2
#define SLI4_EQ_CNT_2048		3
#define SLI4_EQ_CNT_4096		4

#define SLI4_EQE_SIZE_4			0
#define SLI4_EQE_SIZE_16		1

/**
 * @brief Common Destroy EQ
 */
struct sli4_req_common_destroy_eq_s {
	struct sli4_req_hdr_s	hdr;
	__le16		eq_id;
	__le16		rsvd18;
};

/**
 * @brief COMMON_CREATE_MQ_EXT
 *
 * Create a Mailbox Queue; accommodate v0 and v1 forms.
 */
enum {
	SLI4_REQCREATE_MQEXT_RINGSIZE	= 0xf,		/* DW6W1 */
	SLI4_REQCREATE_MQEXT_CQIDV0	= 0xffc0,

	SLI4_REQCREATE_MQEXT_VAL	= (1 << 31),	/* DW7 */

	SLI4_REQCREATE_MQEXT_ACQV	= (1 << 0),	/* DW8 */
	SLI4_REQCREATE_MQEXT_ASYNC_CQIDV0 = 0x7fe,
};

struct sli4_req_common_create_mq_ext_s {
	struct sli4_req_hdr_s	hdr;
	__le16		num_pages;
	__le16		cq_id_v1;
	__le32		async_event_bitmap;
	__le16		async_cq_id_v1;
	__le16		dw6w1_flags;
	__le32		dw7_val;
	__le32		dw8_flags;
	__le32		rsvd36;
	struct {
		__le32	low;
		__le32	high;
	} page_physical_address[8];
};

#define SLI4_MQE_SIZE_16		0x05
#define SLI4_MQE_SIZE_32		0x06
#define SLI4_MQE_SIZE_64		0x07
#define SLI4_MQE_SIZE_128		0x08

#define SLI4_ASYNC_EVT_LINK_STATE	BIT(1)
#define SLI4_ASYNC_EVT_FIP		BIT(2)
#define SLI4_ASYNC_EVT_GRP5		BIT(5)
#define SLI4_ASYNC_EVT_FC		BIT(16)
#define SLI4_ASYNC_EVT_SLI_PORT		BIT(17)

#define	SLI4_ASYNC_EVT_FC_ALL	\
		(SLI4_ASYNC_EVT_LINK_STATE	| \
		SLI4_ASYNC_EVT_FIP		| \
		SLI4_ASYNC_EVT_GRP5		| \
		SLI4_ASYNC_EVT_FC		| \
		SLI4_ASYNC_EVT_SLI_PORT)

/**
 * @brief Common Destroy MQ
 */
struct sli4_req_common_destroy_mq_s {
	struct sli4_req_hdr_s	hdr;
	__le16		mq_id;
	__le16		rsvd18;
};

/**
 * @brief COMMON_GET_CNTL_ATTRIBUTES
 *
 * Query for information about the SLI Port
 */
enum {
	SLI4_CNTL_ATTR_PORTNUM	= 0x3f,		/* Port num and type */
	SLI4_CNTL_ATTR_PORTTYPE	= 0xc0,
};

struct sli4_res_common_get_cntl_attributes_s {
	struct sli4_res_hdr_s	hdr;
	u8		version_string[32];
	u8		manufacturer_name[32];
	__le32		supported_modes;
	u8		eprom_version_lo;
	u8		eprom_version_hi;
	__le16		rsvd17;
	__le32		mbx_data_structure_version;
	__le32		ep_firmware_data_structure_version;
	u8		ncsi_version_string[12];
	__le32		default_extended_timeout;
	u8		model_number[32];
	u8		description[64];
	u8		serial_number[32];
	u8		ip_version_string[32];
	u8		fw_version_string[32];
	u8		bios_version_string[32];
	u8		redboot_version_string[32];
	u8		driver_version_string[32];
	u8		fw_on_flash_version_string[32];
	__le32		functionalities_supported;
	__le16		max_cdb_length;
	u8		asic_revision;
	u8		generational_guid0;
	__le32		generational_guid1_12[3];
	__le16		generational_guid13_14;
	u8		generational_guid15;
	u8		hba_port_count;
	__le16		default_link_down_timeout;
	u8		iscsi_version_min_max;
	u8		multifunctional_device;
	u8		cache_valid;
	u8		hba_status;
	u8		max_domains_supported;
	u8		port_num_type_flags;
	__le32		firmware_post_status;
	__le32		hba_mtu;
	u8		iscsi_features;
	u8		rsvd121[3];
	__le16		pci_vendor_id;
	__le16		pci_device_id;
	__le16		pci_sub_vendor_id;
	__le16		pci_sub_system_id;
	u8		pci_bus_number;
	u8		pci_device_number;
	u8		pci_function_number;
	u8		interface_type;
	__le64		unique_identifier;
	u8		number_of_netfilters;
	u8		rsvd122[3];
};

/**
 * @brief COMMON_GET_CNTL_ATTRIBUTES
 *
 * This command queries the controller information from the Flash ROM.
 */
struct sli4_req_common_get_cntl_addl_attributes_s {
	struct sli4_req_hdr_s	hdr;
};

struct sli4_res_common_get_cntl_addl_attributes_s {
	struct sli4_res_hdr_s	hdr;
	__le16		ipl_file_number;
	u8		ipl_file_version;
	u8		rsvd4;
	u8		on_die_temperature;
	u8		rsvd5[3];
	__le32		driver_advanced_features_supported;
	__le32		rsvd7[4];
	char		universal_bios_version[32];
	char		x86_bios_version[32];
	char		efi_bios_version[32];
	char		fcode_version[32];
	char		uefi_bios_version[32];
	char		uefi_nic_version[32];
	char		uefi_fcode_version[32];
	char		uefi_iscsi_version[32];
	char		iscsi_x86_bios_version[32];
	char		pxe_x86_bios_version[32];
	u8		default_wwpn[8];
	u8		ext_phy_version[32];
	u8		fc_universal_bios_version[32];
	u8		fc_x86_bios_version[32];
	u8		fc_efi_bios_version[32];
	u8		fc_fcode_version[32];
	u8		ext_phy_crc_label[8];
	u8		ipl_file_name[16];
	u8		rsvd139[72];
};

/**
 * @brief COMMON_NOP
 *
 * This command does not do anything; it only returns
 * the payload in the completion.
 */
struct sli4_req_common_nop_s {
	struct sli4_req_hdr_s	hdr;
	__le32			context[2];
};

struct sli4_res_common_nop_s {
	struct sli4_res_hdr_s	hdr;
	__le32			context[2];
};

/**
 * @brief COMMON_GET_RESOURCE_EXTENT_INFO
 */
struct sli4_req_common_get_resource_extent_info_s {
	struct sli4_req_hdr_s	hdr;
	__le16	resource_type;
	__le16	rsvd16;
};

#define SLI4_RSC_TYPE_ISCSI_INI_XRI	0x0c
#define SLI4_RSC_TYPE_VFI		0x20
#define SLI4_RSC_TYPE_VPI		0x21
#define SLI4_RSC_TYPE_RPI		0x22
#define SLI4_RSC_TYPE_XRI		0x23

struct sli4_res_common_get_resource_extent_info_s {
	struct sli4_res_hdr_s	hdr;
	__le16	resource_extent_count;
	__le16	resource_extent_size;
};

#define SLI4_128BYTE_WQE_SUPPORT	0x02
/**
 * @brief COMMON_GET_SLI4_PARAMETERS
 */

enum {
	SLI4_RES_GET_PARAM_PROTOCOL_TYPE = 0xFF,		/* DW4 */

	SLI4_RES_GET_PARAM_FT = (1 << 0),			/* DW5 */
	SLI4_RES_GET_PARAM_SLI_REV = (0xF << 4),
	SLI4_RES_GET_PARAM_SLI_FLY = (0xF << 8),
	SLI4_RES_GET_PARAM_IF_TYPE = (0xF << 12),
	SLI4_RES_GET_PARAM_SLI_HINT1 = (0xFF << 16),
	SLI4_RES_GET_PARAM_SLI_HINT2 = (0x1F << 24),

	SLI4_RES_GET_PARAM_EQ_PAGE_CNT = (0xF << 0),		/* DW6 */
	SLI4_RES_GET_PARAM_EQE_SIZES = (0xF << 8),
	SLI4_RES_GET_PARAM_EQ_PAGE_SIZES = (0xFF << 16),
	SLI4_RES_GET_PARAM_EQE_CNT_MTHD = (0xF << 24),

	SLI4_RES_GET_PARAM_CQ_PAGE_CNT = (0xF << 0),		/* DW8 */
	SLI4_RES_GET_PARAM_CQE_SIZES = (0xF << 8),
	SLI4_RES_GET_PARAM_CQV = (3 << 14),
	SLI4_RES_GET_PARAM_CQ_PAGE_SIZES = (0xFF << 16),
	SLI4_RES_GET_PARAM_CQE_CNT_MTHD = (0xF << 24),

	SLI4_RES_GET_PARAM_MQ_PAGE_CNT = (0xF << 0),		/* DW10 */
	SLI4_RES_GET_PARAM_MQV = (3 << 14),
	SLI4_RES_GET_PARAM_MQ_PAGE_SIZES = (0xFF << 16),
	SLI4_RES_GET_PARAM_MQE_CNT_MTHD = (0xF << 24),

	SLI4_RES_GET_PARAM_WQ_PAGE_CNT = (0xF << 0),		/* DW12 */
	SLI4_RES_GET_PARAM_WQE_SIZES = (0xF << 8),
	SLI4_RES_GET_PARAM_WQV = (3 << 14),
	SLI4_RES_GET_PARAM_WQ_PAGE_SIZES = (0xFF << 16),
	SLI4_RES_GET_PARAM_WQE_CNT_MTHD = (0xF << 24),

	SLI4_RES_GET_PARAM_RQ_PAGE_CNT = (0xF << 0),		/* DW14 */
	SLI4_RES_GET_PARAM_RQE_SIZES = (0xF << 8),
	SLI4_RES_GET_PARAM_RQV = (3 << 14),
	SLI4_RES_GET_PARAM_RQ_PAGE_SIZES = (0xFF << 16),
	SLI4_RES_GET_PARAM_RQE_CNT_MTHD = (0xF << 24),

	SLI4_RES_GET_PARAM_RQ_DB_WINDOW = 0xF000,		/* DW15W1 */

	SLI4_RES_GET_PARAM_FC = (1 << 0),			/* DW16 */
	SLI4_RES_GET_PARAM_EXT = (1 << 1),
	SLI4_RES_GET_PARAM_HDRR = (1 << 2),
	SLI4_RES_GET_PARAM_SGLR = (1 << 3),
	SLI4_RES_GET_PARAM_FBRR = (1 << 4),
	SLI4_RES_GET_PARAM_AREG = (1 << 5),
	SLI4_RES_GET_PARAM_TGT = (1 << 6),
	SLI4_RES_GET_PARAM_TERP = (1 << 7),
	SLI4_RES_GET_PARAM_ASSI = (1 << 8),
	SLI4_RES_GET_PARAM_WCHN = (1 << 9),
	SLI4_RES_GET_PARAM_TCCA = (1 << 10),
	SLI4_RES_GET_PARAM_TRTY = (1 << 11),
	SLI4_RES_GET_PARAM_TRIR = (1 << 12),
	SLI4_RES_GET_PARAM_PHOFF = (1 << 13),
	SLI4_RES_GET_PARAM_PHON = (1 << 14),
	SLI4_RES_GET_PARAM_PHWQ = (1 << 15),
	SLI4_RES_GET_PARAM_BOUND_4GA = (1 << 16),
	SLI4_RES_GET_PARAM_RXC = (1 << 17),
	SLI4_RES_GET_PARAM_HLM = (1 << 18),
	SLI4_RES_GET_PARAM_IPR = (1 << 19),
	SLI4_RES_GET_PARAM_RXRI = (1 << 20),
	SLI4_RES_GET_PARAM_SGLC = (1 << 21),
	SLI4_RES_GET_PARAM_TIMM = (1 << 22),
	SLI4_RES_GET_PARAM_TSMM = (1 << 23),
	SLI4_RES_GET_PARAM_OAS = (1 << 25),
	SLI4_RES_GET_PARAM_LC = (1 << 26),
	SLI4_RES_GET_PARAM_AGXF = (1 << 27),
	SLI4_RES_GET_PARAM_LOOPBACK = (0xF << 28),

	SLI4_RES_GET_PARAM_SGL_PAGE_CNT = (0xF << 0),		/* DW18 */
	SLI4_RES_GET_PARAM_SGL_PAGE_SIZES = (0xFF << 8),
	SLI4_RES_GET_PARAM_SGL_PP_ALIGN = (0xFF << 16)
};

struct sli4_res_common_get_sli4_parameters_s {
	struct sli4_res_hdr_s	hdr;
	__le32		dw4_protocol_type;
	__le32		dw5_sli;
	__le32		dw6_eq_page_cnt;
	__le16		eqe_count_mask;
	__le16		rsvd26;
	__le32		dw8_cq_page_cnt;
	__le16		cqe_count_mask;
	__le16		rsvd34;
	__le32		dw10_mq_page_cnt;
	__le16		mqe_count_mask;
	__le16		rsvd42;
	__le32		dw12_wq_page_cnt;
	__le16		wqe_count_mask;
	__le16		rsvd50;
	__le32		dw14_rq_page_cnt;
	__le16		rqe_count_mask;
	__le16		dw15w1_rq_db_window;
	__le32		dw16_loopback_scope;
	__le32		sge_supported_length;
	__le32		dw18_sgl_page_cnt;
	__le16		min_rq_buffer_size;
	__le16		rsvd75;
	__le32		max_rq_buffer_size;
	__le16		physical_xri_max;
	__le16		physical_rpi_max;
	__le16		physical_vpi_max;
	__le16		physical_vfi_max;
	__le32		rsvd88;
	__le16		frag_num_field_offset;
	__le16		frag_num_field_size;
	__le16		sgl_index_field_offset;
	__le16		sgl_index_field_size;
	__le32		chain_sge_initial_value_lo;
	__le32		chain_sge_initial_value_hi;
};

/**
 * @brief COMMON_QUERY_FW_CONFIG
 *
 * This command retrieves firmware configuration parameters and adapter
 * resources available to the driver.
 */
struct sli4_req_common_query_fw_config_s {
	struct sli4_req_hdr_s	hdr;
};

#define SLI4_FUNCTION_MODE_INI_MODE 0x40
#define SLI4_FUNCTION_MODE_TGT_MODE 0x80
#define SLI4_FUNCTION_MODE_DUA_MODE      0x800

#define SLI4_ULP_MODE_INI           0x40
#define SLI4_ULP_MODE_TGT           0x80

struct sli4_res_common_query_fw_config_s {
	struct sli4_res_hdr_s	hdr;
	__le32		config_number;
	__le32		asic_rev;
	__le32		physical_port;
	__le32		function_mode;
	__le32		ulp0_mode;
	__le32		ulp0_nic_wqid_base;
	__le32		ulp0_nic_wq_total; /* Dword 10 */
	__le32		ulp0_toe_wqid_base;
	__le32		ulp0_toe_wq_total;
	__le32		ulp0_toe_rqid_base;
	__le32		ulp0_toe_rq_total;
	__le32		ulp0_toe_defrqid_base;
	__le32		ulp0_toe_defrq_total;
	__le32		ulp0_lro_rqid_base;
	__le32		ulp0_lro_rq_total;
	__le32		ulp0_iscsi_icd_base;
	__le32		ulp0_iscsi_icd_total; /* Dword 20 */
	__le32		ulp1_mode;
	__le32		ulp1_nic_wqid_base;
	__le32		ulp1_nic_wq_total;
	__le32		ulp1_toe_wqid_base;
	__le32		ulp1_toe_wq_total;
	__le32		ulp1_toe_rqid_base;
	__le32		ulp1_toe_rq_total;
	__le32		ulp1_toe_defrqid_base;
	__le32		ulp1_toe_defrq_total;
	__le32		ulp1_lro_rqid_base;  /* Dword 30 */
	__le32		ulp1_lro_rq_total;
	__le32		ulp1_iscsi_icd_base;
	__le32		ulp1_iscsi_icd_total;
	__le32		function_capabilities;
	__le32		ulp0_cq_base;
	__le32		ulp0_cq_total;
	__le32		ulp0_eq_base;
	__le32		ulp0_eq_total;
	__le32		ulp0_iscsi_chain_icd_base;
	__le32		ulp0_iscsi_chain_icd_total;  /* Dword 40 */
	__le32		ulp1_iscsi_chain_icd_base;
	__le32		ulp1_iscsi_chain_icd_total;
};

/**
 * @brief COMMON_GET_PORT_NAME
 */

struct sli4_req_common_get_port_name_s {
	struct sli4_req_hdr_s	hdr;
	u8      port_type;
	u8      rsvd4[3];
};

struct sli4_res_common_get_port_name_s {
	struct sli4_res_hdr_s	hdr;
	char		port_name[4];
};

/**
 * @brief COMMON_WRITE_FLASHROM
 */
struct sli4_req_common_write_flashrom_s {
	struct sli4_req_hdr_s	hdr;
	__le32		flash_rom_access_opcode;
	__le32		flash_rom_access_operation_type;
	__le32		data_buffer_size;
	__le32		offset;
	u8		data_buffer[4];
};

#define SLI4_MGMT_FLASHROM_OPCODE_FLASH			0x01
#define SLI4_MGMT_FLASHROM_OPCODE_SAVE			0x02
#define SLI4_MGMT_FLASHROM_OPCODE_CLEAR			0x03
#define SLI4_MGMT_FLASHROM_OPCODE_REPORT		0x04
#define SLI4_MGMT_FLASHROM_OPCODE_IMAGE_INFO		0x05
#define SLI4_MGMT_FLASHROM_OPCODE_IMAGE_CRC		0x06
#define SLI4_MGMT_FLASHROM_OPCODE_OFFSET_BASED_FLASH	0x07
#define SLI4_MGMT_FLASHROM_OPCODE_OFFSET_BASED_SAVE	0x08
#define SLI4_MGMT_PHY_FLASHROM_OPCODE_FLASH		0x09
#define SLI4_MGMT_PHY_FLASHROM_OPCODE_SAVE		0x0a

#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_ISCSI		0x00
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_REDBOOT		0x01
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_BIOS		0x02
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_PXE_BIOS		0x03
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_CODE_CONTROL	0x04
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_IPSEC_CFG		0x05
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_INIT_DATA		0x06
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_ROM_OFFSET	0x07
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_FC_BIOS		0x08
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_ISCSI_BAK		0x09
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_FC_ACT		0x0a
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_FC_BAK		0x0b
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_CODE_CTRL_P	0x0c
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_NCSI		0x0d
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_NIC		0x0e
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_DCBX		0x0f
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_PXE_BIOS_CFG	0x10
#define SLI4_FLASH_ROM_ACCESS_OP_TYPE_ALL_CFG_DATA	0x11

/**
 * @brief COMMON_READ_TRANSCEIVER_DATA
 *
 * This command reads SFF transceiver data(Format is defined
 * by the SFF-8472 specification).
 */
struct sli4_req_common_read_transceiver_data_s {
	struct sli4_req_hdr_s	hdr;
	__le32		page_number;
	__le32		port;
};

struct sli4_res_common_read_transceiver_data_s {
	struct sli4_res_hdr_s	hdr;
	__le32		page_number;
	__le32		port;
	__le32		page_data[32];
	__le32		page_data_2[32];
};

/**
 * @brief COMMON_READ_OBJECT
 */

enum {
	SLI4_REQ_DESIRE_READLEN = 0xFFFFFF
};

struct sli4_req_common_read_object_s {
	struct sli4_req_hdr_s	hdr;
	__le32		desired_read_length_dword;
	__le32		read_offset;
	u8		object_name[104];
	__le32		host_buffer_descriptor_count;
	struct sli4_bde_s	host_buffer_descriptor[0];
};

enum {
	SLI4_RES_COM_READ_OBJ_EOF = 0x80000000

};

struct sli4_res_common_read_object_s {
	struct sli4_res_hdr_s	hdr;
	__le32		actual_read_length;
	__le32		eof_dword;
};

/**
 * @brief COMMON_WRITE_OBJECT
 */

enum {
	SLI4_RQ_DES_WRITE_LEN = 0xFFFFFF,
	SLI4_RQ_DES_WRITE_LEN_NOC = 0x40000000,
	SLI4_RQ_DES_WRITE_LEN_EOF = 0x80000000

};

struct sli4_req_common_write_object_s {
	struct sli4_req_hdr_s	hdr;
	__le32		desired_write_len_dword;
	__le32		write_offset;
	u8		object_name[104];
	__le32		host_buffer_descriptor_count;
	struct sli4_bde_s	host_buffer_descriptor[0];
};

enum {
	SLI4_RES_CHANGE_STATUS = 0xFF

};

struct sli4_res_common_write_object_s {
	struct sli4_res_hdr_s	hdr;
	__le32		actual_write_length;
	__le32		change_status_dword;
};

/**
 * @brief COMMON_DELETE_OBJECT
 */
struct sli4_req_common_delete_object_s {
	struct sli4_req_hdr_s	hdr;
	__le32		rsvd4;
	__le32		rsvd5;
	u8		object_name[104];
};

/**
 * @brief COMMON_READ_OBJECT_LIST
 */

enum {
	SLI4_RQ_OBJ_LIST_READ_LEN = 0xFFFFFF

};

struct sli4_req_common_read_object_list_s {
	struct sli4_req_hdr_s	hdr;
	__le32		desired_read_length_dword;
	__le32		read_offset;
	u8		object_name[104];
	__le32		host_buffer_descriptor_count;
	struct sli4_bde_s	host_buffer_descriptor[0];
};

/**
 * @brief COMMON_SET_DUMP_LOCATION
 */

enum {
	SLI4_RQ_COM_SET_DUMP_BUFFER_LEN = 0xFFFFFF,
	SLI4_RQ_COM_SET_DUMP_FDB = 0x20000000,
	SLI4_RQ_COM_SET_DUMP_BLP = 0x40000000,
	SLI4_RQ_COM_SET_DUMP_QRY = 0x80000000,

};

struct sli4_req_common_set_dump_location_s {
	struct sli4_req_hdr_s	hdr;
	__le32		buffer_length_dword;
	__le32		buf_addr_low;
	__le32		buf_addr_high;
};

enum {
	SLI4_RES_SET_DUMP_BUFFER_LEN = 0xFFFFFF

};

struct sli4_res_common_set_dump_location_s {
	struct sli4_res_hdr_s	hdr;
	__le32		buffer_length_dword;
};

/**
 * @brief COMMON_SET_SET_FEATURES
 */
#define SLI4_SET_FEATURES_DIF_SEED			0x01
#define SLI4_SET_FEATURES_XRI_TIMER			0x03
#define SLI4_SET_FEATURES_MAX_PCIE_SPEED		0x04
#define SLI4_SET_FEATURES_FCTL_CHECK			0x05
#define SLI4_SET_FEATURES_FEC				0x06
#define SLI4_SET_FEATURES_PCIE_RECV_DETECT		0x07
#define SLI4_SET_FEATURES_DIF_MEMORY_MODE		0x08
#define SLI4_SET_FEATURES_DISABLE_SLI_PORT_PAUSE_STATE	0x09
#define SLI4_SET_FEATURES_ENABLE_PCIE_OPTIONS		0x0A
#define SLI4_SET_FEAT_CFG_AUTO_XFER_RDY_T10PI	0x0C
#define SLI4_SET_FEATURES_ENABLE_MULTI_RECEIVE_QUEUE	0x0D
#define SLI4_SET_FEATURES_SET_FTD_XFER_HINT		0x0F
#define SLI4_SET_FEATURES_SLI_PORT_HEALTH_CHECK		0x11

struct sli4_req_common_set_features_s {
	struct sli4_req_hdr_s	hdr;
	__le32		feature;
	__le32		param_len;
	__le32		params[8];
};

struct sli4_req_common_set_features_dif_seed_s {
	__le16		seed;
	__le16		rsvd16;
};

enum {
	SLI4_RQ_COM_SET_T10_PI_MEM_MODEL = 0x1

};

struct sli4_req_common_set_features_t10_pi_mem_model_s {
	__le32		tmm_dword;
};

enum {
	SLI4_RQ_MULTIRQ_ISR = 0x1,
	SLI4_RQ_MULTIRQ_AUTOGEN_XFER_RDY = 0x2,

	SLI4_RQ_MULTIRQ_NUM_RQS = 0xFF,
	SLI4_RQ_MULTIRQ_RQ_SELECT = 0xF00
};

struct sli4_req_common_set_features_multirq_s {
	__le32		auto_gen_xfer_dword; /* Include Sequence Reporting */
					/* Auto Generate XFER-RDY Enabled */
	__le32		num_rqs_dword;
};

enum {
	SLI4_SETFEAT_XFERRDY_T10PI_RTC	= (1 << 0),	/* DW0 */
	SLI4_SETFEAT_XFERRDY_T10PI_ATV	= (1 << 1),
	SLI4_SETFEAT_XFERRDY_T10PI_TMM	= (1 << 2),
	SLI4_SETFEAT_XFERRDY_T10PI_PTYPE = (0x7 << 4),
	SLI4_SETFEAT_XFERRDY_T10PI_BLKSIZ = (0x7 << 7),
};

struct sli4_req_common_set_features_xfer_rdy_t10pi_s {
	__le32		dw0_flags;
	__le16		app_tag;
	__le16		rsvd6;
};

enum {
	SLI4_RQ_HEALTH_CHECK_ENABLE = 0x1,
	SLI4_RQ_HEALTH_CHECK_QUERY = 0x2

};

struct sli4_req_common_set_features_health_check_s {
	__le32		health_check_dword;
};

struct sli4_req_common_set_features_set_fdt_xfer_hint_s {
	__le32		fdt_xfer_hint;
};

/**
 * @brief DMTF_EXEC_CLP_CMD
 */
struct sli4_req_dmtf_exec_clp_cmd_s {
	struct sli4_req_hdr_s	hdr;
	__le32		cmd_buf_length;
	__le32		resp_buf_length;
	__le32		cmd_buf_addr_low;
	__le32		cmd_buf_addr_high;
	__le32		resp_buf_addr_low;
	__le32		resp_buf_addr_high;
};

struct sli4_res_dmtf_exec_clp_cmd_s {
	struct sli4_res_hdr_s	hdr;
	__le32		rsvd4;
	__le32		resp_length;
	__le32		rsvd6;
	__le32		rsvd7;
	__le32		rsvd8;
	__le32		rsvd9;
	__le32		clp_status;
	__le32		clp_detailed_status;
};

#define SLI4_PROTOCOL_FC			0x10
#define SLI4_PROTOCOL_DEFAULT			0xff

struct sli4_resource_descriptor_v1_s {
	u8		descriptor_type;
	u8		descriptor_length;
	__le16		rsvd16;
	__le32		type_specific[0];
};

enum {
	SLI4_PCIE_DESC_IMM = 0x4000,
	SLI4_PCIE_DESC_NOSV = 0x8000,

	SLI4_PCIE_DESC_PF_NO = 0x3FF0000,

	SLI4_PCIE_DESC_MISSN_ROLE = 0xFF,
	SLI4_PCIE_DESC_PCHG = 0x8000000,
	SLI4_PCIE_DESC_SCHG = 0x10000000,
	SLI4_PCIE_DESC_XCHG = 0x20000000,
	SLI4_PCIE_DESC_XROM = 0xC0000000
};

struct sli4_pcie_resource_descriptor_v1_s {
	u8		descriptor_type;
	u8		descriptor_length;
	__le16		imm_nosv_dword;
	__le32		pf_number_dword;
	__le32		rsvd3;
	u8		sriov_state;
	u8		pf_state;
	u8		pf_type;
	u8		rsvd4;
	__le16		number_of_vfs;
	__le16		rsvd5;
	__le32		mission_roles_dword;
	__le32		rsvd7[16];
};

/**
 * @brief COMMON_GET_FUNCTION_CONFIG
 */
struct sli4_req_common_get_function_config_s {
	struct sli4_req_hdr_s  hdr;
};

struct sli4_res_common_get_function_config_s {
	struct sli4_res_hdr_s  hdr;
	__le32		desc_count;
	__le32		desc[54];
};

/**
 * @brief COMMON_GET_PROFILE_CONFIG
 */

enum {
	SLI4_RQ_GET_PROFILE_ID = 0XFF,
	SLI4_RQ_GET_PROFILE_TYPE = 0x300
};

struct sli4_req_common_get_profile_config_s {
	struct sli4_req_hdr_s  hdr;
	__le32		profile_id_dword;
};

struct sli4_res_common_get_profile_config_s {
	struct sli4_res_hdr_s  hdr;
	__le32			desc_count;
	__le32			desc[0];
};

/**
 * @brief COMMON_SET_PROFILE_CONFIG
 */

enum {
	SLI4_RQ_SET_PROFILE_ID = 0XFF,
	SLI4_RQ_SET_PROFILE_ISAP = 0x80000000

};

struct sli4_req_common_set_profile_config_s {
	struct sli4_req_hdr_s  hdr;
	__le32		profile_id_dword;
	__le32		desc_count;
	__le32		desc[0];
};

struct sli4_res_common_set_profile_config_s {
	struct sli4_res_hdr_s  hdr;
};

/**
 * @brief Profile Descriptor for profile functions
 */
struct sli4_profile_descriptor_s {
	u8		profile_id;
	u8		rsvd8;
	u8		profile_index;
	u8		rsvd24;
	__le32		profile_description[128];
};

/*
 * We don't know in advance how many descriptors there are.  We have
 * to pick a number that we think will be big enough and ask for that
 * many.
 */

#define MAX_PROD_DES	40

/**
 * @brief COMMON_GET_PROFILE_LIST
 */

enum {
	SLI4_RQ_PROFILE_INDEX = 0XFF

};

struct sli4_req_common_get_profile_list_s {
	struct sli4_req_hdr_s  hdr;
	__le32	start_profile_index_dword;
};

struct sli4_res_common_get_profile_list_s {
	struct sli4_res_hdr_s  hdr;
	__le32		profile_descriptor_count;
	struct sli4_profile_descriptor_s profile_descriptor[MAX_PROD_DES];
};

/**
 * @brief COMMON_GET_ACTIVE_PROFILE
 */
struct sli4_req_common_get_active_profile_s {
	struct sli4_req_hdr_s  hdr;
};

struct sli4_res_common_get_active_profile_s {
	struct sli4_res_hdr_s  hdr;
	u8		active_profile_id;
	u8		rsvd0;
	u8		next_profile_id;
	u8		rsvd1;
};

/**
 * @brief COMMON_SET_ACTIVE_PROFILE
 */

enum {
	SLI4_REQ_SETACTIVE_PROF_ID = 0xFF,
	SLI4_REQ_SETACTIVE_PROF_FD = 0x80000000
};

struct sli4_req_common_set_active_profile_s {
	struct sli4_req_hdr_s  hdr;
	__le32	active_profile_id_dword;
};

struct sli4_res_common_set_active_profile_s {
	struct sli4_res_hdr_s  hdr;
};

/**
 * @brief Link Config Descriptor for link config functions
 */
struct sli4_link_config_descriptor_s {
	u8		link_config_id;
	u8		rsvd1[3];
	__le32		config_description[8];
};

#define MAX_LINK_DES	10

/**
 * @brief COMMON_GET_RECONFIG_LINK_INFO
 */
struct sli4_req_common_get_reconfig_link_info_s {
	struct sli4_req_hdr_s  hdr;
};

struct sli4_res_common_get_reconfig_link_info_s {
	struct sli4_res_hdr_s  hdr;
	u8		active_link_config_id;
	u8		rsvd17;
	u8		next_link_config_id;
	u8		rsvd19;
	__le32		link_configuration_descriptor_count;
	struct sli4_link_config_descriptor_s    desc[MAX_LINK_DES];
};

/**
 * @brief COMMON_SET_RECONFIG_LINK_ID
 */
enum {
	SLI4_SET_RECONFIG_LINKID_NEXT	= 0xff,
	SLI4_SET_RECONFIG_LINKID_FD	= (1 << 31),
};

struct sli4_req_common_set_reconfig_link_id_s {
	struct sli4_req_hdr_s  hdr;
	__le32		dw4_flags;
};

struct sli4_res_common_set_reconfig_link_id_s {
	struct sli4_res_hdr_s  hdr;
};

struct sli4_req_lowlevel_set_watchdog_s {
	struct sli4_req_hdr_s	hdr;
	__le16		watchdog_timeout;
	__le16		rsvd18;
};

struct sli4_res_lowlevel_set_watchdog_s {
	struct sli4_res_hdr_s	hdr;
	__le32			rsvd;
};

/**
 * @brief Event Queue Entry
 */
enum {
	SLI4_EQE_VALID	= 1,
	SLI4_EQE_MJCODE	= 0xe,
	SLI4_EQE_MNCODE	= 0xfff0,
};

struct sli4_eqe_s {
	__le16		dw0w0_flags;
	__le16		resource_id;
};

#define SLI4_MAJOR_CODE_STANDARD	0
#define SLI4_MAJOR_CODE_SENTINEL	1

/**
 * @brief Mailbox Completion Queue Entry
 *
 * A CQE generated on the completion of a MQE from a MQ.
 */
enum {
	SLI4_MCQE_CONSUMED	= (1 << 27),
	SLI4_MCQE_COMPLETED	= (1 << 28),
	SLI4_MCQE_AE		= (1 << 30),
	SLI4_MCQE_VALID		= (1 << 31),
};

struct sli4_mcqe_s {
	__le16		completion_status;
	__le16		extended_status;
	__le32		mqe_tag_low;
	__le32		mqe_tag_high;
	__le32		dw3_flags;
};

/**
 * @brief Asynchronous Completion Queue Entry
 *
 * A CQE generated asynchronously in response
 * to the link or other internal events.
 */
enum {
	SLI4_ACQE_AE	= (1 << 6), /** async event - this is an ACQE */
	SLI4_ACQE_VAL	= (1 << 7), /** valid - contents of CQE are valid */
};

struct sli4_acqe_s {
	__le32		event_data[3];
	u8		rsvd12;
	u8		event_code;
	u8		event_type;	/* values are protocol specific */
	u8		ae_val;
};

#define SLI4_ACQE_EVENT_CODE_LINK_STATE		0x01
#define SLI4_ACQE_EVENT_CODE_FIP		0x02
#define SLI4_ACQE_EVENT_CODE_DCBX		0x03
#define SLI4_ACQE_EVENT_CODE_ISCSI		0x04
#define SLI4_ACQE_EVENT_CODE_GRP_5		0x05
#define SLI4_ACQE_EVENT_CODE_FC_LINK_EVENT	0x10
#define SLI4_ACQE_EVENT_CODE_SLI_PORT_EVENT	0x11
#define SLI4_ACQE_EVENT_CODE_VF_EVENT		0x12
#define SLI4_ACQE_EVENT_CODE_MR_EVENT		0x13

/**
 * @brief Register name enums
 */
enum sli4_regname_en {
	SLI4_REG_BMBX,
	SLI4_REG_EQ_DOORBELL,
	SLI4_REG_CQ_DOORBELL,
	SLI4_REG_RQ_DOORBELL,
	SLI4_REG_IO_WQ_DOORBELL,
	SLI4_REG_MQ_DOORBELL,
	SLI4_REG_PHYSDEV_CONTROL,
	SLI4_REG_SLIPORT_CONTROL,
	SLI4_REG_SLIPORT_ERROR1,
	SLI4_REG_SLIPORT_ERROR2,
	SLI4_REG_SLIPORT_SEMAPHORE,
	SLI4_REG_SLIPORT_STATUS,
	SLI4_REG_MAX			/* must be last */
};

struct sli4_reg_s {
	u32	rset;
	u32	off;
};

enum sli4_qtype_e {
	SLI_QTYPE_EQ,
	SLI_QTYPE_CQ,
	SLI_QTYPE_MQ,
	SLI_QTYPE_WQ,
	SLI_QTYPE_RQ,
	SLI_QTYPE_MAX,			/* must be last */
};

#define SLI_USER_MQ_COUNT	1	/** User specified max mail queues */
#define SLI_MAX_CQ_SET_COUNT	16
#define SLI_MAX_RQ_SET_COUNT	16

enum sli4_qentry_e {
	SLI_QENTRY_ASYNC,
	SLI_QENTRY_MQ,
	SLI_QENTRY_RQ,
	SLI_QENTRY_WQ,
	SLI_QENTRY_WQ_RELEASE,
	SLI_QENTRY_OPT_WRITE_CMD,
	SLI_QENTRY_OPT_WRITE_DATA,
	SLI_QENTRY_XABT,
	SLI_QENTRY_MAX			/* must be last */
};

enum {
	/* CQ has MQ/Async completion */
	SLI4_QUEUE_FLAG_MQ	= (1 << 0),

	/* RQ for packet headers */
	SLI4_QUEUE_FLAG_HDR	= (1 << 1),

	/* RQ index increment by 8 */
	SLI4_QUEUE_FLAG_RQBATCH	= (1 << 2),
};

struct sli4_queue_s {
	/* Common to all queue types */
	struct efc_dma_s	dma;
	spinlock_t	lock;	/* protect the queue operations */
	u32	index;		/* current host entry index */
	u16	size;		/* entry size */
	u16	length;		/* number of entries */
	u16	n_posted;	/* number entries posted */
	u16	id;		/* Port assigned xQ_ID */
	u16	ulp;		/* ULP assigned to this queue */
	void __iomem    *db_regaddr;	/* register address for the doorbell */
	u8		type;		/* queue type ie EQ, CQ, ... */
	u32	proc_limit;	/* limit CQE processed per iteration */
	u32	posted_limit;	/* CQE/EQE process before ring doorbel */
	u32	max_num_processed;
	time_t		max_process_time;
	u16	phase;		/* For if_type = 6, this value toggle
				 * for each iteration of the queue,
				 * a queue entry is valid when a cqe
				 * valid bit matches this value
				 */

	/* Type specific gunk */
	union {
		u32	r_idx;	/** "read" index (MQ only) */
		struct {
			u32	dword;
		} flag;
	} u;
};

#define SLI4_QUEUE_DEFAULT_CQ	U16_MAX /** Use the default CQ */

#define SLI4_QUEUE_RQ_BATCH	8

enum sli4_callback_e {
	SLI4_CB_LINK,
	SLI4_CB_MAX			/* must be last */
};

enum sli4_link_status_e {
	SLI_LINK_STATUS_UP,
	SLI_LINK_STATUS_DOWN,
	SLI_LINK_STATUS_NO_ALPA,
	SLI_LINK_STATUS_MAX,
};

enum sli4_link_topology_e {
	SLI_LINK_TOPO_NPORT = 1,	/** fabric or point-to-point */
	SLI_LINK_TOPO_LOOP,
	SLI_LINK_TOPO_LOOPBACK_INTERNAL,
	SLI_LINK_TOPO_LOOPBACK_EXTERNAL,
	SLI_LINK_TOPO_NONE,
	SLI_LINK_TOPO_MAX,
};

enum sli4_link_medium_e {
	SLI_LINK_MEDIUM_ETHERNET,
	SLI_LINK_MEDIUM_FC,
	SLI_LINK_MEDIUM_MAX,
};

struct sli4_link_event_s {
	enum sli4_link_status_e	status;		/* link up/down */
	enum sli4_link_topology_e	topology;
	enum sli4_link_medium_e	medium;		/* Ethernet / FC */
	u32		speed;		/* Mbps */
	u8			*loop_map;
	u32		fc_id;
};

/**
 * @brief Fields used to build chained SGL
 */
struct sli4_sgl_chaining_params_s {
	u8		chaining_capable;
	u16	frag_num_field_offset;
	u16	sgl_index_field_offset;
	u64	frag_num_field_mask;
	u64	sgl_index_field_mask;
	u32	chain_sge_initial_value_lo;
	u32	chain_sge_initial_value_hi;
};

struct sli4_fip_event_s {
	u32	type;
	u32	index;		/* FCF index or U32_MAX if invalid */
};

enum sli4_resource_e {
	SLI_RSRC_VFI,
	SLI_RSRC_VPI,
	SLI_RSRC_RPI,
	SLI_RSRC_XRI,
	SLI_RSRC_FCFI,
	SLI_RSRC_MAX			/* must be last */
};

enum sli4_asic_type_e {
	SLI4_ASIC_INTF_2 = 3,
	SLI4_ASIC_INTF_2_G6 = 5,
	SLI4_ASIC_INTF_2_G7 = 6,
};

enum sli4_asic_rev_e {
	SLI4_ASIC_REV_FPGA = 1,
	SLI4_ASIC_REV_A0,
	SLI4_ASIC_REV_A1,
	SLI4_ASIC_REV_A2,
	SLI4_ASIC_REV_A3,
	SLI4_ASIC_REV_B0,
	SLI4_ASIC_REV_B1,
	SLI4_ASIC_REV_C0,
	SLI4_ASIC_REV_D0,
};

struct sli4_extent_s {
	u32	number;	/* number of extents */
	u32	size; /* no of element in each extent */
	u32	n_alloc;  /* no of elements allocated */
	u32	*base;
	unsigned long	*use_map; /* shows resource in use */
	u32	map_size; /* no of bits in bitmap */
};

struct sli4_s {
	void	*os;
	struct pci_dev	*pdev;
#define	SLI_PCI_MAX_BAR		6
	void __iomem *reg[SLI_PCI_MAX_BAR];

	u32	sli_rev;	/* SLI revision number */
	u32	sli_family;
	u32	if_type;	/* SLI Interface type */

	enum sli4_asic_type_e asic_type;	/*<< ASIC type */
	enum sli4_asic_rev_e asic_rev;	/*<< ASIC revision */
	u32	physical_port;

	u16		e_d_tov;
	u16		r_a_tov;
	u16		max_qcount[SLI_QTYPE_MAX];
	u32		max_qentries[SLI_QTYPE_MAX];
	u16		count_mask[SLI_QTYPE_MAX];
	u16		count_method[SLI_QTYPE_MAX];
	u32		qpage_count[SLI_QTYPE_MAX];
	u16		link_module_type;
	u8			rq_batch;
	u16		rq_min_buf_size;
	u32		rq_max_buf_size;
	u8			topology;
	u8			wwpn[8];	/* WW Port Name */
	u8			wwnn[8];	/* WW Node Name */
	u32		fw_rev[2];
	u8			fw_name[2][16];
	char			ipl_name[16];
	u32		hw_rev[3];
	u8			port_number;
	char			port_name[2];
	char                    modeldesc[64];
	char			bios_version_string[32];
	u8			dual_ulp_capable;
	u8			is_ulp_fc[2];
	/*
	 * Tracks the port resources using extents metaphor. For
	 * devices that don't implement extents (i.e.
	 * has_extents == FALSE), the code models each resource as
	 * a single large extent.
	 */
	struct sli4_extent_s	extent[SLI_RSRC_MAX];
	u32			features;
	u32		has_extents:1,
				auto_reg:1,
				auto_xfer_rdy:1,
				hdr_template_req:1,
				perf_hint:1,
				perf_wq_id_association:1,
				cq_create_version:2,
				mq_create_version:2,
				high_login_mode:1,
				sgl_pre_registered:1,
				sgl_pre_registration_required:1,
				t10_dif_inline_capable:1,
				t10_dif_separate_capable:1;
	u32		sge_supported_length;
	u32		sgl_page_sizes;
	u32		max_sgl_pages;
	struct sli4_sgl_chaining_params_s sgl_chaining_params;
	size_t			wqe_size;

	/*
	 * Callback functions
	 */
	int		(*link)(void *ctx, void *event);
	void		*link_arg;

	struct efc_dma_s	bmbx;

	/* Save pointer to physical memory descriptor for non-embedded
	 * SLI_CONFIG commands for BMBX dumping purposes
	 */
	struct efc_dma_s	*bmbx_non_emb_pmd;

	struct efc_dma_s	vpd_data;
	u32		vpd_length;
};

/**
 * Get / set parameter functions
 */

static inline int
sli_set_hlm(struct sli4_s *sli4, u32 value)
{
	if (value && !(sli4->features & SLI4_REQFEAT_HLM)) {
		pr_info("HLM not supported\n");
		return -1;
	}

	sli4->high_login_mode = value != 0 ? true : false;

	return 0;
}

static inline int
sli_set_sgl_preregister(struct sli4_s *sli4, u32 value)
{
	if (value == 0 && sli4->sgl_pre_registration_required) {
		pr_info("SGL pre-registration required\n");
		return -1;
	}

	sli4->sgl_pre_registered = value != 0 ? true : false;

	return 0;
}

static inline u32
sli_get_max_sgl(struct sli4_s *sli4)
{
	if (sli4->sgl_page_sizes != 1) {
		pr_info("unsupported SGL page sizes %#x\n",
			sli4->sgl_page_sizes);
		return 0;
	}

	return ((sli4->max_sgl_pages * SLI_PAGE_SIZE)
		/ sizeof(struct sli4_sge_s));
}

static inline enum sli4_link_medium_e
sli_get_medium(struct sli4_s *sli4)
{
	switch (sli4->topology) {
	case SLI4_READ_CFG_TOPO_FC:
	case SLI4_READ_CFG_TOPO_FC_DA:
	case SLI4_READ_CFG_TOPO_FC_AL:
		return SLI_LINK_MEDIUM_FC;
	default:
		return SLI_LINK_MEDIUM_MAX;
	}
}

static inline int
sli_set_topology(struct sli4_s *sli4, u32 value)
{
	int	rc = 0;

	switch (value) {
	case SLI4_READ_CFG_TOPO_FC:
	case SLI4_READ_CFG_TOPO_FC_DA:
	case SLI4_READ_CFG_TOPO_FC_AL:
		sli4->topology = value;
		break;
	default:
		pr_info("unsupported topology %#x\n", value);
		rc = -1;
	}

	return rc;
}

static inline u32
sli_convert_mask_to_count(u32 method, u32 mask)
{
	u32 count = 0;

	if (method) {
		count = 1 << (31 - __builtin_clz(mask));
		count *= 16;
	} else {
		count = mask;
	}

	return count;
}

static inline u32
sli_reg_read_status(struct sli4_s *sli)
{
	return readl(sli->reg[0] + SLI4_PORT_STATUS_REGOFFSET);
}

static inline int
sli_fw_error_status(struct sli4_s *sli4)
{
	return ((sli_reg_read_status(sli4) & SLI4_PORT_STATUS_ERR) ? 1 : 0);
}

static inline u32
sli_reg_read_err1(struct sli4_s *sli)
{
	return readl(sli->reg[0] + SLI4_SLIPORT_ERROR1);
}

static inline u32
sli_reg_read_err2(struct sli4_s *sli)
{
	return readl(sli->reg[0] + SLI4_SLIPORT_ERROR2);
}

/****************************************************************************
 * Function prototypes
 */
extern int
sli_cmd_config_auto_xfer_rdy(struct sli4_s *sli4, void *buf, size_t size,
			     u32 max_burst_len);
extern int
sli_cmd_config_auto_xfer_rdy_hp(struct sli4_s *sli4, void *buf,
				size_t size, u32 max_burst_len, u32 esoc,
			u32 block_size);
extern int
sli_cmd_config_link(struct sli4_s *sli4, void *buf, size_t size);
extern int
sli_cmd_down_link(struct sli4_s *sli4, void *buf, size_t size);
extern int
sli_cmd_dump_type4(struct sli4_s *sli4, void *buf,
		   size_t size, u16 wki);
extern int
sli_cmd_common_read_transceiver_data(struct sli4_s *sli4, void *buf,
				     size_t size, u32 page_num,
				     struct efc_dma_s *dma);
extern int
sli_cmd_read_link_stats(struct sli4_s *sli4, void *buf, size_t size,
			u8 req_ext_counters, u8 clear_overflow_flags,
			u8 clear_all_counters);
extern int
sli_cmd_read_status(struct sli4_s *sli4, void *buf, size_t size,
		    u8 clear_counters);
extern int
sli_cmd_init_link(struct sli4_s *sli4, void *buf, size_t size,
		  u32 speed, u8 reset_alpa);
extern int
sli_cmd_init_vfi(struct sli4_s *sli4, void *buf, size_t size, u16 vfi,
		 u16 fcfi, u16 vpi);
extern int
sli_cmd_init_vpi(struct sli4_s *sli4, void *buf, size_t size, u16 vpi,
		 u16 vfi);
extern int
sli_cmd_post_xri(struct sli4_s *sli4, void *buf, size_t size,
		 u16 xri_base, u16 xri_count);
extern int
sli_cmd_release_xri(struct sli4_s *sli4, void *buf, size_t size,
		    u8 num_xri);
extern int
sli_cmd_read_sparm64(struct sli4_s *sli4, void *buf, size_t size,
		     struct efc_dma_s *dma, u16 vpi);
extern int
sli_cmd_read_topology(struct sli4_s *sli4, void *buf, size_t size,
		      struct efc_dma_s *dma);
extern int
sli_cmd_read_nvparms(struct sli4_s *sli4, void *buf, size_t size);
extern int
sli_cmd_write_nvparms(struct sli4_s *sli4, void *buf, size_t size,
		      u8 *wwpn, u8 *wwnn, u8 hard_alpa,
		      u32 preferred_d_id);
struct sli4_cmd_rq_cfg_s {
	__le16	rq_id;
	u8	r_ctl_mask;
	u8	r_ctl_match;
	u8	type_mask;
	u8	type_match;
};

extern int
sli_cmd_reg_fcfi(struct sli4_s *sli4, void *buf, size_t size,
		 u16 index,
		struct sli4_cmd_rq_cfg_s rq_cfg[SLI4_CMD_REG_FCFI_NUM_RQ_CFG]);
extern int
sli_cmd_reg_fcfi_mrq(struct sli4_s *sli4, void *buf, size_t size,
		     u8 mode, u16 fcf_index,
	    u8 rq_selection_policy, u8 mrq_bit_mask,
	    u16 num_mrqs,
	    struct sli4_cmd_rq_cfg_s rq_cfg[SLI4_CMD_REG_FCFI_NUM_RQ_CFG]);

extern int
sli_cmd_reg_rpi(struct sli4_s *sli4, void *buf, size_t size,
		u32 nport_id, u16 rpi, u16 vpi,
		     struct efc_dma_s *dma, u8 update,
		     u8 enable_t10_pi);
extern int
sli_cmd_sli_config(struct sli4_s *sli4, void *buf, size_t size,
		   u32 length, struct efc_dma_s *dma);
extern int
sli_cmd_unreg_fcfi(struct sli4_s *sli4, void *buf, size_t size,
		   u16 indicator);
extern int
sli_cmd_unreg_rpi(struct sli4_s *sli4, void *buf, size_t size,
		  u16 indicator,
		  enum sli4_resource_e which, u32 fc_id);
extern int
sli_cmd_reg_vpi(struct sli4_s *sli4, void *buf, size_t size,
		u32 fc_id, __be64 sli_wwpn, u16 vpi, u16 vfi,
		bool update);
extern int
sli_cmd_reg_vfi(struct sli4_s *sli4, void *buf, size_t size,
		u16 vfi, u16 fcfi, struct efc_dma_s dma,
		u16 vpi, __be64 sli_wwpn, u32 fc_id);
extern int
sli_cmd_unreg_vpi(struct sli4_s *sli4, void *buf, size_t size,
		  u16 indicator, u32 which);
extern int
sli_cmd_unreg_vfi(struct sli4_s *sli4, void *buf, size_t size,
		  u16 index, u32 which);
extern int
sli_cmd_common_nop(struct sli4_s *sli4, void *buf, size_t size,
		   uint64_t context);
extern int
sli_cmd_common_get_resource_extent_info(struct sli4_s *sli4, void *buf,
					size_t size, u16 rtype);
extern int
sli_cmd_common_get_sli4_parameters(struct sli4_s *sli4,
				   void *buf, size_t size);
extern int
sli_cmd_common_write_object(struct sli4_s *sli4, void *buf, size_t size,
			    u16 noc, u16 eof, u32 desired_write_length,
		u32 offset, char *object_name, struct efc_dma_s *dma);
extern int
sli_cmd_common_delete_object(struct sli4_s *sli4, void *buf, size_t size,
			     char *object_name);
extern int
sli_cmd_common_read_object(struct sli4_s *sli4, void *buf, size_t size,
			   u32 desired_read_length, u32 offset,
			   char *object_name, struct efc_dma_s *dma);
extern int
sli_cmd_dmtf_exec_clp_cmd(struct sli4_s *sli4, void *buf, size_t size,
			  struct efc_dma_s *cmd, struct efc_dma_s *resp);
extern int
sli_cmd_common_set_dump_location(struct sli4_s *sli4,
				 void *buf, size_t size, bool query,
				 bool is_buffer_list,
				 struct efc_dma_s *buffer, u8 fdb);
extern int
sli_cmd_common_set_features(struct sli4_s *sli4, void *buf, size_t size,
			    u32 feature, u32 param_len,
			    void *parameter);

int sli_cqe_mq(void *buf);
int sli_cqe_async(struct sli4_s *sli4, void *buf);

extern int
sli_setup(struct sli4_s *sli4, void *os, struct pci_dev  *pdev,
	  void __iomem *reg[]);
void sli_calc_max_qentries(struct sli4_s *sli4);
int sli_init(struct sli4_s *sli4);
int sli_reset(struct sli4_s *sli4);
int sli_fw_reset(struct sli4_s *sli4);
int sli_teardown(struct sli4_s *sli4);
extern int
sli_callback(struct sli4_s *sli4, enum sli4_callback_e which,
	     void *func, void *arg);
extern int
sli_bmbx_command(struct sli4_s *sli4);
extern int
__sli_queue_init(struct sli4_s *sli4, struct sli4_queue_s *q,
		 u32 qtype, size_t size, u32 n_entries,
		      u32 align);
extern int
__sli_create_queue(struct sli4_s *sli4, struct sli4_queue_s *q);
extern int
sli_eq_modify_delay(struct sli4_s *sli4, struct sli4_queue_s *eq,
		    u32 num_eq, u32 shift, u32 delay_mult);
extern int
sli_queue_alloc(struct sli4_s *sli4, u32 qtype,
		struct sli4_queue_s *q, u32 n_entries,
		     struct sli4_queue_s *assoc, u16 ulp);
extern int
sli_cq_alloc_set(struct sli4_s *sli4, struct sli4_queue_s *qs[],
		 u32 num_cqs, u32 n_entries, struct sli4_queue_s *eqs[]);
extern int
sli_get_queue_entry_size(struct sli4_s *sli4, u32 qtype);
extern int
sli_queue_free(struct sli4_s *sli4, struct sli4_queue_s *q,
	       u32 destroy_queues, u32 free_memory);
extern int
sli_queue_eq_arm(struct sli4_s *sli4, struct sli4_queue_s *q, bool arm);
extern int
sli_queue_arm(struct sli4_s *sli4, struct sli4_queue_s *q, bool arm);

extern int
sli_wq_write(struct sli4_s *sli4, struct sli4_queue_s *q,
	     u8 *entry);
extern int
sli_mq_write(struct sli4_s *sli4, struct sli4_queue_s *q,
	     u8 *entry);
extern int
sli_rq_write(struct sli4_s *sli4, struct sli4_queue_s *q,
	     u8 *entry);
extern int
sli_eq_read(struct sli4_s *sli4, struct sli4_queue_s *q,
	    u8 *entry);
extern int
sli_cq_read(struct sli4_s *sli4, struct sli4_queue_s *q,
	    u8 *entry);
extern int
sli_mq_read(struct sli4_s *sli4, struct sli4_queue_s *q,
	    u8 *entry);
extern int
sli_queue_index(struct sli4_s *sli4, struct sli4_queue_s *q);
extern int
_sli_queue_poke(struct sli4_s *sli4, struct sli4_queue_s *q,
		u32 index, u8 *entry);
extern int
sli_queue_poke(struct sli4_s *sli4, struct sli4_queue_s *q, u32 index,
	       u8 *entry);
extern int
sli_resource_alloc(struct sli4_s *sli4, enum sli4_resource_e rtype,
		   u32 *rid, u32 *index);
extern int
sli_resource_free(struct sli4_s *sli4, enum sli4_resource_e rtype,
		  u32 rid);
extern int
sli_resource_reset(struct sli4_s *sli4, enum sli4_resource_e rtype);
extern int
sli_eq_parse(struct sli4_s *sli4, u8 *buf, u16 *cq_id);
extern int
sli_cq_parse(struct sli4_s *sli4, struct sli4_queue_s *cq, u8 *cqe,
	     enum sli4_qentry_e *etype, u16 *q_id);

int sli_raise_ue(struct sli4_s *sli4, u8 dump);
int sli_dump_is_ready(struct sli4_s *sli4);
int sli_dump_is_present(struct sli4_s *sli4);
int sli_reset_required(struct sli4_s *sli4);
int sli_fw_ready(struct sli4_s *sli4);
int sli_link_is_configurable(struct sli4_s *sli);

/**
 * @brief Maximum value for a FCFI
 *
 * Note that although most commands provide a 16 bit field for the FCFI,
 * the FC/FCoE Asynchronous Recived CQE format only provides 6 bits for
 * the returned FCFI. Then effectively, the FCFI cannot be larger than
 * 1 << 6 or 64.
 */
#define SLI4_MAX_FCFI	64

/**
 * @brief Maximum value for FCF index
 *
 * The SLI-4 specification uses a 16 bit field in most places for the FCF
 * index, but practically, this value will be much smaller. Arbitrarily
 * limit the max FCF index to match the max FCFI value.
 */
#define SLI4_MAX_FCF_INDEX	SLI4_MAX_FCFI

/*************************************************************************
 * SLI-4 FC mailbox command formats and definitions.
 */

/**
 * FC/FCoE opcode (OPC) values.
 */
#define SLI4_OPC_WQ_CREATE			0x1
#define SLI4_OPC_WQ_DESTROY		0x2
#define SLI4_OPC_POST_SGL_PAGES		0x3
#define SLI4_OPC_RQ_CREATE			0x5
#define SLI4_OPC_RQ_DESTROY		0x6
#define SLI4_OPC_READ_FCF_TABLE		0x8
#define SLI4_OPC_POST_HDR_TEMPLATES	0xb
#define SLI4_OPC_REDISCOVER_FCF		0x10

/* Use the default CQ associated with the WQ */
#define SLI4_CQ_DEFAULT 0xffff

struct sli4_physical_page_descriptor_s {
	__le32		low;
	__le32		high;
};

/**
 * @brief WQ_CREATE
 *
 * Create a Work Queue for FC.
 */

struct sli4_req_wq_create_s {
	struct sli4_req_hdr_s	hdr;
	u8		num_pages;
	u8		dua_byte;
	__le16		cq_id;
#define SLI4_WQ_CREATE_V0_MAX_PAGES	4
	struct sli4_physical_page_descriptor_s
		   page_physical_address[SLI4_WQ_CREATE_V0_MAX_PAGES];
	u8		bqu_byte;
	u8		ulp;
	__le16		rsvd;
};

/**
 * @brief WQ_CREATE_V1
 *
 * Create a version 1 Work Queue for FC/FCoE use.
 */
struct sli4_req_wq_create_v1_s {
	struct sli4_req_hdr_s	hdr;
	__le16		num_pages;
	__le16		cq_id;
	u8		page_size;
	u8		wqe_size_byte;
	__le16		wqe_count;
	__le32		rsvd;
#define SLI4_WQ_CREATE_V1_MAX_PAGES	8
	struct sli4_physical_page_descriptor_s
			page_physical_address[SLI4_WQ_CREATE_V1_MAX_PAGES];
};

/**
 * @brief WQ_DESTROY
 *
 * Destroy an FC/FCoE Work Queue.
 */
struct sli4_req_wq_destroy_s {
	struct sli4_req_hdr_s	hdr;
	__le16		wq_id;
	__le16		rsvd;
};

/**
 * @brief POST_SGL_PAGES
 *
 * Register the scatter gather list (SGL) memory and associate it with an XRI.
 */
struct sli4_req_post_sgl_pages_s {
	struct sli4_req_hdr_s	hdr;
	__le16		xri_start;
	__le16		xri_count;
	struct {
		__le32		page0_low;
		__le32		page0_high;
		__le32		page1_low;
		__le32		page1_high;
	} page_set[10];
};

/**
 * @brief RQ_CREATE
 *
 * Create a Receive Queue for FC.
 */
enum {
	SLI4_RQCREATE_DUA = 0x1,
	SLI4_RQCREATE_BQU = 0x2,
};

struct sli4_req_rq_create_s {
	struct sli4_req_hdr_s	hdr;
	__le16		num_pages;
	u8		dua_bqu_byte;
	u8		ulp;
	__le16		rsvd16;
	u8		rqe_count_byte;
	u8		rsvd19;
	__le32		rsvd20;
	__le16		buffer_size;
	__le16		cq_id;
	__le32		rsvd28;
#define SLI4_RQE_SIZE			8
#define SLI4_RQE_SIZE_8			0x2
#define SLI4_RQE_SIZE_16		0x3
#define SLI4_RQE_SIZE_32		0x4
#define SLI4_RQE_SIZE_64		0x5
#define SLI4_RQE_SIZE_128		0x6

#define SLI4_RQ_PAGE_SIZE_4096		0x1
#define SLI4_RQ_PAGE_SIZE_8192		0x2
#define SLI4_RQ_PAGE_SIZE_16384		0x4
#define SLI4_RQ_PAGE_SIZE_32768		0x8
#define SLI4_RQ_PAGE_SIZE_64536		0x10

#define SLI4_RQ_CREATE_V0_MAX_PAGES	8
#define SLI4_RQ_CREATE_V0_MIN_BUF_SIZE	128
#define SLI4_RQ_CREATE_V0_MAX_BUF_SIZE	2048
	struct sli4_physical_page_descriptor_s
			page_physical_address[SLI4_RQ_CREATE_V0_MAX_PAGES];
};

/**
 * @brief RQ_CREATE_V1
 *
 * Create a version 1 Receive Queue for FC.
 */
enum {
	SLI4_RQCREATEV1_DNB = 0x80,
};

struct sli4_req_rq_create_v1_s {
	struct sli4_req_hdr_s	hdr;
	__le16		num_pages;
	u8		rsvd14;
	u8		dim_dfd_dnb;
	u8		page_size;
	u8		rqe_size_byte;
	__le16		rqe_count;
	__le32		rsvd20;
	__le16		rsvd24;
	__le16		cq_id;
	__le32		buffer_size;
#define SLI4_RQ_CREATE_V1_MAX_PAGES	8
#define SLI4_RQ_CREATE_V1_MIN_BUF_SIZE	64
#define SLI4_RQ_CREATE_V1_MAX_BUF_SIZE	2048
	struct sli4_physical_page_descriptor_s
			page_physical_address[SLI4_RQ_CREATE_V1_MAX_PAGES];
};

/**
 * @brief RQ_CREATE_V2
 *
 * Create a version 2 Receive Queue for FC/FCoE use.
 */
enum {
	SLI4_RQCREATEV2_DNB = 0x80,
};

struct sli4_req_rq_create_v2_s {
	struct sli4_req_hdr_s	hdr;
	__le16		num_pages;
	u8		rq_count;
	u8		dim_dfd_dnb;
	u8		page_size;
	u8		rqe_size_byte;
	__le16		rqe_count;
	__le16		hdr_buffer_size;
	__le16		payload_buffer_size;
	__le16		base_cq_id;
	__le16		rsvd26;
	__le32		rsvd42;
	struct sli4_physical_page_descriptor_s page_physical_address[0];
};

/**
 * @brief RQ_DESTROY
 *
 * Destroy an FC/FCoE Receive Queue.
 */
struct sli4_req_rq_destroy_s {
	struct sli4_req_hdr_s	hdr;
	__le16		rq_id;
	__le16		rsvd;
};

/**
 * @brief READ_FCF_TABLE
 *
 * Retrieve a FCF database (also known as a table) entry created by the SLI Port
 * during FIP discovery.
 */
struct sli4_req_read_fcf_table_s {
	struct sli4_req_hdr_s	hdr;
	__le16		fcf_index;
	__le16		rsvd;
};

/* A FCF index of -1 on the request means return the first valid entry */
#define SLI4_FCF_TABLE_FIRST		(U16_MAX)

/**
 * @brief FCF table entry
 *
 * This is the information returned by the READ_FCF_TABLE command.
 */
enum {
	SLI4_FCF_VAL	= 0x1,
	SLI4_FCF_FC	= 0x2,
	SLI4_FCF_SOL	= 0x80,
};

struct sli4_fcf_entry_s {
	__le32		max_receive_size;
	__le32		fip_keep_alive;
	__le32		fip_priority;
	u8		fcf_mac_address[6];
	u8		fcf_available;
	u8		mac_address_provider;
	u8		fabric_name_id[8];
	u8		fc_map[3];
	u8		val_fc_sol;
	__le16		fcf_index;
	__le16		fcf_state;
	u8		vlan_bitmap[512];
	u8		switch_name[8];
};

/**
 * @brief READ_FCF_TABLE response.
 */
struct sli4_res_read_fcf_table_s {
	struct sli4_res_hdr_s	hdr;
	__le32		event_tag;
	__le16		next_index;
	__le16		rsvd;
	struct sli4_fcf_entry_s fcf_entry;
};

/* A next FCF index of -1 in the response means this is the last valid entry */
#define SLI4_FCF_TABLE_LAST		(U16_MAX)

/**
 * @brief POST_HDR_TEMPLATES
 */
struct sli4_req_post_hdr_templates_s {
	struct sli4_req_hdr_s	hdr;
	__le16		rpi_offset;
	__le16		page_count;
	struct sli4_physical_page_descriptor_s page_descriptor[0];
};

#define SLI4_HDR_TEMPLATE_SIZE	64

/**
 * @brief REDISCOVER_FCF
 */
struct sli4_req_rediscover_fcf_s {
	struct sli4_req_hdr_s	hdr;
	__le16		fcf_count;
	__le16		rsvd14;
	__le32		rsvd16;
	__le16		fcf_index[16];
};

/**
 * Work Queue Entry (WQE) types.
 */
#define SLI4_WQE_ABORT			0x0f
#define SLI4_WQE_ELS_REQUEST64		0x8a
#define SLI4_WQE_FCP_IBIDIR64		0xac
#define SLI4_WQE_FCP_IREAD64		0x9a
#define SLI4_WQE_FCP_IWRITE64		0x98
#define SLI4_WQE_FCP_ICMND64		0x9c
#define SLI4_WQE_FCP_TRECEIVE64		0xa1
#define SLI4_WQE_FCP_CONT_TRECEIVE64	0xe5
#define SLI4_WQE_FCP_TRSP64		0xa3
#define SLI4_WQE_FCP_TSEND64		0x9f
#define SLI4_WQE_GEN_REQUEST64		0xc2
#define SLI4_WQE_SEND_FRAME		0xe1
#define SLI4_WQE_XMIT_BCAST64		0X84
#define SLI4_WQE_XMIT_BLS_RSP		0x97
#define SLI4_WQE_ELS_RSP64		0x95
#define SLI4_WQE_XMIT_SEQUENCE64	0x82
#define SLI4_WQE_REQUEUE_XRI		0x93

/**
 * WQE command types.
 */
#define SLI4_CMD_FCP_IREAD64_WQE	0x00
#define SLI4_CMD_FCP_ICMND64_WQE	0x00
#define SLI4_CMD_FCP_IWRITE64_WQE	0x01
#define SLI4_CMD_FCP_TRECEIVE64_WQE	0x02
#define SLI4_CMD_FCP_TRSP64_WQE		0x03
#define SLI4_CMD_FCP_TSEND64_WQE	0x07
#define SLI4_CMD_GEN_REQUEST64_WQE	0x08
#define SLI4_CMD_XMIT_BCAST64_WQE	0x08
#define SLI4_CMD_XMIT_BLS_RSP64_WQE	0x08
#define SLI4_CMD_ABORT_WQE		0x08
#define SLI4_CMD_XMIT_SEQUENCE64_WQE	0x08
#define SLI4_CMD_REQUEUE_XRI_WQE	0x0A
#define SLI4_CMD_SEND_FRAME_WQE		0x0a

#define SLI4_WQE_SIZE			0x05
#define SLI4_WQE_EXT_SIZE		0x06

#define SLI4_WQE_BYTES			(16 * sizeof(u32))
#define SLI4_WQE_EXT_BYTES		(32 * sizeof(u32))

/* Mask for ccp (CS_CTL) */
#define SLI4_MASK_CCP	0xfe /* Upper 7 bits of CS_CTL is priority */

/**
 * @brief Generic WQE
 */
enum {
	SLI4_GEN_WQE_EBDECNT	= (0xf << 0),	/* DW10W0 */
	SLI4_GEN_WQE_LEN_LOC	= (0x3 << 7),
	SLI4_GEN_WQE_QOSD	= (1 << 9),
	SLI4_GEN_WQE_XBL	= (1 << 11),
	SLI4_GEN_WQE_HLM	= (1 << 12),
	SLI4_GEN_WQE_IOD	= (1 << 13),
	SLI4_GEN_WQE_DBDE	= (1 << 14),
	SLI4_GEN_WQE_WQES	= (1 << 15),

	SLI4_GEN_WQE_PRI	= (0x7),
	SLI4_GEN_WQE_PV		= (1 << 3),
	SLI4_GEN_WQE_EAT	= (1 << 4),
	SLI4_GEN_WQE_XC		= (1 << 5),
	SLI4_GEN_WQE_CCPE	= (1 << 7),

	SLI4_GEN_WQE_CMDTYPE	= (0xf),
	SLI4_GEN_WQE_WQEC	= (1 << 7),
};

struct sli4_generic_wqe_s {
	__le32		cmd_spec0_5[6];
	__le16		xri_tag;
	__le16		context_tag;
	u8		ct_byte;
	u8		command;
	u8		class_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		rsvd34;
	__le16		dw10w0_flags;
	u8		eat_xc_ccpe;
	u8		ccp;
	u8		cmdtype_wqec_byte;
	u8		rsvd41;
	__le16		cq_id;
};

/**
 * @brief WQE used to abort exchanges.
 */
enum {
	SLI4_ABRT_WQE_IR	= 0x02,

	SLI4_ABRT_WQE_EBDECNT	= (0xf << 0),	/* DW10W0 */
	SLI4_ABRT_WQE_LEN_LOC	= (0x3 << 7),
	SLI4_ABRT_WQE_QOSD	= (1 << 9),
	SLI4_ABRT_WQE_XBL	= (1 << 11),
	SLI4_ABRT_WQE_IOD	= (1 << 13),
	SLI4_ABRT_WQE_DBDE	= (1 << 14),
	SLI4_ABRT_WQE_WQES	= (1 << 15),

	SLI4_ABRT_WQE_PRI	= (0x7),
	SLI4_ABRT_WQE_PV	= (1 << 3),
	SLI4_ABRT_WQE_EAT	= (1 << 4),
	SLI4_ABRT_WQE_XC	= (1 << 5),
	SLI4_ABRT_WQE_CCPE	= (1 << 7),

	SLI4_ABRT_WQE_CMDTYPE	= (0xf),
	SLI4_ABRT_WQE_WQEC	= (1 << 7),
};

struct sli4_abort_wqe_s {
	__le32		rsvd0;
	__le32		rsvd4;
	__le32		ext_t_tag;
	u8		ia_ir_byte;
	u8		criteria;
	__le16		rsvd10;
	__le32		ext_t_mask;
	__le32		t_mask;
	__le16		xri_tag;
	__le16		context_tag;
	u8		ct_byte;
	u8		command;
	u8		class_byte;
	u8		timer;
	__le32		t_tag;
	__le16		request_tag;
	__le16		rsvd34;
	__le16		dw10w0_flags;
	u8		eat_xc_ccpe;
	u8		ccp;
	u8		cmdtype_wqec_byte;
	u8		rsvd41;
	__le16		cq_id;
};

#define SLI4_ABORT_CRITERIA_XRI_TAG		0x01
#define SLI4_ABORT_CRITERIA_ABORT_TAG		0x02
#define SLI4_ABORT_CRITERIA_REQUEST_TAG		0x03
#define SLI4_ABORT_CRITERIA_EXT_ABORT_TAG	0x04

enum sli4_abort_type_e {
	SLI_ABORT_XRI,
	SLI_ABORT_ABORT_ID,
	SLI_ABORT_REQUEST_ID,
	SLI_ABORT_MAX,		/* must be last */
};

/**
 * @brief WQE used to create an ELS request.
 */
enum {
	SLI4_REQ_WQE_QOSD	= 0x2,
	SLI4_REQ_WQE_DBDE	= 0x40,
	SLI4_REQ_WQE_XBL	= 0x8,
	SLI4_REQ_WQE_XC		= 0x20,
	SLI4_REQ_WQE_IOD	= 0x20,
	SLI4_REQ_WQE_HLM	= 0x10,
	SLI4_REQ_WQE_CCPE	= 0x80,
	SLI4_REQ_WQE_EAT	= 0x10,
	SLI4_REQ_WQE_WQES	= 0x80,
	SLI4_REQ_WQE_PU_SHFT	= 4,
	SLI4_REQ_WQE_CT_SHFT	= 2,
	SLI4_REQ_WQE_CT		= 0xc,
	SLI4_REQ_WQE_ELSID_SHFT	= 4,
	SLI4_REQ_WQE_SP_SHFT	= 24,
	SLI4_REQ_WQE_LEN_LOC_BIT1 = 0x80,
	SLI4_REQ_WQE_LEN_LOC_BIT2 = 0x1,
};

struct sli4_els_request64_wqe_s {
	struct sli4_bde_s	els_request_payload;
	__le32		els_request_payload_length;
	__le32		sid_sp_dword;
	__le32		remote_id_dword;
	__le16		xri_tag;
	__le16		context_tag;
	u8		ct_byte;
	u8		command;
	u8		class_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		temporary_rpi;
	u8		len_loc1_byte;
	u8		qosd_xbl_hlm_iod_dbde_wqes;
	u8		eat_xc_ccpe;
	u8		ccp;
	u8		cmdtype_elsid_byte;
	u8		rsvd41;
	__le16		cq_id;
	struct sli4_bde_s	els_response_payload_bde;
	__le32		max_response_payload_length;
};

#define SLI4_GENERIC_CONTEXT_RPI	0x0
#define SLI4_GENERIC_CONTEXT_VPI	0x1
#define SLI4_GENERIC_CONTEXT_VFI	0x2
#define SLI4_GENERIC_CONTEXT_FCFI	0x3

#define SLI4_GENERIC_CLASS_CLASS_2	0x1
#define SLI4_GENERIC_CLASS_CLASS_3	0x2

#define SLI4_ELS_REQUEST64_DIR_WRITE	0x0
#define SLI4_ELS_REQUEST64_DIR_READ	0x1

#define SLI4_ELS_REQUEST64_OTHER	0x0
#define SLI4_ELS_REQUEST64_LOGO		0x1
#define SLI4_ELS_REQUEST64_FDISC	0x2
#define SLI4_ELS_REQUEST64_FLOGIN	0x3
#define SLI4_ELS_REQUEST64_PLOGI	0x4

#define SLI4_ELS_REQUEST64_CMD_GEN		0x08
#define SLI4_ELS_REQUEST64_CMD_NON_FABRIC	0x0c
#define SLI4_ELS_REQUEST64_CMD_FABRIC		0x0d

/**
 * @brief WQE used to create an FCP initiator no data command.
 */
enum {
	SLI4_ICMD_WQE_DBDE	= 0x40,
	SLI4_ICMD_WQE_XBL	= 0x8,
	SLI4_ICMD_WQE_XC	= 0x20,
	SLI4_ICMD_WQE_IOD	= 0x20,
	SLI4_ICMD_WQE_HLM	= 0x10,
	SLI4_ICMD_WQE_CCPE	= 0x80,
	SLI4_ICMD_WQE_EAT	= 0x10,
	SLI4_ICMD_WQE_APPID	= 0x10,
	SLI4_ICMD_WQE_WQES	= 0x80,
	SLI4_ICMD_WQE_PU_SHFT	= 4,
	SLI4_ICMD_WQE_CT_SHFT	= 2,
	SLI4_ICMD_WQE_BS_SHFT	= 4,
	SLI4_ICMD_WQE_LEN_LOC_BIT1 = 0x80,
	SLI4_ICMD_WQE_LEN_LOC_BIT2 = 0x1,
};

struct sli4_fcp_icmnd64_wqe_s {
	struct sli4_bde_s	bde;
	__le16		payload_offset_length;
	__le16		fcp_cmd_buffer_length;
	__le32		rsvd12;
	__le32		remote_n_port_id_dword;
	__le16		xri_tag;
	__le16		context_tag;
	u8		dif_ct_bs_byte;
	u8		command;
	u8		class_pu_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		rsvd34;
	u8		len_loc1_byte;
	u8		qosd_xbl_hlm_iod_dbde_wqes;
	u8		eat_xc_ccpe;
	u8		ccp;
	u8		cmd_type_byte;
	u8		rsvd41;
	__le16		cq_id;
	__le32		rsvd44;
	__le32		rsvd48;
	__le32		rsvd52;
	__le32		rsvd56;
};

/**
 * @brief WQE used to create an FCP initiator read.
 */
enum {
	SLI4_IR_WQE_DBDE	= 0x40,
	SLI4_IR_WQE_XBL		= 0x8,
	SLI4_IR_WQE_XC		= 0x20,
	SLI4_IR_WQE_IOD		= 0x20,
	SLI4_IR_WQE_HLM		= 0x10,
	SLI4_IR_WQE_CCPE	= 0x80,
	SLI4_IR_WQE_EAT		= 0x10,
	SLI4_IR_WQE_APPID	= 0x10,
	SLI4_IR_WQE_WQES	= 0x80,
	SLI4_IR_WQE_PU_SHFT	= 4,
	SLI4_IR_WQE_CT_SHFT	= 2,
	SLI4_IR_WQE_BS_SHFT	= 4,
	SLI4_IR_WQE_LEN_LOC_BIT1 = 0x80,
	SLI4_IR_WQE_LEN_LOC_BIT2 = 0x1,
};

struct sli4_fcp_iread64_wqe_s {
	struct sli4_bde_s	bde;
	__le16		payload_offset_length;
	__le16		fcp_cmd_buffer_length;

	__le32		total_transfer_length;

	__le32		remote_n_port_id_dword;

	__le16		xri_tag;
	__le16		context_tag;

	u8		dif_ct_bs_byte;
	u8		command;
	u8		class_pu_byte;
	u8		timer;

	__le32		abort_tag;

	__le16		request_tag;
	__le16		rsvd34;

	u8		len_loc1_byte;
	u8		qosd_xbl_hlm_iod_dbde_wqes;
	u8		eat_xc_ccpe;
	u8		ccp;

	u8		cmd_type_byte;
	u8		rsvd41;
	__le16		cq_id;

	__le32		rsvd44;
	/* reserved if performance hints disabled */
	struct sli4_bde_s	first_data_bde;
};

/**
 * @brief WQE used to create an FCP initiator write.
 */
enum {
	SLI4_IWR_WQE_DBDE	= 0x40,
	SLI4_IWR_WQE_XBL	= 0x8,
	SLI4_IWR_WQE_XC		= 0x20,
	SLI4_IWR_WQE_IOD	= 0x20,
	SLI4_IWR_WQE_HLM	= 0x10,
	SLI4_IWR_WQE_DNRX	= 0x10,
	SLI4_IWR_WQE_CCPE	= 0x80,
	SLI4_IWR_WQE_EAT	= 0x10,
	SLI4_IWR_WQE_APPID	= 0x10,
	SLI4_IWR_WQE_WQES	= 0x80,
	SLI4_IWR_WQE_PU_SHFT	= 4,
	SLI4_IWR_WQE_CT_SHFT	= 2,
	SLI4_IWR_WQE_BS_SHFT	= 4,
	SLI4_IWR_WQE_LEN_LOC_BIT1 = 0x80,
	SLI4_IWR_WQE_LEN_LOC_BIT2 = 0x1,
};

struct sli4_fcp_iwrite64_wqe_s {
	struct sli4_bde_s	bde;
	__le16		payload_offset_length;
	__le16		fcp_cmd_buffer_length;
	__le16		total_transfer_length;
	__le16		initial_transfer_length;
	__le16		xri_tag;
	__le16		context_tag;
	u8		dif_ct_bs_byte;
	u8		command;
	u8		class_pu_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		rsvd34;
	u8		len_loc1_byte;
	u8		qosd_xbl_hlm_iod_dbde_wqes;
	u8		eat_xc_ccpe;
	u8		ccp;
	u8		cmd_type_byte;
	u8		rsvd41;
	__le16		cq_id;
	__le32		remote_n_port_id_dword;
	struct sli4_bde_s	first_data_bde;
};

struct sli4_fcp_128byte_wqe_s {
	u32 dw[32];
};

/**
 * @brief WQE used to create an FCP target receive, and FCP target
 * receive continue.
 */
enum {
	SLI4_TRCV_WQE_DBDE	= 0x40,
	SLI4_TRCV_WQE_XBL	= 0x8,
	SLI4_TRCV_WQE_AR	= 0x8,
	SLI4_TRCV_WQE_XC	= 0x20,
	SLI4_TRCV_WQE_IOD	= 0x20,
	SLI4_TRCV_WQE_HLM	= 0x10,
	SLI4_TRCV_WQE_DNRX	= 0x10,
	SLI4_TRCV_WQE_CCPE	= 0x80,
	SLI4_TRCV_WQE_EAT	= 0x10,
	SLI4_TRCV_WQE_APPID	= 0x10,
	SLI4_TRCV_WQE_WQES	= 0x80,
	SLI4_TRCV_WQE_PU_SHFT	= 4,
	SLI4_TRCV_WQE_CT_SHFT	= 2,
	SLI4_TRCV_WQE_BS_SHFT	= 4,
	SLI4_TRCV_WQE_LEN_LOC_BIT2 = 0x1,
};

struct sli4_fcp_treceive64_wqe_s {
	struct sli4_bde_s	bde;
	__le32		payload_offset_length;
	__le32		relative_offset;
	/**
	 * DWord 5 can either be the task retry identifier (HLM=0) or
	 * the remote N_Port ID (HLM=1),the secondary xri tag
	 */
	union {
		__le16		sec_xri_tag;
		__le16		rsvd;
		__le32		dword;
	} dword5;
	__le16		xri_tag;
	__le16		context_tag;
	u8		dif_ct_bs_byte;
	u8		command;
	u8		class_ar_pu_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		remote_xid;
	u8		lloc1_appid;
	u8		qosd_xbl_hlm_iod_dbde_wqes;
	u8		eat_xc_ccpe;
	u8		ccp;
	u8		cmd_type_byte;
	u8		rsvd41;
	__le16		cq_id;
	__le32		fcp_data_receive_length;
	struct sli4_bde_s	first_data_bde; /* For performance hints */
};

/**
 * @brief WQE used to create an FCP target response.
 */
enum {
	SLI4_TRSP_WQE_AG	= 0x8,
	SLI4_TRSP_WQE_DBDE	= 0x40,
	SLI4_TRSP_WQE_XBL	= 0x8,
	SLI4_TRSP_WQE_XC	= 0x20,
	SLI4_TRSP_WQE_HLM	= 0x10,
	SLI4_TRSP_WQE_DNRX	= 0x10,
	SLI4_TRSP_WQE_CCPE	= 0x80,
	SLI4_TRSP_WQE_EAT	= 0x10,
	SLI4_TRSP_WQE_APPID	= 0x10,
	SLI4_TRSP_WQE_WQES	= 0x80,
};

struct sli4_fcp_trsp64_wqe_s {
	struct sli4_bde_s	bde;
	__le32		fcp_response_length;
	__le32		rsvd12;
	/**
	 * DWord 5 can either be the task retry identifier (HLM=0) or
	 * the remote N_Port ID (HLM=1)
	 */
	__le32		dword5;
	__le16		xri_tag;
	__le16		rpi;
	u8		ct_dnrx_byte;
	u8		command;
	u8		class_ag_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		remote_xid;
	u8		lloc1_appid;
	u8		qosd_xbl_hlm_dbde_wqes;
	u8		eat_xc_ccpe;
	u8		ccp;
	u8		cmd_type_byte;
	u8		rsvd41;
	__le16		cq_id;
	__le32		rsvd44;
	__le32		rsvd48;
	__le32		rsvd52;
	__le32		rsvd56;
};

/**
 * @brief WQE used to create an FCP target send (DATA IN).
 */
enum {
	SLI4_TSEND_WQE_XBL	= 0x8,
	SLI4_TSEND_WQE_DBDE	= 0x40,
	SLI4_TSEND_WQE_IOD	= 0x20,
	SLI4_TSEND_WQE_QOSD	= 0x2,
	SLI4_TSEND_WQE_HLM	= 0x10,
	SLI4_TSEND_WQE_PU_SHFT	= 4,
	SLI4_TSEND_WQE_AR	= 0x8,
	SLI4_TSEND_CT_SHFT	= 2,
	SLI4_TSEND_BS_SHFT	= 4,
	SLI4_TSEND_LEN_LOC_BIT2 = 0x1,
	SLI4_TSEND_CCPE		= 0x80,
	SLI4_TSEND_APPID_VALID	= 0x20,
	SLI4_TSEND_WQES		= 0x80,
	SLI4_TSEND_XC		= 0x20,
	SLI4_TSEND_EAT		= 0x10,
};

struct sli4_fcp_tsend64_wqe_s {
	struct sli4_bde_s	bde;
	__le32		payload_offset_length;
	__le32		relative_offset;
	/**
	 * DWord 5 can either be the task retry identifier (HLM=0) or
	 * the remote N_Port ID (HLM=1)
	 */
	__le32		dword5;
	__le16		xri_tag;
	__le16		rpi;
	u8		ct_byte;
	u8		command;
	u8		class_pu_ar_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		remote_xid;
	u8		dw10byte0;
	u8		ll_qd_xbl_hlm_iod_dbde;
	u8		dw10byte2;
	u8		ccp;
	u8		cmd_type_byte;
	u8		rsvd45;
	__le16		cq_id;
	__le32		fcp_data_transmit_length;
	struct sli4_bde_s	first_data_bde; /* For performance hints */
};

/** The XRI associated with this IO is already active */
#define SLI4_IO_CONTINUATION		BIT(0)
/** Automatically generate a good RSP frame */
#define SLI4_IO_AUTO_GOOD_RESPONSE	BIT(1)
#define SLI4_IO_NO_ABORT		BIT(2)
/** Set the DNRX bit because no auto xref rdy buffer is posted */
#define SLI4_IO_DNRX			BIT(3)

/* WQE DIF field contents */
#define SLI4_DIF_DISABLED		0
#define SLI4_DIF_PASS_THROUGH		1
#define SLI4_DIF_STRIP			2
#define SLI4_DIF_INSERT			3

/**
 * @brief WQE used to create a general request.
 */
enum {
	SLI4_GEN_REQ64_WQE_XBL	= 0x8,
	SLI4_GEN_REQ64_WQE_DBDE	= 0x40,
	SLI4_GEN_REQ64_WQE_IOD	= 0x20,
	SLI4_GEN_REQ64_WQE_QOSD	= 0x2,
	SLI4_GEN_REQ64_WQE_HLM	= 0x10,
	SLI4_GEN_REQ64_CT_SHFT	= 2,
};

struct sli4_gen_request64_wqe_s {
	struct sli4_bde_s	bde;
	__le32		request_payload_length;
	__le32		relative_offset;
	u8		rsvd17;
	u8		df_ctl;
	u8		type;
	u8		r_ctl;
	__le16		xri_tag;
	__le16		context_tag;
	u8		ct_byte;
	u8		command;
	u8		class_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		rsvd34;
	u8		dw10flags0;
	u8		dw10flags1;
	u8		dw10flags2;
	u8		ccp;
	u8		cmd_type_byte;
	u8		rsvd41;
	__le16		cq_id;
	__le32		remote_n_port_id_dword;
	__le32		rsvd48;
	__le32		rsvd52;
	__le32		max_response_payload_length;
};

/**
 * @brief WQE used to create a send frame request.
 */
enum {
	SLI4_SF_WQE_DBDE	= 0x40,
	SLI4_SF_PU		= 0x30,
	SLI4_SF_CT		= 0xc,
	SLI4_SF_QOSD		= 0x2,
	SLI4_SF_LEN_LOC_BIT1	= 0x80,
	SLI4_SF_LEN_LOC_BIT2	= 0x1,
	SLI4_SF_XC		= 0x20,
	SLI4_SF_XBL		= 0x8,
};

struct sli4_send_frame_wqe_s {
	struct sli4_bde_s	bde;
	__le32		frame_length;
	__le32		fc_header_0_1[2];
	__le16		xri_tag;
	__le16		context_tag;
	u8		ct_byte;
	u8		command;
	u8		dw7flags0;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	u8		eof;
	u8		sof;
	u8		dw10flags0;
	u8		dw10flags1;
	u8		dw10flags2;
	u8		ccp;
	u8		cmd_type_byte;
	u8		rsvd41;
	__le16		cq_id;
	__le32		fc_header_2_5[4];
};

/**
 * @brief WQE used to create a transmit sequence.
 */
enum {
	SLI4_SEQ_WQE_DBDE	= 0x4000,
	SLI4_SEQ_WQE_XBL	= 0x800,
	SLI4_SEQ_WQE_SI		= 0x4,
	SLI4_SEQ_WQE_FT		= 0x8,
	SLI4_SEQ_WQE_XO		= 0x40,
	SLI4_SEQ_WQE_LS		= 0x80,
	SLI4_SEQ_WQE_DIF	= 0x3,
	SLI4_SEQ_WQE_BS		= 0x70,
	SLI4_SEQ_WQE_PU		= 0x30,
	SLI4_SEQ_WQE_HLM	= 0x1000,
	SLI4_SEQ_WQE_IOD_SHIFT	= 13,
	SLI4_SEQ_WQE_CT_SHIFT	= 2,
	SLI4_SEQ_WQE_LEN_LOC_SHIFT = 7,
};

struct sli4_xmit_sequence64_wqe_s {
	struct sli4_bde_s	bde;
	__le32		remote_n_port_id_dword;
	__le32		relative_offset;
	u8		dw5flags0;
	u8		df_ctl;
	u8		type;
	u8		r_ctl;
	__le16		xri_tag;
	__le16		context_tag;
	u8		dw7flags0;
	u8		command;
	u8		dw7flags1;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		remote_xid;
	__le16		dw10w0;
	u8		dw10flags0;
	u8		ccp;
	u8		cmd_type_wqec_byte;
	u8		rsvd45;
	__le16		cq_id;
	__le32		sequence_payload_len;
	__le32		rsvd48;
	__le32		rsvd52;
	__le32		rsvd56;
};

/**
 * @brief WQE used unblock the specified XRI and to release
 * it to the SLI Port's free pool.
 */
enum {
	SLI4_REQU_XRI_WQE_XC	= 0x20,
	SLI4_REQU_XRI_WQE_QOSD	= 0x2,
};

struct sli4_requeue_xri_wqe_s {
	__le32		rsvd0;
	__le32		rsvd4;
	__le32		rsvd8;
	__le32		rsvd12;
	__le32		rsvd16;
	__le32		rsvd20;
	__le16		xri_tag;
	__le16		context_tag;
	u8		ct_byte;
	u8		command;
	u8		class_byte;
	u8		timer;
	__le32		rsvd32;
	__le16		request_tag;
	__le16		rsvd34;
	__le16		flags0;
	__le16		flags1;
	__le16		flags2;
	u8		ccp;
	u8		cmd_type_wqec_byte;
	u8		rsvd42;
	__le16		cq_id;
	__le32		rsvd44;
	__le32		rsvd48;
	__le32		rsvd52;
	__le32		rsvd56;
};

/**
 * @brief WQE used to send a single frame sequence to broadcast address
 * SLI4_BCAST_WQE_DBDE:  dw10 bit15
 * BCAST_WQE_CT : dw7 bits 3,4
 * BCAST_WQE_LEN_LOC:  dw10 8,9
 * BCAST_WQE_IOD:  dw10 bit 13
 */
enum {
	SLI4_BCAST_WQE_DBDE		= 0x4000,
	SLI4_BCAST_WQE_CT_SHIFT		= 2,
	SLI4_BCAST_WQE_LEN_LOC_SHIFT	= 7,
	SLI4_BCAST_WQE_IOD_SHIFT	= 13,
};

struct sli4_xmit_bcast64_wqe_s {
	struct sli4_bde_s	sequence_payload;
	__le32		sequence_payload_length;
	__le32		rsvd16;
	u8		rsvd17;
	u8		df_ctl;
	u8		type;
	u8		r_ctl;
	__le16		xri_tag;
	__le16		context_tag;
	u8		ct_byte;
	u8		command;
	u8		dw7flags0;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		temporary_rpi;
	__le16		dw10w0;
	u8		dw10flags1;
	u8		ccp;
	u8		dw11flags0;
	u8		rsvd41;
	__le16		cq_id;
	__le32		rsvd44;
	__le32		rsvd45;
	__le32		rsvd46;
	__le32		rsvd47;
};

/**
 * @brief WQE used to create a BLS response.
 * SLI4_BLS_RSP_WQE_AR : 6th dword, bit 31
 * BLS_RSP_WQE_CT:  8th dword, bits 3 and 4
 * SLI4_BLS_RSP_WQE_QOSD:  dword 11, bit 10
 * SLI4_BLS_RSP_WQE_HLM:  dword 11, bit 13
 */
enum {
	SLI4_BLS_RSP_RID		= 0xffffff,
	SLI4_BLS_RSP_WQE_AR		= 0x40000000,
	SLI4_BLS_RSP_WQE_CT_SHFT	= 2,
	SLI4_BLS_RSP_WQE_QOSD		= 0x2,
	SLI4_BLS_RSP_WQE_HLM		= 0x10,
};

struct sli4_xmit_bls_rsp_wqe_s {
	__le32		payload_word0;
	__le16		rx_id;
	__le16		ox_id;
	__le16		high_seq_cnt;
	__le16		low_seq_cnt;
	__le32		rsvd12;
	__le32		local_n_port_id_dword;
	__le32		remote_id_dword;
	__le16		xri_tag;
	__le16		context_tag;
	u8		dw8flags0;
	u8		command;
	u8		dw8flags1;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		rsvd38;
	u8		dw11flags0;
	u8		dw11flags1;
	u8		dw11flags2;
	u8		ccp;
	u8		dw12flags0;
	u8		rsvd45;
	__le16		cq_id;
	__le16		temporary_rpi;
	u8		rsvd50;
	u8		rsvd51;
	__le32		rsvd52;
	__le32		rsvd56;
	__le32		rsvd60;
};

enum sli_bls_type_e {
	SLI4_SLI_BLS_ACC,
	SLI4_SLI_BLS_RJT,
	SLI4_SLI_BLS_MAX
};

struct sli_bls_payload_s {
	enum sli_bls_type_e	type;
	__le16		ox_id;
	__le16		rx_id;
	union {
		struct {
			u8		seq_id_validity;
			u8		seq_id_last;
			u8		rsvd2;
			u8		rsvd3;
			u16		ox_id;
			u16		rx_id;
			__le16		low_seq_cnt;
			__le16		high_seq_cnt;
		} acc;
		struct {
			u8		vendor_unique;
			u8		reason_explanation;
			u8		reason_code;
			u8		rsvd3;
		} rjt;
	} u;
};

/**
 * @brief WQE used to create an ELS response.
 * flags2 bits: rsvd, qosd, rsvd, xbl, hlm, iod, dbde, wqes
 * flags3 bits: pri : 3, pv , eat, xc, rsvd, ccpe
 */

enum {
	SLI4_ELS_SID		= 0xffffff,
	SLI4_ELS_RID		= 0xffffff,
	SLI4_ELS_DBDE		= 0x40,
	SLI4_ELS_XBL		= 0x8,
	SLI4_ELS_IOD		= 0x20,
	SLI4_ELS_QOSD		= 0x2,
	SLI4_ELS_XC		= 0x20,
	SLI4_ELS_CT_OFFSET	= 0X2,
	SLI4_ELS_SP		= 0X1000000,
	SLI4_ELS_HLM		= 0X10,
};

struct sli4_xmit_els_rsp64_wqe_s {
	struct sli4_bde_s	els_response_payload;
	__le32		els_response_payload_length;
	__le32		sid_dw;
	__le32		rid_dw;
	__le16		xri_tag;
	__le16		context_tag;
	u8		ct_byte;
	u8		command;
	u8		class_byte;
	u8		timer;
	__le32		abort_tag;
	__le16		request_tag;
	__le16		ox_id;
	u8		flags1;
	u8		flags2;
	u8		flags3;
	u8		flags4;
	u8		cmd_type_wqec;
	u8		rsvd34;
	__le16		cq_id;
	__le16		temporary_rpi;
	__le16		rsvd38;
	u32	rsvd40;
	u32	rsvd44;
	u32	rsvd48;
};

/**
 * @brief Asynchronouse Event :  Link State ACQE.
 */
enum {
	SLI4_LINK_TYPE = 0xc0,
};

struct sli4_link_state_s {
	u8		link_num_type;
	u8		port_link_status;
	u8		port_duplex;
	u8		port_speed;
	u8		port_fault;
	u8		rsvd5;
	__le16		logical_link_speed;
	__le32		event_tag;
	u8		rsvd12;
	u8		event_code;
	u8		event_type;
	u8		flags;
};

#define SLI4_LINK_ATTN_TYPE_LINK_UP		0x01
#define SLI4_LINK_ATTN_TYPE_LINK_DOWN		0x02
#define SLI4_LINK_ATTN_TYPE_NO_HARD_ALPA	0x03

#define SLI4_LINK_ATTN_P2P			0x01
#define SLI4_LINK_ATTN_FC_AL			0x02
#define SLI4_LINK_ATTN_INTERNAL_LOOPBACK	0x03
#define SLI4_LINK_ATTN_SERDES_LOOPBACK		0x04

#define SLI4_LINK_ATTN_1G			0x01
#define SLI4_LINK_ATTN_2G			0x02
#define SLI4_LINK_ATTN_4G			0x04
#define SLI4_LINK_ATTN_8G			0x08
#define SLI4_LINK_ATTN_10G			0x0a
#define SLI4_LINK_ATTN_16G			0x10

#define SLI4_SLI4_LINK_TYPE_ETHERNET		0x0
#define SLI4_SLI4_LINK_TYPE_FC			0x1

/**
 * @brief Asynchronouse Event :  FC Link Attention Event.
 */
struct sli4_link_attention_s {
	u8		link_number;
	u8		attn_type;
	u8		topology;
	u8		port_speed;
	u8		port_fault;
	u8		shared_link_status;
	__le16		logical_link_speed;
	__le32		event_tag;
	u8		rsvd12;
	u8		event_code;
	u8		event_type;
	u8		flags;
};

/**
 * @brief FC/FCoE event types.
 */
#define SLI4_LINK_STATE_PHYSICAL		0x00
#define SLI4_LINK_STATE_LOGICAL			0x01

#define SLI4_GRP5_QOS_SPEED			0x01

#define SLI4_FC_EVENT_LINK_ATTENTION		0x01
#define SLI4_FC_EVENT_SHARED_LINK_ATTENTION	0x02

#define SLI4_PORT_SPEED_NO_LINK			0x0
#define SLI4_PORT_SPEED_10_MBPS			0x1
#define SLI4_PORT_SPEED_100_MBPS		0x2
#define SLI4_PORT_SPEED_1_GBPS			0x3
#define SLI4_PORT_SPEED_10_GBPS			0x4

#define SLI4_PORT_DUPLEX_NONE			0x0
#define SLI4_PORT_DUPLEX_HWF			0x1
#define SLI4_PORT_DUPLEX_FULL			0x2

#define SLI4_PORT_LINK_STATUS_PHYSICAL_DOWN	0x0
#define SLI4_PORT_LINK_STATUS_PHYSICAL_UP	0x1
#define SLI4_PORT_LINK_STATUS_LOGICAL_DOWN	0x2
#define SLI4_PORT_LINK_STATUS_LOGICAL_UP	0x3

/**
 * @brief FC/FCoE WQ completion queue entry.
 */
enum {
	SLI4_WCQE_XB = 0x10,
	SLI4_WCQE_QX = 0x80,
};

struct sli4_fc_wcqe_s {
	u8		hw_status;
	u8		status;
	__le16		request_tag;
	__le32		wqe_specific_1;
	__le32		wqe_specific_2;
	u8		rsvd12;
	u8		qx_byte;
	u8		code;
	u8		flags;
};

/**
 * @brief FC/FCoE WQ consumed CQ queue entry.
 */
struct sli4_fc_wqec_s {
	__le32		rsvd0;
	__le32		rsvd1;
	__le16		wqe_index;
	__le16		wq_id;
	__le16		rsvd12;
	u8		code;
	u8		vld_byte;
};

/**
 * @brief FC/FCoE Completion Status Codes.
 */
#define SLI4_FC_WCQE_STATUS_SUCCESS		0x00
#define SLI4_FC_WCQE_STATUS_FCP_RSP_FAILURE	0x01
#define SLI4_FC_WCQE_STATUS_REMOTE_STOP		0x02
#define SLI4_FC_WCQE_STATUS_LOCAL_REJECT	0x03
#define SLI4_FC_WCQE_STATUS_NPORT_RJT		0x04
#define SLI4_FC_WCQE_STATUS_FABRIC_RJT		0x05
#define SLI4_FC_WCQE_STATUS_NPORT_BSY		0x06
#define SLI4_FC_WCQE_STATUS_FABRIC_BSY		0x07
#define SLI4_FC_WCQE_STATUS_LS_RJT		0x09
#define SLI4_FC_WCQE_STATUS_CMD_REJECT		0x0b
#define SLI4_FC_WCQE_STATUS_FCP_TGT_LENCHECK	0x0c
#define SLI4_FC_WCQE_STATUS_RQ_BUF_LEN_EXCEEDED	0x11
#define SLI4_FC_WCQE_STATUS_RQ_INSUFF_BUF_NEEDED 0x12
#define SLI4_FC_WCQE_STATUS_RQ_INSUFF_FRM_DISC	0x13
#define SLI4_FC_WCQE_STATUS_RQ_DMA_FAILURE	0x14
#define SLI4_FC_WCQE_STATUS_FCP_RSP_TRUNCATE	0x15
#define SLI4_FC_WCQE_STATUS_DI_ERROR		0x16
#define SLI4_FC_WCQE_STATUS_BA_RJT		0x17
#define SLI4_FC_WCQE_STATUS_RQ_INSUFF_XRI_NEEDED 0x18
#define SLI4_FC_WCQE_STATUS_RQ_INSUFF_XRI_DISC	0x19
#define SLI4_FC_WCQE_STATUS_RX_ERROR_DETECT	0x1a
#define SLI4_FC_WCQE_STATUS_RX_ABORT_REQUEST	0x1b

/* driver generated status codes; better not overlap
 * with chip's status codes!
 */
#define SLI4_FC_WCQE_STATUS_TARGET_WQE_TIMEOUT  0xff
#define SLI4_FC_WCQE_STATUS_SHUTDOWN		0xfe
#define SLI4_FC_WCQE_STATUS_DISPATCH_ERROR	0xfd

/**
 * @brief DI_ERROR Extended Status
 */
#define SLI4_FC_DI_ERROR_GE	BIT(0) /* Guard Error */
#define SLI4_FC_DI_ERROR_AE	BIT(1) /* Application Tag Error */
#define SLI4_FC_DI_ERROR_RE	BIT(2) /* Reference Tag Error */
#define SLI4_FC_DI_ERROR_TDPV	BIT(3) /* Total Data Placed Valid */
#define SLI4_FC_DI_ERROR_UDB	BIT(4) /* Uninitialized DIF Block */
#define SLI4_FC_DI_ERROR_EDIR   BIT(5) /* Error direction */

/**
 * @brief Local Reject Reason Codes.
 */
#define SLI4_FC_LOCAL_REJECT_MISSING_CONTINUE	0x01
#define SLI4_FC_LOCAL_REJECT_SEQUENCE_TIMEOUT	0x02
#define SLI4_FC_LOCAL_REJECT_INTERNAL_ERROR	0x03
#define SLI4_FC_LOCAL_REJECT_INVALID_RPI	0x04
#define SLI4_FC_LOCAL_REJECT_NO_XRI		0x05
#define SLI4_FC_LOCAL_REJECT_ILLEGAL_COMMAND	0x06
#define SLI4_FC_LOCAL_REJECT_XCHG_DROPPED	0x07
#define SLI4_FC_LOCAL_REJECT_ILLEGAL_FIELD	0x08
#define SLI4_FC_LOCAL_REJECT_NO_ABORT_MATCH	0x0c
#define SLI4_FC_LOCAL_REJECT_TX_DMA_FAILED	0x0d
#define SLI4_FC_LOCAL_REJECT_RX_DMA_FAILED	0x0e
#define SLI4_FC_LOCAL_REJECT_ILLEGAL_FRAME	0x0f
#define SLI4_FC_LOCAL_REJECT_NO_RESOURCES	0x11
#define SLI4_FC_LOCAL_REJECT_FCP_CONF_FAILURE	0x12
#define SLI4_FC_LOCAL_REJECT_ILLEGAL_LENGTH	0x13
#define SLI4_FC_LOCAL_REJECT_UNSUPPORTED_FEATURE 0x14
#define SLI4_FC_LOCAL_REJECT_ABORT_IN_PROGRESS	0x15
#define SLI4_FC_LOCAL_REJECT_ABORT_REQUESTED	0x16
#define SLI4_FC_LOCAL_REJECT_RCV_BUFFER_TIMEOUT	0x17
#define SLI4_FC_LOCAL_REJECT_LOOP_OPEN_FAILURE	0x18
#define SLI4_FC_LOCAL_REJECT_LINK_DOWN		0x1a
#define SLI4_FC_LOCAL_REJECT_CORRUPTED_DATA	0x1b
#define SLI4_FC_LOCAL_REJECT_CORRUPTED_RPI	0x1c
#define SLI4_FC_LOCAL_REJECT_OUTOFORDER_DATA	0x1d
#define SLI4_FC_LOCAL_REJECT_OUTOFORDER_ACK	0x1e
#define SLI4_FC_LOCAL_REJECT_DUP_FRAME		0x1f
#define SLI4_FC_LOCAL_REJECT_LINK_CONTROL_FRAME	0x20
#define SLI4_FC_LOCAL_REJECT_BAD_HOST_ADDRESS	0x21
#define SLI4_FC_LOCAL_REJECT_MISSING_HDR_BUFFER	0x23
#define SLI4_FC_LOCAL_REJECT_MSEQ_CHAIN_CORRUPTED 0x24
#define SLI4_FC_LOCAL_REJECT_ABORTMULT_REQUESTED 0x25
#define SLI4_FC_LOCAL_REJECT_BUFFER_SHORTAGE	0x28
#define SLI4_FC_LOCAL_REJECT_RCV_XRIBUF_WAITING	0x29
#define SLI4_FC_LOCAL_REJECT_INVALID_VPI	0x2e
#define SLI4_FC_LOCAL_REJECT_MISSING_XRIBUF	0x30
#define SLI4_FC_LOCAL_REJECT_INVALID_RELOFFSET	0x40
#define SLI4_FC_LOCAL_REJECT_MISSING_RELOFFSET	0x41
#define SLI4_FC_LOCAL_REJECT_INSUFF_BUFFERSPACE	0x42
#define SLI4_FC_LOCAL_REJECT_MISSING_SI		0x43
#define SLI4_FC_LOCAL_REJECT_MISSING_ES		0x44
#define SLI4_FC_LOCAL_REJECT_INCOMPLETE_XFER	0x45
#define SLI4_FC_LOCAL_REJECT_SLER_FAILURE	0x46
#define SLI4_FC_LOCAL_REJECT_SLER_CMD_RCV_FAILURE 0x47
#define SLI4_FC_LOCAL_REJECT_SLER_REC_RJT_ERR	0x48
#define SLI4_FC_LOCAL_REJECT_SLER_REC_SRR_RETRY_ERR 0x49
#define SLI4_FC_LOCAL_REJECT_SLER_SRR_RJT_ERR	0x4a
#define SLI4_FC_LOCAL_REJECT_SLER_RRQ_RJT_ERR	0x4c
#define SLI4_FC_LOCAL_REJECT_SLER_RRQ_RETRY_ERR	0x4d
#define SLI4_FC_LOCAL_REJECT_SLER_ABTS_ERR	0x4e

enum {
	SLI4_RACQE_RQ_EL_INDX = 0xfff,
	SLI4_RACQE_FCFI = 0x3f,
	SLI4_RACQE_HDPL = 0x3f,
	SLI4_RACQE_RQ_ID = 0xffc0,
};

struct sli4_fc_async_rcqe_s {
	u8		rsvd0;
	u8		status;
	__le16		rq_elmt_indx_word;
	__le32		rsvd4;
	__le16		fcfi_rq_id_word;
	__le16		data_placement_length;
	u8		sof_byte;
	u8		eof_byte;
	u8		code;
	u8		hdpl_byte;
};

struct sli4_fc_async_rcqe_v1_s {
	u8		rsvd0;
	u8		status;
	__le16		rq_elmt_indx_word;
	u8		fcfi_byte;
	u8		rsvd5;
	__le16		rsvd6;
	__le16		rq_id;
	__le16		data_placement_length;
	u8		sof_byte;
	u8		eof_byte;
	u8		code;
	u8		hdpl_byte;
};

#define SLI4_FC_ASYNC_RQ_SUCCESS		0x10
#define SLI4_FC_ASYNC_RQ_BUF_LEN_EXCEEDED	0x11
#define SLI4_FC_ASYNC_RQ_INSUFF_BUF_NEEDED	0x12
#define SLI4_FC_ASYNC_RQ_INSUFF_BUF_FRM_DISC	0x13
#define SLI4_FC_ASYNC_RQ_DMA_FAILURE		0x14
enum {
	SLI4_RCQE_RQ_EL_INDX = 0xfff,
};

struct sli4_fc_coalescing_rcqe_s {
	u8		rsvd0;
	u8		status;
	__le16		rq_elmt_indx_word;
	__le32		rsvd4;
	__le16		rq_id;
	__le16		sequence_reporting_placement_length;
	__le16		rsvd14;
	u8		code;
	u8		vld_byte;
};

#define SLI4_FC_COALESCE_RQ_SUCCESS		0x10
#define SLI4_FC_COALESCE_RQ_INSUFF_XRI_NEEDED	0x18
/*
 * @SLI4_OCQE_RQ_EL_INDX: bits 0 to 15 in word1
 * @SLI4_OCQE_FCFI: bits 0 to 6 in dw1
 * @SLI4_OCQE_OOX: bit 15 in dw1
 * @SLI4_OCQE_AGXR: bit 16 in dw1
 */
enum {
	SLI4_OCQE_RQ_EL_INDX = 0x7f,
	SLI4_OCQE_FCFI = 0x3f,
	SLI4_OCQE_OOX = (1 << 6),
	SLI4_OCQE_AGXR = (1 << 7),
	SLI4_OCQE_HDPL = 0x3f,
};

struct sli4_fc_optimized_write_cmd_cqe_s {
	u8		rsvd0;
	u8		status;
	__le16		w1;
	u8		flags0;
	u8		flags1;
	__le16		xri;
	__le16		rq_id;
	__le16		data_placement_length;
	__le16		rpi;
	u8		code;
	u8		hdpl_vld;
};

enum {
	SLI4_OCQE_XB = (1 << 4),
};

struct sli4_fc_optimized_write_data_cqe_s {
	u8		hw_status;
	u8		status;
	__le16		xri;
	__le32		total_data_placed;
	__le32		extended_status;
	__le16		rsvd12;
	u8		code;
	u8		flags;
};

struct sli4_fc_xri_aborted_cqe_s {
	u8		rsvd0;
	u8		status;
	__le16		rsvd2;
	__le32		extended_status;
	__le16		xri;
	__le16		remote_xid;
	__le16		rsvd12;
	u8		code;
	u8		flags;
};

/**
 * Code definitions applicable to all FC/FCoE CQE types.
 */
#define SLI4_CQE_CODE_OFFSET		14

#define SLI4_CQE_CODE_WORK_REQUEST_COMPLETION	0x01
#define SLI4_CQE_CODE_RELEASE_WQE		0x02
#define SLI4_CQE_CODE_RQ_ASYNC			0x04
#define SLI4_CQE_CODE_XRI_ABORTED		0x05
#define SLI4_CQE_CODE_RQ_COALESCING		0x06
#define SLI4_CQE_CODE_RQ_CONSUMPTION		0x07
#define SLI4_CQE_CODE_MEASUREMENT_REPORTING	0x08
#define SLI4_CQE_CODE_RQ_ASYNC_V1		0x09
#define SLI4_CQE_CODE_OPTIMIZED_WRITE_CMD	0x0B
#define SLI4_CQE_CODE_OPTIMIZED_WRITE_DATA	0x0C

extern int
sli_fc_process_link_state(struct sli4_s *sli4, void *acqe);
extern int
sli_fc_process_link_attention(struct sli4_s *sli4, void *acqe);
extern int
sli_fc_cqe_parse(struct sli4_s *sli4, struct sli4_queue_s *cq,
		 u8 *cqe, enum sli4_qentry_e *etype,
		 u16 *rid);
u32 sli_fc_response_length(struct sli4_s *sli4, u8 *cqe);
u32 sli_fc_io_length(struct sli4_s *sli4, u8 *cqe);
int sli_fc_els_did(struct sli4_s *sli4, u8 *cqe,
		   u32 *d_id);
u32 sli_fc_ext_status(struct sli4_s *sli4, u8 *cqe);
extern int
sli_fc_rqe_rqid_and_index(struct sli4_s *sli4, u8 *cqe,
			  u16 *rq_id, u32 *index);
extern int
sli_cmd_wq_create(struct sli4_s *sli4, void *buf, size_t size,
		  struct efc_dma_s *qmem, u16 cq_id,
		       u16 ulp);
extern int
sli_cmd_wq_create_v1(struct sli4_s *sli4, void *buf, size_t size,
		     struct efc_dma_s *qmem,
			  u16 cq_id, u16 ignored);
int sli_cmd_wq_destroy(struct sli4_s *sli4, void *buf,
		       size_t size, u16 wq_id);
int sli_cmd_post_sgl_pages(struct sli4_s *sli4, void *buf,
			   size_t size, u16 xri, u32 xri_count,
			   struct efc_dma_s *page0[],
			   struct efc_dma_s *page1[], struct efc_dma_s *dma);
extern int
sli_cmd_rq_create(struct sli4_s *sli4, void *buf, size_t size,
		  struct efc_dma_s *qmem,
		       u16 cq_id, u16 ulp, u16 buffer_size);
extern int
sli_cmd_rq_create_v1(struct sli4_s *sli4, void *buf, size_t size,
		     struct efc_dma_s *qmem, u16 cq_id, u16 ulp,
			  u16 buffer_size);
int sli_cmd_rq_destroy(struct sli4_s *sli4, void *buf,
		       size_t size, u16 rq_id);
extern int
sli_cmd_read_fcf_table(struct sli4_s *sli4, void *buf, size_t size,
		       struct efc_dma_s *dma, u16 index);
extern int
sli_cmd_post_hdr_templates(struct sli4_s *sli4, void *buf,
			   size_t size, struct efc_dma_s *dma,
				     u16 rpi,
				     struct efc_dma_s *payload_dma);
extern int
sli_cmd_rediscover_fcf(struct sli4_s *sli4, void *buf, size_t size,
		       u16 index);
extern int
sli_fc_rq_alloc(struct sli4_s *sli4, struct sli4_queue_s *q,
		u32 n_entries, u32 buffer_size,
		struct sli4_queue_s *cq, u16 ulp, bool is_hdr);
extern int
sli_fc_rq_set_alloc(struct sli4_s *sli4, u32 num_rq_pairs,
		    struct sli4_queue_s *qs[], u32 base_cq_id,
		    u32 n_entries, u32 header_buffer_size,
		    u32 payload_buffer_size,  u16 ulp);
u32 sli_fc_get_rpi_requirements(struct sli4_s *sli4,
				u32 n_rpi);
extern int
sli_abort_wqe(struct sli4_s *sli4, void *buf, size_t size,
	      enum sli4_abort_type_e type, bool send_abts,
	u32 ids, u32 mask, u16 tag, u16 cq_id);

extern int
sli_send_frame_wqe(struct sli4_s *sli4, void *buf, size_t size,
		   u8 sof, u8 eof, u32 *hdr,
			struct efc_dma_s *payload, u32 req_len,
			u8 timeout, u16 xri, u16 req_tag);

extern int
sli_xmit_els_rsp64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		       struct efc_dma_s *rsp, u32 rsp_len,
		u16 xri, u16 tag, u16 cq_id,
		u16 ox_id, u16 rnodeindicator,
		u16 sportindicator, bool hlm, bool rnodeattached,
		u32 rnode_fcid, u32 flags, u32 s_id);

extern int
sli_els_request64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		      struct efc_dma_s *sgl,
		u8 req_type, u32 req_len, u32 max_rsp_len,
		u8 timeout, u16 xri, u16 tag,
		u16 cq_id, u16 rnodeindicator,
		u16 sportindicator, bool hlm, bool rnodeattached,
		u32 rnode_fcid, u32 sport_fcid);

extern int
sli_fcp_icmnd64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		    struct efc_dma_s *sgl, u16 xri, u16 tag,
		u16 cq_id, u32 rpi, bool hlm,
		u32 rnode_fcid, u8 timeout);

extern int
sli_fcp_iread64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		    struct efc_dma_s *sgl, u32 first_data_sge,
		u32 xfer_len, u16 xri, u16 tag,
		u16 cq_id, u32 rpi, bool hlm, u32 rnode_fcid,
		u8 dif, u8 bs, u8 timeout);

extern int
sli_fcp_iwrite64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		     struct efc_dma_s *sgl,
		u32 first_data_sge, u32 xfer_len,
		u32 first_burst, u16 xri, u16 tag,
		u16 cq_id, u32 rpi,
		bool hlm, u32 rnode_fcid,
		u8 dif, u8 bs, u8 timeout);

extern int
sli_fcp_treceive64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		       struct efc_dma_s *sgl,
		u32 first_data_sge, u32 relative_off,
		u32 xfer_len, u16 xri, u16 tag,
		u16 cq_id, u16 xid, u32 rpi, bool hlm,
		u32 rnode_fcid, u32 flags, u8 dif,
		u8 bs, u8 csctl, u32 app_id);

extern int
sli_fcp_cont_treceive64_wqe(struct sli4_s *sli4, void *buf, size_t size,
			    struct efc_dma_s *sgl, u32 first_data_sge,
		u32 relative_off, u32 xfer_len,
		u16 xri, u16 sec_xri, u16 tag,
		u16 cq_id, u16 xid, u32 rpi,
		bool hlm, u32 rnode_fcid, u32 flags,
		u8 dif, u8 bs, u8 csctl,
		u32 app_id);

extern int
sli_fcp_trsp64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		   struct efc_dma_s *sgl,
		u32 rsp_len, u16 xri, u16 tag, u16 cq_id,
		u16 xid, u32 rpi, bool hlm, u32 rnode_fcid,
		u32 flags, u8 csctl, u8 port_owned,
		u32 app_id);

extern int
sli_fcp_tsend64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		    struct efc_dma_s *sgl,
		u32 first_data_sge, u32 relative_off,
		u32 xfer_len, u16 xri, u16 tag,
		u16 cq_id, u16 xid, u32 rpi,
		bool hlm, u32 rnode_fcid, u32 flags, u8 dif,
		u8 bs, u8 csctl, u32 app_id);

extern int
sli_gen_request64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		      struct efc_dma_s *sgl, u32 req_len,
		u32 max_rsp_len, u8 timeout, u16 xri,
		u16 tag, u16 cq_id, bool hlm, u32 rnode_fcid,
		u16 rnodeindicator, u8 r_ctl, u8 type,
		u8 df_ctl);

extern int
sli_xmit_bls_rsp64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		       struct sli_bls_payload_s *payload, u16 xri,
		u16 tag, u16 cq_id,
		bool rnodeattached, bool hlm, u16 rnodeindicator,
		u16 sportindicator, u32 rnode_fcid,
		u32 sport_fcid, u32 s_id);

extern int
sli_xmit_sequence64_wqe(struct sli4_s *sli4, void *buf, size_t size,
			struct efc_dma_s *payload, u32 payload_len,
		u8 timeout, u16 ox_id, u16 xri,
		u16 tag, bool hlm, u32 rnode_fcid,
		u16 rnodeindicator, u8 r_ctl,
		u8 type, u8 df_ctl);

extern int
sli_requeue_xri_wqe(struct sli4_s *sli4, void *buf, size_t size,
		    u16 xri, u16 tag, u16 cq_id);
extern void
sli4_cmd_lowlevel_set_watchdog(struct sli4_s *sli4, void *buf,
			       size_t size, u16 timeout);

/**
 * @ingroup sli_fc
 * @brief Retrieve the received header and payload length.
 *
 * @param sli4 SLI context.
 * @param cqe Pointer to the CQ entry.
 * @param len_hdr Pointer where the header length is written.
 * @param len_data Pointer where the payload length is written.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static inline int
sli_fc_rqe_length(struct sli4_s *sli4, void *cqe, u32 *len_hdr,
		  u32 *len_data)
{
	struct sli4_fc_async_rcqe_s	*rcqe = cqe;

	*len_hdr = *len_data = 0;

	if (rcqe->status == SLI4_FC_ASYNC_RQ_SUCCESS) {
		*len_hdr  = rcqe->hdpl_byte & SLI4_RACQE_HDPL;
		*len_data = le16_to_cpu(rcqe->data_placement_length);
		return 0;
	} else {
		return -1;
	}
}

/**
 * @ingroup sli_fc
 * @brief Retrieve the received FCFI.
 *
 * @param sli4 SLI context.
 * @param cqe Pointer to the CQ entry.
 *
 * @return Returns the FCFI in the CQE. or U8_MAX if invalid CQE code.
 */
static inline u8
sli_fc_rqe_fcfi(struct sli4_s *sli4, void *cqe)
{
	u8 code = ((u8 *)cqe)[SLI4_CQE_CODE_OFFSET];
	u8 fcfi = U8_MAX;

	switch (code) {
	case SLI4_CQE_CODE_RQ_ASYNC: {
		struct sli4_fc_async_rcqe_s *rcqe = cqe;

		fcfi = le16_to_cpu(rcqe->fcfi_rq_id_word) & SLI4_RACQE_FCFI;
		break;
	}
	case SLI4_CQE_CODE_RQ_ASYNC_V1: {
		struct sli4_fc_async_rcqe_v1_s *rcqev1 = cqe;

		fcfi = rcqev1->fcfi_byte & SLI4_RACQE_FCFI;
		break;
	}
	case SLI4_CQE_CODE_OPTIMIZED_WRITE_CMD: {
		struct sli4_fc_optimized_write_cmd_cqe_s *opt_wr = cqe;

		fcfi = opt_wr->flags0 & SLI4_OCQE_FCFI;
		break;
	}
	}

	return fcfi;
}

const char *sli_fc_get_status_string(u32 status);

#endif /* !_SLI4_H */
