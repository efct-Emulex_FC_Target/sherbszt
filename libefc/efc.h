/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/*
 * EFC linux driver common include file
 */

#if !defined(__EFC_H__)
#define __EFC_H__

#define MODPARAM_DEF_INITIATOR		0
#define MODPARAM_DEF_TARGET		1
#define MODPARAM_DEF_HOLDOFF_LINK_ONLINE 2

/***************************************************************************
 * OS specific includes
 */
#include <stdarg.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/interrupt.h>
#include <asm-generic/ioctl.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/dma-mapping.h>
#include <linux/bitmap.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <asm/byteorder.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/sched.h>
#include <asm/current.h>
#include <asm/cacheflush.h>
#include <linux/pagemap.h>
#include <linux/kthread.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/random.h>
#include <linux/sched.h>
#include <linux/jiffies.h>
#include <linux/ctype.h>
#include <linux/debugfs.h>
#include <linux/firmware.h>

#include "efc_fcp.h"
#include "efclib.h"

/* Linux driver specific definitions */

#define EFC_MIN_DMA_ALIGNMENT		16
/* maximum DMA allocation that is expected to reliably succeed  */
#define EFC_MAX_DMA_ALLOC		(64 * 1024)

#define EFC_MAX_LUN			256
#define EFC_NUM_UNSOLICITED_FRAMES	1024

#define EFC_MAX_NUMA_NODES		8

/* Per driver instance (efc_t) definitions */
#define EFC_MAX_DOMAINS		1
#define EFC_MAX_REMOTE_NODES		2048

/**
 * @brief Sparse vector structure.
 */
struct sparse_vector_s {
	void *os;
	u32 max_idx;		/**< maximum index value */
	void **array;			/**< pointer to 3D array */
};

/**
 * @brief Sparse Vector API
 *
 * This is a trimmed down sparse vector implementation tuned to the problem of
 * 24-bit FC_IDs. In this case, the 24-bit index value is broken down in three
 * 8-bit values. These values are used to index up to three 256 element arrays.
 * Arrays are allocated, only when needed. @n @n
 * The lookup can complete in constant time (3 indexed array references). @n @n
 * A typical use case would be that the fabric/directory FC_IDs would cause two
 * rows to be allocated, and the fabric assigned remote nodes would cause two
 * rows to be allocated, with the root row always allocated. This gives five
 * rows of 256 x sizeof(void*), resulting in 10k.
 */
/*!
 * @defgroup spv Sparse Vector
 */

#define SPV_ROWLEN	256
#define SPV_DIM		3

void efc_spv_del(struct sparse_vector_s *spv);
struct sparse_vector_s *efc_spv_new(void *os);
void efc_spv_set(struct sparse_vector_s *sv, u32 idx, void *value);
void *efc_spv_get(struct sparse_vector_s *sv, u32 idx);

#define efc_assert(cond, ...)	\
	do {			\
		if (!(cond)) {	\
			pr_err("%s(%d) assertion (%s) failed\n",\
				__FILE__, __LINE__, #cond);\
			dump_stack();\
		} \
	} while (0)

/**
 * @brief create a thread ID
 *
 * A thread ID is created using the interrupt state and/or
 * the cpu id or thread id
 *
 * @return returns PID
 */

static inline union efc_pid_u
efc_mkpid(void)
{
	union efc_pid_u pid;

	/* if in ISR or soft IRQ (includes tasklets), set PID to CPU num
	 * else, set PID to ID of "current" task
	 */
	if (in_interrupt()) {
		pid.s.irq = true;
		pid.s.pid = get_cpu();
		put_cpu();
	} else {
		pid.s.irq = false;
		pid.s.pid = current->pid;
	}

	return pid;
}

/**
 * @brief counting semaphore
 *
 * Declaration of the counting semaphore object
 *
 */
struct efc_sem_s {
	struct semaphore sem;		/**< OS counting semaphore structure */
	char name[32];
};

#define EFC_SEM_FOREVER		(-1)
#define EFC_SEM_TRY		(0)

/**
 * @brief Initialize a counting semaphore
 *
 * The semaphore is initiatlized to the value
 *
 * @param sem pointer to semaphore
 * @param val initial value
 * @param name label for the semaphore
 *
 * @return returns 0 for success, a negative error code value for failure.
 */

int efc_sem_init(struct efc_sem_s *sem,
		 int val, const char *name, ...);

/**
 * @brief execute a P (decrement) operation
 *
 * A P (decrement and block if negative) operation is performed on the
 * semaphore.
 *
 * If timeout_usec is zero, the semaphore attempts one time and returns 0 if
 * acquired.
 * If timeout_usec is greater than zero, then the call will block until the
 * semaphore is acquired, or a timeout occurred.  If timeout_usec is less than
 * zero, then the call will block until the semaphore is acquired.
 *
 * @param sem pointer to semaphore
 * @param timeout_usec timeout in microseconds
 *
 * @return returns 0 for success, negative value if the semaphore was not
 * acquired.
 */

static inline int
efc_sem_p(struct efc_sem_s *sem, int timeout_usec)
{
	int rc = 0;

	if (!sem) {
		pr_err("sem is null\n");
		return -1;
	}

	if (timeout_usec == 0)
		rc = down_trylock(&sem->sem);
	else if (timeout_usec > 0)
		rc = down_timeout(&sem->sem, usecs_to_jiffies(timeout_usec));
	else
		rc = down_interruptible(&sem->sem);

	if (rc)
		rc = -1;

	return rc;
}

/**
 * @brief perform a V (increment) operation on a counting semaphore
 *
 * The semaphore is incremented, unblocking one thread that is waiting on the
 * sempahore
 *
 * @param sem pointer to the semaphore
 *
 * @return none
 */

static inline void
efc_sem_v(struct efc_sem_s *sem)
{
	if (!sem) {
		pr_err("sem is null\n");
		return;
	}

	up(&sem->sem);
}

void efc_rlock_init(struct efc_lport *efc, struct efc_rlock_s *lock,
		    const char *name);
bool efc_rlock_try(struct efc_rlock_s *lock);
void efc_rlock_acquire(struct efc_rlock_s *lock);
void efc_rlock_release(struct efc_rlock_s *lock);

int efc_dma_copy_in(struct efc_dma_s *dma, void *buffer,
		    u32 buffer_length);

#include "efc_sm.h"

/*==========================================================================*/
struct efc_drv_s {
	bool attached;
};

static inline int
efc_item_on_list(struct list_head *list)
{
	return (list->next != NULL);
}

static inline int
efc_list_valid(struct list_head *list)
{
	return (list->next != NULL);
}

#define efc_is_fc_initiator_enabled()	(efc->enable_ini)
#define efc_is_fc_target_enabled()	(efc->enable_tgt)

static inline void
efc_device_lock_init(struct efc_lport *efc)
{
	efc_rlock_init(efc, &efc->lock, "efcdevicelock");
}

static inline bool
efc_device_lock_try(struct efc_lport *efc)
{
	return efc_rlock_try(&efc->lock);
}

static inline void
efc_device_lock(struct efc_lport *efc)
{
	efc_rlock_acquire(&efc->lock);
}

static inline void
efc_device_unlock(struct efc_lport *efc)
{
	efc_rlock_release(&efc->lock);
}

#define EFC_LOG_LIB		0x01 /* General logging, not categorized */
#define EFC_LOG_NODE		0x02 /* lport layer logging */
#define EFC_LOG_PORT		0x04 /* lport layer logging */
#define EFC_LOG_DOMAIN		0x08 /* lport layer logging */
#define EFC_LOG_ELS		0x10 /* lport layer logging */
#define EFC_LOG_DOMAIN_SM	0x20 /* lport layer logging */
#define EFC_LOG_SM		0x40 /* lport layer logging */

extern unsigned int efc_log_level;

#define EFC_CHECK_LOGGING(LEVEL, CMD)			\
	do {						\
		if (unlikely(efc_log_level & LEVEL))	\
			do {				\
				CMD;			\
			} while (0);			\
	} while (0)

#define domain_sm_trace(domain)						\
	EFC_CHECK_LOGGING(EFC_LOG_DOMAIN_SM,				\
		pr_info("[domain:%s] %-20s %-20s\n",			\
		domain->display_name, __func__, efc_sm_event_name(evt)))\

#define domain_trace(domain, fmt, ...) \
	EFC_CHECK_LOGGING(EFC_LOG_DOMAIN_SM,\
		pr_info("[%s]" fmt, domain->display_name, ##__VA_ARGS__))\

#define domain_printf(domain, fmt, ...) \
	EFC_CHECK_LOGGING(EFC_LOG_DOMAIN,\
		pr_info("[%s]" fmt, domain->display_name, ##__VA_ARGS__))\

#define node_sm_trace()\
	EFC_CHECK_LOGGING(EFC_LOG_DOMAIN_SM,\
		pr_info("[%s] %-20s\n",\
			node->display_name, efc_sm_event_name(evt)))\

#define sport_sm_trace(sport)\
	EFC_CHECK_LOGGING(EFC_LOG_DOMAIN_SM,\
		pr_info("[%s] %-20s\n",\
			sport->display_name, efc_sm_event_name(evt)))\

#define efc_log_crit(efc, fmt, args...) \
	EFC_CHECK_LOGGING(EFC_LOG_LIB,\
		dev_crit(&((efc)->pcidev)->dev, fmt, ##args))\

#define efc_log_err(efc, fmt, args...) \
	EFC_CHECK_LOGGING(EFC_LOG_LIB,\
		dev_err(&((efc)->pcidev)->dev, fmt, ##args))\

#define efc_log_warn(efc, fmt, args...) \
	EFC_CHECK_LOGGING(EFC_LOG_LIB,\
		dev_warn(&((efc)->pcidev)->dev, fmt, ##args))\

#define efc_log_info(efc, fmt, args...) \
	EFC_CHECK_LOGGING(EFC_LOG_LIB,\
		dev_info(&((efc)->pcidev)->dev, fmt, ##args))\

#define efc_log_test(efc, fmt, args...) \
	EFC_CHECK_LOGGING(EFC_LOG_LIB,\
		dev_dbg(&((efc)->pcidev)->dev, fmt, ##args))\

#define efc_log_debug(efc, fmt, args...) \
	EFC_CHECK_LOGGING(EFC_LOG_LIB,\
		dev_dbg(&((efc)->pcidev)->dev, fmt, ##args))\

enum efc_hw_rtn_e {
	EFC_HW_RTN_SUCCESS = 0,
	EFC_HW_RTN_SUCCESS_SYNC = 1,
	EFC_HW_RTN_ERROR = -1,
	EFC_HW_RTN_NO_RESOURCES = -2,
	EFC_HW_RTN_NO_MEMORY = -3,
	EFC_HW_RTN_IO_NOT_ACTIVE = -4,
	EFC_HW_RTN_IO_ABORT_IN_PROGRESS = -5,
	EFC_HW_RTN_IO_PORT_OWNED_ALREADY_ABORTED = -6,
	EFC_HW_RTN_INVALID_ARG = -7,
};

#define EFC_HW_RTN_IS_ERROR(e) ((e) < 0)

enum efc_scsi_del_initiator_reason_e {
	EFC_SCSI_INITIATOR_DELETED,
	EFC_SCSI_INITIATOR_MISSING,
};

enum efc_scsi_del_target_reason_e {
	EFC_SCSI_TARGET_DELETED,
	EFC_SCSI_TARGET_MISSING,
};

#define EFC_SCSI_CALL_COMPLETE	0 /* All work is done */
#define EFC_SCSI_CALL_ASYNC	1 /* Work will be completed asynchronously */

#include "efc_domain.h"
#include "efc_sport.h"
#include "efc_node.h"
//#include "efc_fc_config.h"
/***************************************************************************
 * Timeouts
 */
#ifndef EFC_FC_ELS_SEND_DEFAULT_TIMEOUT
#define EFC_FC_ELS_SEND_DEFAULT_TIMEOUT		0
#endif

#ifndef EFC_FC_ELS_DEFAULT_RETRIES
#define EFC_FC_ELS_DEFAULT_RETRIES		3
#endif

#ifndef EFC_FC_FLOGI_TIMEOUT_SEC
#define EFC_FC_FLOGI_TIMEOUT_SEC		5 /* shorter than default */
#endif

#ifndef EFC_FC_DOMAIN_SHUTDOWN_TIMEOUT_USEC
#define EFC_FC_DOMAIN_SHUTDOWN_TIMEOUT_USEC	30000000 /* 30 seconds */
#endif

#endif /* __EFC_H__ */
