/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/**
 * EFC FC SLI port (SPORT) exported declarations
 *
 */

#if !defined(__EFC_SPORT_H__)
#define __EFC_SPORT_H__

extern struct efc_sli_port_s *
efc_sport_alloc(struct efc_domain_s *domain, uint64_t wwpn, uint64_t wwnn,
		u32 fc_id, bool enable_ini, bool enable_tgt);
extern void
efc_sport_free(struct efc_sli_port_s *sport);
extern void
efc_sport_force_free(struct efc_sli_port_s *sport);

static inline void
efc_sport_lock_init(struct efc_sli_port_s *sport)
{
}

static inline void
efc_sport_lock_free(struct efc_sli_port_s *sport)
{
}

static inline bool
efc_sport_lock_try(struct efc_sli_port_s *sport)
{
	/* Use the device wide lock */
	return efc_device_lock_try(sport->efc);
}

static inline void
efc_sport_lock(struct efc_sli_port_s *sport)
{
	/* Use the device wide lock */
	efc_device_lock(sport->efc);
}

static inline void
efc_sport_unlock(struct efc_sli_port_s *sport)
{
	/* Use the device wide lock */
	efc_device_unlock(sport->efc);
}

extern struct efc_sli_port_s *
efc_sport_find_wwn(struct efc_domain_s *domain, uint64_t wwnn, uint64_t wwpn);
extern int
efc_sport_attach(struct efc_sli_port_s *sport, u32 fc_id);

extern void *
__efc_sport_allocated(struct efc_sm_ctx_s *ctx,
		      enum efc_sm_event_e evt, void *arg);
extern void *
__efc_sport_wait_shutdown(struct efc_sm_ctx_s *ctx,
			  enum efc_sm_event_e evt, void *arg);
extern void *
__efc_sport_wait_port_free(struct efc_sm_ctx_s *ctx,
			   enum efc_sm_event_e evt, void *arg);
extern void *
__efc_sport_vport_init(struct efc_sm_ctx_s *ctx,
		       enum efc_sm_event_e evt, void *arg);
extern void *
__efc_sport_vport_wait_alloc(struct efc_sm_ctx_s *ctx,
			     enum efc_sm_event_e evt, void *arg);
extern void *
__efc_sport_vport_allocated(struct efc_sm_ctx_s *ctx,
			    enum efc_sm_event_e evt, void *arg);
extern void *
__efc_sport_attached(struct efc_sm_ctx_s *ctx,
		     enum efc_sm_event_e evt, void *arg);

extern int
efc_vport_start(struct efc_domain_s *domain);

extern int
efc_sparm_cmp(u8 *sp1, u8 *sp2);

#endif /* __EFC_SPORT_H__ */
