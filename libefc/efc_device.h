/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/*
 * Node state machine functions for remote device node sm
 */

#if !defined(__EFCT_DEVICE_H__)
#define __EFCT_DEVICE_H__
extern void
efc_node_init_device(struct efc_node_s *node, bool send_plogi);
extern void
efc_process_prli_payload(struct efc_node_s *node,
			 struct fc_prli_payload_s *prli);
extern void
efc_d_send_prli_rsp(struct efc_node_s *node, uint16_t ox_id);
extern void
efc_send_ls_acc_after_attach(struct efc_node_s *node,
			     struct fc_frame_header *hdr,
			     enum efc_node_send_ls_acc_e ls);
extern void *
__efc_d_wait_loop(struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_plogi_acc_cmpl(struct efc_sm_ctx_s *ctx,
			    enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_init(struct efc_sm_ctx_s *ctx, enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_plogi_rsp(struct efc_sm_ctx_s *ctx,
		       enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_plogi_rsp_recvd_prli(struct efc_sm_ctx_s *ctx,
				  enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_domain_attach(struct efc_sm_ctx_s *ctx,
			   enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_topology_notify(struct efc_sm_ctx_s *ctx,
			     enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_node_attach(struct efc_sm_ctx_s *ctx,
			 enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_attach_evt_shutdown(struct efc_sm_ctx_s *ctx,
				 enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_initiate_shutdown(struct efc_sm_ctx_s *ctx,
			  enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_port_logged_in(struct efc_sm_ctx_s *ctx,
		       enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_logo_acc_cmpl(struct efc_sm_ctx_s *ctx,
			   enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_device_ready(struct efc_sm_ctx_s *ctx,
		     enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_device_gone(struct efc_sm_ctx_s *ctx,
		    enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_adisc_rsp(struct efc_sm_ctx_s *ctx,
		       enum efc_sm_event_e evt, void *arg);
extern void *
__efc_d_wait_logo_rsp(struct efc_sm_ctx_s *ctx,
		      enum efc_sm_event_e evt, void *arg);

#endif /* __EFCT_DEVICE_H__ */
