/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/*
 * Generic state machine framework.
 */
#include "efc.h"
#include "efc_sm.h"

const char *efc_sm_id[] = {
	"common",
	"domain",
	"login"
};

/**
 * @brief Post an event to a context.
 *
 * @param ctx State machine context
 * @param evt Event to post
 * @param data Event-specific data (if any)
 *
 * @return 0 if successfully posted event; -1 if state machine
 *         is disabled
 */
int
efc_sm_post_event(struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *data)
{
	if (ctx->current_state) {
		ctx->current_state(ctx, evt, data);
		return 0;
	} else {
		return -1;
	}
}

/**
 * @brief Transition to a new state.
 */
void
efc_sm_transition(struct efc_sm_ctx_s *ctx,
		  void *(*state)(struct efc_sm_ctx_s *,
				 enum efc_sm_event_e, void *), void *data)

{
	if (ctx->current_state == state) {
		efc_sm_post_event(ctx, EFC_EVT_REENTER, data);
	} else {
		efc_sm_post_event(ctx, EFC_EVT_EXIT, data);
		ctx->current_state = state;
		efc_sm_post_event(ctx, EFC_EVT_ENTER, data);
	}
}

/**
 * @brief Disable further state machine processing.
 */
void
efc_sm_disable(struct efc_sm_ctx_s *ctx)
{
	ctx->current_state = NULL;
}

const char *efc_sm_event_name(enum efc_sm_event_e evt)
{
	switch (evt) {
	case EFC_EVT_ENTER:
		return "EFC_EVT_ENTER";
	case EFC_EVT_REENTER:
		return "EFC_EVT_REENTER";
	case EFC_EVT_EXIT:
		return "EFC_EVT_EXIT";
	case EFC_EVT_SHUTDOWN:
		return "EFC_EVT_SHUTDOWN";
	case EFC_EVT_RESPONSE:
		return "EFC_EVT_RESPONSE";
	case EFC_EVT_RESUME:
		return "EFC_EVT_RESUME";
	case EFC_EVT_TIMER_EXPIRED:
		return "EFC_EVT_TIMER_EXPIRED";
	case EFC_EVT_ERROR:
		return "EFC_EVT_ERROR";
	case EFC_EVT_SRRS_ELS_REQ_OK:
		return "EFC_EVT_SRRS_ELS_REQ_OK";
	case EFC_EVT_SRRS_ELS_CMPL_OK:
		return "EFC_EVT_SRRS_ELS_CMPL_OK";
	case EFC_EVT_SRRS_ELS_REQ_FAIL:
		return "EFC_EVT_SRRS_ELS_REQ_FAIL";
	case EFC_EVT_SRRS_ELS_CMPL_FAIL:
		return "EFC_EVT_SRRS_ELS_CMPL_FAIL";
	case EFC_EVT_SRRS_ELS_REQ_RJT:
		return "EFC_EVT_SRRS_ELS_REQ_RJT";
	case EFC_EVT_NODE_ATTACH_OK:
		return "EFC_EVT_NODE_ATTACH_OK";
	case EFC_EVT_NODE_ATTACH_FAIL:
		return "EFC_EVT_NODE_ATTACH_FAIL";
	case EFC_EVT_NODE_FREE_OK:
		return "EFC_EVT_NODE_FREE_OK";
	case EFC_EVT_ELS_REQ_TIMEOUT:
		return "EFC_EVT_ELS_REQ_TIMEOUT";
	case EFC_EVT_ELS_REQ_ABORTED:
		return "EFC_EVT_ELS_REQ_ABORTED";
	case EFC_EVT_ABORT_ELS:
		return "EFC_EVT_ABORT_ELS";
	case EFC_EVT_ELS_ABORT_CMPL:
		return "EFC_EVT_ELS_ABORT_CMPL";

	case EFC_EVT_DOMAIN_FOUND:
		return "EFC_EVT_DOMAIN_FOUND";
	case EFC_EVT_DOMAIN_ALLOC_OK:
		return "EFC_EVT_DOMAIN_ALLOC_OK";
	case EFC_EVT_DOMAIN_ALLOC_FAIL:
		return "EFC_EVT_DOMAIN_ALLOC_FAIL";
	case EFC_EVT_DOMAIN_REQ_ATTACH:
		return "EFC_EVT_DOMAIN_REQ_ATTACH";
	case EFC_EVT_DOMAIN_ATTACH_OK:
		return "EFC_EVT_DOMAIN_ATTACH_OK";
	case EFC_EVT_DOMAIN_ATTACH_FAIL:
		return "EFC_EVT_DOMAIN_ATTACH_FAIL";
	case EFC_EVT_DOMAIN_LOST:
		return "EFC_EVT_DOMAIN_LOST";
	case EFC_EVT_DOMAIN_FREE_OK:
		return "EFC_EVT_DOMAIN_FREE_OK";
	case EFC_EVT_DOMAIN_FREE_FAIL:
		return "EFC_EVT_DOMAIN_FREE_FAIL";
	case EFC_EVT_HW_DOMAIN_REQ_ATTACH:
		return "EFC_EVT_HW_DOMAIN_REQ_ATTACH";
	case EFC_EVT_HW_DOMAIN_REQ_FREE:
		return "EFC_EVT_HW_DOMAIN_REQ_FREE";
	case EFC_EVT_ALL_CHILD_NODES_FREE:
		return "EFC_EVT_ALL_CHILD_NODES_FREE";

	case EFC_EVT_SPORT_ALLOC_OK:
		return "EFC_EVT_SPORT_ALLOC_OK";
	case EFC_EVT_SPORT_ALLOC_FAIL:
		return "EFC_EVT_SPORT_ALLOC_FAIL";
	case EFC_EVT_SPORT_ATTACH_OK:
		return "EFC_EVT_SPORT_ATTACH_OK";
	case EFC_EVT_SPORT_ATTACH_FAIL:
		return "EFC_EVT_SPORT_ATTACH_FAIL";
	case EFC_EVT_SPORT_FREE_OK:
		return "EFC_EVT_SPORT_FREE_OK";
	case EFC_EVT_SPORT_FREE_FAIL:
		return "EFC_EVT_SPORT_FREE_FAIL";
	case EFC_EVT_SPORT_TOPOLOGY_NOTIFY:
		return "EFC_EVT_SPORT_TOPOLOGY_NOTIFY";
	case EFC_EVT_HW_PORT_ALLOC_OK:
		return "EFC_EVT_HW_PORT_ALLOC_OK";
	case EFC_EVT_HW_PORT_ALLOC_FAIL:
		return "EFC_EVT_HW_PORT_ALLOC_FAIL";
	case EFC_EVT_HW_PORT_ATTACH_OK:
		return "EFC_EVT_HW_PORT_ATTACH_OK";
	case EFC_EVT_HW_PORT_REQ_ATTACH:
		return "EFC_EVT_HW_PORT_REQ_ATTACH";
	case EFC_EVT_HW_PORT_REQ_FREE:
		return "EFC_EVT_HW_PORT_REQ_FREE";
	case EFC_EVT_HW_PORT_FREE_OK:
		return "EFC_EVT_HW_PORT_FREE_OK";

	case EFC_EVT_NODE_FREE_FAIL:
		return "EFC_EVT_NODE_FREE_FAIL";

	case EFC_EVT_ABTS_RCVD:
		return "EFC_EVT_ABTS_RCVD";

	case EFC_EVT_NODE_MISSING:
		return "EFC_EVT_NODE_MISSING";
	case EFC_EVT_NODE_REFOUND:
		return "EFC_EVT_NODE_REFOUND";
	case EFC_EVT_SHUTDOWN_IMPLICIT_LOGO:
		return "EFC_EVT_SHUTDOWN_IMPLICIT_LOGO";
	case EFC_EVT_SHUTDOWN_EXPLICIT_LOGO:
		return "EFC_EVT_SHUTDOWN_EXPLICIT_LOGO";

	case EFC_EVT_ELS_FRAME:
		return "EFC_EVT_ELS_FRAME";
	case EFC_EVT_PLOGI_RCVD:
		return "EFC_EVT_PLOGI_RCVD";
	case EFC_EVT_FLOGI_RCVD:
		return "EFC_EVT_FLOGI_RCVD";
	case EFC_EVT_LOGO_RCVD:
		return "EFC_EVT_LOGO_RCVD";
	case EFC_EVT_PRLI_RCVD:
		return "EFC_EVT_PRLI_RCVD";
	case EFC_EVT_PRLO_RCVD:
		return "EFC_EVT_PRLO_RCVD";
	case EFC_EVT_PDISC_RCVD:
		return "EFC_EVT_PDISC_RCVD";
	case EFC_EVT_FDISC_RCVD:
		return "EFC_EVT_FDISC_RCVD";
	case EFC_EVT_ADISC_RCVD:
		return "EFC_EVT_ADISC_RCVD";
	case EFC_EVT_RSCN_RCVD:
		return "EFC_EVT_RSCN_RCVD";
	case EFC_EVT_SCR_RCVD:
		return "EFC_EVT_SCR_RCVD";
	case EFC_EVT_ELS_RCVD:
		return "EFC_EVT_ELS_RCVD";
	case EFC_EVT_LAST:
		return "EFC_EVT_LAST";
	case EFC_EVT_FCP_CMD_RCVD:
		return "EFC_EVT_FCP_CMD_RCVD";

	case EFC_EVT_RFT_ID_RCVD:
		return "EFC_EVT_RFT_ID_RCVD";
	case EFC_EVT_RFF_ID_RCVD:
		return "EFC_EVT_RFF_ID_RCVD";
	case EFC_EVT_GNN_ID_RCVD:
		return "EFC_EVT_GNN_ID_RCVD";
	case EFC_EVT_GPN_ID_RCVD:
		return "EFC_EVT_GPN_ID_RCVD";
	case EFC_EVT_GFPN_ID_RCVD:
		return "EFC_EVT_GFPN_ID_RCVD";
	case EFC_EVT_GFF_ID_RCVD:
		return "EFC_EVT_GFF_ID_RCVD";
	case EFC_EVT_GID_FT_RCVD:
		return "EFC_EVT_GID_FT_RCVD";
	case EFC_EVT_GID_PT_RCVD:
		return "EFC_EVT_GID_PT_RCVD";
	case EFC_EVT_RPN_ID_RCVD:
		return "EFC_EVT_RPN_ID_RCVD";
	case EFC_EVT_RNN_ID_RCVD:
		return "EFC_EVT_RNN_ID_RCVD";
	case EFC_EVT_RCS_ID_RCVD:
		return "EFC_EVT_RCS_ID_RCVD";
	case EFC_EVT_RSNN_NN_RCVD:
		return "EFC_EVT_RSNN_NN_RCVD";
	case EFC_EVT_RSPN_ID_RCVD:
		return "EFC_EVT_RSPN_ID_RCVD";
	case EFC_EVT_RHBA_RCVD:
		return "EFC_EVT_RHBA_RCVD";
	case EFC_EVT_RPA_RCVD:
		return "EFC_EVT_RPA_RCVD";

	case EFC_EVT_GIDPT_DELAY_EXPIRED:
		return "EFC_EVT_GIDPT_DELAY_EXPIRED";

	case EFC_EVT_ABORT_IO:
		return "EFC_EVT_ABORT_IO";
	case EFC_EVT_ABORT_IO_NO_RESP:
		return "EFC_EVT_ABORT_IO_NO_RESP";
	case EFC_EVT_IO_CMPL:
		return "EFC_EVT_IO_CMPL";
	case EFC_EVT_IO_CMPL_ERRORS:
		return "EFC_EVT_IO_CMPL_ERRORS";
	case EFC_EVT_RESP_CMPL:
		return "EFC_EVT_RESP_CMPL";
	case EFC_EVT_ABORT_CMPL:
		return "EFC_EVT_ABORT_CMPL";
	case EFC_EVT_NODE_ACTIVE_IO_LIST_EMPTY:
		return "EFC_EVT_NODE_ACTIVE_IO_LIST_EMPTY";
	case EFC_EVT_NODE_DEL_INI_COMPLETE:
		return "EFC_EVT_NODE_DEL_INI_COMPLETE";
	case EFC_EVT_NODE_DEL_TGT_COMPLETE:
		return "EFC_EVT_NODE_DEL_TGT_COMPLETE";
	case EFC_EVT_IO_ABORTED_BY_TMF:
		return "EFC_EVT_IO_ABORTED_BY_TMF";
	case EFC_EVT_IO_ABORT_IGNORED:
		return "EFC_EVT_IO_ABORT_IGNORED";
	case EFC_EVT_IO_FIRST_BURST:
		return "EFC_EVT_IO_FIRST_BURST";
	case EFC_EVT_IO_FIRST_BURST_ERR:
		return "EFC_EVT_IO_FIRST_BURST_ERR";
	case EFC_EVT_IO_FIRST_BURST_ABORTED:
		return "EFC_EVT_IO_FIRST_BURST_ABORTED";

	default:
		break;
	}
	return "unknown";
}
