/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/**
 * Generic state machine framework declarations.
 */

#ifndef _EFC_SM_H
#define _EFC_SM_H

/**
 * State Machine (SM) IDs.
 */
enum {
	EFC_SM_COMMON = 0,
	EFC_SM_DOMAIN,
	EFC_SM_PORT,
	EFC_SM_LOGIN,
	EFC_SM_LAST
};

#define EFC_SM_EVENT_SHIFT		24
#define EFC_SM_EVENT_START(id)		((id) << EFC_SM_EVENT_SHIFT)

/* String format of the above enums. */
extern const char *efc_sm_id[];

struct efc_sm_ctx_s;

/*
 * State Machine events.
 */
enum efc_sm_event_e {
	/* Common Events */
	EFC_EVT_ENTER = EFC_SM_EVENT_START(EFC_SM_COMMON),
	EFC_EVT_REENTER,
	EFC_EVT_EXIT,
	EFC_EVT_SHUTDOWN,
	EFC_EVT_ALL_CHILD_NODES_FREE,
	EFC_EVT_RESUME,
	EFC_EVT_TIMER_EXPIRED,

	/* Domain Events */
	EFC_EVT_RESPONSE = EFC_SM_EVENT_START(EFC_SM_DOMAIN),
	EFC_EVT_ERROR,

	EFC_EVT_DOMAIN_FOUND,
	EFC_EVT_DOMAIN_ALLOC_OK,
	EFC_EVT_DOMAIN_ALLOC_FAIL,
	EFC_EVT_DOMAIN_REQ_ATTACH,
	EFC_EVT_DOMAIN_ATTACH_OK,
	EFC_EVT_DOMAIN_ATTACH_FAIL,
	EFC_EVT_DOMAIN_LOST,
	EFC_EVT_DOMAIN_FREE_OK,
	EFC_EVT_DOMAIN_FREE_FAIL,
	EFC_EVT_HW_DOMAIN_REQ_ATTACH,
	EFC_EVT_HW_DOMAIN_REQ_FREE,

	/* Sport Events */
	EFC_EVT_SPORT_ALLOC_OK = EFC_SM_EVENT_START(EFC_SM_PORT),
	EFC_EVT_SPORT_ALLOC_FAIL,
	EFC_EVT_SPORT_ATTACH_OK,
	EFC_EVT_SPORT_ATTACH_FAIL,
	EFC_EVT_SPORT_FREE_OK,
	EFC_EVT_SPORT_FREE_FAIL,
	EFC_EVT_SPORT_TOPOLOGY_NOTIFY,
	EFC_EVT_HW_PORT_ALLOC_OK,
	EFC_EVT_HW_PORT_ALLOC_FAIL,
	EFC_EVT_HW_PORT_ATTACH_OK,
	EFC_EVT_HW_PORT_REQ_ATTACH,
	EFC_EVT_HW_PORT_REQ_FREE,
	EFC_EVT_HW_PORT_FREE_OK,

	/* Login Events */
	EFC_EVT_SRRS_ELS_REQ_OK = EFC_SM_EVENT_START(EFC_SM_LOGIN),
	EFC_EVT_SRRS_ELS_CMPL_OK,
	EFC_EVT_SRRS_ELS_REQ_FAIL,
	EFC_EVT_SRRS_ELS_CMPL_FAIL,
	EFC_EVT_SRRS_ELS_REQ_RJT,
	EFC_EVT_NODE_ATTACH_OK,
	EFC_EVT_NODE_ATTACH_FAIL,
	EFC_EVT_NODE_FREE_OK,
	EFC_EVT_NODE_FREE_FAIL,
	EFC_EVT_ELS_FRAME,
	EFC_EVT_ELS_REQ_TIMEOUT,
	EFC_EVT_ELS_REQ_ABORTED,
	/* request an ELS IO be aborted */
	EFC_EVT_ABORT_ELS,
	/* ELS abort process complete */
	EFC_EVT_ELS_ABORT_CMPL,

	EFC_EVT_ABTS_RCVD,

	/* node is not in the GID_PT payload */
	EFC_EVT_NODE_MISSING,
	/* node is allocated and in the GID_PT payload */
	EFC_EVT_NODE_REFOUND,
	/* node shutting down due to PLOGI recvd (implicit logo) */
	EFC_EVT_SHUTDOWN_IMPLICIT_LOGO,
	/* node shutting down due to LOGO recvd/sent (explicit logo) */
	EFC_EVT_SHUTDOWN_EXPLICIT_LOGO,

	EFC_EVT_PLOGI_RCVD,
	EFC_EVT_FLOGI_RCVD,
	EFC_EVT_LOGO_RCVD,
	EFC_EVT_PRLI_RCVD,
	EFC_EVT_PRLO_RCVD,
	EFC_EVT_PDISC_RCVD,
	EFC_EVT_FDISC_RCVD,
	EFC_EVT_ADISC_RCVD,
	EFC_EVT_RSCN_RCVD,
	EFC_EVT_SCR_RCVD,
	EFC_EVT_ELS_RCVD,

	EFC_EVT_FCP_CMD_RCVD,

	/* Used by fabric emulation */
	EFC_EVT_RFT_ID_RCVD,
	EFC_EVT_RFF_ID_RCVD,
	EFC_EVT_GNN_ID_RCVD,
	EFC_EVT_GPN_ID_RCVD,
	EFC_EVT_GFPN_ID_RCVD,
	EFC_EVT_GFF_ID_RCVD,
	EFC_EVT_GID_FT_RCVD,
	EFC_EVT_GID_PT_RCVD,
	EFC_EVT_RPN_ID_RCVD,
	EFC_EVT_RNN_ID_RCVD,
	EFC_EVT_RCS_ID_RCVD,
	EFC_EVT_RSNN_NN_RCVD,
	EFC_EVT_RSPN_ID_RCVD,
	EFC_EVT_RHBA_RCVD,
	EFC_EVT_RPA_RCVD,

	EFC_EVT_GIDPT_DELAY_EXPIRED,

	/* SCSI Target Server events */
	EFC_EVT_ABORT_IO,
	EFC_EVT_ABORT_IO_NO_RESP,
	EFC_EVT_IO_CMPL,
	EFC_EVT_IO_CMPL_ERRORS,
	EFC_EVT_RESP_CMPL,
	EFC_EVT_ABORT_CMPL,
	EFC_EVT_NODE_ACTIVE_IO_LIST_EMPTY,
	EFC_EVT_NODE_DEL_INI_COMPLETE,
	EFC_EVT_NODE_DEL_TGT_COMPLETE,
	EFC_EVT_IO_ABORTED_BY_TMF,
	EFC_EVT_IO_ABORT_IGNORED,
	EFC_EVT_IO_FIRST_BURST,
	EFC_EVT_IO_FIRST_BURST_ERR,
	EFC_EVT_IO_FIRST_BURST_ABORTED,

	/* Must be last */
	EFC_EVT_LAST
};

int
efc_sm_post_event(struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *data);
void
efc_sm_transition(struct efc_sm_ctx_s *ctx,
		  void *(*state)(struct efc_sm_ctx_s *ctx,
				 enum efc_sm_event_e evt, void *arg),
		  void *data);
void efc_sm_disable(struct efc_sm_ctx_s *ctx);
const char *efc_sm_event_name(enum efc_sm_event_e evt);

#endif /* ! _EFC_SM_H */
