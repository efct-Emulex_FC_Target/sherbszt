/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/*
 * Define Fibre Channel types and structures.
 */

#ifndef _EFCT_FCP_H
#define _EFCT_FCP_H
#include "scsi/fc/fc_els.h"
#include "scsi/fc/fc_fs.h"
#include "scsi/fc/fc_ns.h"
#include "scsi/fc/fc_gs.h"

/** is well known domain controller */
#define FC_ADDR_IS_DOMAIN_CTRL(x)	(((x) & 0xffff00) == 0xfffc00)
/** get domain controller number */
#define FC_ADDR_GET_DOMAIN_CTRL(x)	((x) & 0x0000ff)

#define FC_GS_SUBTYPE_NAME_SERVER		0x02

/**
 * Generic Services FC Type Bit mask macros:
 */
#define FC_GS_TYPE_WORD(type)	((type) >> 5)
#define FC_GS_TYPE_BIT(type)	((type) & 0x1f)

/**
 * Generic Services Name Server Request Command codes:
 */
#define FC_GS_NAMESERVER_GFPN_ID	0x011c
#define FC_GS_NAMESERVER_GFF_ID		0x011f
#define FC_GS_NAMESERVER_RHBA		0x0200
#define FC_GS_NAMESERVER_RPA		0x0211
#define FC_GS_NAMESERVER_RCS_ID		0x0214

#define FC_GS_REVISION		0x03

#define FC_GS_IO_PARAMS		{ .fc_ct.r_ctl = 0x02, \
				.fc_ct.type = FC_TYPE_CT, \
				.fc_ct.df_ctl = 0x00 }

struct fc_vft_header_s {
	u32	rsvd : 1,
			vf_id : 12,
			priority : 3,
			e : 1,
			: 1,
			type : 4,
			ver : 2,
			r_ctl : 8;
	u32	rsvd2 : 24,
			hopct : 8;
};

#ifdef __LITTLE_ENDIAN
static inline u32 fc_be24toh(u32 x) { return (be32_to_cpu(x) >> 8); }
#else
static inline u32 fc_be24toh(u32 x) { }
#endif
static inline u32 fc_htobe24(u32 x) { return fc_be24toh(x); }

#define sli4_sid_from_fc_hdr(fc_hdr)  \
	((fc_hdr)->fh_s_id[0] << 16 | \
	(fc_hdr)->fh_s_id[1] <<  8 | \
	(fc_hdr)->fh_s_id[2])

#define sli4_did_from_fc_hdr(fc_hdr)  \
	((fc_hdr)->fh_d_id[0] << 16 | \
	(fc_hdr)->fh_d_id[1] <<  8 | \
	(fc_hdr)->fh_d_id[2])

#define sli4_f_ctl_from_fc_hdr(fc_hdr)  \
	((fc_hdr)->fh_f_ctl[0] << 16 | \
	(fc_hdr)->fh_f_ctl[1] <<  8 | \
	(fc_hdr)->fh_f_ctl[2])

static inline void sli4_f_ctl_to_fc_hdr(struct fc_frame_header *fc_hdr,
					u32 f_ctl)
{
	fc_hdr->fh_f_ctl[0] = (f_ctl >> 16) & 0xff;
	fc_hdr->fh_f_ctl[1] = (f_ctl >> 8) & 0xff;
	fc_hdr->fh_f_ctl[2] = f_ctl & 0xff;
}

#define FC_SOFI3	0x2e
#define FC_SOFn3	0x36
#define FC_EOFN		0x41
#define FC_EOFT		0x42

/**
 * @brief FC header in little-endian order
 */
struct fc_header_le_s {
#ifdef __LITTLE_ENDIAN
	u32	d_id : 24,
			info : 4,
			r_ctl : 4;
	u32	s_id : 24,
			cs_ctl : 8;
	u32	f_ctl : 24,
			type : 8;
	u32	seq_cnt : 16,
			df_ctl : 8,
			seq_id : 8;
	u32	rx_id : 16,
			ox_id : 16;
	u32	parameter;
#else
#error big endian version not defined
#endif
};

/**
 * @brief FC VM header in big-endian order
 */
struct fc_vm_header_s {
	u32	dst_vmid;
	__be32		src_vmid;
	u32	rsvd0;
	u32	rsvd1;
};

#define FC_DFCTL_DEVICE_HDR_16_MASK	0x1
#define FC_DFCTL_NETWORK_HDR_MASK	0x20
#define FC_DFCTL_ESP_HDR_MASK		0x40
#define FC_DFCTL_NETWORK_HDR_SIZE	16
#define FC_DFCTL_ESP_HDR_SIZE		0 /*FIXME*/

#define FC_RCTL_FC4_DATA	0
#define FC_RCTL_ELS		2
#define FC_RCTL_BLS		8

#define FC_FCTL_EXCHANGE_RESPONDER	0x800000
#define FC_FCTL_SEQUENCE_CONTEXT	0x400000
#define FC_FCTL_FIRST_SEQUENCE		0x200000
#define FC_FCTL_LAST_SEQUENCE		0x100000
#define FC_FCTL_END_SEQUENCE		0x080000
#define FC_FCTL_END_CONNECTION		0x040000
#define FC_FCTL_PRIORITY_ENABLE		0x020000
#define FC_FCTL_FILL_DATA_BYTES_MASK	0x000003

/**
 * Common BLS definitions:
 */
#define FC_INFO_NOP			0x0
#define FC_INFO_ABTS			0x1
#define FC_INFO_RMC			0x2
/* reserved				0x3 */
#define FC_INFO_BA_ACC			0x4
#define FC_INFO_BA_RJT			0x5
#define FC_INFO_PRMT			0x6

/* (FC-LS) LS_RJT Reason Codes */
#define FC_REASON_INVALID_COMMAND_CODE		0x01
#define FC_REASON_LOGICAL_ERROR			0x03
#define FC_REASON_LOGICAL_BUSY			0x05
#define FC_REASON_PROTOCOL_ERROR		0x07
#define FC_REASON_UNABLE_TO_PERFORM		0x09
#define FC_REASON_COMMAND_NOT_SUPPORTED		0x0b
#define FC_REASON_COMMAND_IN_PROGRESS		0x0e
#define FC_REASON_VENDOR_SPECIFIC		0xff

/* (FC-LS) LS_RJT Reason Codes Explanations */
#define FC_EXPL_NO_ADDITIONAL			0x00
#define FC_EXPL_SPARAM_OPTIONS			0x01
#define FC_EXPL_SPARAM_INITIATOR		0x03
#define FC_EXPL_SPARAM_RECPIENT			0x05
#define FC_EXPL_SPARM_DATA_SIZE			0x07
#define FC_EXPL_SPARM_CONCURRENT		0x09
#define FC_EXPL_SPARM_CREDIT			0x0b
#define FC_EXPL_INV_PORT_NAME			0x0d
#define FC_EXPL_INV_NODE_NAME			0x0e
#define FC_EXPL_INV_COMMON_SPARAMS		0x0f
#define FC_EXPL_INV_ASSOC_HEADER		0x11
#define FC_EXPL_ASSOC_HDR_REQUIRED		0x13
#define FC_EXPL_INV_ORIGINATOR_S_ID		0x15
#define FC_EXPL_INV_X_ID_COMBINATION		0x17
#define FC_EXPL_COMMAND_IN_PROGRESS		0x19
#define FC_EXPL_NPORT_LOGIN_REQUIRED		0x1e
#define FC_EXPL_N_PORT_ID			0x1f
#define FC_EXPL_INSUFFICIENT_RESOURCES		0x29
#define FC_EXPL_UNABLE_TO_SUPPLY_DATA		0x2a
#define FC_EXPL_REQUEST_NOT_SUPPORTED		0x2c
#define FC_EXPL_INV_PAYLOAD_LEN			0x1d
#define FC_EXPL_INV_PORT_NODE_NAME		0x44
#define FC_EXPL_LOGIN_EXT_NOT_SUPPORTED		0x46
#define FC_EXPL_AUTH_REQUIRED			0x48
#define FC_EXPL_SCAN_VALUE_NOT_ALLOWED		0x50
#define FC_EXPL_SCAN_VALUE_NOT_SUPPORTED	0x51
#define FC_EXPL_NO_RESOURCES_ASSIGNED		0x52
#define FC_EXPL_MAC_ADDR_MODE_NOT_SUPPORTED	0x60
#define FC_EXPL_MAC_ADDR_INCORRECTLY_FORMED	0x61
#define FC_EXPL_VN2VN_PORT_NOT_IN_NEIGHBOR_SET	0x62

/* invalid OX_ID - RX_ID combination */
#define FC_EXPL_INV_X_ID			0x03
#define FC_EXPL_SEQUENCE_ABORTED		0x05

struct fc_ba_acc_payload_s {
#define FC_SEQ_ID_VALID			0x80
#define FC_SEQ_ID_INVALID		0x00
	u32	seq_id_validity : 8,
			seq_id : 8,
			: 16;
	__be16		ox_id;
	__be16		rx_id;
	u32	low_seq_cnt : 16,
			high_seq_cnt : 16;
};

struct fc_ba_rjt_payload_s {
	u32	vendor_unique : 8,
			reason_explanation : 8,
			reason_code : 8,
			rsvd0 : 8;
};

struct fc_els_gen_s {
	u32	command_code : 8,
			resv1 : 24;
};

struct fc_plogi_payload_s {
	u32	command_code : 8,
			resv1 : 24;
	__be32		common_service_parameters[4];
	__be32		port_name_hi;
	__be32		port_name_lo;
	__be32		node_name_hi;
	__be32		node_name_lo;
	u32	class1_service_parameters[4];
	u32	class2_service_parameters[4];
	u32	class3_service_parameters[4];
	u32	class4_service_parameters[4];
	u32	vendor_version_level[4];
};

struct fc_logo_payload_s {
	u32	command_code: 8,
			resv1:24;
	u32	rsvd0 : 8,
			port_id : 24;
	__be32		port_name_hi;
	__be32		port_name_lo;
};

struct fc_acc_payload_s {
	u32	command_code: 8,
			resv1:24;
};

struct fc_ls_rjt_payload_s {
	u32	command_code:8,
			resv1:24;
	u32	resv2:8,
			reason_code:8,
			reason_code_exp:8,
			vendor_unique:8;
};

struct fc_prli_payload_s {
	u16	command_code:8,
			page_length:8;
	__be16		payload_length;
	u16	type:8,
			type_ext:8;
	__be16		flags;
	u32	originator_pa;
	u32	responder_pa;
	u16	rsvd0;
	__be16		service_params;
};

struct fc_prlo_payload_s {
	u32	command_code:8,
			page_length:8,
			payload_length:16;
	u32	type:8,
			type_ext:8,
			rsvd3 : 16;
	u32	rsvd0 : 32;
	u32	rsvd1 : 32;
	u32	rsvd2 : 32;
};

struct fc_prlo_acc_payload_s {
	u32	command_code:8,
			page_length:8,
			payload_length:16;
	u32	type:8,
			type_ext:8,
			rsvd3 : 4,
			response_code:4,
			rsvd4 : 8;
	u32	rsvd0: 32;
	u32	rsvd1: 32;
	u32	rsvd2: 32;
};

struct fc_adisc_payload_s {
	u32	command_code:8,
			payload_length:24;
	u32	rsvd0 : 8,
			hard_address:24;
	__be32		port_name_hi;
	__be32		port_name_lo;
	__be32		node_name_hi;
	__be32		node_name_lo;
	u32	rsvd1 : 8,
			port_id:24;
};

/* PRLI flags */
#define FC_PRLI_ORIGINATOR_PA_VALID	0x8000
#define FC_PRLI_RESPONDER_PA_VALID	0x4000
#define FC_PRLI_ESTABLISH_IMAGE_PAIR	0x2000
#define FC_PRLI_SERVICE_PARAM_INVALID	0x0800
#define FC_PRLI_REQUEST_EXECUTED	0x0100

/* PRLI Service Parameters */
#define FC_PRLI_TASK_RETRY_ID_REQ	0x0200
#define FC_PRLI_RETRY			0x0100
#define FC_PRLI_CONFIRMED_COMPLETION	0x0080
#define FC_PRLI_DATA_OVERLAY		0x0040
#define FC_PRLI_INITIATOR_FUNCTION      0x0020
#define FC_PRLI_TARGET_FUNCTION         0x0010
#define FC_PRLI_READ_XRDY_DISABLED	0x0002
#define FC_PRLI_WRITE_XRDY_DISABLED	0x0001

/* PRLO Logout flags */
#define FC_PRLO_REQUEST_EXECUTED	0x0001

struct fc_scr_payload_s {
	u32	command_code : 8,
			rsvd1 : 24;
	u32	rsvd0 : 24,
			function : 8;
};

#define FC_SCR_REG_FABRIC		1
#define FC_SCR_REG_NPORT		2
#define FC_SCR_REG_FULL			3

struct fc_rscn_affected_port_id_page_s {
	u32 rsvd0 : 2,
		rscn_event_qualifier : 4,
		address_format : 2,
		port_id : 24;
};

struct fc_rscn_payload_s {
	u32	command_code:8,
			page_length:8,
			payload_length:16;
	struct fc_rscn_affected_port_id_page_s port_list[1];
};

struct fcct_iu_header_s {
#ifdef __LITTLE_ENDIAN
	u32	revision:8,
			in_id:24;
	u32	gs_type:8,
			gs_subtype:8,
			options:8,
			resv1:8;
	__be16		cmd_rsp_code;
	__be16		max_residual_size;
	u32	fragment_id:8,
			reason_code:8,
			reason_code_explanation:8,
			vendor_specific:8;
#else
#error big endian version not defined
#endif
};

#define FCCT_REJECT_INVALID_COMMAND_CODE	1
#define FCCT_REJECT_INVALID_VERSION_LEVEL	2
#define FCCT_LOGICAL_ERROR			3
#define FCCT_INVALID_CT_IU_SIZE			4
#define FCCT_LOGICAL_BUSY			5
#define FCCT_PROTOCOL_ERROR			7
#define FCCT_UNABLE_TO_PERFORM			9
#define FCCT_COMMAND_NOT_SUPPORTED		0x0b
#define FCCT_FABRIC_PORT_NAME_NOT_REGISTERED	0x0c
#define FCCT_SERVER_NOT_AVAILABLE		0x0d
#define FCCT_SESSION_COULD_NOT_BE_ESTABLISHED	0x0e
#define FCCT_VENDOR_SPECIFIC_ERROR		0xff

#define FCCT_NO_ADDITIONAL_EXPLANATION		0
#define FCCT_AUTHORIZATION_EXCEPTION		0xf0
#define FCCT_AUTHENTICATION_EXCEPTION		0xf1
#define FCCT_DATA_BASE_FULL			0xf2
#define FCCT_DATA_BASE_EMPTY			0xf3
#define FCCT_PROCESSING_REQUEST			0xf4
#define FCCT_UNABLE_TO_VERIFY_CONNECTION	0xf5
#define FCCT_DEVICES_NOT_IN_COMMON_ZONE		0xf6

struct fcgs_rft_id_s {
	struct fcct_iu_header_s  hdr;
#ifdef __LITTLE_ENDIAN
	u32	port_id;
	u32	fc4_types;
#else
#error big endian version not defined
#endif
};

struct fcgs_rff_id_s {
	struct fcct_iu_header_s  hdr;
#ifdef __LITTLE_ENDIAN
	u32	port_id;
	u32	rsvd0 : 16,
			fc4_features : 8,
			type_code : 8;
#else
#error big endian version not defined
#endif
};

struct fcgs_rpn_id_s {
	struct fcct_iu_header_s  hdr;
#ifdef __LITTLE_ENDIAN
	u32	port_id;
	u64	port_name;
#else
#error big endian version not defined
#endif
};

struct fcgs_rnn_id_s {
	struct fcct_iu_header_s  hdr;
#ifdef __LITTLE_ENDIAN
	u32	port_id;
	u64	node_name;
/*	u32	node_name[2]; */
#else
#error big endian version not defined
#endif
};

#define FCCT_CLASS_OF_SERVICE_F	0x1
#define FCCT_CLASS_OF_SERVICE_2	0x4
#define FCCT_CLASS_OF_SERVICE_3	0x8

struct fcgs_rcs_id_s {
	struct fcct_iu_header_s  hdr;
#ifdef __LITTLE_ENDIAN
	u32	port_id;
	u32	class_of_srvc;
#else
#error big endian version not defined
#endif
};

struct fcgs_rsnn_nn_s {
	struct fcct_iu_header_s  hdr;
#ifdef __LITTLE_ENDIAN
	u64	node_name;
	u8		name_len;
	char		sym_node_name[1];
	u8		rsvd26[2];
#else
#error big endian version not defined
#endif
};

#define FCCT_HDR_CMDRSP_ACCEPT	0x8002
#define FCCT_HDR_CMDRSP_REJECT	0x8001

static inline void fcct_build_req_header(struct fcct_iu_header_s  *hdr,
					 u16 cmd, u16 max_size)
{
	/* use old rev (1) to accommodate older switches */
	hdr->revision = 1;
	hdr->in_id = 0;
	hdr->gs_type = FC_FST_DIR;
	hdr->gs_subtype = FC_GS_SUBTYPE_NAME_SERVER;
	hdr->options = 0;
	hdr->resv1 = 0;
	hdr->cmd_rsp_code = cpu_to_be16(cmd);
	/* words */
	hdr->max_residual_size = cpu_to_be16(max_size / (sizeof(u32)));
	hdr->fragment_id = 0;
	hdr->reason_code = 0;
	hdr->reason_code_explanation = 0;
	hdr->vendor_specific = 0;
}

struct fcct_rftid_req_s {
	struct fcct_iu_header_s	hdr;
	__be32			port_id;
	u32		fc4_types[8];
};

#define FC4_FEATURE_TARGET	BIT(0)
#define FC4_FEATURE_INITIATOR	BIT(1)

struct fcct_rffid_req_s {
	struct fcct_iu_header_s	hdr;
	__be32			port_id;
	u32		rsvd0 : 16,
				fc4_feature_bits:8,
				type:8;
};

struct fcct_gnnid_req_s {
	struct fcct_iu_header_s	hdr;
	u32		rsvd0:8,
				port_id:24;
};

struct fcct_gpnid_req_s {
	struct fcct_iu_header_s	hdr;
	u32		rsvd0:8,
				port_id:24;
};

struct fcct_gffid_req_s {
	struct fcct_iu_header_s	hdr;
	u32		rsvd0:8,
				port_id:24;
};

struct fcct_gidpt_req_s {
	struct fcct_iu_header_s	hdr;
	u32		rsvd0:8,
				domain_id_scope:8,
				area_id_scope:8,
				type:8;
};

struct fcct_gnnid_acc_s {
	struct fcct_iu_header_s	hdr;
	u64		node_name;
};

struct fcct_gpnid_acc_s {
	struct fcct_iu_header_s	hdr;
	u64		port_name;
};

struct fcct_gffid_acc_s {
	struct fcct_iu_header_s	hdr;
	u8			fc4_feature_bits;
};

struct fcct_gidpt_acc_s {
	struct fcct_iu_header_s	hdr;
	struct {
		u32	ctl : 8,
				port_id : 24;
	} port_list[1];
};

struct fcct_gidft_acc_s {
	struct fcct_iu_header_s	hdr;
	struct {
		u32	ctl:8,
				port_id:24;
	} port_list[1];
};

#define FCCT_GID_PT_LAST_ID	0x80
#define FCCT_GIDPT_ID_MASK	0x00ffffff

struct fcp_cmnd_iu_s {
	u8		fcp_lun[8];
	u8		command_reference_number;
	u8		task_attribute:3,
			command_priority:4,
			: 1;
	u8		task_management_flags;
	u8		wrdata : 1,
			rddata : 1,
			additional_fcp_cdb_length:6;
	u8		fcp_cdb[16];
	/* < May contain up to 16 bytes of CDB, followed by fcp_dl */
	u8		fcp_cdb_and_dl[20];
};

#define FCP_LUN_ADDRESS_METHOD_SHIFT	6
#define FCP_LUN_ADDRESS_METHOD_MASK	0xc0
#define FCP_LUN_ADDR_METHOD_PERIPHERAL	0x0
#define FCP_LUN_ADDR_METHOD_FLAT	0x1
#define FCP_LUN_ADDR_METHOD_LOGICAL	0x2
#define FCP_LUN_ADDR_METHOD_EXTENDED	0x3

#define FCP_LUN_ADDR_SIMPLE_MAX		0xff
#define FCP_LUN_ADDR_FLAT_MAX		0x3fff

#define FCP_TASK_ATTR_SIMPLE		0x0
#define FCP_TASK_ATTR_HEAD_OF_QUEUE	0x1
#define FCP_TASK_ATTR_ORDERED		0x2
#define FCP_TASK_ATTR_ACA		0x4
#define FCP_TASK_ATTR_UNTAGGED          0x5

#define FCP_QUERY_TASK_SET		BIT(0)
#define FCP_ABORT_TASK_SET		BIT(1)
#define FCP_CLEAR_TASK_SET		BIT(2)
#define FCP_QUERY_ASYNCHRONOUS_EVENT	BIT(3)
#define FCP_LOGICAL_UNIT_RESET		BIT(4)
#define FCP_TARGET_RESET		BIT(5)
#define FCP_CLEAR_ACA			BIT(6)

/* SPC-4 says that the maximum length of sense data is 252 bytes */
#define FCP_MAX_SENSE_LEN		252
#define FCP_MAX_RSP_LEN			  8
/*
 * FCP_RSP buffer will either have sense or response data, but not both
 * so pick the larger.
 */
#define FCP_MAX_RSP_INFO_LEN		FCP_MAX_SENSE_LEN

struct fcp_rsp_iu_s {
	u8		rsvd[8];
	u8		status_qualifier[2];
	u8		flags;
	u8		scsi_status;
	u8		fcp_resid[4];
	u8		fcp_sns_len[4];
	u8		fcp_rsp_len[4];
	u8		data[FCP_MAX_RSP_INFO_LEN];
};

/** Flag field defines: */
#define FCP_RSP_LEN_VALID		BIT(0)
#define FCP_SNS_LEN_VALID		BIT(1)
#define FCP_RESID_OVER			BIT(2)
#define FCP_RESID_UNDER			BIT(3)
#define FCP_CONF_REQ			BIT(4)
#define FCP_BIDI_READ_RESID_OVER	BIT(5)
#define FCP_BIDI_READ_RESID_UNDER	BIT(6)
#define FCP_BIDI_RSP			BIT(7)

/** Status values: */
#define FCP_TMF_COMPLETE		0x00
#define FCP_DATA_LENGTH_MISMATCH	0x01
#define FCP_INVALID_FIELD		0x02
#define FCP_DATA_RO_MISMATCH		0x03
#define FCP_TMF_REJECTED		0x04
#define FCP_TMF_FAILED			0x05
#define FCP_TMF_SUCCEEDED		0x08
#define FCP_TMF_INCORRECT_LUN		0x09

/** FCP-4 Table 28, TMF response information: */
struct fcp_rsp_info_s {
	u8 addl_rsp_info[3];
	u8 rsp_code;
	u32 rsvd0 : 32;
};

struct fcp_xfer_rdy_iu_s {
	u8		fcp_data_ro[4];
	u8		fcp_burst_len[4];
	u8		rsvd[4];
};

#define MAX_ACC_REJECT_PAYLOAD (sizeof(struct fc_ls_rjt_payload_s) >	\
				sizeof(struct fc_acc_payload_s) ?	\
				sizeof(struct fc_ls_rjt_payload_s) :	\
				sizeof(struct fc_acc_payload_s))	\

#endif /* !_EFCT_FCP_H */
