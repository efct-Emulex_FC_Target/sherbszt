/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/*
 * Declarations for the interface exported by efc_fabric
 */

#if !defined(__EFCT_FABRIC_H__)
#define __EFCT_FABRIC_H__
#include "scsi/fc/fc_els.h"
#include "scsi/fc/fc_fs.h"
#include "scsi/fc/fc_ns.h"

void *
__efc_fabric_init(struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *arg);
void *
__efc_fabric_flogi_wait_rsp(struct efc_sm_ctx_s *ctx,
			    enum efc_sm_event_e evt, void *arg);
void *
__efc_fabric_domain_attach_wait(struct efc_sm_ctx_s *ctx,
				enum efc_sm_event_e evt, void *arg);
void *
__efc_fabric_wait_domain_attach(struct efc_sm_ctx_s *ctx,
				enum efc_sm_event_e evt, void *arg);

void *
__efc_vport_fabric_init(struct efc_sm_ctx_s *ctx,
			enum efc_sm_event_e evt, void *arg);
void *
__efc_fabric_fdisc_wait_rsp(struct efc_sm_ctx_s *ctx,
			    enum efc_sm_event_e evt, void *arg);
void *
__efc_fabric_wait_sport_attach(struct efc_sm_ctx_s *ctx,
			       enum efc_sm_event_e evt, void *arg);

void *
__efc_ns_init(struct efc_sm_ctx_s *ctx, enum efc_sm_event_e evt, void *arg);
void *
__efc_ns_plogi_wait_rsp(struct efc_sm_ctx_s *ctx,
			enum efc_sm_event_e evt, void *arg);
void *
__efc_ns_rftid_wait_rsp(struct efc_sm_ctx_s *ctx,
			enum efc_sm_event_e evt, void *arg);
void *
__efc_ns_rffid_wait_rsp(struct efc_sm_ctx_s *ctx,
			enum efc_sm_event_e evt, void *arg);
void *
__efc_ns_wait_node_attach(struct efc_sm_ctx_s *ctx,
			  enum efc_sm_event_e evt, void *arg);
void *
__efc_fabric_wait_attach_evt_shutdown(struct efc_sm_ctx_s *ctx,
				      enum efc_sm_event_e evt, void *arg);
void *
__efc_ns_logo_wait_rsp(struct efc_sm_ctx_s *ctx,
		       enum efc_sm_event_e, void *arg);
void *
__efc_ns_gidpt_wait_rsp(struct efc_sm_ctx_s *ctx,
			enum efc_sm_event_e evt, void *arg);
void *
__efc_ns_idle(struct efc_sm_ctx_s *ctx, enum efc_sm_event_e evt, void *arg);
void *
__efc_ns_gidpt_delay(struct efc_sm_ctx_s *ctx,
		     enum efc_sm_event_e evt, void *arg);
void *
__efc_fabctl_init(struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *arg);
void *
__efc_fabctl_wait_node_attach(struct efc_sm_ctx_s *ctx,
			      enum efc_sm_event_e evt, void *arg);
void *
__efc_fabctl_wait_scr_rsp(struct efc_sm_ctx_s *ctx,
			  enum efc_sm_event_e evt, void *arg);
void *
__efc_fabctl_ready(struct efc_sm_ctx_s *ctx,
		   enum efc_sm_event_e evt, void *arg);
void *
__efc_fabctl_wait_ls_acc_cmpl(struct efc_sm_ctx_s *ctx,
			      enum efc_sm_event_e evt, void *arg);
void *
__efc_fabric_idle(struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *arg);

void *
__efc_p2p_rnode_init(struct efc_sm_ctx_s *ctx,
		     enum efc_sm_event_e evt, void *arg);
void *
__efc_p2p_domain_attach_wait(struct efc_sm_ctx_s *ctx,
			     enum efc_sm_event_e evt, void *arg);
void *
__efc_p2p_wait_flogi_acc_cmpl(struct efc_sm_ctx_s *ctx,
			      enum efc_sm_event_e evt, void *arg);
void *
__efc_p2p_wait_plogi_rsp(struct efc_sm_ctx_s *ctx,
			 enum efc_sm_event_e evt, void *arg);
void *
__efc_p2p_wait_plogi_rsp_recvd_prli(struct efc_sm_ctx_s *ctx,
				    enum efc_sm_event_e evt, void *arg);
void *
__efc_p2p_wait_domain_attach(struct efc_sm_ctx_s *ctx,
			     enum efc_sm_event_e evt, void *arg);
void *
__efc_p2p_wait_node_attach(struct efc_sm_ctx_s *ctx,
			   enum efc_sm_event_e evt, void *arg);

int
efc_rnode_is_nport(struct fc_plogi_payload_s  *remote_sparms);
int
efc_rnode_is_npiv_capable(struct fc_plogi_payload_s *remote_sparms);
int
efc_p2p_setup(struct efc_sli_port_s *sport);
void
efc_fabric_set_topology(struct efc_node_s *node,
			enum efc_sport_topology_e topology);
void efc_fabric_notify_topology(struct efc_node_s *node);

#endif /* __EFCT_FABRIC_H__ */
